<?php 
#################### URL BASE ###############################
$arrServer = filter_input_array(INPUT_SERVER);
$currentPath = $arrServer['PHP_SELF'];
$pathInfo = pathinfo($currentPath);
$hostName = $arrServer['HTTP_HOST'];
$protocol = strtolower(substr($arrServer['SERVER_PROTOCOL'], 0, 5)) == 'https://' ? 'https://' : 'http://';
$arrDirname = explode('/', $pathInfo['dirname']);
$project = $arrDirname[1];

$base_url = $protocol . $hostName . '/' . $project . '/';
if(getenv('ENTORNO')!='LOCAL'){
    $base_url = $protocol . $hostName . '/siong/';
}

#################### FIN URL BASE #############################


################### CONEXION BASE DE DATOS LOCAL ###################
$dbServer = 'localhost';
$dbUser = 'root';
$dbPassword = '';
$dbSchema = 'c0siong_gestor';


try {
    $db = new PDO('mysql:host=' . $dbServer . ';dbname=' . $dbSchema.';charset=utf8', $dbUser, $dbPassword);
    $conn = mysqli_connect($dbServer, $dbUser, $dbPassword, $dbSchema) or die("Connection failed: " . mysqli_connect_error());
} catch (PDOException  $e) {
    echo "ERROR: " . $e->getMessage();
}
################### FIN CONEXION BASE DE DATOS ###################

 ?>
