<?php

//include connection file 
include_once(dirname(__FILE__) . '/../../session.php');

//$params = $columns = $totalRecords = $data = array();

$params = filter_input_array(INPUT_POST);
//var_dump($params);

//define index of column
$columns = array(
    0 => 'id_cuenta',
    1 => 'cedula',
    2 => 'nombre_completo',
    3 => 'nom_empresa',
    4 => 'email',
    5 => 'nom_tipo_profesional',
    6 => 'celular'
);

$sqlTot = $sqlRec = "";

// check search value exist
$where = "";

if (isset($params['search']['value']) && $params['search']['value'] != '') {
    $where .= "
            WHERE 
                (ct.id_cuenta LIKE '%" . $params['search']['value'] . "%' 
                OR ct.cedula LIKE '%" . $params['search']['value'] . "%'
                OR (CONVERT(CAST(CONVERT(CONCAT(UPPER(primer_nombre),' ',UPPER(segundo_nombre),' ',UPPER(primer_apellido),' ',UPPER(segundo_apellido)) USING latin1) AS BINARY) USING utf8)) LIKE '%" . $params['search']['value'] . "%'
                OR emp.nom_empresa LIKE '%" . $params['search']['value'] . "%' 
                OR ct.email LIKE '%" . $params['search']['value'] . "%' 
                OR tp.nom_tipo_profesional LIKE '%" . $params['search']['value'] . "%'
                OR ct.celular LIKE '%" . $params['search']['value']. "%')";
}


// getting total number records without any search
$sql = "SELECT 
            ct.id_cuenta,
            ct.cedula,
            (CONVERT(CAST(CONVERT(CONCAT(UPPER(primer_nombre),' ',UPPER(segundo_nombre),' ',UPPER(primer_apellido),' ',UPPER(segundo_apellido)) USING latin1) AS BINARY) USING utf8)) AS nombre_completo,
            emp.nom_empresa,
            ct.email,
            tp.nom_tipo_profesional,
            ct.celular
            FROM gddt_cuentas ct
            INNER JOIN gddt_empresas emp ON ct.id_empresa = emp.id_empresa
            INNER JOIN gddt_tipo_profesionales tp ON ct.id_tipo_profesional = tp.id_tipo_profesional";


$sqlTot .= $sql;
$sqlRec .= $sql;

//concatenate search sql if value exist
if (isset($where) && $where != '') {
    $sqlTot .= $where;
    $sqlRec .= $where;
}

$sqlRec .= " ORDER BY " . $columns[$params['order'][0]['column']] . "  " . $params['order'][0]['dir'] . "  LIMIT " . $params['start'] . " ," . $params['length'] . " ";

$queryTot = mysqli_query($conn, $sqlTot) or die("database error:" . mysqli_error($conn));

$totalRecords = mysqli_num_rows($queryTot);

$queryRecords = mysqli_query($conn, $sqlRec) or die("error to fetch employees data");

$data = array();
//iterate on results row and create new index array of data
while ($row = mysqli_fetch_row($queryRecords)) {
    $data[] = $row;
}

foreach ($data as $key => $val) {

    /*if (!in_array("82", $_SESSION["RESTRICCIONES"])) {*/
        $data[$key][7] = '
        &nbsp;
        <a href="#" style="color:#000;" onClick="fnVerUsuario(\'' . $val[0] . '\');">
        <span data-toggle="tooltip" title="Ver usuario" class="fa fa-eye"></span>
        </a>';    

        $data[$key][7] .= '
        &nbsp;
        <a href="#" style="color:#000;" data-toggle="modal" data-target="#modalModificarUsuario" onClick="fnModificarUsuario(\'' . $val[0] . '\');">
        <span data-toggle="tooltip" title="Editar usuario" class="fas fa-pencil-alt"></span>
        </a>
        ';
        $data[$key][7] .= '
        &nbsp;
        <a href="#" style="color:#000;" data-toggle="modal" data-target="#modalEliminarUsuario" onClick="fnEnviarParam(\'' . $val[0] . '\');">
        <span data-toggle="tooltip" title="Eliminar usuario" class="fa fa-times"></span>
        </a>';

}

$json_data = array(
    "draw" => intval($params['draw']),
    "recordsTotal" => intval($totalRecords),
    "recordsFiltered" => intval($totalRecords),
    "data" => $data   // total data array
);

echo json_encode($json_data);  // send data as json format