<?php include_once(dirname(__FILE__) . '/../../session.php');



$sqlUsuario = "SELECT * FROM gddt_cuentas WHERE id_cuenta = ". $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);

?>
<form name="editarUsuario" method="POST" action="procesarUsuario.php?tipo=update" enctype="multipart/form-data">
	<div class="row">
		<div class="form-group col-md-4">
			<label for="recipient-name" class="form-control-label">Empresa <i style="color: darkorange">*</i></label>
			<select class="form-control" name="empresa" >
				<option>Selecciona una opción</option>
				<?php
				$queryEmpresa = mysqli_query($conn, "SELECT id_empresa,nom_empresa FROM gddt_empresas ;");
				while ($fetchEmpresa = mysqli_fetch_row($queryEmpresa)) {
					if ($fetchEmpresa[0] == $fetchUsuario[1]) {
						echo "<option selected value=" . $fetchEmpresa[0] . ">" . $fetchEmpresa[1] . "</option>";
					} else {
						echo "<option value=" . $fetchEmpresa[0] . ">" . $fetchEmpresa[1] . "</option>";
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Primer nombre <i style="color: darkorange">*</i></label>
			<input type="text" class="form-control" name="primer_nombre" value="<?php echo $fetchUsuario[2]; ?>"required style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();" />
		</div>

		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Segundo nombre </label>
			<input type="text" class="form-control" name="segundo_nombre" value="<?php echo $fetchUsuario[3]; ?>" required style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();" />
		</div>

		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Primer apellido <i style="color: darkorange">*</i></label>
			<input type="text" class="form-control" name="primer_apellido" value="<?php echo $fetchUsuario[4];; ?>" required style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();" />
		</div>

		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Segundo Apellido </label>
			<input type="text" class="form-control" name="segundo_apellido" value="<?php echo $fetchUsuario[5]; ?>" required style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();" />
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Numero de documento <i style="color: darkorange">*</i></label>
			<input type="number" class="form-control" name="numero_documento" value="<?php echo $fetchUsuario[6]; ?>"required style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();" />
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Email <i style="color: darkorange">*</i></label>
			<input type="email" class="form-control" name="email" value="<?php echo $fetchUsuario[7]; ?>"required style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();" />
		</div>
		<div class="form-group col-md-4">
			<label for="recipient-name" class="form-control-label">Cargo <i style="color: darkorange">*</i></label>
			<select class="form-control" name="cargo" >
				<option>Selecciona una opción</option>
				<?php
				$queryCargo = mysqli_query($conn, "SELECT id_tipo_profesional, nom_tipo_profesional FROM gddt_tipo_profesionales ;");
				while ($fetchCargo = mysqli_fetch_row($queryCargo)) {
					if ($fetchCargo[0] == $fetchUsuario[8]) {
						echo "<option selected value=" . $fetchCargo[0] . ">" . $fetchCargo[1] . "</option>";
					} else {
						echo "<option value=" . $fetchCargo[0] . ">" . $fetchCargo[1] . "</option>";
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Celular</label>
			<input type="number" class="form-control" name="celular" value="<?php echo $fetchUsuario[9]; ?>"required style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();" />
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Numero de registro</label>
			<input type="text" class="form-control" name="numero_registro" value="<?php echo $fetchUsuario[10]; ?>"required style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();" />
		</div>
	</div>
	<!-- End Row -->



	<!-- Modal Footer -->
	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">
			Cancelar
		</button>
		<button type="submit" class="btn btn-primary" name="Actualizar">
			Actualizar
		</button>
	</div>
</div>    