<?php

//include connection file 
include_once(dirname(__FILE__) . '/../../session.php');

$sqlUsuario = "
SELECT 
ct.id_cuenta,
ct.cedula,
(CONVERT(CAST(CONVERT(CONCAT(UPPER(primer_nombre),' ',UPPER(segundo_nombre),' ',UPPER(primer_apellido),' ',UPPER(segundo_apellido)) USING latin1) AS BINARY) USING utf8)) AS nombre_completo,
emp.nom_empresa,
ct.email,
tp.nom_tipo_profesional,
ct.celular,
tp.img_profesional
FROM gddt_cuentas ct
INNER JOIN gddt_empresas emp ON ct.id_empresa = emp.id_empresa
INNER JOIN gddt_tipo_profesionales tp ON ct.id_tipo_profesional = tp.id_tipo_profesional
WHERE ct.id_cuenta = " . $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);

$imagenProfesional = $fetchUsuario[7];
?>

<div class="content">

    <div class="user-block col-sm-4 col-xs-6">
     <img style="width: 200px;height:200px" src="<?php echo $base_url; ?>images/<?php 
     if($imagenProfesional != ''){
        echo 'profesionales/'.$imagenProfesional;
    }else{
        echo("user-icon.png");
    } ?>"  class="img-circle" >
 </div>

 <div class="user-block col-sm-4 col-xs-6">
    <label>
        Nombre Completo
    </label>
    <p><?php echo $fetchUsuario[2]; ?></p>
</div>

<div class="user-block col-sm-4 col-xs-6">
    <label>
        Cedula
    </label>
    <p><?php echo strtoupper($fetchUsuario[1]); ?></p>
</div>

<div class="user-block col-sm-4 col-xs-6">
    <label>
        Empresa
    </label>
    <p><?php echo strtoupper($fetchUsuario[3]); ?></p>
</div>

<div class="user-block col-sm-4 col-xs-6">
    <label>
        Email
    </label>
    <p><?php echo $fetchUsuario[4]; ?></p>
</div>

<div class="user-block col-sm-4 col-xs-6">
    <label>
        Cargo
    </label>
    <p><?php echo strtoupper($fetchUsuario[5]); ?></p>
</div>



<div class="user-block">&nbsp;</div>

</div><!-- Fin del content -->