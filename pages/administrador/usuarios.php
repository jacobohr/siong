<!DOCTYPE html>
<html>
<?php require_once dirname(__FILE__) . '/../../session.php'; ?>
<?php require_once dirname(__FILE__) . '/../../head.php'; ?>
<body class="hold-transition skin-blue-light sidebar-mini">
  <div class="wrapper">

    <!-- Navbar -->
    <?php require_once dirname(__FILE__) . '/../../menu.php'; ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" style="background-color: white">
      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Usuarios</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="<?= $base_url ?>index.php">Home</a></li>
                <li class="breadcrumb-item active">Usuarios</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->
      <!-- Main content -->
      <section class="content">
       <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <div class="row">
                <h5 class="box-title" style="margin-left: 15px">Usuarios registrados</h5>
                <div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
                  <a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevoUsuario" >
                    <i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
                  </a>
                </div>
              </div>
            </div>
            <div class="box-body">
              <table id="listUsuarios" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
                <thead>
                  <tr>
                    <th>Id cuenta</th>
                    <th>Documento</th>
                    <th>Nombre Completo</th>
                    <th>Empresa</th>
                    <th>Email</th>
                    <th>Cargo</th>
                    <th>Celular</th>
                    <th>Acciones</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Modal Ver Datos de Usuario -->
  <div id="modalVerUsuario" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Ver usuario registrado</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div id="divVerUsuario" class="modal-body">
          <p>Modal body text goes here.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal Actualizar Datos de Usuario -->
  <div id="modalModificarUsuario" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header header-modal-sany">
          <h4 class="modal-title">MODIFICAR DATOS DEL USUARIO</h4>
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        </div>

        <div id="divModificarUsuario" class="modal-body">

        </div>

      </div>
    </div>
  </div>
  <!-- Modal Registrar Nuevo Usuario -->
  <div id="modalNuevoUsuario" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header header-modal-sany">
          <h4 class="modal-title " style="text-align: center;">REGISTRAR UN NUEVO USUARIO</h4>
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        </div>

        <div class="modal-body">
          <div class="content">
            <form name="registroUsuarios" id="registroUsuarios" method="POST" action="procesarUsuario.php?tipo=insert" enctype="multipart/form-data">
              <div class="row">
                <div class="form-group col-md-4">
                  <label for="recipient-name" class="form-control-label">Documento <i style="color: darkorange">*</i></label>
                  <input type="number" class="form-control" name="documento" required />
                </div>
                <div class="form-group col-md-4">
                  <label for="message-text" class="form-control-label">Primer nombre <i style="color: darkorange">*</i></label>
                  <input type="text" class="form-control" name="primer_nombre" required style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();" />
                </div>

                <div class="form-group col-md-4">
                  <label for="message-text" class="form-control-label">Segundo nombre </label>
                  <input type="text" class="form-control" name="segundo_nombre" required style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();" />
                </div>

                <div class="form-group col-md-4">
                  <label for="message-text" class="form-control-label">Primer apellido <i style="color: darkorange">*</i></label>
                  <input type="text" class="form-control" name="primer_apellido" required style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();" />
                </div>

                <div class="form-group col-md-4">
                  <label for="message-text" class="form-control-label">Segundo apellido </label>
                  <input type="text" class="form-control" name="segundo_apellido" required style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();" />
                </div>

                <div class="form-group col-md-4">
                  <label for="message-text" class="form-control-label">Email <i style="color: darkorange">*</i></label>
                  <input type="text" class="form-control" name="numero_documento" required style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();" />
                </div>

                <div class="form-group col-md-4">
                  <label for="message-text" class="form-control-label">Celular <i style="color: darkorange">*</i></label>
                  <input type="number" class="form-control" name="fecha_nacimiento" />
                </div>

                <div class="form-group col-md-4">
                  <label for="recipient-name" class="form-control-label">Empresa  <i style="color: darkorange">*</i></label>
                  <select class="form-control" name="empresa" required>
                    <option value="">Selecciona una opción</option>
                    <?php
                    $queryEmpresa = mysqli_query($conn, 
                      "SELECT id_empresa,nom_empresa FROM gddt_empresas;");
                    while ($fetchEmpresa = mysqli_fetch_row($queryEmpresa)) {
                      echo "<option value=" . $fetchEmpresa[0] . ">" . $fetchEmpresa[1] . "</option>";
                    }
                    ?>
                  </select>
                </div>

                <div class="form-group col-md-4">
                  <label for="recipient-name" class="form-control-label">Cargo  <i style="color: darkorange">*</i></label>
                  <select class="form-control" name="tipo_profesional" required>
                    <option value="">Selecciona una opción</option>
                    <?php
                    $queryTipoProfesional = mysqli_query($conn, "SELECT id_tipo_profesional, nom_tipo_profesional FROM gddt_tipo_profesionales;");
                    while ($fetchTipoProfesional = mysqli_fetch_row($queryTipoProfesional)) {
                      echo "<option value=" . $fetchTipoProfesional[0] . ">" . $fetchTipoProfesional[1] . "</option>";
                    }
                    ?>
                  </select>
                </div>

                <div class="form-group col-md-4 div-datos-usuario" style="display:none;">
                  <label for="message-text" class="form-control-label">Numero de registro <i style="color: darkorange">*</i></label>
                  <input type="text" class="form-control" name="numero_registro" required />
                </div>

                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    Cancelar
                  </button>
                  <button type="submit" class="btn btn-primary" name="Guardar">
                    Registrar
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>











    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
  </div>
  <!-- ./wrapper -->

  <?php require_once('../../footer.php') ?>
  <!-- Scripts -->
  <?php require('../../scripts.php') ?>

  <script type="text/javascript">
    $.fn.dataTable.ext.errMode = 'none';
    $('#listUsuarios').DataTable({
      language: {
        "decimal": "",
        "emptyTable": "No hay información",
        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
        "infoPostFix": "",
        "thousands": ",",
        "lengthMenu": " Mostrar  _MENU_  Entradas ",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "search": "Buscar:",
        "zeroRecords": "Sin resultados encontrados",
        "paginate": {
          "first": "Primero",
          "last": "Ultimo",
          "next": "Siguiente",
          "previous": "Anterior"
        }
      },
      "processing": true,
      "serverSide": true,
      "ajax": {
        url: "response.php",
        type: "post",
        error: function () {}
      }
    });

    function fnVerUsuario(idUsuario) {
      $.ajax({
        url: 'modalVer.php',
        type: 'POST',
        dataType: 'html',
        data: {idu: idUsuario},
        success: function (data) {
          $('#divVerUsuario').html(data);
          $('#modalVerUsuario').modal('show');
        }
      });
    }
    function fnModificarUsuario(idUsuario) {
      $.ajax({
        url: 'formModal.php',
        type: 'POST',
        dataType: 'html',
        data: {idu: idUsuario},
        success: function (data) {
          $('#divModificarUsuario').html(data);
        }
      });
    }
  </script>
</body>
</html>
