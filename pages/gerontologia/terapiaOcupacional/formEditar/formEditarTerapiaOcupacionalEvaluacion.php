<?php require "../../../../session.php";



$sqlUsuario = "SELECT  
*,
(CONVERT(CAST(CONVERT(cual_discapacidad USING latin1) AS BINARY) USING utf8))AS cual_discapacidad,
(CONVERT(CAST(CONVERT(comunicacion_otra_cual USING latin1) AS BINARY) USING utf8))AS comunicacion_otra_cual,
(CONVERT(CAST(CONVERT(movilidad USING latin1) AS BINARY) USING utf8))AS movilidad,
(CONVERT(CAST(CONVERT(historia_laboral USING latin1) AS BINARY) USING utf8))AS historia_laboral,
(CONVERT(CAST(CONVERT(actividad_ocupacional USING latin1) AS BINARY) USING utf8))AS actividad_ocupacional,
(CONVERT(CAST(CONVERT(observacion USING latin1) AS BINARY) USING utf8))AS observacion
FROM gddt_terapia_ocupacional 
WHERE id_terapia_ocupacional = ". $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);

// FORMACION ACADEMICA
$sqlUsuarioFormacionAcademica = "SELECT * FROM gddt_terapia_ocupacional_formacion WHERE id_terapia_ocupacional =  ". $_REQUEST["idu"];

$queryUsuarioFormacionAcademica = mysqli_query($conn, $sqlUsuarioFormacionAcademica);
$fetchUsuarioFormacionAcademica = mysqli_fetch_row($queryUsuarioFormacionAcademica);
?>
<form name="editarUsuario" method="POST" action="procesarPsicologiaAntceMental.php?tipo=update" enctype="multipart/form-data">
	<h4 class="text-center mb-4"> Evaluacion Ocupacional </h4>
	<div class="row">
		<div class="form-group col-md-4">
			<label for="recipient-name" class="form-control-label">Usuario que registra <i style="color: darkorange">*</i></label>
			<select class="form-control" name="usuario" >
				<option>Selecciona una opción</option>
				<?php
				$queryCuenta = mysqli_query($conn, "SELECT id_cuenta,
					UPPER((CONVERT(CAST(CONVERT(CONCAT(primer_nombre,' ',segundo_nombre,' ',primer_apellido,' ',segundo_apellido) USING latin1) AS BINARY) USING utf8))) AS nombreUsuario FROM gddt_cuentas ;");
				while ($fetchCuenta = mysqli_fetch_row($queryCuenta)) {
					if ($fetchCuenta[0] == $fetchUsuario[1]) {
						echo "<option selected value=" . $fetchCuenta[0] . ">" . $fetchCuenta[1] . "</option>";
					} else {
						echo "<option value=" . $fetchCuenta[0] . ">" . $fetchCuenta[1] . "</option>";
					}
				}
				?>
			</select>
		</div>
		<?php 
		if(strlen($fetchUsuario[3]) < 2){$mes = '0'.$fetchUsuario[3];}else{$mes = $fetchUsuario[3];}
		if(strlen($fetchUsuario[4]) < 2){$dia = '0'.$fetchUsuario[4];}else{$dia = $fetchUsuario[4];}
		$year = $fetchUsuario[2];
		$fecha = $year.'-'.$mes.'-'.$dia;
		?>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Fecha de modificacion <i style="color: darkorange">*</i></label>
			<input type="date" class="form-control" name="fechaMod" value="<?= $fecha ?>"/>
		</div>
		<div class="form-group col-md-4"></div>
		<div class="form-group col-md-12 mb-1">
			<h5 class="text-center mb-4"> 	INFORMACIÓN DIAGNOSTICA </h5>
		</div>
		<div class="form-group col-md-4">
			<label for="recipient-name" class="form-control-label" >Tipo discapacidad?<i style="color: darkorange">*</i></label>
			<?php 
			$selectTieneDiscapacidad = array(
				0 => 'Si',
				1 => 'No'
			);
			?>
			<select id="selectTieneDiscapacidad" class="form-control" name="tieneDiscapacidad" onchange="tieneDiscapacidadCual();" >
				<option>Selecciona una opción</option>
				<?php
				foreach ($selectTieneDiscapacidad as $key) {
					if($key == $fetchUsuario[7]){
						echo '<option selected value='.$key.'>'.$key.'</option>';
					}else{
						echo '<option value='.$key.'>'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-8" id="divTienediscapacidadCual" >
			
			<?php 
			if($fetchUsuario[7] == 'Si'){ ?>
				<label for="message-text" class="form-control-label">Cual <i style="color: darkorange">*</i></label>
				<input id="inputTieneDispacadiadCual"type="text" class="form-control" name="TipoDiscapacidadCual" value="<?= $fetchUsuario[31] ?>"/>
			<?php }else{?>
				<label for="message-text" class="form-control-label" style="visibility: hidden;">Cual <i style="color: darkorange">*</i></label>
			<?php } ?>
		</div>
		<div class="form-group col-md-12 mb-1">
			<h5 class="text-center mb-4">AUTONOMIA </h5>
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Se desplaza por sí solo <i style="color: darkorange">*</i></label>
			<?php 
			$selectAutonomia = array(
				0 => 'Si',
				1 => 'No'
			);
			?>
			<select class="form-control" name="autonomiaDesplaza" >
				<option>Selecciona una opción</option>
				<?php
				foreach ($selectAutonomia as $key) {
					if($key == $fetchUsuario[9]){
						echo '<option selected value='.$key.'>'.$key.'</option>';
					}else{
						echo '<option value='.$key.'>'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Permanece sentado <i style="color: darkorange">*</i></label>
			<select class="form-control" name="autonomiaSentado" >
				<option>Selecciona una opción</option>
				<?php
				foreach ($selectAutonomia as $key) {
					if($key == $fetchUsuario[10]){
						echo '<option selected value='.$key.'>'.$key.'</option>';
					}else{
						echo '<option value='.$key.'>'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Permanece en cama <i style="color: darkorange">*</i></label>
			<select class="form-control" name="autonomiaSentado" >
				<option>Selecciona una opción</option>
				<?php
				foreach ($selectAutonomia as $key) {
					if($key == $fetchUsuario[11]){
						echo '<option selected value='.$key.'>'.$key.'</option>';
					}else{
						echo '<option value='.$key.'>'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-12 mb-1">
			<h5 class="text-center mb-4">ACTIVIDADES DE AUTO CUIDADO </h5>
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Higiene<i style="color: darkorange">*</i></label>
			<?php 
			$selectAutoCuidado = array(
				0 => 'Independiente',
				1 => 'Semidependiente',
				2 => 'Dependiente'
			);
			?>
			<select class="form-control" name="autocuidadoHigiene" >
				<option>Selecciona una opción</option>
				<?php
				foreach ($selectAutoCuidado as $key) {
					if($key == $fetchUsuario[12]){
						echo '<option selected value='.$key.'>'.$key.'</option>';
					}else{
						echo '<option value='.$key.'>'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Vestido<i style="color: darkorange">*</i></label>
			<select class="form-control" name="autocuidadoVestido" >
				<option>Selecciona una opción</option>
				<?php
				foreach ($selectAutoCuidado as $key) {
					if($key == $fetchUsuario[13]){
						echo '<option selected value='.$key.'>'.$key.'</option>';
					}else{
						echo '<option value='.$key.'>'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Alimentación<i style="color: darkorange">*</i></label>
			<select class="form-control" name="autocuidadoAlimentacion" >
				<option>Selecciona una opción</option>
				<?php
				foreach ($selectAutoCuidado as $key) {
					if($key == $fetchUsuario[14]){
						echo '<option selected value='.$key.'>'.$key.'</option>';
					}else{
						echo '<option value='.$key.'>'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-12 mb-1">
			<h5 class="text-center mb-4">CONTROL DE ESFINTER </h5>
		</div>
		<div class="form-group col-md-4">
			<label for="recipient-name" class="form-control-label" >Vesical<i style="color: darkorange">*</i></label>
			<?php 
			$selectControlEsfinter = array(
				0 => 'Si',
				1 => 'No'
			);
			?>
			<select class="form-control" name="controlEsfinterVesical" >
				<option>Selecciona una opción</option>
				<?php
				foreach ($selectControlEsfinter as $key) {
					if($key == $fetchUsuario[15]){
						echo '<option selected value='.$key.'>'.$key.'</option>';
					}else{
						echo '<option value='.$key.'>'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="recipient-name" class="form-control-label" >Anal<i style="color: darkorange">*</i></label>
			<select class="form-control" name="controlEsfinterAnal" >
				<option>Selecciona una opción</option>
				<?php
				foreach ($selectControlEsfinter as $key) {
					if($key == $fetchUsuario[16]){
						echo '<option selected value='.$key.'>'.$key.'</option>';
					}else{
						echo '<option value='.$key.'>'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-4"></div>
		<div class="form-group col-md-12 mb-1">
			<h5 class="text-center mb-4">COMUNICACIÓN</h5>
		</div>
		<div class="form-group col-md-6 text-center">
			<div class="custom-control custom-checkbox custom-control-inline">
				<input type="checkbox" class="custom-control-input" id="defaultInline1" value="Oral" <?php if(!(empty($fetchUsuario[17]))){echo 'checked';} ?>>
				<label class="custom-control-label" for="defaultInline1">Oral</label>
			</div>
			<!-- Default inline 2-->
			<div class="custom-control custom-checkbox custom-control-inline">
				<input type="checkbox" class="custom-control-input" id="defaultInline2" value="Escrita" <?php if(!(empty($fetchUsuario[18]))){echo 'checked';} ?>>
				<label class="custom-control-label" for="defaultInline2">Escrita</label>
			</div>
			<!-- Default inline 3-->
			<div class="custom-control custom-checkbox custom-control-inline">
				<input type="checkbox" class="custom-control-input" id="defaultInline3" value="Gestual" <?php if(!(empty($fetchUsuario[19]))){echo 'checked';} ?>>
				<label class="custom-control-label" for="defaultInline3">Gestual</label>
			</div>
			<!-- Default inline 4-->
			<div class="custom-control custom-checkbox custom-control-inline">
				<input type="checkbox" class="custom-control-input" id="defaultInline4" value="Otra" <?php if(!(empty($fetchUsuario[20]))){echo 'checked';} ?>>
				<label class="custom-control-label" for="defaultInline4">Otra</label>
			</div>
		</div>
		<div id="divComunicacionOtroCual" class="form-group col-md-6" style="visibility: hidden;">
			<?php 
			if(!(empty($fetchUsuario[20]))){?>
				<input id="inputComunicacionOtroCual" type="text" class="form-control" name="comunicacionOtraCual" value="<?= $fetchUsuario[32] ?>"/>
			<?php } ?>
		</div>
		<div class="form-group col-md-12 mb-1" >
			<h5 class="text-center mb-4">FORMACIÓN ACADÉMICA</h5>
		</div>
		<div class="form-group col-md-12" >
			<table class="table table-bordered">
				<thead>
					<tr>
						<th scope="col" class="text-center">Nivel Educativo</th>
						<th scope="col" class="text-center">Último Grado Cursado</th>
						<th scope="col" class="text-center">Observaciones</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<th scope="row" class="text-center"></th>
						<td align="center">
							<div class="custom-control custom-checkbox">
								<input type="checkbox" class="custom-control-input" id="Desescolarizado" <?php if(!(empty($fetchUsuarioFormacionAcademica[2]))){echo 'checked';} ?>>
								<label class="custom-control-label" for="Desescolarizado">Desescolarizado</label>
							</div>
						</td>
						<td><input type="text" name="desescolarizadoObervacion" class="form-control" value="<?= $fetchUsuarioFormacionAcademica[3]?>"></td>
					</tr>
					<tr>
						<th scope="row" class="text-center">Primaria</th>
						<td><input type="text" name="primariaUltimoGradoCursado" class="form-control"  value="<?= $fetchUsuarioFormacionAcademica[4]?>"></td>
						<td><input type="text" name="primariaObservacion" class="form-control"  value="<?= $fetchUsuarioFormacionAcademica[5]?>"></td>
					</tr>
					<tr>
						<th scope="row" class="text-center">Secudaria</th>
						<td><input type="text" name="SecudariaUltimoGradoCursado" class="form-control"  value="<?= $fetchUsuarioFormacionAcademica[6]?>"></td>
						<td><input type="text" name="SecudariaObservacion" class="form-control"  value="<?= $fetchUsuarioFormacionAcademica[7]?>"></td>
					</tr>
					<tr>
						<th scope="row" class="text-center">Técnico</th>
						<td><input type="text" name="TecnicoUltimoGradoCursado" class="form-control" value="<?= $fetchUsuarioFormacionAcademica[8]?>"></td>
						<td><input type="text" name="TecnicoObservacion" class="form-control"value="<?= $fetchUsuarioFormacionAcademica[9]?>"></td>
					</tr>
					<tr>
						<th scope="row" class="text-center">Tecnólogo</th>
						<td><input type="text" name="TecnologoUltimoGradoCursado" class="form-control" value="<?= $fetchUsuarioFormacionAcademica[10]?>"></td>
						<td><input type="text" name="TecnologoObservacion" class="form-control" value="<?= $fetchUsuarioFormacionAcademica[11]?>"></td>
					</tr>
					<tr>
						<th scope="row" class="text-center">Superior</th>
						<td><input type="text" name="SuperiorUltimoGradoCursado" class="form-control" value="<?= $fetchUsuarioFormacionAcademica[12]?>"></td>
						<td><input type="text" name="SuperiorObservacion" class="form-control" value="<?= $fetchUsuarioFormacionAcademica[13]?>"></td>
					</tr>
					<tr>
						<th scope="row" class="text-center">Otro</th>
						<td><input type="text" name="OtroUltimoGradoCursado" class="form-control" value="<?= $fetchUsuarioFormacionAcademica[14]?>"></td>
						<td><input type="text" name="OtroObservacion" class="form-control" value="<?= $fetchUsuarioFormacionAcademica[15]?>"></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="form-group col-md-12 mb-1" >
			<h5 class="text-center mb-4">COORDINACIÓN MOTORA GRUESA</h5>
		</div>
		<div class="form-group col-md-4" >
			<?php 
			$selectCordinacion = array(
				0 => 'Se desplaza sola',
				1 => 'Silla de ruedas',
				2 => 'Caminador',
				3 => 'Baston',
				4 => 'Muletas',
				5 => 'Con la ayuda de otra persona'
			);
			?>
			<label for="recipient-name" class="form-control-label">Se desplaza con la ayuda de <i style="color: darkorange">*</i></label>
			<select class="form-control" name="coordinacionSeDesplazaConAyuda" >
				<option>Selecciona una opción</option>
				<?php
				foreach ($selectCordinacion as $key) {
					if($key == $fetchUsuario[22]){
						echo '<option selected value='.$key.'>'.$key.'</option>';
					}else{
						echo '<option value='.$key.'>'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-12 mb-1" >
			<h5 class="text-center mb-4">COORDINACIÓN MOTORA FINA</h5>
		</div>
		<div class="form-group col-md-4" >
			<?php 
			$selectCordinacionDominancia = array(
				0 => 'Diestra',
				1 => 'Izquierda'
			);
			?>
			<label for="recipient-name" class="form-control-label">Dominancia <i style="color: darkorange">*</i></label>
			<select class="form-control" name="coordinacionDominancia" >
				<option>Selecciona una opción</option>
				<?php
				foreach ($selectCordinacionDominancia as $key) {
					if($key == $fetchUsuario[23]){
						echo '<option selected value='.$key.'>'.$key.'</option>';
					}else{
						echo '<option value='.$key.'>'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-4" >
			<?php 
			$selectCordinacionAgarreManos = array(
				0 => 'Llena',
				1 => 'Cilindrico',
				2 => 'Pinza Fina',
				3 => 'Pinza Trípode'
			);
			?>
			<label for="recipient-name" class="form-control-label">Hace agarres mano <i style="color: darkorange">*</i></label>
			<select class="form-control" name="coordinacionAgarreManos">
				<option>Selecciona una opción</option>
				<?php
				foreach ($selectCordinacionAgarreManos as $key) {
					if($key == $fetchUsuario[24]){
						echo '<option selected value='.$key.'>'.$key.'</option>';
					}else{
						echo '<option value='.$key.'>'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-4" >
			<?php 
			$selectCordinacionNivelesFlexion = array(
				0 => 'Total',
				1 => 'Parcial',
				2 => 'Pinza Fina',
				3 => 'Nula'
			);
			?>
			<label for="recipient-name" class="form-control-label">Niveles de flexión y extensión <i style="color: darkorange">*</i></label>
			<select class="form-control" name="coordinacionAgarreManos">
				<option>Selecciona una opción</option>
				<?php
				foreach ($selectCordinacionNivelesFlexion as $key) {
					if($key == $fetchUsuario[25]){
						echo '<option selected value='.$key.'>'.$key.'</option>';
					}else{
						echo '<option value='.$key.'>'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label"> Movilidad miembros superiores <i style="color: darkorange">*</i></label>
			<textarea class="form-control" name="movilidadMiembrosSuperiores" cols="40" rows="5" ><?= $fetchUsuario[33] ?></textarea>
		</div>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label"> Historia laboral <i style="color: darkorange">*</i></label>
			<textarea class="form-control" name="historialaboral" cols="40" rows="5" ><?= $fetchUsuario[34] ?></textarea>
		</div>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label"> Actividades ocupacionales en las que participa <i style="color: darkorange">*</i></label>
			<textarea class="form-control" name="actividadesOcupacionalesParticipa" cols="40" rows="5" ><?= $fetchUsuario[35] ?></textarea>
		</div>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label"> Observaciones y recomendaciones <i style="color: darkorange">*</i></label>
			<textarea class="form-control" name="observacionesRecomendaciones" cols="40" rows="5" ><?= $fetchUsuario[36] ?></textarea>
		</div>
		<div class="form-group col-md-4" >
			<?php 
			$selectAreaOcupacional = array(
				0 => 'Traperas',
				1 => 'Bolsas',
				2 => 'Limpiones',
				3 => 'Jardineria',
				4 => 'Domesticas',
				5 => 'Otras'
			);
			?>
			<label for="recipient-name" class="form-control-label">Area ocupacional a la cual se remite<i style="color: darkorange">*</i></label>
			<select class="form-control" name="areaOcupacional">
				<option>Selecciona una opción</option>
				<?php
				foreach ($selectAreaOcupacional as $key) {
					if($key == $fetchUsuario[30]){
						echo '<option selected value='.$key.'>'.$key.'</option>';
					}else{
						echo '<option value='.$key.'>'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
	</div>

	
	<!-- End Row -->


	<!-- Modal Footer -->
	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">
			Cancelar
		</button>
		<button type="submit" class="btn btn-primary" name="Actualizar">
			Actualizar
		</button>
	</div>
</form>
<script type="text/javascript">

	$("#defaultInline4").change(function (){
		var divOtro = document.getElementById('divComunicacionOtroCual');
		if($(this).prop('checked') == true){
			if(document.getElementById('inputComunicacionOtroCual')){
				divOtro.style.visibility = "visible";
			}else{
				divOtro.style.visibility = "visible";
				var input  = document.createElement("input");

				input.setAttribute("id", "inputComunicacionOtroCual");
				input.setAttribute("class", "form-control");
				input.setAttribute("type", "text");
				input.setAttribute("name", "comunicacionOtraCual");

				document.getElementById("divComunicacionOtroCual").appendChild(input);
			}
		}else{
			if(document.getElementById('inputComunicacionOtroCual')){
				divOtro.style.visibility = "hidden";
			}
		}
	});
	function tieneDiscapacidadCual(){
		var selectValue = document.getElementById('selectTieneDiscapacidad').value;
		var div = document.getElementById('divTienediscapacidadCual');
		var label = document.getElementById('labelTienediscapacidadCualNuevo');
		if(selectValue == "Si"){
			if(document.getElementById('inputTieneDispacadiadCual')){
				div.style.visibility = "visible";
				input = document.getElementById('inputTieneDispacadiadCual');
				label.style.visibility = "visible";
				input.value = "";
			}else{
				div.style.visibility = "visible";
				label.style.visibility = "visible";
				var input  = document.createElement("input");
				input.setAttribute("id", "inputTieneDispacadiadCual");
				input.setAttribute("class", "form-control");
				input.setAttribute("type", "text");
				input.setAttribute("name", "TipoDiscapacidadCual");
				document.getElementById("divTienediscapacidadCual").appendChild(input);
			}
			
		}else{
			if(document.getElementById('inputTieneDispacadiadCual')){
				div.style.visibility = "hidden";
				label.style.visibility = "hidden";
			}
		}
	}


</script>