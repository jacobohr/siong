<?php

//include connection file 
require "../../../../session.php";

$sqlUsuario = "
SELECT  
UPPER((CONVERT(CAST(CONVERT(CONCAT(c.primer_nombre,' ',c.segundo_nombre,' ',c.primer_apellido,' ',c.segundo_apellido) USING latin1) AS BINARY) USING utf8))) AS nombreUsuario,
CONCAT(an.ano_mod,'/',an.mes_mod,'/',an.dia_mod) AS fecha,  
an.hora_mod,
an.tipo_discapacidad,
(CONVERT(CAST(CONVERT(an.cual_discapacidad USING latin1) AS BINARY) USING utf8))AS cual_discapacidad,
an.autonomia_desplaza,
an.autonomia_sentado,
an.autonomia_cama,
an.auto_higiene,
an.auto_vestido,
an.auto_alimento,
an.control_vesical,
an.control_anal,
an.comunicacion_oral,
an.comunicacion_escrita,
an.comunicacion_gestual,
an.comunicacion_otra,
(CONVERT(CAST(CONVERT(an.comunicacion_otra_cual USING latin1) AS BINARY) USING utf8))AS comunicacion_otra_cual,
an.coordinacion_desplaza,
an.coordinacion_dominancia,
an.agarre,
an.nivel_flexion_extension,
(CONVERT(CAST(CONVERT(an.movilidad USING latin1) AS BINARY) USING utf8))AS movilidad,
(CONVERT(CAST(CONVERT(an.historia_laboral USING latin1) AS BINARY) USING utf8))AS historia_laboral,
(CONVERT(CAST(CONVERT(an.actividad_ocupacional USING latin1) AS BINARY) USING utf8))AS actividad_ocupacional,
(CONVERT(CAST(CONVERT(an.observacion USING latin1) AS BINARY) USING utf8))AS observacion,
an.area_ocupacional
FROM gddt_terapia_ocupacional an
INNER JOIN gddt_cuentas c ON c.id_cuenta = an.id_cuenta 
WHERE an.id_terapia_ocupacional = " . $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);
$rows = mysqli_num_rows($queryUsuario);

if($rows == 0){
	echo "<p style='color:red;font-size:0.9em;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No se encontraron datos :(</p>";
}else{


	?>

	<div class="content">
		<h6 class="text-center mb-4"><b>Actualizado por <?= $fetchUsuario[0];?> el <?= $fetchUsuario[1]; ?> a las <?= $fetchUsuario[2]; ?></b></h6>
		<div class="row m-4">
			<div class="col-md-6 border-right">
				<div class="row">
					<h6 class="mb-4"><b>INFORMACIÓN DIAGNOSTICA</b></h6>
					<div class="col-md-12">
						<label>
							Discapacidad
						</label>
						<p><?php if(empty($fetchUsuario[3])){echo "Sin dato";}else{echo $fetchUsuario[3];} ?></p>
					</div>
					<?php if($fetchUsuario[3] == 'Si'){ ?>
						<div class="col-md-12">
							<label>
								Cual
							</label>
							<p><?php if(empty($fetchUsuario[4])){echo "Sin dato";}else{echo $fetchUsuario[4];} ?></p>
						</div>
					<?php } ?>
					<h6 class="mb-4"><b>AUTONOMÍA</b></h6>
					<div class="col-md-12">
						<label>
							Se desplaza por si solo
						</label>
						<p><?php if(empty($fetchUsuario[5])){echo "Sin dato";}else{echo $fetchUsuario[5];} ?></p>
					</div>
					<div class="col-md-12">
						<label>
							Permanece sentado
						</label>
						<p><?php if(empty($fetchUsuario[6])){echo "Sin dato";}else{echo $fetchUsuario[6];} ?></p>
					</div>
					<div class="col-md-12">
						<label>
							Permanece en cama
						</label>
						<p><?php if(empty($fetchUsuario[7])){echo "Sin dato";}else{echo $fetchUsuario[7];} ?></p>
					</div>
					<h6 class="mb-4"><b>ACTIVIDADES DE AUTO CUIDADO</b></h6>
					<div class="col-md-12">
						<label>
							Higiene
						</label>
						<p><?php if(empty($fetchUsuario[8])){echo "Sin dato";}else{echo $fetchUsuario[8];} ?></p>
					</div>
					<div class="col-md-12">
						<label>
							Vestido
						</label>
						<p><?php if(empty($fetchUsuario[9])){echo "Sin dato";}else{echo $fetchUsuario[9];} ?></p>
					</div>
					<div class="col-md-12">
						<label>
							Alimentación
						</label>
						<p><?php if(empty($fetchUsuario[10])){echo "Sin dato";}else{echo $fetchUsuario[10];} ?></p>
					</div>
					<h6 class="mb-4"><b>CONTROL DE ESFINTER</b></h6>
					<div class="col-md-12">
						<label>
							Vesical
						</label>
						<p><?php if(empty($fetchUsuario[11])){echo "Sin dato";}else{echo $fetchUsuario[11];} ?></p>
					</div>
					<div class="col-md-12">
						<label>
							Anal
						</label>
						<p><?php if(empty($fetchUsuario[12])){echo "Sin dato";}else{echo $fetchUsuario[12];} ?></p>
					</div>
					<h6 class="mb-4"><b>COMUNICACIÓN</b></h6>
					<div class="col-md-12">
						<label>Oral: </label><?php if(empty($fetchUsuario[13])){echo "No";}else{echo 'Si';} ?><br>
						<label>Escritura: </label><?php if(empty($fetchUsuario[14])){echo "No";}else{echo 'Si';} ?><br>
						<label>Gestual: </label> <?php if(empty($fetchUsuario[15])){echo "No";}else{echo 'Si';} ?><br>
						<?php if(!empty($fetchUsuario[16])){ echo '<label>Otra: </label>'.$fetchUsuario[17];} ?>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<h6 class="text-center mb-4"><b>FORMACIÓN ACADÉMICA</b></h6>
				<?php 

				$sqlFormacionAcademica = "SELECT * FROM gddt_terapia_ocupacional_formacion WHERE id_terapia_ocupacional = " . $_REQUEST["idu"];
				$queryFormacionAcademica = mysqli_query($conn, $sqlFormacionAcademica);
				$fetchFormacionAcademica = mysqli_fetch_row($queryFormacionAcademica);

				if(!empty($fetchFormacionAcademica[2])){
					echo '<div class="col-md-12">';
					echo '<label>Desescolarizado: Si</label>';
					echo '<label>Observacion: '.$fetchFormacionAcademica[3].'</label>';
					echo '</div>';
				}else{?>
					<div class="col-md-12">
						<table class="table table-bordered">
							<thead>
								<tr >
									<th scope="col">Nivel Educativo</th>
									<th scope="col">Último Grado Cursado</th>
									<th scope="col">Observaciones</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<th scope="row">Primaria</th>
									<td><?php echo $fetchFormacionAcademica[4];?></td>
									<td><?php echo $fetchFormacionAcademica[5];?></td>
								</tr>
								<tr>
									<th scope="row">Secundaria</th>
									<td><?php echo $fetchFormacionAcademica[6];?></td>
									<td><?php echo $fetchFormacionAcademica[7];?></td>
								</tr>
								<tr>
									<th scope="row">Técnico</th>
									<td><?php echo $fetchFormacionAcademica[8];?></td>
									<td><?php echo $fetchFormacionAcademica[9];?></td>
								</tr>
								<tr>
									<th scope="row">Tecnólogo</th>
									<td><?php echo $fetchFormacionAcademica[10];?></td>
									<td><?php echo $fetchFormacionAcademica[11];?></td>
								</tr>
								<tr>
									<th scope="row">Superior</th>
									<td><?php echo $fetchFormacionAcademica[12];?></td>
									<td><?php echo $fetchFormacionAcademica[13];?></td>
								</tr>
								<tr>
									<th scope="row">Otro</th>
									<td><?php echo $fetchFormacionAcademica[14];?></td>
									<td><?php echo $fetchFormacionAcademica[15];?></td>
								</tr>
							</tbody>
						</table>
					</div>
				<?php } ?>
				<h6 class="text-center mb-4"><b>COORDINACIÓN MOTORA GRUESA</b></h6>
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-6">
							<label>
								Desplazamiento
							</label>
							<p><?php if(empty($fetchUsuario[18])){echo "Sin dato";}else{echo $fetchUsuario[18];} ?></p>
						</div>
						<div class="col-md-6">
							<label>
								Dominancia
							</label>
							<p><?php if(empty($fetchUsuario[19])){echo "Sin dato";}else{echo $fetchUsuario[19];} ?></p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<label>
								Hace agarres mano
							</label>
							<p><?php if(empty($fetchUsuario[20])){echo "Sin dato";}else{echo $fetchUsuario[20];} ?></p>
						</div>
						<div class="col-md-6">
							<label>
								Niveles de flexión y extensión
							</label>
							<p><?php if(empty($fetchUsuario[21])){echo "Sin dato";}else{echo $fetchUsuario[21];} ?></p>
						</div>
						<div class="col-md-6">
							<label>
								Movilidad miembros superiores
							</label>
							<p><?php if(empty($fetchUsuario[22])){echo "Sin dato";}else{echo $fetchUsuario[22];} ?></p>
						</div>
						<div class="col-md-6">
							<label>
								Historia laboral
							</label>
							<p><?php if(empty($fetchUsuario[23])){echo "Sin dato";}else{echo $fetchUsuario[23];} ?></p>
						</div>
						<div class="col-md-6">
							<label>
								Área ocupacional a la cual se remite
							</label>
							<p><?php if(empty($fetchUsuario[26])){echo "Sin dato";}else{echo $fetchUsuario[26];} ?></p>
						</div>
						<div class="col-md-6">
							<label>
								Observaciones y recomendaciones
							</label>
							<p><?php if(empty($fetchUsuario[25])){echo "Sin dato";}else{echo $fetchUsuario[25];} ?></p>
						</div>
						<div class="col-md-12">
							<label>
								Actividades ocupacionales en las que participa
							</label>
							<p><?php if(empty($fetchUsuario[24])){echo "Sin dato";}else{echo $fetchUsuario[24];} ?></p>
						</div>
						
						
					</div>
				</div>
			</div>
			

		</div>
		
		
		
		

		<div class="user-block">&nbsp;</div>

	</div><!-- Fin del content -->
	<?php } ?>