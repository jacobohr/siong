<?php 
include_once(dirname(__FILE__) . '/../../session.php');



if (isset($_REQUEST["param"])) {
	$sqlPaciente = "
	SELECT id_matricula_dato AS matricula
	FROM gddt_matricula_datos
	WHERE id_matricula_dato = '" .$_REQUEST["param"] . "'";
	$queryPaciente = mysqli_query($conn, $sqlPaciente);
	$fetchPaciente = mysqli_fetch_row($queryPaciente);
}


?>
<!DOCTYPE html>
<html>
<?php require_once dirname(__FILE__) . '/../../head.php'; ?>
<body class="hold-transition sidebar-mini layout-fixed">
	<div class="wrapper">
		<!-- Modal Nueva Atención -->
		<div id="modalw" class="modal modal-warning" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
		aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Atención!</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>

				<div class="modal-body">

					<p>Debe ingresar un número de documento primero.</p>

				</div><!-- End of Modal body -->

				<div class="modal-footer">
					<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cerrar</button>
				</div>

			</div><!-- End of Modal content -->
		</div><!-- End of Modal dialog -->
	</div><!-- End of Modal -->

	<!-- Navbar -->
	<?php require_once dirname(__FILE__) . '/../../menu.php'; ?>

	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper" style="background-color: white">
		<!-- Content Header (Page header) -->
		<div class="content-header">
			<div class="container-fluid">
				<div class="row mb-2">
					<div class="col-sm-6">
						<h1 class="m-0">Gerontologia</h1>
					</div><!-- /.col -->
					<div class="col-sm-6">
						<ol class="breadcrumb float-sm-right">
							<li class="breadcrumb-item"><a href="#">Gerontologia</a></li>
							<li class="breadcrumb-item active">Usuarios</li>
						</ol>
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div><!-- /.container-fluid -->
		</div>
		<!-- /.content-header -->
		<section style="margin-top:10px; margin-right: 10px">
			<div class="row m-2">
				<div class="col-md-5">
					<div class="input-group">
						<select id="paramPaciente" style="width:500px" name="categoryName" style="margin-right: 4px" ></select>
						<span class="input-group-btn">
							<a id="buscar" href="javascript:recargarDatosPaciente(document.getElementById('paramPaciente').value);"
							class="btn btn-info" style="height: 34px;"><i class="fa fa-search"></i>
						</a>
					</span>
				</div>
			</div>
			<div class="col-md-4">
			</div>
			<div class="col-md-3">
				<div class="box-profile">
					<a href="JavaScript:void(0)" class="btn btn-info btn-block" onclick="fnAgregarPaciente();">
						<i class="fa fa-fw fa-user-plus" style="margin-right:5px;"></i>
						<b>Registrar Nuevo Usuario</b>
					</a>
				</div>
			</div>
		</div>
	</section>
	<div>&nbsp;</div>
	<div id="cargarPacienteHC">
		<section class="content">
			<div class="row m-2">
				<div class="col-md-12">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Últimos Usuarios registrados</h3>
						</div>
						<!-- /.box-header -->
						<div class="box-body table-responsive no-padding">
							<table class="table table-hover">
								<tr>
									<th style="width: 15px">#</th>
									<th>Documento</th>
									<th>Nombre Completo</th>
									<th>Perfil</th>
									<th>Estado</th>
									<th>Tipo de usuario</th>
									<th>Procedencia</th>
								</tr>

								<?php
								$sql = "SELECT  
								md.id_matricula_dato id_matricula,								
								REPLACE(md.num_documento,'.','') documento,
								md.num_documento documentoSinReplace,
								(CONVERT(CAST(CONVERT(CONCAT(UPPER(md.primer_nombre),' ',UPPER(md.segundo_nombre),' ',UPPER(md.primer_apellido),' ',UPPER(md.segundo_apellido)) USING latin1) AS BINARY) USING utf8))AS nombre,
								pu.descripcion perfil, 
								e.descripcion estado,
								tu.descripcion tipo, 
								p.descripcion procedencia 
								FROM gddt_matricula_datos md
								INNER JOIN gddt_estados e ON md.id_estado = e.id_estado
								INNER JOIN gddt_procedencias p ON md.id_procedencia = p.id_procedencia 
								INNER JOIN gddt_tipo_usuarios tu ON md.id_tipo_usuario = tu.id_tipo_usuario 
								INNER JOIN gddt_perfil_usuarios pu ON md.id_perfil_usuario = pu.id_perfil_usuario 
								ORDER BY id_matricula DESC LIMIT 10;";
								$query = mysqli_query($conn, $sql);

								$contador = 1;
								while ($fetch = mysqli_fetch_array($query)) {
									?>
									<tr>
										<td><?php echo $contador; ?></td>
										<td><a href="pacientes.php?param=<?php echo $fetch["documentoSinReplace"]; ?>"><?php echo $fetch['documento'] ?></a></td>
										<td><?php echo strtoupper($fetch["nombre"]); ?></td>
										<td><?php echo $fetch["perfil"]; ?></td>
										<td><?php echo $fetch["estado"]; ?></td>
										<td><?php echo $fetch["tipo"]; ?></td>
										<td><?php echo $fetch["procedencia"]; ?></td>
									</tr>
									<?php

									$contador++;
								}
								?>

							</table>
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</div>
			</div>
		</section>
	</div>
</div>
<!-- /.content-wrapper -->
<!-- Modal Ver Datos de Usuario -->
<div id="modalVerDatos" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #17A2B8">
				<h5 class="modal-title" style="text-align: center;">Informacion</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div id="divVerDatos" class="modal-body">
				<p>Modal body text goes here.</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>
<!-- Modal Actualizar Datos de Usuario -->
<div id="modalModificarUsuario" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-xl">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #17A2B8">
				<div class="row">
					<div class="form-group col-md-12 text-center" style="margin-bottom: -8px;background-color: #17A2B8">
						<h4 class="modal-title">MODIFICAR DATOS DEL USUARIO</h4>
					</div>
				</div>
			</div>

			<div id="divModificarDatos" class="modal-body">

			</div>

		</div>
	</div>
</div>

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
	<!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->
<div id="respuesta"></div>
<?php require_once('../../footer.php') ?>
<div id="loadModals"></div>
<!-- Scripts -->
<?php require('../../scripts.php') ?>

<script type="text/javascript">
	function fnAgregarPaciente() {
		$.ajax({
			url: 'nuevoPaciente.php',
			type: 'POST',
			dataType: 'html',
			success: function (data) {
				$('#cargarPacienteHC').html(data);
			}
		});
	}

	function recargarDatosPaciente(id_paciente) {
		if (id_paciente !== "") {
			$.ajax({
				url: 'cargarPacienteHC.php',
				type: 'POST',
				dataType: 'html',
				async: false,
				data: {docPaciente: id_paciente},
				success: function (data) {
					$('#cargarPacienteHC').html(data);
				}
			});
			fnCargarDatosGrid(id_paciente);
		} else {
			$('#paramPaciente').focus();
			$('#modalw').modal("show");
		}
	}
	function fnCargarDatosGrid(id_paciente) {
		$.fn.dataTableExt.sErrMode = 'none';
		$('#employee_grid').DataTable({
			"bProcessing": true,
			"serverSide": true,
			"order": [[5, 'desc']],
			"ajax": {
				url: "response.php",
				type: "post",
				data: {idPaciente: id_paciente},
				error: function () {
					$("#employee_grid_processing").css("display", "none");
				}
			},
		});
	}
	$.fn.select2.defaults.set('language', 'es');
	$('#paramPaciente').select2({
		placeholder: "Digite el número de documento del paciente...",
		language: {
			noResults: function() {
				return "No hay resultado";        
			},
			searching: function() {
				return "Buscando...";
			},
			errorLoading: function(){
				return "Ningun registro encontrado";
			},
		},
		ajax:{
			url: 'autocomplete/traerPacientes.php',
			dataType: 'json',
			delay: 250,
			processResult: function(data){
				return{
					results:name
				};
			},
			cache: true
		}
	});


	var perfilVisible = 1;

	function fnOcultarMostrarPerfil() {
		if (perfilVisible === 1) {
			$('#box-profile').hide();
			$('#icon-profile-show-hide').removeClass('fa-minus');
			$('#icon-profile-show-hide').addClass('fa-plus');
			$('#box-left-profile').removeClass('col-md-3');
			$('#box-left-profile').addClass('col-md-1');
			$('#box-right-content').removeClass('col-md-9');
			$('#box-right-content').addClass('col-md-11');

			$('.profile-user-img').css({'width': '40px;'});

			perfilVisible = 0;
		} else {
			$('#icon-profile-show-hide').removeClass('fa-plus');
			$('#icon-profile-show-hide').addClass('fa-minus');
			$('#box-left-profile').removeClass('col-md-1');
			$('#box-left-profile').addClass('col-md-3');
			$('#box-right-content').removeClass('col-md-11');
			$('#box-right-content').addClass('col-md-9');

			$('.profile-user-img').css({'width': '100px;'});
			$('#box-profile').show();

			perfilVisible = 1;
		}
	}
	function fnReporteCalidadVida(idQuery){
		$.ajax({
			url: "gerontologia/reportes/reporteGerontologiaCalidadVida.php",
			type: "POST",
			dataType: "html",
			data: {idu: idQuery},
			success: function(data)
			{
				location.href = 'gerontologia/reportes/reporteGerontologiaCalidadVida.php?idu='+idQuery;
			}
		});
		console.log(idQuery);
	}
	function fnCargarInfoDatosC(id_dato_clinico) {
		$.ajax({
			url: 'cargarInfoDatosC.php',
			type: 'POST',
			dataType: 'html',
			data: {idc: id_dato_clinico},
			success: function (data) {
				$('#modalVerDatosC').html(data);
			}
		});
	}
	function fnVerInformacionNutricional(idQuery) {
		$.ajax({
			url: 'modalVerInformacionNutricional.php',
			type: 'POST',
			dataType: 'html',
			data: {idu: idQuery},
			success: function (data) {
				$('#divVerDatos').html(data);
				$('#modalVerDatos').modal('show');
			}
		});
	}


	<?php 
	function fnEditarDatos($nameFunction,$formName){
		$string = 'function '.$nameFunction.'(idUsuario) {
			$.ajax({
				url: "'.$formName.'.php",
				type: "POST",
				dataType: "html",
				data: {idu: idUsuario},
				success: function (data) {
					$("#divModificarDatos").html(data);
				}
			});
		}
		';
		return $string;
	}
	function fnEliminarDatos($nameFunction,$url){
		$string = '
		function '.$nameFunction.'(idUsuario){
			swal({
				title: "Esta informacion puede ser valiciosa, estas seguro que desear eliminarla?",
				icon: "warning",
				buttons: true,
				dangerMode: true,
			})
			.then((willDelete) => {
				if (willDelete) {
					swal("Procesando peticion!", {
						timer: 3000,
					});
					window.location="'.$url.'&idRegistro="+idUsuario;
				} 
			});
		}';
		return $string;
	}
	function fnVerDatos($nameFunction,$formName){
		$string = '
		function '.$nameFunction.'(idQuery){
			$.ajax({
				url: "'.$formName.'.php",
				type: "POST",
				dataType: "html",
				data: {idu: idQuery},
				success: function (data) {
					$("#divVerDatos").html(data);
					$("#modalVerDatos").modal("show");
				}
			});
		}';
		return $string;
	}
	?>
	/*Nutricion*/
	<?php echo fnVerDatos('fnVerSeguimientoNutricion','nutricion/modalVer/modalVerSeguimientoNutricion') ?>
	<?php echo fnVerDatos('fnVerInformacionNutricional','nutricion/modalVer/modalVerInformacionNutricional') ?>
	<?php echo fnEliminarDatos('fnEliminarNutricionInfo',''.$base_url.'pages/gerontologia/nutricion/procesarDatos/procesarInformacionNutricional.php?tipo=delete') ?>
	<?php echo fnEliminarDatos('fnEliminarSeguimientoNutricional',''.$base_url.'pages/gerontologia/nutricion/procesarDatos/procesarSeguimientoNutricional.php?tipo=delete') ?>
	/*Psicologia*/
	<?php echo fnEliminarDatos('fnEliminarPsicologiaAntecedenteMental',''.$base_url.'pages/gerontologia/psicologia/procesarDatos/procesarPsicologiaAntecedenteMental.php?tipo=delete') ?>
	<?php echo fnEliminarDatos('fnEliminarPsicologiaAntecFamiliar',''.$base_url.'pages/gerontologia/psicologia/procesarDatos/procesarPsicologiaAntecedenteFamiliar.php?tipo=delete') ?>
	<?php echo fnEliminarDatos('fnEliminarPsicologiaRelacionFamiliar',''.$base_url.'pages/gerontologia/psicologia/procesarDatos/procesarPsicologiaRelacionFamiliar.php?tipo=delete') ?>
	<?php echo fnEliminarDatos('fnEliminarPsicologiaApoyoFamiliar',''.$base_url.'pages/gerontologia/psicologia/procesarDatos/procesarPsicologiaApoyoFamiliar.php?tipo=delete') ?>
	<?php echo fnEliminarDatos('fnEliminarPsicologiaRelacionesSociales',''.$base_url.'pages/gerontologia/psicologia/procesarDatos/procesarPsicologiaRelacionSocial.php?tipo=delete') ?>
	<?php echo fnEliminarDatos('fnEliminarPsicologiaGrupoSocial',''.$base_url.'pages/gerontologia/psicologia/procesarDatos/procesarPsicologiaGrupoSocial.php?tipo=delete') ?>
	<?php echo fnEliminarDatos('fnEliminarPsicologiaRasgosPersonalidad',''.$base_url.'pages/gerontologia/psicologia/procesarDatos/procesarPsicologiaRasgosPersonalidad.php?tipo=delete') ?>
	<?php echo fnEliminarDatos('fnEliminarPsicologiaSintomaDepresion',''.$base_url.'pages/gerontologia/psicologia/procesarDatos/procesarPsicologiaSintomaDepresion.php?tipo=delete') ?>
	<?php echo fnEliminarDatos('fnEliminarPsicologiaSintomaAnsiedad',''.$base_url.'pages/gerontologia/psicologia/procesarDatos/procesarPsicologiaSintomaAnsiedad.php?tipo=delete') ?>
	<?php echo fnEliminarDatos('fnEliminarPsicologiaImpresionDiagnostica',''.$base_url.'pages/gerontologia/psicologia/procesarDatos/procesarPsicologiaImpresionDiagnostica.php?tipo=delete') ?>
	<?php echo fnEliminarDatos('fnEliminarPsicologiaEvaluacionMultiaxial',''.$base_url.'pages/gerontologia/psicologia/procesarDatos/procesarPsicologiaEvaluacionMultiaxial.php?tipo=delete') ?>
	<?php echo fnEliminarDatos('fnEliminarPsicologiaPlanTratamiento',''.$base_url.'pages/gerontologia/psicologia/procesarDatos/procesarPsicologiaPlanTratamiento.php?tipo=delete') ?>
	<?php echo fnEliminarDatos('fnEliminarPsicologiaSeguimiento',''.$base_url.'pages/gerontologia/psicologia/procesarDatos/procesarPsicologiaSeguimiento.php?tipo=delete') ?>
	<?php echo fnEliminarDatos('fnEliminarPsicologiaFamiliograma',''.$base_url.'pages/gerontologia/psicologia/procesarDatos/procesarPsicologiaFamiliograma.php?tipo=delete') ?>
	<?php echo fnEliminarDatos('fnEliminarPsicologiaPatologia',''.$base_url.'pages/gerontologia/psicologia/procesarDatos/procesarPsicologiaPatologia.php?tipo=delete') ?>
	<?php echo fnVerDatos('fnVerAntecMental','psicologia/modalVer/modalVerPsicologiaAntecedenteMental') ?>
	<?php echo fnVerDatos('fnVerPsicologiaAntecFamiliar','psicologia/modalVer/modalVerPsicologiaAntecFamiliar') ?>
	<?php echo fnVerDatos('fnVerPsicologiaPatologias','psicologia/modalVer/modalVerPsicologiaPatologias') ?>
	<?php echo fnVerDatos('fnVerPsicologiaRelacionesFamiliares','psicologia/modalVer/modalVerPsicologiaRelacionesFamiliares') ?>
	<?php echo fnVerDatos('fnVerPsicologiaApoyoFamiliar','psicologia/modalVer/modalVerPsicologiaApoyoFamiliar') ?>
	<?php echo fnVerDatos('fnVerPsicologiaRelacionesSociales','psicologia/modalVer/modalVerPsicologiaRelacionesSociales') ?>
	<?php echo fnVerDatos('fnVerPsicologiaGruposSociales','psicologia/modalVer/modalVerPsicologiaGruposSociales') ?>
	<?php echo fnVerDatos('fnVerPsicologiaRasgosPersonalidad','psicologia/modalVer/modalVerPsicologiaRasgosPersonalidad') ?>
	<?php echo fnVerDatos('fnVerPsicologiaSintomaDepresion','psicologia/modalVer/modalVerPsicologiaSintomaDepresion') ?>
	<?php echo fnVerDatos('fnVerPsicologiaSintomasAnsiedad','psicologia/modalVer/modalVerPsicologiaSintomasAnsiedad') ?>
	<?php echo fnVerDatos('fnVerPsicologiaImpresionDiagnostica','psicologia/modalVer/modalVerPsicologiaImpresionDiagnostica') ?>
	<?php echo fnVerDatos('fnVerPsicologiaEvaluacionMultiaxial','psicologia/modalVer/modalVerPsicologiaEvaluacionMultiaxial') ?>
	<?php echo fnVerDatos('fnVerPsicologiaFamiliograma','psicologia/modalVer/modalVerPsicologiaFamiliograma') ?>
	<?php echo fnVerDatos('fnVerPsicologiaPlanTratamientos','psicologia/modalVer/modalVerPsicologiaPlanTratamientos') ?>
	<?php echo fnVerDatos('fnVerPsicologiaSeguimientos','psicologia/modalVer/modalVerPsicologiaSeguimientos') ?>
	/* Medicina */
	<?php echo fnEliminarDatos('fnEliminarMedicinaSignosVitales',''.$base_url.'pages/gerontologia/medicina/procesarDatos/procesarMedicinaSignosVitales.php?tipo=delete') ?>
	<?php echo fnEliminarDatos('fnEliminarMedicinaPatologias',''.$base_url.'pages/gerontologia/medicina/procesarDatos/procesarMedicinaPatologias.php?tipo=delete') ?>
	<?php echo fnEliminarDatos('fnEliminarMedicinaConsultas',''.$base_url.'pages/gerontologia/medicina/procesarDatos/procesarMedicinaConsulta.php?tipo=delete') ?>
	<?php echo fnEliminarDatos('fnEliminarMedicinaFormulasMedicas',''.$base_url.'pages/gerontologia/medicina/procesarDatos/procesarMedicinaFormulasMedicas.php?tipo=delete') ?>
	<?php echo fnEliminarDatos('fnEliminarMedicinaRevisionFuncional',''.$base_url.'pages/gerontologia/medicina/procesarDatos/procesarMedicinaRevisionFuncional.php?tipo=delete') ?>
	<?php echo fnEliminarDatos('fnEliminarMedicinaAntecedentePersonal',''.$base_url.'pages/gerontologia/medicina/procesarDatos/procesarMedicinaAntecedentePersonal.php?tipo=delete') ?>
	<?php echo fnEliminarDatos('fnEliminarMedicinaImpresionDiagnostica',''.$base_url.'pages/gerontologia/medicina/procesarDatos/procesarMedicinaImpresionDiagnostica.php?tipo=delete') ?>
	<?php echo fnEliminarDatos('fnEliminarMedicinaExamenFisico',''.$base_url.'pages/gerontologia/medicina/procesarDatos/procesarMedicinaExamenFisico.php?tipo=delete') ?>
	<?php echo fnEliminarDatos('fnEliminarMedicinaResultadoExamenes',''.$base_url.'pages/gerontologia/medicina/procesarDatos/procesarMedicinaResultadoExamenes.php?tipo=delete') ?>
	<?php echo fnEliminarDatos('fnEliminarMedicinaOrdenesMedicas',''.$base_url.'pages/gerontologia/medicina/procesarDatos/procesarMedicinaOrdenesMedicas.php?tipo=delete') ?>
	<?php echo fnEliminarDatos('fnEliminarMedicinaSeguimiento',''.$base_url.'pages/gerontologia/medicina/procesarDatos/procesarMedicinaSeguimiento.php?tipo=delete') ?>

	<?php echo fnVerDatos('fnVerMedicinaSignosVitales','medicina/modalVer/modalVerMedicinaSignosVitales') ?>
	<?php echo fnVerDatos('fnVerMedicinaPatologias','medicina/modalVer/modalVerMedicinaPatologias') ?>
	<?php echo fnVerDatos('fnVerMedicinaConsultas','medicina/modalVer/modalVerMedicinaConsultas') ?>
	<?php echo fnVerDatos('fnVerMedicinaFormulasMedicas','medicina/modalVer/modalVerMedicinaFormulasMedicas') ?>
	<?php echo fnVerDatos('fnVerMedicinaRevisionFuncional','medicina/modalVer/modalVerMedicinaRevisionFuncional') ?>
	<?php echo fnVerDatos('fnVerMedicinaAntecedentePersonal','medicina/modalVer/modalVerMedicinaAntecedentePersonal') ?>
	<?php echo fnVerDatos('fnVerMedicinaImpresionDiagnostica','medicina/modalVer/modalVerMedicinaImpresionDiagnostica') ?>
	<?php echo fnVerDatos('fnVerMedicinaExamenFisico','medicina/modalVer/modalVerMedicinaExamenFisico') ?>
	<?php echo fnVerDatos('fnVerMedicinaSeguimientos','medicina/modalVer/modalVerMedicinaSeguimientos') ?>
	<?php echo fnVerDatos('fnVerMedicinaResultadoExamen','medicina/modalVer/modalVerMedicinaResultadoExamen') ?>
	<?php echo fnVerDatos('fnVerMedicinaOrdenMedica','medicina/modalVer/modalVerMedicinaOrdenMedica') ?>

	/*Enfermeria*/
	<?php echo fnVerDatos('fnVerEnfermeriaNotas','enfermeria/modalVer/modalVerEnfermeriaNotas') ?>
	<?php echo fnVerDatos('fnVerEnfermeriaJefe','enfermeria/modalVer/modalVerEnfermeriaJefe') ?>
	<?php echo fnVerDatos('fnVerEnfermeriaCuidados','enfermeria/modalVer/modalVerEnfermeriaCuidados') ?>
	<?php echo fnVerDatos('fnVerEnfermeriaBalanceLiquidos','enfermeria/modalVer/modalVerEnfermeriaBalanceLiquidos') ?>
	<?php echo fnVerDatos('fnVerEnfermeriaControlGlicema','enfermeria/modalVer/modalVerEnfermeriaControlGlicema') ?>
	<?php echo fnVerDatos('fnVerEnfermeriaControlSuministros','enfermeria/modalVer/modalVerEnfermeriaControlSuministros') ?>
	<?php echo fnVerDatos('fnVerEnfermeriaControlCuraciones','enfermeria/modalVer/modalVerEnfermeriaControlCuraciones') ?>
	<?php echo fnVerDatos('fnVerEnfermeriaControlSignosVitales','enfermeria/modalVer/modalVerEnfermeriaSignosVitales') ?>
	/* Fisioterapia */
	<?php echo fnVerDatos('fnVerFisioterapiaTonoMuscular','fisioterapia/modalVer/modalVerFisioterapiaTonoMuscular') ?>
	<?php echo fnVerDatos('fnVerFisioterapiaReflejosPatologicos','fisioterapia/modalVer/modalVerFisioterapiaReflejosPatologicos') ?>
	<?php echo fnVerDatos('fnVerFisioterapiaFuncionesMentales','fisioterapia/modalVer/modalVerFisioterapiaFuncionesMentales') ?>
	<?php echo fnVerDatos('fnVerFisioterapiaSensibilidad','fisioterapia/modalVer/modalVerFisioterapiaSensibilidad') ?>
	<?php echo fnVerDatos('fnVerFisioterapiaCoordinacion','fisioterapia/modalVer/modalVerFisioterapiaCoordinacion') ?>
	<?php echo fnVerDatos('fnVerFisioterapiaEquilibrio','fisioterapia/modalVer/modalVerFisioterapiaEquilibrio') ?>
	<?php echo fnVerDatos('fnVerFisioterapiaMotricidad','fisioterapia/modalVer/modalVerFisioterapiaMotricidad') ?>
	<?php echo fnVerDatos('fnVerFisioterapiaDiagnosticoFisioterapeutico','fisioterapia/modalVer/modalVerFisioterapiaDiagnosticoFisioterapeutico') ?>
	<?php echo fnVerDatos('fnVerFisioterapiaFuerzaMuscular','fisioterapia/modalVer/modalVerFisioterapiaFuerzaMuscular') ?>
	<?php echo fnVerDatos('fnVerFisioterapiaPostura','fisioterapia/modalVer/modalVerFisioterapiaPostura') ?>
	<?php echo fnVerDatos('fnVerFisioterapiaMarcha','fisioterapia/modalVer/modalVerFisioterapiaMarcha') ?>
	<?php echo fnVerDatos('fnVerFisioterapiaSeguimiento','fisioterapia/modalVer/modalVerFisioterapiaSeguimiento') ?>

	/* Gerontologia */
	<?php echo fnVerDatos('fnVerGerontologiaAbcInstrumental','gerontologia/modalVer/modalVerGerontologiaAbcInstrumental') ?>
	<?php echo fnVerDatos('fnVerGerontologiaActitudPersonal','gerontologia/modalVer/modalVerGerontologiaActitudPersonal') ?>
	<?php echo fnVerDatos('fnVerGerontologiaAditamientos','gerontologia/modalVer/modalVerGerontologiaAditamientos') ?>
	<?php echo fnVerDatos('fnVerGerontologiaActividadesRealiza','gerontologia/modalVer/modalVerGerontologiActividadesRealiza') ?>
	<?php echo fnVerDatos('fnVerGerontologiaActividadesQueRealizaria','gerontologia/modalVer/modalVerGerontologiaActividadesQueRealizaria') ?>
	<?php echo fnVerDatos('fnVerGerontologiaSeguimiento','gerontologia/modalVer/modalVerGerontologiaSeguimiento') ?>

	/* Psiquiatria*/
	<?php echo fnVerDatos('fnVerPsiquiatriaInformacion','psiquiatria/modalVer/modalVerPsiquiatriaInformacion') ?>

	/* Terapia ocupacional*/
	<?php echo fnVerDatos('fnVerTerapiaOcupacionalEvaluacion','terapiaOcupacional/modalVer/modalVerTerapiaOcupacionalEvaluacion') ?>
	<?php echo fnVerDatos('fnVerTerapiaOcupacionalSeguimiento','terapiaOcupacional/modalVer/modalVerTerapiaOcupacionalSeguimiento') ?>

	/*Trabajo social*/
	<?php echo fnVerDatos('fnVerTrabajoSocialInformacion','trabajoSocial/modalVer/modalVerTrabajoSocialInformacion') ?>
	<?php echo fnVerDatos('fnVerTrabajoSocialReingreso','trabajoSocial/modalVer/modalVerTrabajoSocialReingreso') ?>
	<?php echo fnVerDatos('fnVerTrabajoSocialVisitaDomiciliaria','trabajoSocial/modalVer/modalVerTrabajoSocialVisitaDomiciliaria') ?>
	<?php echo fnVerDatos('fnVerTrabajoSocialAspectoSocioeconomico','trabajoSocial/modalVer/modalVerTrabajoSocialAspectoSocioeconomico') ?>
	<?php echo fnVerDatos('fnVerTrabajoSocialSeguimiento','trabajoSocial/modalVer/modalVerTrabajoSocialSeguimiento') ?>
	<?php echo fnVerDatos('fnVerTrabajoSocialRedApoyoFamiliar','trabajoSocial/modalVer/modalVerTrabajoSocialRedApoyoFamiliar') ?>
	<?php echo fnVerDatos('fnVerTrabajoSocialActividadesRealiza','trabajoSocial/modalVer/modalVerTrabajoSocialActividadesRealiza') ?>

	/*Test*/
	<?php echo fnVerDatos('fnVerTestTestMinimental','test/modalVer/modalVerTestTestMinimental') ?>
	<?php echo fnVerDatos('fnVerTestTestYesavage','test/modalVer/modalVerTestTestYesavage') ?>
	<?php echo fnVerDatos('fnVerTestTestBarthel','test/modalVer/modalVerTestTestBarthel') ?>



     /* Cargamos las funciones javascript usando el metodo fnEditarDatos() de php para ahorrar codigo
     Resive como parametros el nombre de la funcion declarada en el form y el nombre de el form donde se
     imprimira la imformacin*/

     /* Nutricion */
     <?php echo fnEditarDatos('fnEditarNutricionInfo','nutricion/formEditar/formEditarNutricionInfo'); ?>
     <?php echo fnEditarDatos('fnEditarNutricionSeguimiento','nutricion/formEditar/formEditarNutricionSeguimiento'); ?>
     /* Psicologia */
     <?php echo fnEditarDatos('fnModificarPsicologiaAntecMental','psicologia/formEditar/formEditarPsicologiaAntecedenteMental'); ?>
     <?php echo fnEditarDatos('fnEditarPsicologiaAntecedenteFamiliar','psicologia/formEditar/formEditarPsicologiaAntecedenteFamiliar'); ?>
     <?php echo fnEditarDatos('fnEditarPsicologiaPatologia','psicologia/formEditar/formEditarPsicologiaPatologia'); ?>
     <?php echo fnEditarDatos('fnEditarPsicologiaRelacionesFamiliares','psicologia/formEditar/formEditarPsicologiaRelacionesFamiliares'); ?>
     <?php echo fnEditarDatos('fnEditarPsicologiaApoyoFamiliar','psicologia/formEditar/formEditarPsicologiaApoyoFamiliar'); ?>
     <?php echo fnEditarDatos('fnEditarPsicologiaRelacionesSociales','psicologia/formEditar/formEditarPsicologiaRelacionesSociales'); ?>
     <?php echo fnEditarDatos('fnEditarPsicologiaGruposSociales','psicologia/formEditar/formEditarPsicologiaGruposSociales'); ?>
     <?php echo fnEditarDatos('fnEditarPsicologiaRasgosPersonalidad','psicologia/formEditar/formEditarPsicologiaRasgosPersonalidad'); ?>
     <?php echo fnEditarDatos('fnEditarPsicologiaSintomasDepresion','psicologia/formEditar/formEditarPsicologiaSintomasDepresion'); ?>
     <?php echo fnEditarDatos('fnEditarPsicologiaSintomasAnsiedad','psicologia/formEditar/formEditarPsicologiasSintomasAnsiedad'); ?>
     <?php echo fnEditarDatos('fnEditarPsicologiaImpresionDiagnostica','psicologia/formEditar/formEditarPsicologiaImpresionDiagnostica'); ?>
     <?php echo fnEditarDatos('fnEditarPsicologiaEvaluacionMultiaxial','psicologia/formEditar/formEditarPsicologiaEvaluacionMultiaxial'); ?>
     <?php echo fnEditarDatos('fnEditarPsicologiaFamiliograma','psicologia/formEditar/formEditarPsicologiaFamiliograma'); ?>
     <?php echo fnEditarDatos('fnEditarPsicologiaPlanTratamientos','psicologia/formEditar/formEditarPsicologiaPlanTratamientos'); ?>
     <?php echo fnEditarDatos('fnEditarPsicologiaSeguimientos','psicologia/formEditar/formEditarPsicologiaSeguimientos'); ?>

     /* Medicina */
     <?php echo fnEditarDatos('fnEditarMedicinaSignosVitales','medicina/formEditar/formEditarMedicinaSignosVitales'); ?>
     <?php echo fnEditarDatos('fnEditarMedicinaPatologias','medicina/formEditar/formEditarMedicinaPatologias'); ?>
     <?php echo fnEditarDatos('fnEditarMedicinaConsultas','medicina/formEditar/formEditarMedicinaConsultas'); ?>
     <?php echo fnEditarDatos('fnEditarMedicinaFormulasMedicas','medicina/formEditar/formEditarMedicinaFormulasMedicas'); ?>
     <?php echo fnEditarDatos('fnEditarMedicinaRevisionFuncional','medicina/formEditar/formEditarMedicinaRevisionFuncional'); ?>
     <?php echo fnEditarDatos('fnEditarMedicinaAntecedentePersonal','medicina/formEditar/formEditarMedicinaAntecedentePersonal'); ?>
     <?php echo fnEditarDatos('fnEditarMedicinaImpresionDiagnostica','medicina/formEditar/formEditarMedicinaImpresionDiagnostica'); ?>
     <?php echo fnEditarDatos('fnEditarMedicinaExamenFisico','medicina/formEditar/formEditarMedicinaExamenFisico'); ?>
     <?php echo fnEditarDatos('fnEditarMedicinaSeguimientos','medicina/formEditar/formEditarMedicinaSeguimientos'); ?>
     <?php echo fnEditarDatos('fnEditarMedicinaResultadoExamen','medicina/formEditar/formEditarMedicinaResultadoExamen'); ?>
     <?php echo fnEditarDatos('fnEditarMedicinaOrdenMedica','medicina/formEditar/formEditarMedicinaOrdenMedica'); ?>

     /*Enfermeria*/
     <?php echo fnEditarDatos('fnEditarEnfermeriaNotas','enfermeria/formEditar/formEditarEnfermeriaNotas'); ?>
     <?php echo fnEditarDatos('fnEditarEnfermeriaJefe','enfermeria/formEditar/formEditarEnfermeriaJefe'); ?>
     <?php echo fnEditarDatos('fnEditarEnfermeriaCuidados','enfermeria/formEditar/formEditarEnfermeriaCuidados'); ?>
     <?php echo fnEditarDatos('fnEditarEnfermeriaBalanceLiquidos','enfermeria/formEditar/formEditarEnfermeriaBalanceLiquidos'); ?>
     <?php echo fnEditarDatos('fnEditarEnfermeriaControlGlicema','enfermeria/formEditar/formEditarEnfermeriaControlGlicema'); ?>
     <?php echo fnEditarDatos('fnEditarEnfermeriaControlSuministros','enfermeria/formEditar/formEditarEnfermeriaControlSuministros'); ?>
     <?php echo fnEditarDatos('fnEditarEnfermeriaControlCuraciones','enfermeria/formEditar/formEditarEnfermeriaControlCuraciones'); ?>
     <?php echo fnEditarDatos('fnEditarEnfermeriaSignosVitales','enfermeria/formEditar/formEditarEnfermeriaSignosVitales'); ?>

     /* Fisioterapia */ 
     <?php echo fnEditarDatos('fnEditarFisioterapiaTonoMuscular','fisioterapia/formEditar/formEditarFisioterapiaTonoMuscular'); ?>
     <?php echo fnEditarDatos('fnEditarFisioterapiaReflejosPatologicos','fisioterapia/formEditar/formEditarFisioterapiaReflejosPatologicos'); ?>
     <?php echo fnEditarDatos('fnEditarFisioterapiaFuncionesMentales','fisioterapia/formEditar/formEditarFisioterapiaFuncionesMentales'); ?>
     <?php echo fnEditarDatos('fnEditarFisioterapiaSensibilidad','fisioterapia/formEditar/formEditarFisioterapiaSensibilidad'); ?>
     <?php echo fnEditarDatos('fnEditarFisioterapiaCoordinacion','fisioterapia/formEditar/formEditarFisioterapiaCoordinacion'); ?>
     <?php echo fnEditarDatos('fnEditarFisioterapiaEquilibrio','fisioterapia/formEditar/formEditarFisioterapiaEquilibrio'); ?>
     <?php echo fnEditarDatos('fnEditarFisioterapiaMotricidad','fisioterapia/formEditar/formEditarFisioterapiaMotricidad'); ?>
     <?php echo fnEditarDatos('fnEditarFisioterapiaDiagnosticoFisioterapeutico','fisioterapia/formEditar/formEditarFisioterapiaDiagnosticoFisioterapeutico'); ?>
     <?php echo fnEditarDatos('fnEditarFisioterapiaFuerzaMuscular','fisioterapia/formEditar/formEditarFisioterapiaFuerzaMuscular'); ?>
     <?php echo fnEditarDatos('fnEditarFisioterapiaPostura','fisioterapia/formEditar/formEditarFisioterapiaPostura'); ?>
     <?php echo fnEditarDatos('fnEditarFisioterapiaMarcha','fisioterapia/formEditar/formEditarFisioterapiaMarcha'); ?>
     <?php echo fnEditarDatos('fnEditarFisioterapiaSeguimiento','fisioterapia/formEditar/formEditarFisioterapiaSeguimiento'); ?>

     /* Gerontologia*/
     <?php echo fnEditarDatos('fnEditarGerontologiaAbcIntrumental','gerontologia/formEditar/formEditarGerontologiaAbcIntrumental'); ?>
     <?php echo fnEditarDatos('fnEditarGerontologiaActitudPersonal','gerontologia/formEditar/formEditarGerontologiaActitudPersonal'); ?>
     <?php echo fnEditarDatos('fnEditarGerontologiaAditamiento','gerontologia/formEditar/formEditarGerontologiaAditamiento'); ?>
     <?php echo fnEditarDatos('fnEditarGerontologiaActividadesRealiza','gerontologia/formEditar/formEditarGerontologiaActividadesRealiza'); ?>
     <?php echo fnEditarDatos('fnEditarGerontologiaActividadesQueRealizaria','gerontologia/formEditar/formEditarGerontologiaActividadesQueRealizaria'); ?>
     <?php echo fnEditarDatos('fnEditarGerontologiaSeguimiento','psiquiatria/formEditar/formEditarGerontologiaSeguimiento'); ?>

     /* Psiquiatria*/
     <?php echo fnEditarDatos('fnEditarPsiquiatriaInformacion','psiquiatria/formEditar/formEditarPsiquiatriaInformacion'); ?>

     /*Terapia Ocupacional*/
     <?php echo fnEditarDatos('fnEditarTerapiaOcupacionalEvaluacion','terapiaOcupacional/formEditar/formEditarTerapiaOcupacionalEvaluacion'); ?>
     <?php echo fnEditarDatos('fnEditarTerapiaOcupacionalSeguimiento','terapiaOcupacional/formEditar/formEditarTerapiaOcupacionalSeguimiento'); ?>

     /*Trabajo Social*/
     <?php echo fnEditarDatos('fnEditarTrabajoSocialInformacion','trabajoSocial/formEditar/formEditarTrabajoSocialInformacion'); ?>	 
     <?php echo fnEditarDatos('fnEditarTrabajoSocialReingreso','trabajoSocial/formEditar/formEditarTrabajoSocialReingreso'); ?>	 
     <?php echo fnEditarDatos('fnEditarTrabajoSocialVisitaDomiciliaria','trabajoSocial/formEditar/formEditarTrabajoSocialVisitaDomiciliaria'); ?>	 
     <?php echo fnEditarDatos('fnEditarTrabajoSocialAspectoSocioeconomico','trabajoSocial/formEditar/formEditarTrabajoSocialAspectoSocioeconomico'); ?>	 
     <?php echo fnEditarDatos('fnEditarTrabajoSocialSeguimiento','trabajoSocial/formEditar/formEditarTrabajoSocialSeguimiento'); ?>	 
     <?php echo fnEditarDatos('fnEditarTrabajoSocialRedApoyoFamiliar','trabajoSocial/formEditar/formEditarTrabajoSocialRedApoyoFamiliar'); ?>	 
     <?php echo fnEditarDatos('fnEditarTrabajoSocialActividadesRealiza','trabajoSocial/formEditar/formEditarTrabajoSocialActividadesRealiza'); ?>

     /*Test*/	 
     <?php echo fnEditarDatos('fnEditarTesTestMinimental','test/formEditar/formEditarTesTestMinimental'); ?>	 
     <?php echo fnEditarDatos('fnEditarTesTestYesavage','test/formEditar/formEditarTestTestYesavage'); ?>	 
     <?php echo fnEditarDatos('fnEditarTesTestBarthel','test/formEditar/formEditarTestTestBarthel'); ?>	 





 </script>
 <!-- VALIDACIONES DE LOS FORMULARIOS JAVASCRIPT CREAR -->
 <script type="text/javascript" defer>

 	$('#registrarInformacionNutricional').valida();
 	$('#registrarSeguimientoNutricional').valida();

 	$('#registrarPsicologiaAntecedenteMental').valida();
 	$('#registrarPsicologiaAntecedenteFamiliar').valida();
 	$('#registrarPsicologiaRelacionesFamiliares').valida();
 	$('#registrarPsicologiaRelacionesSociales').valida();
 	$('#registrarApoyoFamiliar').valida();
 	$('#modalNuevoGrupoSocial').valida();
 	$('#registrarRasgosPersonales').valida();
 	$('#registrarSintomasAnsiedad').valida();
 	$('#registrarImpresionDiagnostica').valida();
 	$('#registrarEvaluacionMultiaxial').valida();
 	$('#registrarFamiliograma').valida();
 	$('#registrarPlanTratamiento').valida();
 	$('#registrarSeguimiento').valida();

 	$('#registrarMedSignosVitales').valida();
 	$('#formularioMedicinaPatologia').valida();
 	$('#formularioMedicinaConsulta').valida();
 	$('#modalNuevoMedFormulasMedicas').valida();
 	$('#registrarMedRevisionFuncional').valida();
 	$('#registrarMedAntecedentePersonal').valida();
 	$('#registrarMedImpresionDiagnostica').valida();
 	$('#registrarMedExamenfisico').valida();
 	$('#registrarMedSeguimientos').valida();
 	$('#registrarMedOrdenMedica').valida();

 	$('#registrarEnfermeriaNotas').valida();
 	$('#registrarEnfermeriaJefe').valida();
 	$('#registrarEnfermeriaBalanceLiquidos').valida();
 	$('#registrarEnfermeriaControlGlicemia').valida();
 	$('#registrarEnfermeriaControlSuministros').valida();
 	$('#registrarEnfermeriaControlCuraciones').valida();
 	$('#registrarEnfermeriaSignosVitales').valida();

 	$('#registrarFisioterapiaTonoMuscular').valida();
 	$('#registrarFisioterapiaReflejoPatologico').valida();
 	$('#registrarFisioterapiaFuncionMental').valida();
 	$('#registrarFisioterapiaSensibilidad').valida();
 	$('#registrarFisioterapiaCoordinacion').valida();
 	$('#registrarFisioterapiaEquilibrio').valida();
 	$('#registrarFisioterapiaMotricidad').valida();
 	$('#registrarFisioterapiaDiagnosticoFisioterapeutico').valida();
 	$('#registrarFisioterapiaFuerzaMuscular').valida();
 	$('#registrarFisioterapiaPostura').valida();
 	$('#registrarFisioterapiaMarcha').valida();
 	$('#registrarFisioterapiaSeguimiento').valida();

 	$('#registrarGerontologiaAbcInstrumental').valida();
 	$('#registrarGerontologiActitudPersonal').valida();
 	$('#registrarGerontologiAditamentos').valida();
 	$('#registrarGerontologiActividadRealiza').valida();
 	$('#registrarGerontologiSeguimiento').valida();
 	$('#registrarGerontologiaCalidadVida').valida();

 	/*PSIQUIATRIA*/
 	$('#registrarPsiquiatriaInformacion').valida();
 	$('#registrarTerapiaOcupacionalSeguimiento').valida();
 	/**TRABAJO SOCIAL**/
 	$('#registrarTrabajoSocialIformacion').valida();
 	$('#registrarTrabajoSocialReingreso').valida();
 	$('#registrarTrabajoSocialVisitaDomiciliaria').valida();
 	$('#registrarTrabajoSocialAspectoSocioeconomico').valida();
 	$('#registrarTrabajoSocialSeguimiento').valida();
 	$('#registrarTrabajoSocialRedAopoyoFamiliar').valida();
 	/*TEST*/
 	$('#registrarTestTestMinimental').valida();
 	$('#registrarTestTestYesavage').valida();
 	$('#registrarTestTestBarthel').valida();

 </script>

 <?php

 if (isset($_REQUEST["param"])) {
 	echo "<script>";
 	echo "recargarDatosPaciente('" . $_REQUEST["param"] . "');";
 	echo "</script>";
 }
 ?>
</body>
</html>
