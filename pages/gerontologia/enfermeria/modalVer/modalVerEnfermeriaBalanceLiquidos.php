<?php

//include connection file 
require "../../../../session.php";

$sqlUsuario = "
SELECT 
(CONVERT(CAST(CONVERT(CONCAT(UPPER(c.primer_nombre),' ',UPPER(c.segundo_nombre),' ',UPPER(c.primer_apellido),' ',UPPER(c.segundo_apellido)) USING latin1) AS BINARY) USING utf8)) AS nombreUsuario,
CONCAT(bl.ano_mod,'/',bl.mes_mod,'/',bl.dia_mod) AS fecha,
bl.hora_mod,
(CONVERT(CAST(CONVERT(bl.cantidad_indicada USING latin1) AS BINARY) USING utf8))AS cantidad_indicada, 
bl.cantidad_administrar,
bl.cantidad_recibida,
bl.total1,
bl.eliminados,
bl.total2
FROM gddt_balance_liquidos bl
INNER JOIN gddt_cuentas c ON c.id_cuenta = bl.id_cuenta
WHERE bl.id_balance_liquido = " . $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);
$rows = mysqli_num_rows($queryUsuario);

if($rows == 0){
    echo "<p style='color:red;font-size:0.9em;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No se encontraron datos :(</p>";
}else{


?>

<div class="content">
<h6 class="text-center mb-4">Actualizado por <?= $fetchUsuario[0];?> el <?= $fetchUsuario[1]; ?> a las <?= $fetchUsuario[2]; ?></h6>
 <div class="user-block col-sm-4 col-xs-6">
    <label>
        Cantidad indicada
    </label>
    <p><?php if(empty($fetchUsuario[3])){echo "Sin dato";}else{echo $fetchUsuario[3];} ?></p>
</div>
 <div class="user-block col-sm-4 col-xs-6">
    <label>
        Cantidad por administrar
    </label>
    <p><?php if(empty($fetchUsuario[4])){echo "Sin dato";}else{echo $fetchUsuario[4];} ?></p>
</div>
 <div class="user-block col-sm-4 col-xs-6">
    <label>
        Via de administracion
    </label>
    <p><?php if(empty($fetchUsuario[5])){echo "Sin dato";}else{echo $fetchUsuario[5];} ?></p>
</div>
 <div class="user-block col-sm-4 col-xs-6">
    <label>
        Total
    </label>
    <p><?php if(empty($fetchUsuario[6])){echo "Sin dato";}else{echo $fetchUsuario[6];} ?></p>
</div>
 <div class="user-block col-sm-4 col-xs-6">
    <label>
        Eliminados
    </label>
    <p><?php if(empty($fetchUsuario[7])){echo "Sin dato";}else{echo $fetchUsuario[7];} ?></p>
</div>
 <div class="user-block col-sm-4 col-xs-6">
    <label>
        Total
    </label>
    <p><?php if(empty($fetchUsuario[8])){echo "Sin dato";}else{echo $fetchUsuario[8];} ?></p>
</div>

<div class="user-block">&nbsp;</div>

</div><!-- Fin del content -->
<?php } ?>