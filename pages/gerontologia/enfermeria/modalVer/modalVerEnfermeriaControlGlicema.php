<?php

//include connection file 
require "../../../../session.php";

$sqlUsuario = "
SELECT 
(CONVERT(CAST(CONVERT(CONCAT(UPPER(c.primer_nombre),' ',UPPER(c.segundo_nombre),' ',UPPER(c.primer_apellido),' ',UPPER(c.segundo_apellido)) USING latin1) AS BINARY) USING utf8)) AS nombreUsuario,
CONCAT(cg.ano_mod,'/',cg.mes_mod,'/',cg.dia_mod) AS fecha, 
cg.hora_mod,
cg.hora_crontrol,
cg.novedad,
cg.valor
FROM gddt_control_glicemia cg
INNER JOIN gddt_cuentas c ON c.id_cuenta = cg.id_cuenta 
WHERE cg.id_control_glicemia = " . $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);
$rows = mysqli_num_rows($queryUsuario);

if($rows == 0){
    echo "<p style='color:red;font-size:0.9em;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No se encontraron datos :(</p>";
}else{


    ?>

    <div class="content">
        <h6 class="text-center mb-4">Actualizado por <?= $fetchUsuario[0];?> el <?= $fetchUsuario[1]; ?> a las <?= $fetchUsuario[2]; ?></h6>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Hora control
            </label>
            <p><?php if(empty($fetchUsuario[3])){echo "Sin dato";}else{echo $fetchUsuario[3];} ?></p>
        </div>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Sintomatología
            </label>
            <p><?php if(empty($fetchUsuario[4])){echo "Sin dato";}else{echo $fetchUsuario[4];} ?></p>
        </div>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Valor
            </label>
            <p>
                <?php 
                if(empty($fetchUsuario[5])){
                    echo "Sin dato";
                }else{
                    if (($fetchUsuario[5]>= 1)&&($fetchUsuario[5] <= 100)){$tor ='Bajo';}
                    if (($fetchUsuario[5]>= 100)&&($fetchUsuario[5] <= 299)){$tor ='Medio';}
                    if (($fetchUsuario[5]>= 300)&&($fetchUsuario[5] <= 400)){$tor ='Alto';}
                    echo $fetchUsuario[5] .' - '. $tor;
                } 
                ?>

            </p>
        </div>


        <div class="user-block">&nbsp;</div>

    </div><!-- Fin del content -->
    <?php } ?>