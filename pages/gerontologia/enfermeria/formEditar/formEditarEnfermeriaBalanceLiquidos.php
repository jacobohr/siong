<?php require "../../../../session.php";



$sqlUsuario = "SELECT 
*,
(CONVERT(CAST(CONVERT(cantidad_indicada USING latin1) AS BINARY) USING utf8))AS cantidad_indicada 
 FROM gddt_balance_liquidos WHERE id_balance_liquido = ". $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);

?>
<form name="editarUsuario" method="POST" action="procesarPsicologiaAntceMental.php?tipo=update" enctype="multipart/form-data">
	<h4 class="text-center mb-4"> Balance de liquidos</h4>
	<div class="row">
		<div class="form-group col-md-4">
			<label for="recipient-name" class="form-control-label">Usuario que registra <i style="color: darkorange">*</i></label>
			<select class="form-control" name="usuario" >
				<option>Selecciona una opción</option>
				<?php
				$queryCuenta = mysqli_query($conn, "SELECT id_cuenta,
					UPPER((CONVERT(CAST(CONVERT(CONCAT(primer_nombre,' ',segundo_nombre,' ',primer_apellido,' ',segundo_apellido) USING latin1) AS BINARY) USING utf8))) AS nombreUsuario FROM gddt_cuentas ;");
				while ($fetchCuenta = mysqli_fetch_row($queryCuenta)) {
					if ($fetchCuenta[0] == $fetchUsuario[1]) {
						echo "<option selected value=" . $fetchCuenta[0] . ">" . $fetchCuenta[1] . "</option>";
					} else {
						echo "<option value=" . $fetchCuenta[0] . ">" . $fetchCuenta[1] . "</option>";
					}
				}
				?>
			</select>
		</div>
		<?php 
		if(strlen($fetchUsuario[3]) < 2){$mes = '0'.$fetchUsuario[3];}else{$mes = $fetchUsuario[3];}
		if(strlen($fetchUsuario[4]) < 2){$dia = '0'.$fetchUsuario[4];}else{$dia = $fetchUsuario[4];}
		$year = $fetchUsuario[2];
		$fecha = $year.'-'.$mes.'-'.$dia;
		?>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Fecha de modificacion <i style="color: darkorange">*</i></label>
			<input type="date" class="form-control" name="fechaMod" value="<?= $fecha ?>"/>
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Cantidad indicada <i style="color: darkorange">*</i></label>
			<input type="text" class="form-control" name="cantidadIndicada" value="<?= $fetchUsuario[13] ?>"/>
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Cantidad por administrar <i style="color: darkorange">*</i></label>
			<input type="text" class="form-control" name="cantidadIndicada" value="<?= $fetchUsuario[8] ?>"/>
		</div>
		<div class="form-group col-md-4">
			<label for="recipient-name" class="form-control-label">Via de administracion <i style="color: darkorange">*</i></label>
			<?php 
			$selectViaAdministracion = array(
				0 => 'Sonda',
				1 => 'Oral',
				2 => 'Parenteral',
				3 => 'total'
			);
			?>
			<select class="form-control" name="tipo" >
				<option>Selecciona una opción</option>
				<?php
				foreach ($selectViaAdministracion as $key) {
					if($key == $fetchUsuario[9]){
						echo '<option selected value='.$key.'>'.$key.'</option>';
					}else{
						echo '<option value='.$key.'>'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Total <i style="color: darkorange">*</i></label>
			<input type="text" class="form-control" name="total1" value="<?= $fetchUsuario[10] ?>"/>
		</div>
		<div class="form-group col-md-4">
			<label for="recipient-name" class="form-control-label">Eliminados <i style="color: darkorange">*</i></label>
			<?php 
			$selectEliminados = array(
				0 => 'Materia Fecal',
				1 => 'Orina',
				2 => 'Vómito',
				3 => 'total',
				4 => 'Drenajes'

			);
			?>
			<select class="form-control" name="tipo" >
				<option>Selecciona una opción</option>
				<?php
				foreach ($selectEliminados as $key) {
					if($key == $fetchUsuario[11]){
						echo '<option selected value='.$key.'>'.$key.'</option>';
					}else{
						echo '<option value='.$key.'>'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Total <i style="color: darkorange">*</i></label>
			<input type="text" class="form-control" name="total2" value="<?= $fetchUsuario[12] ?>"/>
		</div>
	</div>
	<!-- End Row -->


	<!-- Modal Footer -->
	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">
			Cancelar
		</button>
		<button type="submit" class="btn btn-primary" name="Actualizar">
			Actualizar
		</button>
	</div>
