<?php require "../../../../session.php";



$sqlUsuario = "SELECT *,(CONVERT(CAST(CONVERT(descripcion USING latin1) AS BINARY) USING utf8))AS seguimiento_descripcion FROM gddt_notas_enfermeria WHERE id_nota_enfermeria = ". $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);

?>
<form name="editarUsuario" method="POST" action="procesarPsicologiaAntceMental.php?tipo=update" enctype="multipart/form-data">
	<h4 class="text-center mb-4"> Notas de enfermeria</h4>
	<div class="row">
		<div class="form-group col-md-4">
			<label for="recipient-name" class="form-control-label">Usuario que registra <i style="color: darkorange">*</i></label>
			<select class="form-control" name="usuario" >
				<option>Selecciona una opción</option>
				<?php
				$queryCuenta = mysqli_query($conn, "SELECT id_cuenta,
					UPPER((CONVERT(CAST(CONVERT(CONCAT(primer_nombre,' ',segundo_nombre,' ',primer_apellido,' ',segundo_apellido) USING latin1) AS BINARY) USING utf8))) AS nombreUsuario FROM gddt_cuentas ;");
				while ($fetchCuenta = mysqli_fetch_row($queryCuenta)) {
					if ($fetchCuenta[0] == $fetchUsuario[1]) {
						echo "<option selected value=" . $fetchCuenta[0] . ">" . $fetchCuenta[1] . "</option>";
					} else {
						echo "<option value=" . $fetchCuenta[0] . ">" . $fetchCuenta[1] . "</option>";
					}
				}
				?>
			</select>
		</div>
		<?php 
		if(strlen($fetchUsuario[3]) < 2){$mes = '0'.$fetchUsuario[3];}else{$mes = $fetchUsuario[3];}
		if(strlen($fetchUsuario[4]) < 2){$dia = '0'.$fetchUsuario[4];}else{$dia = $fetchUsuario[4];}
		$year = $fetchUsuario[2];
		$fecha = $year.'-'.$mes.'-'.$dia;
		?>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Fecha de modificacion <i style="color: darkorange">*</i></label>
			<input type="date" class="form-control" name="fechaMod" value="<?= $fecha ?>"/>
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Tipo </label>
			<select class="form-control" name="tipo" >
				<option value="">Seleccione el tipo</option>
				<option value="Evaluacion de ingreso">Evaluacion de ingreso</option>
				<option value="Cita con medico General">Cita con medico General</option>
				<option value="Cita con Especialista">Cita con Especialista</option>
				<option value="Cirugia ">Cirugia</option>
				<option value="Remision a Urgencia">Remision a Urgencia</option>
				<option value="Control Hipertension con Medico">Control Hipertension con Medico</option>
				<option value="Control Hipertension con Enfermera">Control Hipertension con Enfermera</option>
				<option value="Control Diabetes">Control Diabetes</option>     
				<option value="Evolucion Auxiliares de Enfermeria">Evolucion Auxiliares de Enfermeria</option>
				<option value="Registro de egreso">Registro de egreso</option>
				<option value="Nota usuario hospitalizado">Nota usuario hospitalizado</option>
				<option value="Nota usuarios que regresa de hospitalizacion">Nota usuarios que regresa de hospitalizacion</option>
				<option value="Remision a ayudas diagnosticas">Remision a ayudas diagnosticas</option>
				<option value="Remision examenes laboratorio ">Remision Examenes de Laboratorio</option>
				<option value="Nota Egreso">Nota de Egreso</option>
				<option value="Nota Aclaratoria">Nota Aclaratoria</option>
				<option value="Evento adversos" >Evento Adverso</option>
			</select>
		</div>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label"> Descripcion <i style="color: darkorange">*</i></label>
			<textarea class="form-control" name="descripcionNota" cols="40" rows="5" ><?= $fetchUsuario[9] ?></textarea>
		</div>
	</div>
	<!-- End Row -->


	<!-- Modal Footer -->
	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">
			Cancelar
		</button>
		<button type="submit" class="btn btn-primary" name="Actualizar">
			Actualizar
		</button>
	</div>
