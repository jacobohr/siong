<?php require "../../../../session.php";



$sqlUsuario = "SELECT 
*,
(CONVERT(CAST(CONVERT(caracteristica USING latin1) AS BINARY) USING utf8))AS caracteristica 
FROM gddt_control_curaciones 
WHERE id_control_curacion = ". $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);

?>
<form name="editarUsuario" method="POST" action="procesarPsicologiaAntceMental.php?tipo=update" enctype="multipart/form-data">
	<h4 class="text-center mb-4"> Control de curaciones y procedimientos</h4>
	<div class="row">
		<div class="form-group col-md-4">
			<label for="recipient-name" class="form-control-label">Usuario que registra <i style="color: darkorange">*</i></label>
			<select class="form-control" name="usuario" >
				<option>Selecciona una opción</option>
				<?php
				$queryCuenta = mysqli_query($conn, "SELECT id_cuenta,
					UPPER((CONVERT(CAST(CONVERT(CONCAT(primer_nombre,' ',segundo_nombre,' ',primer_apellido,' ',segundo_apellido) USING latin1) AS BINARY) USING utf8))) AS nombreUsuario FROM gddt_cuentas ;");
				while ($fetchCuenta = mysqli_fetch_row($queryCuenta)) {
					if ($fetchCuenta[0] == $fetchUsuario[1]) {
						echo "<option selected value=" . $fetchCuenta[0] . ">" . $fetchCuenta[1] . "</option>";
					} else {
						echo "<option value=" . $fetchCuenta[0] . ">" . $fetchCuenta[1] . "</option>";
					}
				}
				?>
			</select>
		</div>
		<?php 
		if(strlen($fetchUsuario[3]) < 2){$mes = '0'.$fetchUsuario[3];}else{$mes = $fetchUsuario[3];}
		if(strlen($fetchUsuario[4]) < 2){$dia = '0'.$fetchUsuario[4];}else{$dia = $fetchUsuario[4];}
		$year = $fetchUsuario[2];
		$fecha = $year.'-'.$mes.'-'.$dia;
		?>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Fecha de modificacion <i style="color: darkorange">*</i></label>
			<input type="date" class="form-control" name="fechaMod" value="<?= $fecha ?>"/>
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Lugar anatomico <i style="color: darkorange">*</i></label>
			<input type="text" class="form-control" name="lugarAnatomico" value="<?= $fetchUsuario[7] ?>"/>
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label"> Profundidad de tamaño<i style="color: darkorange">*</i></label>
			<input type="text" class="form-control" name="profundidadTamano" value="<?= $fetchUsuario[8] ?>"/>
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Tipo de lesion <i style="color: darkorange">*</i></label>
			<input type="text" class="form-control" name="tipoLesion" value="<?= $fetchUsuario[9] ?>"/>
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Bordes <i style="color: darkorange">*</i></label>
			<input type="text" class="form-control" name="bordes" value="<?= $fetchUsuario[10] ?>"/>
		</div>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label">Caracteristicas <i style="color: darkorange">*</i></label>
			<textarea class="form-control" name="caracteristicas" cols="40" rows="5" ><?= $fetchUsuario[12] ?></textarea>
		</div>
		
	</div>
	<!-- End Row -->


	<!-- Modal Footer -->
	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">
			Cancelar
		</button>
		<button type="submit" class="btn btn-primary" name="Actualizar">
			Actualizar
		</button>
	</div>
