<?php require "../../../../session.php";



$sqlUsuario = "SELECT *,(CONVERT(CAST(CONVERT(observaciones_personalidad USING latin1) AS BINARY) USING utf8))AS observacion FROM gddt_rasgos_personalidades WHERE id_rasgos_personalidad =  ". $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);

?>
<form id="registroPsicologiaRasgosPersonalesEditar" method="POST" action="<?php echo $base_url ?>pages/gerontologia/psicologia/procesarDatos/procesarPsicologiaRasgosPersonalidad.php?tipo=update&idRegistro=<?= $_REQUEST["idu"] ?>&idMatriculaDato=<?= $fetchUsuario[6] ?>" autocomplete="off">
	<h4 class="text-center mb-4"> Rasgos de la personalidad</h4>
	<div class="row">
		<?php 
		if(strlen($fetchUsuario[3]) < 2){$mes = '0'.$fetchUsuario[3];}else{$mes = $fetchUsuario[3];}
		if(strlen($fetchUsuario[4]) < 2){$dia = '0'.$fetchUsuario[4];}else{$dia = $fetchUsuario[4];}
		$year = $fetchUsuario[2];
		$fecha = $year.'-'.$mes.'-'.$dia;
		?>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Fecha<i style="color: darkorange">*</i></label>
			<input type="date" class="form-control" name="fecha" value="<?= $fecha ?>" required="true"/>
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Personalidad <i style="color: darkorange">*</i></label>
			<select class="form-control" name="personalidad" id="personalidadEditar" required="true">
				<option value="0">Selecciona una opción</option>
				<?php $arrayPersonalidad = array(
					0 => 'Autoagresivo',
					1 => 'Colérico',
					2 => 'Defensivo',
					3 => 'Maduro',
					4 => 'Pasivo'
				);
				foreach ($arrayPersonalidad as $key) {
					if($key == $fetchUsuario[7]){
						echo '<option selected value="'.$key.'">'.$key.'</option>';
					}else{
						echo '<option value="'.$key.'">'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Lenguaje <i style="color: darkorange">*</i></label>
			<select class="form-control" name="lenguaje" id="lenguajeEditar" required="true">
				<option value="0">Selecciona una opción</option>
				<?php $arrayLenguaje = array(
					0 => 'Ausente',
					1 => 'Inconexo',
					2 => 'Inintelegible',
					3 => 'Normal',
					4 => 'Fluido',
					5 => 'Lento',
					6 => 'Perseverante',
					7 => 'Verborreico'
				);
				foreach ($arrayLenguaje as $key) {
					if($key == $fetchUsuario[8]){
						echo '<option selected value="'.$key.'">'.$key.'</option>';
					}else{
						echo '<option value="'.$key.'">'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Principio de la realidad <i style="color: darkorange">*</i></label>
			<select class="form-control" name="principioRealidad" id="principioRealidadEditar" required="true"> 
				<option value="0">Selecciona una opción</option>
				<?php $arrayPrincipioRealidad = array(
					0 => 'Racional',
					1 => 'Irracional'
				);
				foreach ($arrayPrincipioRealidad as $key) {
					if($key == $fetchUsuario[9]){
						echo '<option selected value="'.$key.'">'.$key.'</option>';
					}else{
						echo '<option value="'.$key.'">'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label"> Afectividad <i style="color: darkorange">*</i></label>
			<select class="form-control" name="afectividad" id="afectividadEditar" required="true">
				<option value="0">Selecciona una opción</option>
				<?php $arrayAfectividad = array(
					0 => 'Equilibrada',
					1 => 'Eufórica',
					2 => 'Irritable',
					3 => 'Ambivalente',
					4 => 'Depresiva',
					5 => 'Tensionada',
					6 => 'Angustiada'
				);
				foreach ($arrayAfectividad as $key) {
					if($key == $fetchUsuario[10]){
						echo '<option selected value="'.$key.'">'.$key.'</option>';
					}else{
						echo '<option value="'.$key.'">'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label"> Autovaloracion <i style="color: darkorange">*</i></label>
			<select class="form-control" name="autovaloracion" id="autovaloracionEditar" required="true">
				<option value="0">Selecciona una opción</option>
				<?php $arrayAutovaloracion = array(
					0 => 'Se quiere a si mismo',
					1 => 'Se acepta parcialmente',
					2 => 'No se valora a si mismo',
					3 => 'Ambivalente'
				);
				foreach ($arrayAutovaloracion as $key) {
					if($key == $fetchUsuario[11]){
						echo '<option selected value="'.$key.'">'.$key.'</option>';
					}else{
						echo '<option value="'.$key.'">'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label"> Autopercepcion salud <i style="color: darkorange">*</i></label>
			<select class="form-control" name="autopercepcion" id="autopercepcionSaludEditar" required="true">
				<option value="0">Selecciona una opción</option>
				<?php $arrayAutopercepcionSaludEditar = array(
					0 => 'Muy saludable',
					1 => 'Saludable',
					2 => 'Regular',
					3 => 'Mal'
				);
				foreach ($arrayAutopercepcionSaludEditar as $key) {
					if($key == $fetchUsuario[12]){
						echo '<option selected value="'.$key.'">'.$key.'</option>';
					}else{
						echo '<option value="'.$key.'">'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label"> Observaciones <i style="color: darkorange">*</i></label>
			<textarea class="form-control" name="observacionesPersonalidad" cols="40" rows="5" required="true"><?= $fetchUsuario[14] ?></textarea>
		</div>
	</div>
	<!-- End Row -->

	<!-- Modal Footer -->
	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">
			Cancelar
		</button>
		<button type="submit" class="btn btn-primary" name="Actualizar">
			Actualizar
		</button>
	</div>
</form>