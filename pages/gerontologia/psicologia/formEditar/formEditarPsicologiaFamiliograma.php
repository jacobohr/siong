<?php require "../../../../session.php";



$sqlUsuario = "SELECT * FROM gddt_familiograma WHERE id_familiograma = ". $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);

?>
<form id="registroPsicologiaFamiliogramaEditar" method="POST" action="<?php echo $base_url ?>pages/gerontologia/psicologia/procesarDatos/procesarPsicologiaFamiliograma.php?tipo=update&idRegistro=<?= $_REQUEST["idu"] ?>&idMatriculaDato=<?= $fetchUsuario[6] ?>" autocomplete="off" enctype="multipart/form-data">
	<h4 class="text-center mb-4"> Familiograma</h4>
	<div class="row">
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label">Observaciones <i style="color: darkorange">*</i></label>
			<textarea class="form-control" name="observacion" cols="40" rows="5" required><?= $fetchUsuario[7] ?></textarea>
		</div>
		<div class="form-group col-md-12">
			<p>
				Sube un archivo:
				<input type="file" class="form-control-file" name="fotoFamiliograma" id="fotoFamiliogramaEditar" accept="image/jpg"/>
			</p>
		</div>
	</div>
	<!-- End Row -->
	<!-- Modal Footer -->
	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">
			Cancelar
		</button>
		<button type="submit" class="btn btn-primary" name="Actualizar">
			Actualizar
		</button>
	</div>
</form>