<?php require "../../../../session.php";



$sqlUsuario = "SELECT * FROM gddt_patologias_usuarios_psicologia WHERE id_patologia_usuario =  ". $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);

?>
<form id="formularioMedicinaPatologiaEditar" method="POST" action="<?php echo $base_url ?>pages/gerontologia/psicologia/procesarDatos/procesarPsicologiaPatologia.php?tipo=update&idRegistro=<?= $_REQUEST["idu"] ?>&idMatriculaDato=<?= $fetchUsuario[1] ?>" autocomplete="off"> 
    <div class="row">
        <?php 
        if(strlen($fetchUsuario[4]) < 2){$mes = '0'.$fetchUsuario[4];}else{$mes = $fetchUsuario[4];}
        if(strlen($fetchUsuario[5]) < 2){$dia = '0'.$fetchUsuario[5];}else{$dia = $fetchUsuario[5];}
        $year = $fetchUsuario[3];
        $fecha = $year.'-'.$mes.'-'.$dia;
        ?>
        <div class="form-group col-md-6">
            <label for="message-text" class="form-control-label">Fecha<i style="color: darkorange">*</i></label>
            <input type="date" class="form-control" name="fecha" value="<?= $fecha ?>" required="true"/>
        </div>
        <div class="form-group col-md-6">
            <label for="recipient-name" class="form-control-label">Patologia <i style="color: darkorange">*</i></label>
            <select class="form-control" name="patologia" id="patologiaEditar" required="true">
                <option value="">Selecciona una opción</option>
                <?php
                $queryPatologia = mysqli_query($conn, "SELECT id_patologia,patologia FROM gddt_patologias ;");
                while ($fetchPatologia = mysqli_fetch_row($queryPatologia)) {
                    if ($fetchPatologia[0] == $fetchUsuario[2]) {
                        echo "<option selected value=" . $fetchPatologia[0] . ">" . $fetchPatologia[1] . "</option>";
                    } else {
                        echo "<option value=" . $fetchPatologia[0] . ">" . $fetchPatologia[1] . "</option>";
                    }
                }
                ?>
            </select>
        </div>
        <div class="form-group col-md-6">
            <label for="message-text" class="form-control-label">Capitulo <i style="color: darkorange">*</i></label>
            <input type="number" class="form-control" name="capitulo" value="<?= $fetchUsuario[8]; ?>" required="true"/>
        </div>
        <div class="form-group col-md-6">
            <label for="recipient-name" class="form-control-label">Enfermedad <i style="color: darkorange">*</i></label>
            <select class="form-control" name="enfermedad" id="enfermedadEditar" required="true">
                <option value="">Selecciona una opción</option>
                <?php
                $queryEnfermedad = mysqli_query($conn, "SELECT id_enfermedad, des_enfermedad FROM gddt_enfermedades_cie;");
                while ($fetchEnfermedad = mysqli_fetch_row($queryEnfermedad)) {
                    if ($fetchEnfermedad[0] == $fetchUsuario[10]) {
                        echo "<option selected value=" . $fetchEnfermedad[0] . ">" . $fetchEnfermedad[1] . "</option>";
                    } else {
                        echo "<option value=" . $fetchEnfermedad[0] . ">" . $fetchEnfermedad[1] . "</option>";
                    }
                }
                ?>
            </select>
        </div>
    </div>
    <!-- End Row -->
    <!-- Modal Footer -->
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">
            Cancelar
        </button>
        <button type="submit" class="btn btn-primary" name="Actualizar">
            Actualizar
        </button>
    </div>
</div>    
</form>