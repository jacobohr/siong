<?php require "../../../../session.php";



$sqlUsuario = "SELECT *,(CONVERT(CAST(CONVERT(des_plan_tratamiento USING latin1) AS BINARY) USING utf8))AS des_plan_tratamiento FROM gddt_planes_tratamientos WHERE id_plan_tratamiento = ". $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);

?>
<form id="formularioPsicologiaPlanTratamientoEditar" method="POST" action="<?php echo $base_url ?>pages/gerontologia/psicologia/procesarDatos/procesarPsicologiaPlanTratamiento.php?tipo=update&idRegistro=<?= $_REQUEST["idu"] ?>&idMatriculaDato=<?= $fetchUsuario[7] ?>" autocomplete="off">
	<h4 class="text-center mb-4"> Plan de tratamientos</h4>
	<div class="row">
		<?php 
		if(strlen($fetchUsuario[4]) < 2){$mes = '0'.$fetchUsuario[4];}else{$mes = $fetchUsuario[4];}
		if(strlen($fetchUsuario[5]) < 2){$dia = '0'.$fetchUsuario[5];}else{$dia = $fetchUsuario[5];}
		$year = $fetchUsuario[3];
		$fecha = $year.'-'.$mes.'-'.$dia;
		?>
		<div class="form-group col-md-6">
			<label for="message-text" class="form-control-label">Fecha<i style="color: darkorange">*</i></label>
			<input type="date" class="form-control" name="fecha" value="<?= $fecha ?>" required="true"/>
		</div>
		<div class="form-group col-md-6">
			<label for="message-text" class="form-control-label"> Tipo de tratamiento <i style="color: darkorange">*</i></label>
			<select class="form-control" name="tipoTratamiento" id="tipoTratamientoEditar" required="true">
				<option value="">Selecciona una opción</option>
				<?php $arrayTipoTratamiento = array(
					0 => 'Seguimiento',
					1 => 'Objetivo terapeutico'
				);
				foreach ($arrayTipoTratamiento as $key) {
					if($key == $fetchUsuario[8]){
						echo '<option selected value="'.$key.'">'.$key.'</option>';
					}else{
						echo '<option value="'.$key.'">'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label">Plan de tratamiento <i style="color: darkorange">*</i></label> 
			<textarea class="form-control" name="planTratamiento" cols="40" rows="5" required="true"><?= $fetchUsuario[9] ?></textarea>
		</div>
	</div>
	<!-- End Row -->


	<!-- Modal Footer -->
	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">
			Cancelar
		</button>
		<button type="submit" class="btn btn-primary" name="Actualizar">
			Actualizar
		</button>
	</div>
</div>    