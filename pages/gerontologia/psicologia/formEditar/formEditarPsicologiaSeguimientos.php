<?php require "../../../../session.php";



$sqlUsuario = "SELECT *,(CONVERT(CAST(CONVERT(seguimiento_descripcion USING latin1) AS BINARY) USING utf8))AS seguimiento_descripcion FROM gddt_seguimiento_psicologico WHERE id_seguimiento_psicologico = ". $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);

?>
<form id="formularioPsicologiaSeguimientoEditar" method="POST" action="<?php echo $base_url ?>pages/gerontologia/psicologia/procesarDatos/procesarPsicologiaSeguimiento.php?tipo=update&idRegistro=<?= $_REQUEST["idu"] ?>&idMatriculaDato=<?= $fetchUsuario[6] ?>" autocomplete="off">
	<h4 class="text-center mb-4"> Seguimiento </h4>
	<div class="row">
		<?php 
		if(strlen($fetchUsuario[3]) < 2){$mes = '0'.$fetchUsuario[3];}else{$mes = $fetchUsuario[3];}
		if(strlen($fetchUsuario[4]) < 2){$dia = '0'.$fetchUsuario[4];}else{$dia = $fetchUsuario[4];}
		$year = $fetchUsuario[2];
		$fecha = $year.'-'.$mes.'-'.$dia;
		?>
		<div class="form-group col-md-6">
			<label for="message-text" class="form-control-label">Fecha<i style="color: darkorange">*</i></label>
			<input type="date" class="form-control" name="fecha" value="<?= $fecha ?>" required="true"/>
		</div>
		<div class="form-group col-md-6">
			<label for="recipient-name" class="form-control-label">Tipo de Seguimiento <i style="color: darkorange">*</i></label>
			<select class="form-control" name="tipoSeguimiento" id="tipoSeguimientoEditar" required="true">
				<option value="">Selecciona una opción</option>
				<?php
				$queryTipoSeguimiento = mysqli_query($conn, "SELECT (CONVERT(CAST(CONVERT(seguimiento_tipo USING latin1) AS BINARY) USING utf8))AS seguimiento_tipo FROM gddt_seguimiento_psicologico GROUP BY seguimiento_tipo;");
				while ($fetchTipoSeguimiento = mysqli_fetch_row($queryTipoSeguimiento)) {
					if ($fetchTipoSeguimiento[0] == $fetchUsuario[7]) {
						echo "<option selected value='" . $fetchTipoSeguimiento[0] . "'>" . $fetchTipoSeguimiento[0] . "</option>";
					} else {
						echo "<option value='" . $fetchTipoSeguimiento[0] . "'>" . $fetchTipoSeguimiento[0] . "</option>";
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label">Descripcion de el seguimiento <i style="color: darkorange">*</i></label>
			<textarea class="form-control" name="descripcionSeguimiento" cols="40" rows="5" required="true"><?= $fetchUsuario[9] ?></textarea>
		</div>
	</div>
	<!-- End Row -->


	<!-- Modal Footer -->
	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">
			Cancelar
		</button>
		<button type="submit" class="btn btn-primary" name="Actualizar">
			Actualizar
		</button>
	</div>
</div>    