<?php require "../../../../session.php";



$sqlUsuario = "SELECT * FROM gddt_antecedentes_personales WHERE id_antecedente_personal = ". $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);

?>
<form id="formularioPsicologiaAntecedenteMentalEditar" method="POST" action="<?php echo $base_url ?>pages/gerontologia/psicologia/procesarDatos/procesarPsicologiaAntecedenteMental.php?tipo=update&idRegistro=<?= $_REQUEST["idu"] ?>&idMatriculaDato=<?= $fetchUsuario[1] ?>" autocomplete="off">
	<h4 class="text-center mb-4"> Antecedentes Mentales</h4>
	<div class="row">
		<?php 
		if(strlen($fetchUsuario[5]) < 2){$mes = '0'.$fetchUsuario[5];}else{$mes = $fetchUsuario[5];}
		if(strlen($fetchUsuario[4]) < 2){$dia = '0'.$fetchUsuario[4];}else{$dia = $fetchUsuario[4];}
		$year = $fetchUsuario[6];
		$fecha = $year.'-'.$mes.'-'.$dia;
		?>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Fecha de modificacion <i style="color: darkorange">*</i></label>
			<input type="date" class="form-control" name="fecha" value="<?= $fecha ?>" required="true"/>
		</div>

		<div class="form-group col-md-4">
			<label for="recipient-name" class="form-control-label">Antecedente <i style="color: darkorange">*</i></label>
			<select class="form-control" name="antecedente" required="true">
				<option value="0">Selecciona una opción</option>
				<?php
				$queryAntecedente = mysqli_query($conn, "SELECT id_antecedente, antecedente FROM gddt_antecedentes ;");
				while ($fetchAntecedente = mysqli_fetch_row($queryAntecedente)) {
					if ($fetchAntecedente[0] == $fetchUsuario[2]) {
						echo "<option selected value=" . $fetchAntecedente[0] . ">" . $fetchAntecedente[1] . "</option>";
					} else {
						echo "<option value=" . $fetchAntecedente[0] . ">" . $fetchAntecedente[1] . "</option>";
					}
				}
				?>
			</select>
		</div>
	</div>
	<!-- End Row -->
	<!-- Modal Footer -->
	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">
			Cancelar
		</button>
		<button type="submit" class="btn btn-primary" name="Actualizar">
			Actualizar
		</button>
	</div>
</form>
