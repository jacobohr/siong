<?php require "../../../../session.php";



$sqlUsuario = "SELECT *,(CONVERT(CAST(CONVERT(observaciones_familiares USING latin1) AS BINARY) USING utf8))AS observacion FROM gddt_relaciones_familiares WHERE id_relacion_familiar =  ". $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);

?>
<form id="formularioPsicologiaRelacionFamiliarEditar" method="POST" action="<?php echo $base_url ?>pages/gerontologia/psicologia/procesarDatos/procesarPsicologiaRelacionFamiliar.php?tipo=update&idRegistro=<?= $_REQUEST["idu"] ?>&idMatriculaDato=<?= $fetchUsuario[6] ?>" autocomplete="off"> 
	<h4 class="text-center mb-4"> Relaciones Familiares</h4>
	<div class="row">
		<?php 
		if(strlen($fetchUsuario[3]) < 2){$mes = '0'.$fetchUsuario[3];}else{$mes = $fetchUsuario[3];}
		if(strlen($fetchUsuario[4]) < 2){$dia = '0'.$fetchUsuario[4];}else{$dia = $fetchUsuario[4];}
		$year = $fetchUsuario[2];
		$fecha = $year.'-'.$mes.'-'.$dia;
		?>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Fecha<i style="color: darkorange">*</i></label>
			<input type="date" class="form-control" name="fecha" value="<?= $fecha ?>" required="true"/>
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Hijos vivos <i style="color: darkorange">*</i></label>
			<input type="number" class="form-control" name="hijosVivos" value="<?= $fetchUsuario[7]; ?>" required="true"/>
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Hijos fallecidos <i style="color: darkorange">*</i></label>
			<input type="number" class="form-control" name="hijosFallecidos" value="<?= $fetchUsuario[8]; ?>" required="true"/>
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Relaciones familiares <i style="color: darkorange">*</i></label>
			<select class="form-control" name="relacionesFamiliares"  id="relacioneFamiliaresEditar" required="true">
				<option value="">Selecciona una opción</option>
				<?php 
				$arrayRelacionesFamiliares = array(
					0 => 'Buenas',
					1 => 'Aceptables',
					2 => 'Malas',
					3 => 'No se dan'
				);
				foreach ($arrayRelacionesFamiliares as $key) {
					if($key == $fetchUsuario[9]){
						echo '<option selected value='.$key.'>'.$key.'</option>';
					}else{
						echo '<option value='.$key.'>'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label">Observaciones familiares <i style="color: darkorange">*</i></label>
			<textarea class="form-control" name="observacionesFamiliares" cols="40" rows="5" required="true"><?= $fetchUsuario[11] ?></textarea>
		</div>
	</div>
	<!-- End Row -->
	<!-- Modal Footer -->
	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">
			Cancelar
		</button>
		<button type="submit" class="btn btn-primary" name="Actualizar">
			Actualizar
		</button>
	</div>
</div>    
</form>