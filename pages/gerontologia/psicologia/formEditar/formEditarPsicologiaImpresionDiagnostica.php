<?php require "../../../../session.php";



$sqlUsuario = "SELECT
*,
(CONVERT(CAST(CONVERT(impresion_diagnostica USING latin1) AS BINARY) USING utf8))AS impresion_diagnostica,
(CONVERT(CAST(CONVERT(observacion_final_psicologica USING latin1) AS BINARY) USING utf8))AS observacion_final_psicologica
FROM gddt_impresiones_diagnosticas WHERE id_impresion_diagnostica = ". $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);

?>
<form name="editarUsuario" method="POST" action="<?php echo $base_url ?>pages/gerontologia/psicologia/procesarDatos/procesarPsicologiaImpresionDiagnostica.php?tipo=update&idRegistro=<?= $_REQUEST["idu"] ?>&idMatriculaDato=<?= $fetchUsuario[6] ?>" autocomplete="off"> 
	<h4 class="text-center mb-4"> Impreson Diagnostica</h4>
	<div class="row">
		<?php 
		if(strlen($fetchUsuario[3]) < 2){$mes = '0'.$fetchUsuario[3];}else{$mes = $fetchUsuario[3];}
		if(strlen($fetchUsuario[4]) < 2){$dia = '0'.$fetchUsuario[4];}else{$dia = $fetchUsuario[4];}
		$year = $fetchUsuario[2];
		$fecha = $year.'-'.$mes.'-'.$dia;
		?>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label">Fecha<i style="color: darkorange">*</i></label>
			<input type="date" class="form-control" name="fecha" value="<?= $fecha ?>" required="true"/>
		</div>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label"> Impresion diagnostica <i style="color: darkorange">*</i></label>
			<textarea class="form-control" name="impresion" cols="40" rows="5"  required="true"><?= $fetchUsuario[9] ?></textarea>
		</div>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label"> Observacion final psicologia <i style="color: darkorange">*</i></label>
			<textarea class="form-control" name="observacion" cols="40" rows="5"  required="true"><?= $fetchUsuario[10] ?></textarea>
		</div>
	</div>
	<!-- End Row -->


	<!-- Modal Footer -->
	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">
			Cancelar
		</button>
		<button type="submit" class="btn btn-primary" name="Actualizar">
			Actualizar
		</button>
	</div>
</div>    