<?php require "../../../../session.php";



$sqlUsuario = "SELECT * FROM gddt_trastornos_datos WHERE id_trastorno_dato = ". $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);

?>
<form id="registroPsicologiaEvaluacionMultiaxialEditar"  method="POST" action="<?php echo $base_url ?>pages/gerontologia/psicologia/procesarDatos/procesarPsicologiaEvaluacionMultiaxial.php?tipo=update&idRegistro=<?= $_REQUEST["idu"] ?>&idMatriculaDato=<?= $fetchUsuario[1] ?>" autocomplete="off"> 
	<h4 class="text-center mb-4"> Evaluacion Multiaxial</h4>
	<div class="row">
		<?php 
		if(strlen($fetchUsuario[6]) < 2){$mes = '0'.$fetchUsuario[6];}else{$mes = $fetchUsuario[6];}
		if(strlen($fetchUsuario[5]) < 2){$dia = '0'.$fetchUsuario[5];}else{$dia = $fetchUsuario[5];}
		$year = $fetchUsuario[7];
		$fecha = $year.'-'.$mes.'-'.$dia;
		?>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Fecha<i style="color: darkorange">*</i></label>
			<input type="date" class="form-control" name="fecha" value="<?= $fecha ?>" required="true"/>
		</div>
		<div class="form-group col-md-4">
			<label for="recipient-name" class="form-control-label">Transtorno <i style="color: darkorange">*</i></label>
			<select id="transtornoMultiaxialEditar"class="form-control" name="transtorno"  onchange="calcularTranstornoEjeEditar()" required="true">
				<option value="">Selecciona una opción</option>
				<?php
				$queryAntecedente = mysqli_query($conn, "SELECT id_trastorno,trastorno FROM gddt_sel_trastornos ;");
				while ($fetchAntecedente = mysqli_fetch_row($queryAntecedente)) {
					if ($fetchAntecedente[0] == $fetchUsuario[2]) {
						echo "<option selected value=" . $fetchAntecedente[0] . ">" . $fetchAntecedente[1] . "</option>";
					} else {
						echo "<option value=" . $fetchAntecedente[0] . ">" . $fetchAntecedente[1] . "</option>";
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">N# del Eje <i style="color: darkorange">*</i></label>
			<input id="numeroDelEjeEditar" type="number" class="form-control" name="numeroEje"  required="true" value="<?php echo $fetchUsuario[2]; ?>" />
		</div>


	</div>
	<!-- End Row -->


	<!-- Modal Footer -->
	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">
			Cancelar
		</button>
		<button type="submit" class="btn btn-primary" name="Actualizar">
			Actualizar
		</button>
	</div>
</div>    
</form>