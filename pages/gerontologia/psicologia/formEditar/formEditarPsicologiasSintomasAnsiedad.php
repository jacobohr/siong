<?php require "../../../../session.php";



$sqlUsuario = "SELECT * FROM gddt_sintomas_ansiedad_datos WHERE id_sintoma_ansiedad_datos = ". $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);

?>
<form id="registroPsicologiaSintomasAnsiedad" method="POST" action="<?php echo $base_url ?>pages/gerontologia/psicologia/procesarDatos/procesarPsicologiaSintomaAnsiedad.php?tipo=update&idRegistro=<?= $_REQUEST["idu"] ?>&idMatriculaDato=<?= $fetchUsuario[1] ?>" autocomplete="off"> 
	<h4 class="text-center mb-4"> Sintomas de ansiedad</h4>
	<div class="row">
		<?php 
		if(strlen($fetchUsuario[5]) < 2){$mes = '0'.$fetchUsuario[5];}else{$mes = $fetchUsuario[5];}
		if(strlen($fetchUsuario[4]) < 2){$dia = '0'.$fetchUsuario[4];}else{$dia = $fetchUsuario[4];}
		$year = $fetchUsuario[6];
		$fecha = $year.'-'.$mes.'-'.$dia;
		?>
		<div class="form-group col-md-6">
			<label for="message-text" class="form-control-label">Fecha<i style="color: darkorange">*</i></label>
			<input type="date" class="form-control" name="fecha" value="<?= $fecha ?>" require="true"/>
		</div>
		<div class="form-group col-md-6">
			<label for="recipient-name" class="form-control-label">Sintomas <i style="color: darkorange">*</i></label>
			<select class="form-control" name="sintoma" id="sintomaEditar" require="true">
				<option value="">Selecciona una opción</option>
				<?php
				$queryAnsiedad = mysqli_query($conn, "SELECT id_sintoma_ansiedad, sintoma_ansiedad FROM gddt_sintomas_ansiedad ;");
				while ($fetchAnsiedad = mysqli_fetch_row($queryAnsiedad)) {
					if ($fetchAnsiedad[0] == $fetchUsuario[2]) {
						echo "<option selected value=" . $fetchAnsiedad[0] . ">" . $fetchAnsiedad[1] . "</option>";
					} else {
						echo "<option value=" . $fetchAnsiedad[0] . ">" . $fetchAnsiedad[1] . "</option>";
					}
				}
				?>
			</select>
		</div>
	</div>
	<!-- End Row -->
	<!-- Modal Footer -->
	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">
			Cancelar
		</button>
		<button type="submit" class="btn btn-primary" name="Actualizar">
			Actualizar
		</button>
	</div>
</div>    
</form>