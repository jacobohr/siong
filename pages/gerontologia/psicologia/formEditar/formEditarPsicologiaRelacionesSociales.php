<?php require "../../../../session.php";



$sqlUsuario = "SELECT *,
(CONVERT(CAST(CONVERT(observaciones_sociales USING latin1) AS BINARY) USING utf8))AS observaciones_sociales,
(CONVERT(CAST(CONVERT(relaciones_companeros USING latin1) AS BINARY) USING utf8))AS relaciones_companeros
FROM gddt_relaciones_sociales 
WHERE id_relaciones_sociales =". $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);

?>

<form id="formularioPsicologiaRelacionSocialEditar" method="POST" action="<?php echo $base_url ?>pages/gerontologia/psicologia/procesarDatos/procesarPsicologiaRelacionSocial.php?tipo=update&idRegistro=<?= $_REQUEST["idu"] ?>&idMatriculaDato=<?= $fetchUsuario[6] ?>" autocomplete="off">
	<h4 class="text-center mb-4"> Relaciones Sociales</h4>
	<div class="row">
		<?php 
		if(strlen($fetchUsuario[3]) < 2){$mes = '0'.$fetchUsuario[3];}else{$mes = $fetchUsuario[3];}
		if(strlen($fetchUsuario[4]) < 2){$dia = '0'.$fetchUsuario[4];}else{$dia = $fetchUsuario[4];}
		$year = $fetchUsuario[2];
		$fecha = $year.'-'.$mes.'-'.$dia;
		?>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Fecha<i style="color: darkorange">*</i></label>
			<input type="date" class="form-control" name="fecha" value="<?= $fecha ?>" required="true"/>
		</div>

		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Relaciones con los compañeros <i style="color: darkorange">*</i></label>
			<select class="form-control" name="relacionCompañeros"  id="relacionCompanerosEditar" required="true">
				<option value="">Selecciona una opción</option>
				<?php 
				$arrayRelacion = array (
					0 => 'Aceptación',
					1 => 'Rechazo',
					2 => 'Indiferencia',
					3 => 'No se dan'
				);
				foreach($arrayRelacion as $key) {
					if($key  == $fetchUsuario[7]){
						echo '<option selected value="'.$key.'">'.$key.'</option>';
					}else{
						echo '<option value="'.$key.'">'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Participa en actividades <i style="color: darkorange">*</i></label>
			<select class="form-control" name="participaActividades"  id="actividadesRealizaEditar" required="true">
				<option value="">Selecciona una opción</option>
				<?php 
				$arrayActividades = array (
					0 => 'Si',
					1 => 'No',
					2 => 'Algunas veces'
				);
				foreach ($arrayActividades as $key) {
					if($key  == $fetchUsuario[8]){
						echo '<option selected value="'.$key.'">'.$key.'</option>';
					}else{
						echo '<option value="'.$key.'">'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label">Observaciones <i style="color: darkorange">*</i></label>
			<textarea class="form-control" name="observaciones" cols="40" rows="5"  required="true"><?= $fetchUsuario[10] ?></textarea>
		</div>
	</div>
	<!-- End Row -->


	<!-- Modal Footer -->
	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">
			Cancelar
		</button>
		<button type="submit" class="btn btn-primary" name="Actualizar">
			Actualizar
		</button>
	</div>
</div>    