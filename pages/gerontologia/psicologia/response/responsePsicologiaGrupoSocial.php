<?php

//include connection file 
require "../../../../session.php";

$id_matricula_dato = $_POST['id'];
//$params = $columns = $totalRecords = $data = array();

$params = filter_input_array(INPUT_POST);

//var_dump($params);

//define index of column
$columns = array(
    0 => 'numero',
    1 => 'id_grupo_social_dato',
    2 => 'nombreUsuario',
    3 => 'fecha',
    4 => 'hora',
    5 => 'grupo_social'
);

$sqlTot = $sqlRec = "";

// check search value exist
$where = " WHERE gsd.id_matricula_dato = $id_matricula_dato";

if (isset($params['search']['value']) && $params['search']['value'] != '') {
    $where .= "
            AND 
                (UPPER((CONVERT(CAST(CONVERT(CONCAT(c.primer_nombre,' ',c.segundo_nombre,' ',c.primer_apellido,' ',c.segundo_apellido) USING latin1) AS BINARY) USING utf8))) LIKE '%" . $params['search']['value'] . "%' 
                OR id_grupo_social_dato  LIKE '%" . $params['search']['value'] . "%' 
                OR CONCAT(gsd.dia_mod,'/',gsd.mes_mod,'/',gsd.ano_mod) LIKE '%" . $params['search']['value'] . "%'
                OR grupo_social LIKE '%" . $params['search']['value'] . "%'
                OR hora_mod  LIKE '%" . $params['search']['value']. "%')";

}


// getting total number records without any search
$sql = "SELECT 
        ROW_NUMBER() OVER() AS numero,
        gsd.id_grupo_social_dato,
        UPPER((CONVERT(CAST(CONVERT(CONCAT(c.primer_nombre,' ',c.segundo_nombre,' ',c.primer_apellido,' ',c.segundo_apellido) USING latin1) AS BINARY) USING utf8))) AS nombreUsuario,
        CONCAT(gsd.dia_mod,'/',gsd.mes_mod,'/',gsd.ano_mod) fecha,  
        gsd.hora_mod, 
        gs.grupo_social 
        FROM gddt_grupos_sociales gs
        INNER JOIN gddt_grupos_sociales_datos gsd ON gs.id_grupo_social = gsd.id_grupo_social 
        INNER JOIN gddt_cuentas c ON c.id_cuenta = gsd.id_cuenta ";


$sqlTot .= $sql;
$sqlRec .= $sql;



//concatenate search sql if value exist
if (isset($where) && $where != '') {
    $sqlTot .= $where;
    $sqlRec .= $where;
}

$sqlRec .= " ORDER BY " . $columns[$params['order'][0]['column']] . "  " . $params['order'][0]['dir'] . "  LIMIT " . $params['start'] . " ," . $params['length'] . " ";

$queryTot = mysqli_query($conn, $sqlTot) or die("database error:" . mysqli_error($conn));

$totalRecords = mysqli_num_rows($queryTot);

$queryRecords = mysqli_query($conn, $sqlRec) or die("error to fetch employees data");

$data = array();
//iterate on results row and create new index array of data
while ($row = mysqli_fetch_row($queryRecords)) {
    $data[] = $row;
}

foreach ($data as $key => $val) {

    /*if (!in_array("82", $_SESSION["RESTRICCIONES"])) {*/
        $data[$key][6] = '
        &nbsp;
        <a href="#" style="color:#000;" onClick="fnVerPsicologiaGruposSociales(\'' . $val[1] . '\');">
        <span data-toggle="tooltip" title="Ver usuario" class="fa fa-eye"></span>
        </a>';    

        $data[$key][6] .= '
        &nbsp;
        <a href="#" style="color:#000;" data-toggle="modal" data-target="#modalModificarUsuario" onClick="fnEditarPsicologiaGruposSociales(\'' . $val[1] . '\');">
        <span data-toggle="tooltip" title="Editar usuario" class="fas fa-pencil-alt"></span>
        </a>
        ';
        $data[$key][6] .= '
        &nbsp;
        <a href="#" style="color:#000;" data-toggle="modal" data-target="#modalEliminarUsuario" onClick="fnEliminarPsicologiaGrupoSocial(\'' . $val[1] . '\');">
        <span data-toggle="tooltip" title="Eliminar usuario" class="fa fa-times"></span>
        </a>';

}

$json_data = array(
    "draw" => intval($params['draw']),
    "recordsTotal" => intval($totalRecords),
    "recordsFiltered" => intval($totalRecords),
    "data" => $data   // total data array
);

echo json_encode($json_data);  // send data as json format