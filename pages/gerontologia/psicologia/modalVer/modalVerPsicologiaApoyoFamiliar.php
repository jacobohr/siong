<?php

//include connection file 
require "../../../../session.php";

$sqlUsuario = "
SELECT 
(CONVERT(CAST(CONVERT(CONCAT(UPPER(c.primer_nombre),' ',UPPER(c.segundo_nombre),' ',UPPER(c.primer_apellido),' ',UPPER(c.segundo_apellido)) USING latin1) AS BINARY) USING utf8)) AS nombreUsuario,
CONCAT(md.dia_mod,'/',md.mes_mod,'/',md.ano_mod) fecha,
md.hora_mod,
ap.apoyo_familiar
FROM  gddt_apoyo_familiar_datos md
INNER JOIN gddt_cuentas c  ON md.id_cuenta = c.id_cuenta	
INNER JOIN gddt_apoyo_familiar ap ON md.id_apoyo_familiar = ap.id_apoyo_familiar
WHERE id_apoyo_familiar_dato = " . $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);
$rows = mysqli_num_rows($queryUsuario);


if($rows == 0){
    echo "<p style='color:red;font-size:0.9em;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No se encontraron datos :(</p>";
}else{


?>

<div class="content">
<h6 class="text-center mb-4">Actualizado por <?= $fetchUsuario[0];?> el <?= $fetchUsuario[1]; ?> a las <?= $fetchUsuario[2]; ?></h6>
 <div class="user-block col-sm-4 col-xs-6">
    <label>
        Apoyo familiar
    </label>
    <p><?php  if(empty($fetchUsuario[3])){echo 'Sin datos';}else{ echo $fetchUsuario[3];} ?></p>
</div>


<div class="user-block">&nbsp;</div>

</div><!-- Fin del content -->
<?php } ?>