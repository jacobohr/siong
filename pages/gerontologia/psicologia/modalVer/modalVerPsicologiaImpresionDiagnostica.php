<?php

//include connection file 
require "../../../../session.php";

$sqlUsuario = "
SELECT 
UPPER((CONVERT(CAST(CONVERT(CONCAT(c.primer_nombre,' ',c.segundo_nombre,' ',c.primer_apellido,' ',c.segundo_apellido) USING latin1) AS BINARY) USING utf8))) AS nombreUsuario, 
CONCAT(idg.dia_mod,'/',idg.mes_mod,'/',idg.ano_mod) fecha, 
idg.hora_mod,
(CONVERT(CAST(CONVERT(idg.impresion_diagnostica USING latin1) AS BINARY) USING utf8))AS impresion_diagnostica,
(CONVERT(CAST(CONVERT(idg.observacion_final_psicologica USING latin1) AS BINARY) USING utf8))AS observacion_final_psicologica
FROM gddt_impresiones_diagnosticas idg
INNER JOIN gddt_cuentas c ON c.id_cuenta = idg.id_cuenta
WHERE idg.id_impresion_diagnostica = " . $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);
$rows = mysqli_num_rows($queryUsuario);

if($rows == 0){
    echo "<p style='color:red;font-size:0.9em;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No se encontraron datos :(</p>";
}else{

?>

<div class="content">
<h6 class="text-center mb-4">Actualizado por <?= $fetchUsuario[0];?> el <?= $fetchUsuario[1]; ?> a las <?= $fetchUsuario[2]; ?></h6>
 <div class="user-block col-sm-12 col-xs-12">
    <label>
        Impresion Diagnostica
    </label>
    <p><?php if(empty($fetchUsuario[3])){echo "Sin dato";}else{echo $fetchUsuario[3];} ?></p>
</div>
 <div class="user-block col-sm-12 col-xs-12">
    <label>
        Observacion final psicologia
    </label>
    <p><?php if(empty($fetchUsuario[4])){echo "Sin dato";}else{echo $fetchUsuario[4];} ?></p>
</div>

<div class="user-block">&nbsp;</div>

</div><!-- Fin del content -->
<?php } ?>