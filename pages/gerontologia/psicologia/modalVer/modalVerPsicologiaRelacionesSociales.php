<?php

//include connection file 
require "../../../../session.php";

$sqlUsuario = "
SELECT 
UPPER((CONVERT(CAST(CONVERT(CONCAT(c.primer_nombre,' ',c.segundo_nombre,' ',c.primer_apellido,' ',c.segundo_apellido) USING latin1) AS BINARY) USING utf8))) AS nombreUsuario,
CONCAT(rs.dia_mod,'/',rs.mes_mod,'/',rs.ano_mod) fecha, 
rs.hora_mod,
(CONVERT(CAST(CONVERT(rs.relaciones_companeros USING latin1) AS BINARY) USING utf8))AS relacion_companeros,
rs.participa_actividades,
(CONVERT(CAST(CONVERT(rs.observaciones_sociales USING latin1) AS BINARY) USING utf8))AS observaciones_sociales,
rs.relaciones_companerosWW
FROM gddt_relaciones_sociales rs
INNER JOIN gddt_cuentas c ON c.id_cuenta = rs.id_cuenta 
WHERE rs.id_relaciones_sociales = " . $_REQUEST["idu"];


$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);
$rows = mysqli_num_rows($queryUsuario);

if($rows == 0){
    echo "<p style='color:red;font-size:0.9em;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No se encontraron datos :(</p>";
}else{
?>

<div class="content">
<h6 class="text-center mb-4">Actualizado por <?= $fetchUsuario[0];?> el <?= $fetchUsuario[1]; ?> a las <?= $fetchUsuario[2]; ?></h6>
 <div class="user-block col-sm-4 col-xs-6">
    <label>
        Relaciones con los compañeros
    </label>
    <p><?php echo $fetchUsuario[6]; ?></p>
</div>

 <div class="user-block col-sm-4 col-xs-6">
    <label>
        Participa en actividades
    </label>
    <p><?php echo $fetchUsuario[4]; ?></p>
</div>

<div class="user-block col-sm-12 col-xs-12">
    <label>
        Observaciones
    </label>
    <p><?php echo $fetchUsuario[5]; ?></p>
</div>
<div class="user-block">&nbsp;</div>

</div><!-- Fin del content -->
<?php } ?>