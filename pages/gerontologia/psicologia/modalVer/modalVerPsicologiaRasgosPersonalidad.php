<?php

//include connection file 
require "../../../../session.php";

$sqlUsuario = "
SELECT 
UPPER((CONVERT(CAST(CONVERT(CONCAT(c.primer_nombre,' ',c.segundo_nombre,' ',c.primer_apellido,' ',c.segundo_apellido) USING latin1) AS BINARY) USING utf8))) AS nombreUsuario, 
CONCAT(rp.dia_mod,'/',rp.mes_mod,'/',rp.ano_mod) fecha,  
rp.hora_mod,
rp.personalidad,
rp.lenguaje,
rp.principio_realidad,
rp.afectividad,
rp.autovaloracion,
rp.autopercepcion_salud,
(CONVERT(CAST(CONVERT(rp.observaciones_personalidad USING latin1) AS BINARY) USING utf8))AS observacion
FROM gddt_rasgos_personalidades rp
INNER JOIN gddt_cuentas c ON c.id_cuenta = rp.id_cuenta
WHERE id_rasgos_personalidad = " . $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);
$rows = mysqli_num_rows($queryUsuario);

if($rows == 0){
    echo "<p style='color:red;font-size:0.9em;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No se encontraron datos :(</p>";
}else{



?>

<div class="content">
<h6 class="text-center mb-4">Actualizado por <?= $fetchUsuario[0];?> el <?= $fetchUsuario[1]; ?> a las <?= $fetchUsuario[2]; ?></h6>
 <div class="user-block col-sm-4 col-xs-6">
    <label>
        Personalidad
    </label>
    <p><?php if(empty($fetchUsuario[3])){echo "Sin dato";}else{echo $fetchUsuario[3];} ?></p>
</div>
 <div class="user-block col-sm-4 col-xs-6">
    <label>
         Lenguaje
    </label>
    <p><?php if(empty($fetchUsuario[4])){echo "Sin dato";}else{echo $fetchUsuario[4];} ?></p>
</div>
 <div class="user-block col-sm-4 col-xs-6">
    <label>
        Principio de realidad
    </label>
    <p><?php if(empty($fetchUsuario[5])){echo "Sin dato";}else{echo $fetchUsuario[5];} ?></p>
</div>
 <div class="user-block col-sm-4 col-xs-6">
    <label>
        Afectividad
    </label>
    <p><?php if(empty($fetchUsuario[6])){echo "Sin dato";}else{echo $fetchUsuario[6];} ?></p>
</div>
 <div class="user-block col-sm-4 col-xs-6">
    <label>
        Autovaloracion
    </label>
    <p><?php if(empty($fetchUsuario[7])){echo "Sin dato";}else{echo $fetchUsuario[7];} ?></p>
</div>
 <div class="user-block col-sm-4 col-xs-6">
    <label>
        Autopercepcion de su salud
    </label>
    <p><?php if(empty($fetchUsuario[8])){echo "Sin dato";}else{echo $fetchUsuario[8];} ?></p>
</div>
 <div class="user-block col-sm-8 col-xs-12">
    <label>
        Observaciones
    </label>
    <p><?php if(empty($fetchUsuario[9])){echo "Sin dato";}else{echo $fetchUsuario[9];} ?></p>
</div>

<div class="user-block">&nbsp;</div>

</div><!-- Fin del content -->
<?php } ?>