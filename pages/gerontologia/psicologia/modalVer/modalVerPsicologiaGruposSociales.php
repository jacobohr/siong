<?php

//include connection file 
require "../../../../session.php";

$sqlUsuario = "
SELECT 
UPPER((CONVERT(CAST(CONVERT(CONCAT(c.primer_nombre,' ',c.segundo_nombre,' ',c.primer_apellido,' ',c.segundo_apellido) USING latin1) AS BINARY) USING utf8))) AS nombreUsuario, 
CONCAT(gsd.dia_mod,'/',gsd.mes_mod,'/',gsd.ano_mod) fecha,  
gsd.hora_mod,
gs.grupo_social 
FROM gddt_grupos_sociales gs
INNER JOIN gddt_grupos_sociales_datos gsd ON gs.id_grupo_social = gsd.id_grupo_social 
INNER JOIN gddt_cuentas c ON c.id_cuenta = gsd.id_cuenta
WHERE gsd.id_grupo_social_dato = " . $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);
$rows = mysqli_num_rows($queryUsuario);

if($rows == 0){
    echo "<p style='color:red;font-size:0.9em;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No se encontraron datos :(</p>";
}else{



?>

<div class="content">
<h6 class="text-center mb-4">Actualizado por <?= $fetchUsuario[0];?> el <?= $fetchUsuario[1]; ?> a las <?= $fetchUsuario[2]; ?></h6>
 <div class="user-block col-sm-12 col-xs-12">
    <label>
        Grupo Social
    </label>
    <p><?php if(empty($fetchUsuario[3])){echo "Sin dato";}else{echo $fetchUsuario[3];} ?></p>
</div>

<div class="user-block">&nbsp;</div>

</div><!-- Fin del content -->
<?php } ?>