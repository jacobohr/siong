<?php

//include connection file 
require "../../../../session.php";

$sqlUsuario = "
SELECT 
UPPER((CONVERT(CAST(CONVERT(CONCAT(c.primer_nombre,' ',c.segundo_nombre,' ',c.primer_apellido,' ',c.segundo_apellido) USING latin1) AS BINARY) USING utf8))) AS nombreUsuario, 
CONCAT(pt.dia_mod,'/',pt.mes_mod,'/',pt.ano_mod) fecha, 
pt.hora_mod,
pt.tipo_tratamiento,
(CONVERT(CAST(CONVERT(pt.des_plan_tratamiento USING latin1) AS BINARY) USING utf8))AS des_plan_tratamiento
FROM gddt_planes_tratamientos pt
INNER JOIN gddt_cuentas c ON c.id_cuenta = pt.id_cuenta
WHERE id_plan_tratamiento = " . $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);
$rows = mysqli_num_rows($queryUsuario);

if($rows == 0){
    echo "<p style='color:red;font-size:0.9em;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No se encontraron datos :(</p>";
}else{



?>

<div class="content">
<h6 class="text-center mb-4">Actualizado por <?= $fetchUsuario[0];?> el <?= $fetchUsuario[1]; ?> a las <?= $fetchUsuario[2]; ?></h6>
 <div class="user-block col-sm-4 col-xs-6">
    <label>
        Tipo de tratamiento
    </label>
    <p><?php if(empty($fetchUsuario[3])){echo "Sin dato";}else{echo $fetchUsuario[3];} ?></p>
</div>
 <div class="user-block col-sm-12 col-xs-12">
    <label>
        Tipo de tratamiento
    </label>
    <p><?php if(empty($fetchUsuario[4])){echo "Sin dato";}else{echo $fetchUsuario[4];} ?></p>
</div>

<div class="user-block">&nbsp;</div>

</div><!-- Fin del content -->
<?php } ?>