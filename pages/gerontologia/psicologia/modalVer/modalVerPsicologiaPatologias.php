<?php

//include connection file 
require "../../../../session.php";

$sqlUsuario = "SELECT * FROM gddt_patologias_usuarios_psicologia WHERE id_patologia_usuario = " . $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);
$rows = mysqli_num_rows($queryUsuario);

if($rows == 0){
    echo "<p style='color:red;font-size:0.9em;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No se encontraron datos :(</p>";
}else{

    $sqlDatosUsuarioRegistra = "SELECT 
    (CONVERT(CAST(CONVERT(CONCAT(UPPER(c.primer_nombre),' ',UPPER(c.segundo_nombre),' ',UPPER(c.primer_apellido),' ',UPPER(c.segundo_apellido)) USING latin1) AS BINARY) USING utf8)) AS nombreUsuario,
    CONCAT(pum.dia_mod,'/',pum.mes_mod,'/',pum.ano_mod) fecha,
    pum.hora_mod
    FROM gddt_patologias_usuarios_medicina pum
    INNER JOIN gddt_cuentas c ON c.id_cuenta = pum.id_cuenta 
    WHERE id_patologia_usuario = ". $fetchUsuario[6];
    $queryDatosUsuarioRegistra = mysqli_query($conn, $sqlDatosUsuarioRegistra);
    $fetchDatosUsuarioRegistra = mysqli_fetch_row($queryDatosUsuarioRegistra);

    $nomPatologia = '';
    if(empty($fetchUsuario[2])){
        $nomPatologia = 'Sin datos';
    }else{
        $sqlPatologia = "SELECT patologia FROM gddt_patologias WHERE id_patologia =".$fetchUsuario[2];
        $queryPatologia = mysqli_query($conn, $sqlPatologia);
        $fetchPatologia = mysqli_fetch_row($queryPatologia);
        if(empty($fetchPatologia)){$nomPatologia = 'Sin datos';}else{$nomPatologia = $fetchPatologia[0];}
    }
    

    $capitulo = '';
    if(empty($fetchUsuario[8])){
        $capitulo = 'Sin datos';
    }else{
        $sqlCapitulo = "SELECT nom_capitulo FROM gddt_capitulos_cie WHERE id_capitulo =".$fetchUsuario[8];
        $queryCapitulo = mysqli_query($conn, $sqlCapitulo);
        $fetchCapitulo = mysqli_fetch_row($queryCapitulo);
        if(empty($fetchCapitulo)){$capitulo = 'Sin datos';}else{$capitulo = $fetchPatologia[0];}
    }
    
    $enfermedad = '';
    $tipo = '';
    if(empty($fetchUsuario[10])){
        $enfermedad = 'Sin datos';
        $tipo = 'Sin datos';
    }else{
        $sqlEnfermedad = "SELECT id_tipo, des_enfermedad FROM gddt_enfermedades_cie WHERE id_enfermedad =".$fetchUsuario[10];
        $queryEnfermedad = mysqli_query($conn, $sqlEnfermedad);
        $fetchEnfermedad = mysqli_fetch_row($queryEnfermedad);
        if(empty($fetchEnfermedad)){
            $enfermedad = 'Sin datos';
            $tipo = 'Sin datos';
        }else{
            $enfermedad = $fetchEnfermedad[1];
            $tipo = $fetchEnfermedad[0];
        }
    }
    

    ?>

    <div class="content">
        <h6 class="text-center mb-4">Actualizado por <?= $fetchDatosUsuarioRegistra[0];?> el <?= $fetchDatosUsuarioRegistra[1]; ?> a las <?= $fetchDatosUsuarioRegistra[2]; ?></h6>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Patologia
            </label>
            <p><?php echo $nomPatologia; ?></p>
        </div>

        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Capitulo
            </label>
            <p><?php  echo $fetchUsuario[8]; ?></p>
        </div>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Enfermedad
            </label>
            <p><?php  echo $enfermedad; ?></p>
        </div>


        <div class="user-block">&nbsp;</div>

    </div><!-- Fin del content -->
    <?php } ?>