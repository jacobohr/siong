<?php

//include connection file 
require "../../../../session.php";

$sqlUsuario = "
SELECT 
(CONVERT(CAST(CONVERT(CONCAT(UPPER(c.primer_nombre),' ',UPPER(c.segundo_nombre),' ',UPPER(c.primer_apellido),' ',UPPER(c.segundo_apellido)) USING latin1) AS BINARY) USING utf8)) AS nombreUsuario,
CONCAT(af.dia_mod,'/',af.mes_mod,'/',af.ano_mod) fecha,
af.hora_mod, 
a.antecedente 
FROM gddt_antecedentes_familiares af
INNER JOIN gddt_antecedentes a ON af.id_antecedente = a.id_antecedente 
INNER JOIN gddt_cuentas c ON c.id_cuenta = af.id_cuenta 
WHERE af.id_antecedente_familiar = " . $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);
$rows = mysqli_num_rows($queryUsuario);

if($rows == 0){
    echo "<p style='color:red;font-size:0.9em;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No se encontraron datos :(</p>";
}else{



?>

<div class="content">
<h6 class="text-center mb-4">Actualizado por <?= $fetchUsuario[0];?> el <?= $fetchUsuario[1]; ?> a las <?= $fetchUsuario[2]; ?></h6>
 <div class="user-block col-sm-4 col-xs-6">
    <label>
        Antecedente
    </label>
    <p><?php echo $fetchUsuario[3]; ?></p>
</div>

<div class="user-block">&nbsp;</div>

</div><!-- Fin del content -->
<?php } ?>