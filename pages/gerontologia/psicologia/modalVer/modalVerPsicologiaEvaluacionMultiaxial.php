<?php

//include connection file 
require "../../../../session.php";

$sqlUsuario = "
SELECT 
UPPER((CONVERT(CAST(CONVERT(CONCAT(c.primer_nombre,' ',c.segundo_nombre,' ',c.primer_apellido,' ',c.segundo_apellido) USING latin1) AS BINARY) USING utf8))) AS nombreUsuario, 
CONCAT(td.dia_mod,'/',td.mes_mod,'/',td.ano_mod) fecha, 
td.hora_mod, 
td.num_eje,
td.id_trastorno
FROM gddt_trastornos_datos td
INNER JOIN gddt_cuentas c ON c.id_cuenta = td.id_cuenta
WHERE td.id_trastorno_dato = " . $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);
$rows = mysqli_num_rows($queryUsuario);

if($rows == 0){
    echo "<p style='color:red;font-size:0.9em;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No se encontraron datos :(</p>";
}else{



?>

<div class="content">
<h6 class="text-center mb-4">Actualizado por <?= $fetchUsuario[0];?> el <?= $fetchUsuario[1]; ?> a las <?= $fetchUsuario[2]; ?></h6>
 <div class="user-block col-sm-12 col-xs-12">
    <label>
        Eje <?= $fetchUsuario[3]; ?>
    </label>
    <?php 
    $sqlTranstorno = "SELECT id_trastorno,trastorno FROM gddt_sel_trastornos WHERE id_trastorno = ". $fetchUsuario[4]; 
    $queryTranstorno = mysqli_query($conn, $sqlTranstorno);	
    $fetchTranstorno = mysqli_fetch_row($queryTranstorno);
    ?>
    <p><?php if(empty($fetchTranstorno)){echo "Sin dato asociado";}else{echo $fetchTranstorno[1];} ?></p>
</div>

<div class="user-block">&nbsp;</div>

</div><!-- Fin del content -->
<?php } ?>