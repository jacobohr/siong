<?php 
require "../../../../session.php";

$tipo = $_REQUEST["tipo"];
$idMatriculaDato = $_REQUEST["idMatriculaDato"];
$idRegistro = $_REQUEST["idRegistro"];

$grupoSocial = $_POST['grupoSocial'];

$fecha = strtotime($_POST['fecha']);
$ano = date("Y",$fecha);
$dia = date("d",$fecha);
$mes = date("m",$fecha);
$hora = date("H:i");

$sqlCedula = "SELECT num_documento FROM gddt_matricula_datos WHERE id_matricula_dato =".$idMatriculaDato;
$queryCedula = mysqli_query($conn, $sqlCedula);
$fetchCedula = mysqli_fetch_row($queryCedula);

switch ($tipo) {
	case 'insert':
		$insert = $db->prepare("INSERT INTO gddt_grupos_sociales_datos (id_matricula_dato, id_grupo_social,id_cuenta, dia_mod, mes_mod, ano_mod, hora_mod) VALUES(:id_matricula_dato, :id_grupo_social, :id_cuenta, :dia_mod, :mes_mod, :ano_mod, :hora_mod);");
		$insert->bindParam(':id_matricula_dato',$idMatriculaDato);
		$insert->bindParam(':id_grupo_social', $grupoSocial);
		$insert->bindParam(':id_cuenta',$_SESSION['ID_CUENTA']);
		$insert->bindParam(':dia_mod', $dia);
		$insert->bindParam(':mes_mod', $mes);
		$insert->bindParam(':ano_mod', $ano);
		$insert->bindParam(':hora_mod', $hora);
		$insert->execute();
		header('location:'.$base_url.'pages/gerontologia/pacientes.php?param='.$fetchCedula[0]);
		break;
	case 'update':
		$update = $db->prepare("UPDATE gddt_grupos_sociales_datos SET id_grupo_social='".$grupoSocial."',id_cuenta='".$_SESSION['ID_CUENTA']."', dia_mod='".$dia."', mes_mod='".$mes."', ano_mod='".$ano."', hora_mod='".$hora."' WHERE id_grupo_social_dato ='".$idRegistro."';");
		$update->execute();
		header('location:'.$base_url.'pages/gerontologia/pacientes.php?param='.$fetchCedula[0]);
		break;
	case 'delete':
		$sqlBuscarCedula = "SELECT md.num_documento FROM gddt_grupos_sociales_datos nd INNER JOIN gddt_matricula_datos md ON md.id_matricula_dato = nd.id_matricula_dato WHERE nd.id_grupo_social_dato=".$idRegistro;
		$queryBuscarCedula = mysqli_query($conn, $sqlBuscarCedula);
		$fetchBuscarCedula = mysqli_fetch_row($queryBuscarCedula);

		$delete = $db->prepare("DELETE FROM gddt_grupos_sociales_datos WHERE id_grupo_social_dato='".$idRegistro."';");
		$delete->execute();
		
		header('location:'.$base_url.'pages/gerontologia/pacientes.php?param='.$fetchBuscarCedula[0]);
		break;

	break;

	default:
		# code...
	break;
}