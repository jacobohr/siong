<?php 
require "../../../../session.php";

$tipo = $_REQUEST["tipo"];
$idMatriculaDato = $_REQUEST["idMatriculaDato"];
$idRegistro = $_REQUEST["idRegistro"];

$personalidad = $_POST['personalidad'];
$lenguaje = $_POST['lenguaje'];
$principioRealidad = $_POST['principioRealidad'];
$afectividad = $_POST['afectividad'];
$autopercepcion = $_POST['autopercepcion'];
$observacionesPersonalidad = $_POST['observacionesPersonalidad'];
$autovaloracion = $_POST['autovaloracion'];

$fecha = strtotime($_POST['fecha']);
$ano = date("Y",$fecha);
$dia = date("d",$fecha);
$mes = date("m",$fecha);
$hora = date("H:i");

$sqlCedula = "SELECT num_documento FROM gddt_matricula_datos WHERE id_matricula_dato =".$idMatriculaDato;
$queryCedula = mysqli_query($conn, $sqlCedula);
$fetchCedula = mysqli_fetch_row($queryCedula);

echo $_SESSION['ID_CUENTA'];
switch ($tipo) {
	case 'insert':
		$insert = $db->prepare("INSERT INTO gddt_rasgos_personalidades (id_cuenta,ano_mod,mes_mod,dia_mod,hora_mod,id_matricula_dato,personalidad,lenguaje,principio_realidad,afectividad,autovaloracion,autopercepcion_salud,observaciones_personalidad) VALUES (:id_cuenta, :ano_mod, :mes_mod, :dia_mod, :hora_mod, :id_matricula_dato, :personalidad, :lenguaje, :principio_realidad, :afectividad, :autovaloracion, :autopercepcion_salud, :observaciones_personalidad);");
		$insert->bindParam(':id_cuenta', $_SESSION['ID_CUENTA']);
		$insert->bindParam(':ano_mod', $ano);
		$insert->bindParam(':mes_mod', $mes);
		$insert->bindParam(':dia_mod', $dia);
		$insert->bindParam(':hora_mod', $hora);
		$insert->bindParam(':id_matricula_dato',$idMatriculaDato);
		$insert->bindParam(':personalidad', $personalidad);
		$insert->bindParam(':lenguaje', $lenguaje);
		$insert->bindParam(':principio_realidad', $principioRealidad);
		$insert->bindParam(':afectividad', $afectividad);
		$insert->bindParam(':autovaloracion', $autovaloracion);
		$insert->bindParam(':autopercepcion_salud', $autopercepcion);
		$insert->bindParam(':observaciones_personalidad', $observacionesPersonalidad);
		$insert->execute();

		header('location:'.$base_url.'pages/gerontologia/pacientes.php?param='.$fetchCedula[0]);
		break;
	case 'update':
		$update = $db->prepare("UPDATE gddt_rasgos_personalidades SET id_cuenta='".$_SESSION['ID_CUENTA']."',ano_mod='".$ano."',mes_mod='".$mes."', dia_mod='".$dia."', hora_mod='".$hora."', personalidad='".$personalidad."', lenguaje='".$lenguaje."', principio_realidad='".$principioRealidad."',afectividad='". $afectividad."',autovaloracion='".$autovaloracion."',autopercepcion_salud='".$autopercepcion."',observaciones_personalidad='".$observacionesPersonalidad."' WHERE id_rasgos_personalidad ='".$idRegistro."';");
		$update->execute();
		header('location:'.$base_url.'pages/gerontologia/pacientes.php?param='.$fetchCedula[0]);
		break;
	case 'delete':
		$sqlBuscarCedula = "SELECT md.num_documento FROM gddt_rasgos_personalidades nd INNER JOIN gddt_matricula_datos md ON md.id_matricula_dato = nd.id_matricula_dato WHERE nd.id_rasgos_personalidad=".$idRegistro;
		$queryBuscarCedula = mysqli_query($conn, $sqlBuscarCedula);
		$fetchBuscarCedula = mysqli_fetch_row($queryBuscarCedula);

		$delete = $db->prepare("DELETE FROM gddt_rasgos_personalidades WHERE id_rasgos_personalidad='".$idRegistro."';");
		$delete->execute();
		
		header('location:'.$base_url.'pages/gerontologia/pacientes.php?param='.$fetchBuscarCedula[0]);
		break;

	break;

	default:
		# code...
	break;
}