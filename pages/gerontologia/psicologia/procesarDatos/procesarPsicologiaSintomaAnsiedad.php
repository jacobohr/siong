<?php 
require "../../../../session.php";

$tipo = $_REQUEST["tipo"];
$idMatriculaDato = $_REQUEST["idMatriculaDato"];
$idRegistro = $_REQUEST["idRegistro"];

$sintoma = $_POST['sintoma'];

$fecha = strtotime($_POST['fecha']);
$ano = date("Y",$fecha);
$dia = date("d",$fecha);
$mes = date("m",$fecha);
$hora = date("H:i");

$sqlCedula = "SELECT num_documento FROM gddt_matricula_datos WHERE id_matricula_dato =".$idMatriculaDato;
$queryCedula = mysqli_query($conn, $sqlCedula);
$fetchCedula = mysqli_fetch_row($queryCedula);

switch ($tipo) {
	case 'insert':
		$insert = $db->prepare("INSERT INTO gddt_sintomas_ansiedad_datos (id_matricula_dato,id_sintoma_ansiedad,id_cuenta,dia_mod,mes_mod,ano_mod,hora_mod) VALUES (:id_matricula_dato, :id_sintoma_ansiedad, :id_cuenta, :dia_mod, :mes_mod, :ano_mod, :hora_mod);");
		$insert->bindParam(':id_matricula_dato', $idMatriculaDato);
		$insert->bindParam(':id_sintoma_ansiedad', $sintoma);
		$insert->bindParam(':id_cuenta', $_SESSION['ID_CUENTA']);
		$insert->bindParam(':dia_mod', $dia);
		$insert->bindParam(':mes_mod', $mes);
		$insert->bindParam(':ano_mod', $ano);
		$insert->bindParam(':hora_mod', $hora);
		$insert->execute();

		header('location:'.$base_url.'pages/gerontologia/pacientes.php?param='.$fetchCedula[0]);
		break;
	case 'update':
		$update = $db->prepare("UPDATE gddt_sintomas_ansiedad_datos SET id_sintoma_ansiedad='".$sintoma."',id_cuenta='".$_SESSION['ID_CUENTA']."',dia_mod='".$dia."',mes_mod='".$mes."',ano_mod='".$ano."',hora_mod='".$hora."' WHERE id_sintoma_ansiedad_datos ='".$idRegistro."';");
		$update->execute();
		header('location:'.$base_url.'pages/gerontologia/pacientes.php?param='.$fetchCedula[0]);
		break;
	case 'delete':
		$sqlBuscarCedula = "SELECT md.num_documento FROM gddt_sintomas_ansiedad_datos nd INNER JOIN gddt_matricula_datos md ON md.id_matricula_dato = nd.id_matricula_dato WHERE nd.id_sintoma_ansiedad_datos=".$idRegistro;
		$queryBuscarCedula = mysqli_query($conn, $sqlBuscarCedula);
		$fetchBuscarCedula = mysqli_fetch_row($queryBuscarCedula);
		
		$delete = $db->prepare("DELETE FROM gddt_sintomas_ansiedad_datos WHERE id_sintoma_ansiedad_datos='".$idRegistro."';");
		$delete->execute();
		
		header('location:'.$base_url.'pages/gerontologia/pacientes.php?param='.$fetchBuscarCedula[0]);
		break;

	break;

	default:
		# code...
	break;
}