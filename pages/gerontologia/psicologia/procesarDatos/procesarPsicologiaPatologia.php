<?php 
require "../../../../session.php";
error_reporting(E_ALL);
$tipo = $_REQUEST["tipo"];
$idMatriculaDato = $_REQUEST["idMatriculaDato"];
$idRegistro = $_REQUEST["idRegistro"];

$patologia = $_POST['patologia'];
$capitulo = $_POST['capitulo'];
$enfermedadPsicologia = $_POST['enfermedad'];

$fecha = strtotime($_POST['fecha']);
$ano = date("Y",$fecha);
$dia = date("d",$fecha);
$mes = date("m",$fecha);
$hora = date("H:i");

$sqlCedula = "SELECT num_documento FROM gddt_matricula_datos WHERE id_matricula_dato =".$idMatriculaDato;
$queryCedula = mysqli_query($conn, $sqlCedula);
$fetchCedula = mysqli_fetch_row($queryCedula);

switch ($tipo) {
	case 'insert':
		$insert = $db->prepare("INSERT INTO gddt_patologias_usuarios_psicologia (id_matricula_dato, id_patologia, ano_mod, mes_mod, dia_mod, id_cuenta, hora_mod, id_capitulo, id_enfermedad) VALUES (:id_matricula_dato, :id_patologia, :ano_mod, :mes_mod, :dia_mod, :id_cuenta, :hora_mod, :id_capitulo, :id_enfermedad);");
		$insert->bindParam(':id_matricula_dato', $idMatriculaDato);	
		$insert->bindParam(':id_patologia', $patologia);
		$insert->bindParam(':ano_mod', $ano);
		$insert->bindParam(':mes_mod', $mes);
		$insert->bindParam(':dia_mod', $dia);
		$insert->bindParam(':id_cuenta', $_SESSION['ID_CUENTA']);
		$insert->bindParam(':hora_mod', $hora);
		$insert->bindParam(':id_capitulo', $capitulo);
		$insert->bindParam(':id_enfermedad', $enfermedadPsicologia);

		$insert->execute();
		header('location:'.$base_url.'pages/gerontologia/pacientes.php?param='.$fetchCedula[0]);
		break;
	case 'update':
		$update = $db->prepare("UPDATE gddt_patologias_usuarios_psicologia SET id_cuenta='".$_SESSION['ID_CUENTA']."',dia_mod='".$dia."',mes_mod='".$mes."',ano_mod='".$ano."',hora_mod='".$hora."',id_patologia='".$patologia."', id_capitulo='". $capitulo."',id_enfermedad='".$enfermedadPsicologia."'  WHERE id_patologia_usuario ='".$idRegistro."';");
		$update->execute();
		header('location:'.$base_url.'pages/gerontologia/pacientes.php?param='.$fetchCedula[0]);
		break;
	case 'delete':
		$sqlBuscarCedula = "SELECT md.num_documento FROM gddt_patologias_usuarios_psicologia nd INNER JOIN gddt_matricula_datos md ON md.id_matricula_dato = nd.id_matricula_dato WHERE nd.id_patologia_usuario=".$idRegistro;
		$queryBuscarCedula = mysqli_query($conn, $sqlBuscarCedula);
		$fetchBuscarCedula = mysqli_fetch_row($queryBuscarCedula);
		
		$delete = $db->prepare("DELETE FROM gddt_patologias_usuarios_psicologia WHERE id_patologia_usuario='".$idRegistro."';");
		$delete->execute();
		
		header('location:'.$base_url.'pages/gerontologia/pacientes.php?param='.$fetchBuscarCedula[0]);
		break;

	break;

	default:
		# code...
	break;
}