<?php 
require "../../../../session.php";

$tipo = $_REQUEST["tipo"];
$idMatriculaDato = $_REQUEST["idMatriculaDato"];
$idRegistro = $_REQUEST["idRegistro"];

$relacionCompañeros = $_POST['relacionCompañeros'];
$participaActividades = $_POST['participaActividades'];
$observaciones = $_POST['observaciones'];

$fecha = strtotime($_POST['fecha']);
$ano = date("Y",$fecha);
$dia = date("d",$fecha);
$mes = date("m",$fecha);
$hora = date("H:i");

$sqlCedula = "SELECT num_documento FROM gddt_matricula_datos WHERE id_matricula_dato =".$idMatriculaDato;
$queryCedula = mysqli_query($conn, $sqlCedula);
$fetchCedula = mysqli_fetch_row($queryCedula);

switch ($tipo) {
	case 'insert':
		$insert = $db->prepare("INSERT INTO gddt_relaciones_sociales (id_cuenta,ano_mod,mes_mod,dia_mod,hora_mod,id_matricula_dato,relaciones_companeros,participa_actividades,observaciones_sociales) VALUES(:id_cuenta, :ano_mod, :mes_mod, :dia_mod, :hora_mod, :id_matricula_dato, :relaciones_companeros, :participa_actividades, :observaciones_sociales);");
		$insert->bindParam(':id_cuenta', $_SESSION['ID_CUENTA']);
		$insert->bindParam(':ano_mod', $ano);
		$insert->bindParam(':mes_mod', $mes);
		$insert->bindParam(':dia_mod', $dia);
		$insert->bindParam(':hora_mod', $hora);
		$insert->bindParam(':id_matricula_dato', $idMatriculaDato);
		$insert->bindParam(':relaciones_companeros', $relacionCompañeros);
		$insert->bindParam(':participa_actividades', $participaActividades);
		$insert->bindParam(':observaciones_sociales', $observaciones);
		$insert->execute();

		header('location:'.$base_url.'pages/gerontologia/pacientes.php?param='.$fetchCedula[0]);
		break;
	case 'update':
		$update = $db->prepare("UPDATE gddt_relaciones_sociales SET id_cuenta='".$_SESSION['ID_CUENTA']."',dia_mod='".$dia."',mes_mod='".$mes."',ano_mod='".$ano."',hora_mod='".$hora."',relaciones_companeros='".$relacionCompañeros."',participa_actividades='".$participaActividades."',observaciones_sociales='".$observaciones."' WHERE id_relaciones_sociales='".$idRegistro."';");
		$update->execute();
		header('location:'.$base_url.'pages/gerontologia/pacientes.php?param='.$fetchCedula[0]);
		break;
	case 'delete':
		$sqlBuscarCedula = "SELECT md.num_documento FROM gddt_relaciones_sociales nd INNER JOIN gddt_matricula_datos md ON md.id_matricula_dato = nd.id_matricula_dato WHERE nd.id_relaciones_sociales=".$idRegistro;
		$queryBuscarCedula = mysqli_query($conn, $sqlBuscarCedula);
		$fetchBuscarCedula = mysqli_fetch_row($queryBuscarCedula);

		$delete = $db->prepare("DELETE FROM gddt_relaciones_sociales WHERE id_relaciones_sociales='".$idRegistro."';");
		$delete->execute();
		
		header('location:'.$base_url.'pages/gerontologia/pacientes.php?param='.$fetchBuscarCedula[0]);
		break;

	break;

	default:
		# code...
	break;
}