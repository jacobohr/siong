<?php

//include connection file 
require "../../../../session.php";

$id_matricula_dato = $_POST['id'];
//$params = $columns = $totalRecords = $data = array();

$params = filter_input_array(INPUT_POST);

//var_dump($params);

//define index of column
$columns = array(
    0 => 'numero',
    1 => 'id_postura',
    2 => 'nombreUsuario',
    3 => 'fecha',
    4 => 'hora_mod'
);

$sqlTot = $sqlRec = "";

// check search value exist
$where = " WHERE p.id_matricula_dato = $id_matricula_dato";

if (isset($params['search']['value']) && $params['search']['value'] != '') {
    $where .= "
            AND 
                (id_postura LIKE '%" . $params['search']['value'] . "%' 
                OR UPPER((CONVERT(CAST(CONVERT(CONCAT(c.primer_nombre,' ',c.segundo_nombre,' ',c.primer_apellido,' ',c.segundo_apellido) USING latin1) AS BINARY) USING utf8))) LIKE '%" . $params['search']['value'] . "%'
                OR CONCAT(p.ano_mod,'/',p.mes_mod,'/',p.dia_mod) LIKE '%" . $params['search']['value'] . "%'
                OR hora_mod LIKE '%" . $params['search']['value'] . "%')";
}


// getting total number records without any search
$sql = "SELECT 
        ROW_NUMBER() OVER() AS numero,
        p.id_postura, 
        UPPER((CONVERT(CAST(CONVERT(CONCAT(c.primer_nombre,' ',c.segundo_nombre,' ',c.primer_apellido,' ',c.segundo_apellido) USING latin1) AS BINARY) USING utf8))) AS nombreUsuario,
        CONCAT(p.ano_mod,'/',p.mes_mod,'/',p.dia_mod) AS fecha, 
        p.hora_mod
        FROM gddt_postura p
        INNER JOIN gddt_cuentas c ON c.id_cuenta = p.id_cuenta ";


$sqlTot .= $sql;
$sqlRec .= $sql;



//concatenate search sql if value exist
if (isset($where) && $where != '') {
    $sqlTot .= $where;
    $sqlRec .= $where;
}

$sqlRec .= " ORDER BY " . $columns[$params['order'][0]['column']] . "  " . $params['order'][0]['dir'] . "  LIMIT " . $params['start'] . " ," . $params['length'] . " ";

$queryTot = mysqli_query($conn, $sqlTot) or die("database error:" . mysqli_error($conn));

$totalRecords = mysqli_num_rows($queryTot);

$queryRecords = mysqli_query($conn, $sqlRec) or die("error to fetch employees data");

$data = array();
//iterate on results row and create new index array of data
while ($row = mysqli_fetch_row($queryRecords)) {
    $data[] = $row;
}

foreach ($data as $key => $val) {

    /*if (!in_array("82", $_SESSION["RESTRICCIONES"])) {*/
        $data[$key][5] = '
        &nbsp;
        <a href="#" style="color:#000;" onClick="fnVerFisioterapiaPostura(\'' . $val[1] . '\');">
        <span data-toggle="tooltip" title="Ver usuario" class="fa fa-eye"></span>
        </a>';    

        $data[$key][5] .= '
        &nbsp;
        <a href="#" style="color:#000;" data-toggle="modal" data-target="#modalModificarUsuario" onClick="fnEditarFisioterapiaPostura(\'' . $val[1] . '\');">
        <span data-toggle="tooltip" title="Editar usuario" class="fas fa-pencil-alt"></span>
        </a>
        ';
        $data[$key][5] .= '
        &nbsp;
        <a href="#" style="color:#000;" data-toggle="modal" data-target="#modalEliminarUsuario" onClick="fnEnviarParam(\'' . $val[1] . '\');">
        <span data-toggle="tooltip" title="Eliminar usuario" class="fa fa-times"></span>
        </a>';

}

$json_data = array(
    "draw" => intval($params['draw']),
    "recordsTotal" => intval($totalRecords),
    "recordsFiltered" => intval($totalRecords),
    "data" => $data   // total data array
);

echo json_encode($json_data);  // send data as json format