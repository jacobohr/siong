<?php

//include connection file 
require "../../../../session.php";

$sqlUsuario = "
SELECT 
UPPER((CONVERT(CAST(CONVERT(CONCAT(c.primer_nombre,' ',c.segundo_nombre,' ',c.primer_apellido,' ',c.segundo_apellido) USING latin1) AS BINARY) USING utf8))) AS nombreUsuario,
CONCAT(tm.ano_mod,'/',tm.mes_mod,'/',tm.dia_mod) AS fecha,  
tm.hora_mod,
(CONVERT(CAST(CONVERT(tm.dominio_cardiovascular_pulmonar USING latin1) AS BINARY) USING utf8))AS dominio_cardiovascular_pulmonar,
tm.manipulacion, 
tm.reflejo_tonico, 
tm.reflejo_osteotendinoso, 
(CONVERT(CAST(CONVERT(tm.inversion_reciproca USING latin1) AS BINARY) USING utf8))AS inversion_reciproca,
(CONVERT(CAST(CONVERT(tm.inversion_reciproca_observaciones  USING latin1) AS BINARY) USING utf8))AS inversion_reciproca_observaciones 
FROM gddt_tonos_musculares tm
INNER JOIN gddt_cuentas c ON c.id_cuenta = tm.id_cuenta 
WHERE tm.id_tono_muscular =  " . $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);
$rows = mysqli_num_rows($queryUsuario);

if($rows == 0){
    echo "<p style='color:red;font-size:0.9em;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No se encontraron datos :(</p>";
}else{


    ?>

    <div class="content">
        <h6 class="text-center mb-4">Actualizado por <?= $fetchUsuario[0];?> el <?= $fetchUsuario[1]; ?> a las <?= $fetchUsuario[2]; ?></h6>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Manipulación
            </label>
            <p><?php if(empty($fetchUsuario[4])){echo "Sin dato";}else{echo $fetchUsuario[4];} ?></p>
        </div>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Reflejos Tónico
            </label>
            <p><?php if(empty($fetchUsuario[5])){echo "Sin dato";}else{echo $fetchUsuario[5];} ?></p>
        </div>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Reflejos osteotendinosos
            </label>
            <p><?php if(empty($fetchUsuario[6])){echo "Sin dato";}else{echo $fetchUsuario[6];} ?></p>
        </div>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Inervación recíproca
            </label>
            <p><?php if(empty($fetchUsuario[7])){echo "Sin dato";}else{echo $fetchUsuario[7];} ?></p>
        </div>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Observaciones
            </label>
            <p><?php if(empty($fetchUsuario[8])){echo "Sin dato";}else{echo $fetchUsuario[8];} ?></p>
        </div>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Dominio cardiovascular y pulmonar
            </label>
            <p><?php if(empty($fetchUsuario[3])){echo "Sin dato";}else{echo $fetchUsuario[3];} ?></p>
        </div>


        <div class="user-block">&nbsp;</div>

    </div><!-- Fin del content -->
    <?php } ?>