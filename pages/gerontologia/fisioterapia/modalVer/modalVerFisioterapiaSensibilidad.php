<?php

//include connection file 
require "../../../../session.php";

$sqlUsuario = "
SELECT 
UPPER((CONVERT(CAST(CONVERT(CONCAT(c.primer_nombre,' ',c.segundo_nombre,' ',c.primer_apellido,' ',c.segundo_apellido) USING latin1) AS BINARY) USING utf8))) AS nombreUsuario,
CONCAT(s.ano_mod,'/',s.mes_mod,'/',s.dia_mod) AS fecha,  
s.hora_mod,
s.superficial,
(CONVERT(CAST(CONVERT(s.observacion_superficial USING latin1) AS BINARY) USING utf8))AS observacion_superficial,
s.profunda,
(CONVERT(CAST(CONVERT(s.observacion_profunda USING latin1) AS BINARY) USING utf8))AS observacion_profunda,
s.cortical,
(CONVERT(CAST(CONVERT(s.observacion_cortical USING latin1) AS BINARY) USING utf8))AS observacion_cortical,
(CONVERT(CAST(CONVERT(s.dolor USING latin1) AS BINARY) USING utf8))AS dolor
FROM gddt_sensibilidades s
INNER JOIN gddt_cuentas c ON c.id_cuenta = s.id_cuenta 
WHERE s.id_sensibilidad =  " . $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);
$rows = mysqli_num_rows($queryUsuario);

if($rows == 0){
    echo "<p style='color:red;font-size:0.9em;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No se encontraron datos :(</p>";
}else{


    ?>

    <div class="content">
        <h6 class="text-center mb-4">Actualizado por <?= $fetchUsuario[0];?> el <?= $fetchUsuario[1]; ?> a las <?= $fetchUsuario[2]; ?></h6>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Superficial
            </label>
            <p><?php if(empty($fetchUsuario[3])){echo "Sin dato";}else{echo $fetchUsuario[3];} ?></p>
        </div>
        <div class="user-block col-sm-8 col-xs-6">
            <label>
                Observacion
            </label>
            <p><?php if(empty($fetchUsuario[4])){echo "Sin dato";}else{echo $fetchUsuario[4];} ?></p>
        </div>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Profunda
            </label>
            <p><?php if(empty($fetchUsuario[5])){echo "Sin dato";}else{echo $fetchUsuario[5];} ?></p>
        </div>
        <div class="user-block col-sm-8 col-xs-6">
            <label>
                Observacion
            </label>
            <p><?php if(empty($fetchUsuario[6])){echo "Sin dato";}else{echo $fetchUsuario[6];} ?></p>
        </div>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Cortical
            </label>
            <p><?php if(empty($fetchUsuario[7])){echo "Sin dato";}else{echo $fetchUsuario[7];} ?></p>
        </div>
        <div class="user-block col-sm-8 col-xs-6">
            <label>
                Observacion
            </label>
            <p><?php if(empty($fetchUsuario[8])){echo "Sin dato";}else{echo $fetchUsuario[8];} ?></p>
        </div>
         <div class="user-block col-sm-8 col-xs-6">
            <label>
                Dolor
            </label>
            <p><?php if(empty($fetchUsuario[9])){echo "Sin dato";}else{echo $fetchUsuario[9];} ?></p>
        </div>
        


        <div class="user-block">&nbsp;</div>

    </div><!-- Fin del content -->
    <?php } ?>