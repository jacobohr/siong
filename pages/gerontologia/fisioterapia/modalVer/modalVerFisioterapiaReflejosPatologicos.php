<?php
//include connection file 
require "../../../../session.php";

$sqlUsuario = "
SELECT 
UPPER((CONVERT(CAST(CONVERT(CONCAT(c.primer_nombre,' ',c.segundo_nombre,' ',c.primer_apellido,' ',c.segundo_apellido) USING latin1) AS BINARY) USING utf8))) AS nombreUsuario,
CONCAT(rpd.ano_mod,'/',rpd.mes_mod,'/',rpd.dia_mod) AS fecha,  
rpd.hora_mod,
rpd.id_reflejo_patologico
FROM gddt_reflejos_patologicos_datos rpd
INNER JOIN gddt_cuentas c ON c.id_cuenta = rpd.id_cuenta 
WHERE rpd.id_reflejo_patologico_dato =  " . $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);
$rows = mysqli_num_rows($queryUsuario);

if($rows == 0){
    echo "<p style='color:red;font-size:0.9em;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No se encontraron datos :(</p>";
}else{

    if($fetchUsuario[3] == 0){
        $reflejoPatolgico = 'Sin dato';
    }else{
        $sqlReflejoPatologico = "SELECT id_reflejo_patologico,reflejo_patologico FROM gddt_reflejos_patologicos WHERE id_reflejo_patologico = ".$fetchUsuario[3];
        $queryReflejoPatologico = mysqli_query($conn, $sqlReflejoPatologico);
        $fetchReflejoPatologico = mysqli_fetch_row($queryReflejoPatologico);
    }

    ?>

    <div class="content">
        <h6 class="text-center mb-4">Actualizado por <?= $fetchUsuario[0];?> el <?= $fetchUsuario[1]; ?> a las <?= $fetchUsuario[2]; ?></h6>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Reflejo patologico
            </label>
            <p><?php if(empty($fetchReflejoPatologico[2])){echo "Sin dato";}else{echo $fetchReflejoPatologico[2];} ?></p>
        </div>

        <div class="user-block">&nbsp;</div>

    </div><!-- Fin del content -->
    <?php } ?>