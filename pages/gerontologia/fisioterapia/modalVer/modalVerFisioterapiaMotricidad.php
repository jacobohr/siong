<?php
//include connection file 
require "../../../../session.php";

$sqlUsuario = "
SELECT 
UPPER((CONVERT(CAST(CONVERT(CONCAT(c.primer_nombre,' ',c.segundo_nombre,' ',c.primer_apellido,' ',c.segundo_apellido) USING latin1) AS BINARY) USING utf8))) AS nombreUsuario,
CONCAT(m.ano_mod,'/',m.mes_mod,'/',m.dia_mod) AS fecha,  
m.hora_mod,
m.fina,
(CONVERT(CAST(CONVERT(observacion_fina USING latin1) AS BINARY) USING utf8))AS observacion_fina,
m.gruesa,
(CONVERT(CAST(CONVERT(observacion_gruesa USING latin1) AS BINARY) USING utf8))AS observacion_gruesa
FROM gddt_motricidades m
INNER JOIN gddt_cuentas c ON c.id_cuenta = m.id_cuenta 
WHERE m.id_motricidad =  " . $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);
$rows = mysqli_num_rows($queryUsuario);

if($rows == 0){
    echo "<p style='color:red;font-size:0.9em;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No se encontraron datos :(</p>";
}else{

    ?>

    <div class="content">
        <h6 class="text-center mb-4">Actualizado por <?= $fetchUsuario[0];?> el <?= $fetchUsuario[1]; ?> a las <?= $fetchUsuario[2]; ?></h6>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Fina
            </label>
            <p><?php if(empty($fetchUsuario[3])){echo "Sin dato";}else{echo $fetchUsuario[3];} ?></p>
        </div>
        <div class="user-block col-sm-8 col-xs-6">
            <label>
                Observaciones
            </label>
            <p><?php if(empty($fetchUsuario[4])){echo "Sin dato";}else{echo $fetchUsuario[4];} ?></p>
        </div>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Gruesa
            </label>
            <p><?php if(empty($fetchUsuario[5])){echo "Sin dato";}else{echo $fetchUsuario[5];} ?></p>
        </div>
        <div class="user-block col-sm-8 col-xs-6">
            <label>
                Observaciones
            </label>
            <p><?php if(empty($fetchUsuario[6])){echo "Sin dato";}else{echo $fetchUsuario[6];} ?></p>
        </div>

        <div class="user-block">&nbsp;</div>

    </div><!-- Fin del content -->
<?php } ?>