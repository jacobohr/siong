<?php require "../../../../session.php";



$sqlUsuario = "SELECT * FROM gddt_reflejos_patologicos_datos  WHERE id_reflejo_patologico_dato = ". $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);

?>
<form name="editarUsuario" method="POST" action="procesarPsicologiaAntceMental.php?tipo=update" enctype="multipart/form-data">
	<h4 class="text-center mb-4"> Reflejos Patologicos </h4>
	<div class="row">
		<div class="form-group col-md-4">
			<label for="recipient-name" class="form-control-label">Usuario que registra <i style="color: darkorange">*</i></label>
			<select class="form-control" name="usuario" >
				<option>Selecciona una opción</option>
				<?php
				$queryCuenta = mysqli_query($conn, "SELECT id_cuenta,
					UPPER((CONVERT(CAST(CONVERT(CONCAT(primer_nombre,' ',segundo_nombre,' ',primer_apellido,' ',segundo_apellido) USING latin1) AS BINARY) USING utf8))) AS nombreUsuario FROM gddt_cuentas ;");
				while ($fetchCuenta = mysqli_fetch_row($queryCuenta)) {
					if ($fetchCuenta[0] == $fetchUsuario[3]) {
						echo "<option selected value=" . $fetchCuenta[0] . ">" . $fetchCuenta[1] . "</option>";
					} else {
						echo "<option value=" . $fetchCuenta[0] . ">" . $fetchCuenta[1] . "</option>";
					}
				}
				?>
			</select>
		</div>
		<?php 
		if(strlen($fetchUsuario[5]) < 2){$mes = '0'.$fetchUsuario[5];}else{$mes = $fetchUsuario[5];}
		if(strlen($fetchUsuario[6]) < 2){$dia = '0'.$fetchUsuario[6];}else{$dia = $fetchUsuario[6];}
		$year = $fetchUsuario[4];
		$fecha = $year.'-'.$mes.'-'.$dia;
		?>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Fecha de modificacion <i style="color: darkorange">*</i></label>
			<input type="date" class="form-control" name="fechaMod" value="<?= $fecha ?>"/>
		</div>
		<div class="form-group col-md-4">
			<label for="recipient-name" class="form-control-label">Reflejo Patologico <i style="color: darkorange">*</i></label>
			<select class="form-control" name="grupoSocial" >
				<option>Selecciona una opción</option>
				<?php
				$queryReflejoPatologico = mysqli_query($conn, "SELECT id_reflejo_patologico,reflejo_patologico FROM gddt_reflejos_patologicos ;");
				while ($fetchReflejoPatologico = mysqli_fetch_row($queryReflejoPatologico)) {
					if ($fetchReflejoPatologico[0] == $fetchUsuario[2]) {
						echo "<option selected value=" . $fetchReflejoPatologico[0] . ">" . $fetchReflejoPatologico[1] . "</option>";
					} else {
						echo "<option value=" . $fetchReflejoPatologico[0] . ">" . $fetchReflejoPatologico[1] . "</option>";
					}
				}
				?>
			</select>
		</div>
	</div>
	<!-- End Row -->


	<!-- Modal Footer -->
	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">
			Cancelar
		</button>
		<button type="submit" class="btn btn-primary" name="Actualizar">
			Actualizar
		</button>
	</div>
