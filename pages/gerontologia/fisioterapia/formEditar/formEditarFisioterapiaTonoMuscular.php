<?php require "../../../../session.php";



$sqlUsuario = "SELECT 
*,
(CONVERT(CAST(CONVERT(dominio_cardiovascular_pulmonar USING latin1) AS BINARY) USING utf8))AS dominio_cardiovascular_pulmonar,
(CONVERT(CAST(CONVERT(inversion_reciproca USING latin1) AS BINARY) USING utf8))AS inversion_reciproca,
(CONVERT(CAST(CONVERT(inversion_reciproca_observaciones  USING latin1) AS BINARY) USING utf8))AS inversion_reciproca_observaciones 
FROM gddt_tonos_musculares
WHERE id_tono_muscular = ". $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);

?>
<form name="editarUsuario" method="POST" action="procesarPsicologiaAntceMental.php?tipo=update" enctype="multipart/form-data">
	<h4 class="text-center mb-4"> Tono Muscular</h4>
	<div class="row">
		<div class="form-group col-md-4">
			<label for="recipient-name" class="form-control-label">Usuario que registra <i style="color: darkorange">*</i></label>
			<select class="form-control" name="usuario" >
				<option>Selecciona una opción</option>
				<?php
				$queryCuenta = mysqli_query($conn, "SELECT id_cuenta,
					UPPER((CONVERT(CAST(CONVERT(CONCAT(primer_nombre,' ',segundo_nombre,' ',primer_apellido,' ',segundo_apellido) USING latin1) AS BINARY) USING utf8))) AS nombreUsuario FROM gddt_cuentas ;");
				while ($fetchCuenta = mysqli_fetch_row($queryCuenta)) {
					if ($fetchCuenta[0] == $fetchUsuario[1]) {
						echo "<option selected value=" . $fetchCuenta[0] . ">" . $fetchCuenta[1] . "</option>";
					} else {
						echo "<option value=" . $fetchCuenta[0] . ">" . $fetchCuenta[1] . "</option>";
					}
				}
				?>
			</select>
		</div>
		<?php 
		if(strlen($fetchUsuario[3]) < 2){$mes = '0'.$fetchUsuario[3];}else{$mes = $fetchUsuario[3];}
		if(strlen($fetchUsuario[4]) < 2){$dia = '0'.$fetchUsuario[4];}else{$dia = $fetchUsuario[4];}
		$year = $fetchUsuario[2];
		$fecha = $year.'-'.$mes.'-'.$dia;
		?>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Fecha de modificacion <i style="color: darkorange">*</i></label>
			<input type="date" class="form-control" name="fechaMod" value="<?= $fecha ?>"/>
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Dominio cardiovascular y pulmonar <i style="color: darkorange">*</i></label>
			<input type="text" class="form-control" name="dominioCardiovascularPulmonar" value="<?= $fetchUsuario[13] ?>"/>
		</div>
		<div class="form-group col-md-4">
			<label for="recipient-name" class="form-control-label" >Manipulacion  <i style="color: darkorange">*</i></label>
			<?php 
			$selectManipulacion = array(
				0 => 'Ninguno',
				1 => 'Rueda dentada',
				2 => 'Tubo plomo',
				3 => 'Navaja'
			);
			?>
			<select class="form-control" name="manipulacion" >
				<option>Selecciona una opción</option>
				<?php
				foreach ($selectManipulacion as $key) {
					if($key == $fetchUsuario[8]){
						echo '<option selected value='.$key.'>'.$key.'</option>';
					}else{
						echo '<option value='.$key.'>'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="recipient-name" class="form-control-label" >Reflejos tonicos  <i style="color: darkorange">*</i></label>
			<?php 
			$selectReflejosTonicos = array(
				0 => 'Ninguno',
				1 => 'Tono cervical asimétrico',
				2 => 'Tono cervical simétrico'
			);
			?>
			<select class="form-control" name="reflejosTonico" >
				<option>Selecciona una opción</option>
				<?php
				foreach ($selectReflejosTonicos as $key) {
					if($key == $fetchUsuario[9]){
						echo '<option selected value='.$key.'>'.$key.'</option>';
					}else{
						echo '<option value='.$key.'>'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="recipient-name" class="form-control-label" >Reflejos osteotendinoso  <i style="color: darkorange">*</i></label>
			<?php 
			$selectReflejosOsteotendinosos = array(
				0 => 'Arreflexia',
				1 => 'Eurreflexia',
				2 => 'Hiporreflexia',
				3 => 'Hiperreflexia'
			);
			?>
			<select class="form-control" name="reflejosOsteotedinosos" >
				<option>Selecciona una opción</option>
				<?php
				foreach ($selectReflejosOsteotendinosos as $key) {
					if($key == $fetchUsuario[10]){
						echo '<option selected value='.$key.'>'.$key.'</option>';
					}else{
						echo '<option value='.$key.'>'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="recipient-name" class="form-control-label" >Inervación recíproca  <i style="color: darkorange">*</i></label>
			<?php 
			$selectReflejosOsteotendinoso = array(
				0 => 'Eutónico',
				1 => 'Hipotónico',
				2 => 'Flacidez',
				3 => 'Espasticidad',
				4 => 'Rigidez'
			);
			?>
			<select class="form-control" name="inervacionReciproca" >
				<option>Selecciona una opción</option>
				<?php
				foreach ($selectReflejosOsteotendinoso as $key) {
					if($key == $fetchUsuario[14]){
						echo '<option selected value='.$key.'>'.$key.'</option>';
					}else{
						echo '<option value='.$key.'>'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label">Observaciones <i style="color: darkorange">*</i></label>
			<textarea class="form-control" name="observaciones" cols="40" rows="5" ><?= $fetchUsuario[15] ?></textarea>
		</div>
		
	</div>
	<!-- End Row -->


	<!-- Modal Footer -->
	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">
			Cancelar
		</button>
		<button type="submit" class="btn btn-primary" name="Actualizar">
			Actualizar
		</button>
	</div>
