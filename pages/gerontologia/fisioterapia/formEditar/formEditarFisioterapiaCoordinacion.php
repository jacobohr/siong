<?php require "../../../../session.php";



$sqlUsuario = "SELECT*,
(CONVERT(CAST(CONVERT(co.observacion_estatica USING latin1) AS BINARY) USING utf8))AS observacion_estatica,
(CONVERT(CAST(CONVERT(co.observacion_dinamica USING latin1) AS BINARY) USING utf8))AS observacion_dinamica
FROM gddt_coordinaciones co
WHERE co.id_coordinacion =  ". $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);

?>
<form name="editarUsuario" method="POST" action="procesarPsicologiaAntceMental.php?tipo=update" enctype="multipart/form-data">
	<h4 class="text-center mb-4"> Coordinacion </h4>
	<div class="row">
		<div class="form-group col-md-4">
			<label for="recipient-name" class="form-control-label">Usuario que registra <i style="color: darkorange">*</i></label>
			<select class="form-control" name="usuario" >
				<option>Selecciona una opción</option>
				<?php
				$queryCuenta = mysqli_query($conn, "SELECT id_cuenta,
					UPPER((CONVERT(CAST(CONVERT(CONCAT(primer_nombre,' ',segundo_nombre,' ',primer_apellido,' ',segundo_apellido) USING latin1) AS BINARY) USING utf8))) AS nombreUsuario FROM gddt_cuentas ;");
				while ($fetchCuenta = mysqli_fetch_row($queryCuenta)) {
					if ($fetchCuenta[0] == $fetchUsuario[1]) {
						echo "<option selected value=" . $fetchCuenta[0] . ">" . $fetchCuenta[1] . "</option>";
					} else {
						echo "<option value=" . $fetchCuenta[0] . ">" . $fetchCuenta[1] . "</option>";
					}
				}
				?>
			</select>
		</div>
		<?php 
		if(strlen($fetchUsuario[3]) < 2){$mes = '0'.$fetchUsuario[3];}else{$mes = $fetchUsuario[3];}
		if(strlen($fetchUsuario[4]) < 2){$dia = '0'.$fetchUsuario[4];}else{$dia = $fetchUsuario[4];}
		$year = $fetchUsuario[2];
		$fecha = $year.'-'.$mes.'-'.$dia;
		?>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Fecha de modificacion <i style="color: darkorange">*</i></label>
			<input type="date" class="form-control" name="fechaMod" value="<?= $fecha ?>"/>
		</div>
		<div class="form-group col-md-12">
			<label for="recipient-name" class="form-control-label" >Oculopédica  <i style="color: darkorange">*</i></label>
			<?php 
			$selectOculopedica = array(
				0 => 'conservado',
				1 => 'no conservado'
			);
			?>
			<select class="form-control" name="oculopedica" >
				<option>Selecciona una opción</option>
				<?php
				foreach ($selectOculopedica as $key) {
					if($key == $fetchUsuario[7]){
						echo '<option selected value='.$key.'>'.$key.'</option>';
					}else{
						echo '<option value='.$key.'>'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-12">
			<textarea class="form-control" name="observacionesOculopedica" cols="40" rows="5" ><?= $fetchUsuario[11] ?></textarea>
		</div>
		<div class="form-group col-md-12">
			<label for="recipient-name" class="form-control-label" >Oculomanual  <i style="color: darkorange">*</i></label>
			<?php 
			$selectOculomanual = array(
				0 => 'conservado',
				1 => 'no conservado'
			);
			?>
			<select class="form-control" name="oculomanual" >
				<option>Selecciona una opción</option>
				<?php
				foreach ($selectOculomanual as $key) {
					if($key == $fetchUsuario[9]){
						echo '<option selected value='.$key.'>'.$key.'</option>';
					}else{
						echo '<option value='.$key.'>'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-12">
			<textarea class="form-control" name="observacionesOculopedica" cols="40" rows="5" ><?= $fetchUsuario[12] ?></textarea>
		</div>
	</div>
	<!-- End Row -->


	<!-- Modal Footer -->
	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">
			Cancelar
		</button>
		<button type="submit" class="btn btn-primary" name="Actualizar">
			Actualizar
		</button>
	</div>
