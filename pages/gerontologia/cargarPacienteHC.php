<?php
//include connection file
include_once(dirname(__FILE__) . '/../../session.php');
$docPaciente = $_REQUEST["docPaciente"];
$sqlPaciente = "
SELECT 
md.id_matricula_dato id_matricula,
emp.nom_empresa nom_empresa,
md.empresa,
md.cargo,
UPPER((CONVERT(CAST(CONVERT(CONCAT(cta.primer_nombre,' ',cta.segundo_nombre,' ',cta.primer_apellido,' ',cta.segundo_apellido) USING latin1) AS BINARY) USING utf8))) AS nombre_cuenta,
UPPER((CONVERT(CAST(CONVERT(CONCAT(md.primer_nombre,' ',md.segundo_nombre,' ',md.primer_apellido,' ',md.segundo_apellido) USING latin1) AS BINARY) USING utf8))) AS nombre_completo,
pu.descripcion perfil, 
e.descripcion estado,
tu.descripcion tipo_usuario, 
md.num_documento documento,
UPPER((CONVERT(CAST(CONVERT(mun.descripcion USING latin1) AS BINARY) USING utf8)))AS lugar_documento,
tpdo.descripcion tipo_documento,
gen.nom_genero genero,
ev.descripcion estado_civil,
CONCAT(md.nacimiento_dia,'/',md.nacimiento_mes,'/',md.nacimiento_ano)AS fecha_nacimiento,
CONCAT(md.ingreso_dia,'/',md.ingreso_mes,'/',md.ingreso_ano)AS fecha_ingreso,
p.descripcion procedencia,
ne.descripcion estudios,
md.lectura,
md.escritura,
im.nom_imagen_perfil,
(CONVERT(CAST(CONVERT(CONCAT(UPPER(md.nom_padre)) USING latin1) AS BINARY) USING utf8)) AS nombre_padre,
(CONVERT(CAST(CONVERT(CONCAT(UPPER(md.nom_madre)) USING latin1) AS BINARY) USING utf8)) AS nombre_madre,
(CONVERT(CAST(CONVERT(CONCAT(UPPER(md.nom_acudiente)) USING latin1) AS BINARY) USING utf8)) AS nombre_acudiente,
md.tel_acudiente,
(CONVERT(CAST(CONVERT(CONCAT(md.antecedente_laboral) USING latin1) AS BINARY) USING utf8)) AS antecedente_laboral,
md.motivo_ingreso,
md.ficha_remision_ingreso,
md.cual_pais,
md.aseguradora,
md.telefono_usuario,
(CONVERT(CAST(CONVERT(CONCAT(UPPER(md.diagnostico_base)) USING latin1) AS BINARY) USING utf8)) AS diagnostico_base,
md.entidad,
md.id_lugar_nacimiento,
md.id_procedencia_municipio
FROM gddt_matricula_datos md
INNER JOIN gddt_empresas emp 		ON md.id_empresa = emp.id_empresa
INNER JOIN gddt_cuentas cta  		ON md.id_cuenta = cta.id_cuenta	 
INNER JOIN gddt_tipo_documento tpdo 	ON md.id_tipo_documento = tpdo.id_tipo_documento
INNER JOIN gddt_perfil_usuarios pu	ON md.id_perfil_usuario = pu.id_perfil_usuario 
INNER JOIN gddt_estados e  		ON md.id_estado = e.id_estado
INNER JOIN gddt_tipo_usuarios tu	ON md.id_tipo_usuario = tu.id_tipo_usuario 
INNER JOIN gddt_procedencias p  	ON md.id_procedencia = p.id_procedencia 
INNER JOIN gddt_generos gen		ON md.id_genero = gen.id_genero
INNER JOIN gddt_municipios mun		ON md.id_lugar_documento = mun.id_municipio
INNER JOIN gddt_estados_civiles ev	ON md.id_estado_civil = ev.id_estado_civil
INNER JOIN gddt_nivel_estudios ne	ON md.id_nivel_estudio = ne.id_nivel_estudio
INNER JOIN gddt_imagen_perfiles	im	ON md.id_imagen_perfil = im.id_imagen_perfil
WHERE md.num_documento = '".$docPaciente."'";

$queryPaciente = mysqli_query($conn, $sqlPaciente);
$numPaciente = mysqli_num_rows($queryPaciente);


if ($numPaciente == 0) {
	echo "<p style='color:red;font-size:0.9em;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Número de documento no encontrado. Presione el botón de 'Registrar Nuevo Paciente' para registrar un paciente nuevo.</p>";
} else {
	$fetchPaciente = mysqli_fetch_row($queryPaciente);


	$sqlNacimiento = "SELECT nacimiento_ano FROM gddt_matricula_datos WHERE num_documento = '".$docPaciente."'";
	$queryNacimiento = mysqli_query($conn, $sqlNacimiento);
	$fetchNacimiento = mysqli_fetch_row($queryNacimiento);
	$edad = date("Y") - $fetchNacimiento[0];
	

	?>

	<style type="text/css">

		/* custom inclusion of right, left and below tabs */

		.tabs-below > .nav-tabs,
		.tabs-right > .nav-tabs,
		.tabs-left > .nav-tabs {
			border-bottom: 0;
		}

		.tab-content > .tab-pane,
		.pill-content > .pill-pane {
			display: none;
		}

		.tab-content > .active,
		.pill-content > .active {
			display: block;
		}

		.tabs-below > .nav-tabs {
			border-top: 1px solid #ddd;
		}

		.tabs-below > .nav-tabs > li {
			margin-top: -1px;
			margin-bottom: 0;
		}

		.tabs-below > .nav-tabs > li > a {
			-webkit-border-radius: 0 0 4px 4px;
			-moz-border-radius: 0 0 4px 4px;
			border-radius: 0 0 4px 4px;
		}

		.tabs-below > .nav-tabs > li > a:hover,
		.tabs-below > .nav-tabs > li > a:focus {
			border-top-color: #ddd;
			border-bottom-color: transparent;
		}

		.tabs-below > .nav-tabs > .active > a,
		.tabs-below > .nav-tabs > .active > a:hover,
		.tabs-below > .nav-tabs > .active > a:focus {
			border-color: transparent #ddd #ddd #ddd;
		}

		.tabs-left > .nav-tabs > li,
		.tabs-right > .nav-tabs > li {
			float: none;
		}

		.tabs-left > .nav-tabs > li > a,
		.tabs-right > .nav-tabs > li > a {
			min-width: 74px;
			margin-right: 0;
			margin-bottom: 3px;
		}

		.tabs-left > .nav-tabs {
			float: left;
			margin-right: 19px;
			border-right: 1px solid #ddd;
		}

		.tabs-left > .nav-tabs > li > a {
			margin-right: -1px;
			-webkit-border-radius: 4px 0 0 4px;
			-moz-border-radius: 4px 0 0 4px;
			border-radius: 4px 0 0 4px;
		}

		.tabs-left > .nav-tabs > li > a:hover,
		.tabs-left > .nav-tabs > li > a:focus {
			border-color: #eeeeee #dddddd #eeeeee #eeeeee;
		}

		.tabs-left > .nav-tabs .active > a,
		.tabs-left > .nav-tabs .active > a:hover,
		.tabs-left > .nav-tabs .active > a:focus {
			border-color: #ddd transparent #ddd #ddd;
			*border-right-color: #ffffff;
		}

		.tabs-right > .nav-tabs {
			float: right;
			margin-left: 19px;
			border-left: 1px solid #ddd;
		}

		.tabs-right > .nav-tabs > li > a {
			margin-left: -1px;
			-webkit-border-radius: 0 4px 4px 0;
			-moz-border-radius: 0 4px 4px 0;
			border-radius: 0 4px 4px 0;
		}

		.tabs-right > .nav-tabs > li > a:hover,
		.tabs-right > .nav-tabs > li > a:focus {
			border-color: #eeeeee #eeeeee #eeeeee #dddddd;
		}

		.tabs-right > .nav-tabs .active > a,
		.tabs-right > .nav-tabs .active > a:hover,
		.tabs-right > .nav-tabs .active > a:focus {
			border-color: #ddd #ddd #ddd transparent;
			border-left-color: #ffffff;
		}

	</style>

	<!-- Main content -->
	<section class="content">
		<div class="row m-2">
			<div class="col-md-3" id="box-left-profile">
				<!-- Profile Image -->
				<div class="box box-primary" style="border-top: 3px solid #17A2B8; border-bottom: 1px solid #ccc; border-radius: 3px">
					<div class="col-md-12" style="margin-bottom:10px; text-align:right;">
						<i id="icon-profile-show-hide" class="fa fa-minus" style="cursor:pointer;"
						title="Ocultar Perfil" onclick="fnOcultarMostrarPerfil();"></i>
					</div>
					<div class="box-body box-profile text-center">
						<?php
						if (is_file('../../images/perfiles/' . $fetchPaciente[20].'.jpg')) { ?>
							<img class="profile-user-img img-responsive img-circle" style="width: 100px;border-color: #17A2B8; height: 130px"
							src="<?php echo $base_url; ?>images/perfiles/<?php echo $fetchPaciente[20]; ?>.jpg"
							alt="Usuario">
						<?php } else { ?>
							<img class="profile-user-img img-responsive img-circle"
							src="<?php echo $base_url; ?>/images/user-icon.png" alt="Usuario">
						<?php } ?>

						<div id="box-profile" >
							<h3 class="profile-username text-center">
								<?php echo $fetchPaciente[5]; ?>
								<a href="JavaScript:void(0)" data-toggle="modal"
								data-target="#modalActualizaPaciente"
								style="color:#000" onClick="fnCargarFormModal();">
								<i class="fa fa-pencil"></i>
							</a>
						</h3>
						<p class="text-muted text-center m-1"><?php echo 'Documento: '.$fetchPaciente[9]; ?></p>
						<p class="text-muted text-center m-1"><?php echo 'Edad: '.$edad.' años'; ?></p>
						<p class="text-muted text-center m-1"><?php echo 'EPS: '; if(empty($fetchPaciente[32])){echo "Sin dato";}else{echo $fetchPaciente[32];} ?></p>
					</div>
				</div>
			</div>
		</div>

		<!-- /.col -->
		<div class="col-md-9" id="box-right-content">
			<div class="nav-tabs-custom" style="font-size: 14px;">
				<nav>
					<ul class="nav nav-tabs" id="tabPrincipal" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" id="nav-datosPaciente-tab" data-toggle="tab" href="#nav-datosPaciente" role="tab" aria-controls="home" aria-selected="true">Datos Usuario</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="nav-nutricion-tab" data-toggle="tab" href="#nav-nutricion" role="tab" aria-controls="profile" aria-selected="false">Nutricion</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="nav-psicologia-tab" data-toggle="tab" href="#nav-psicologia" role="tab" aria-controls="contact" aria-selected="false">Psicológica</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="nav-medicina-tab" data-toggle="tab" href="#nav-medicina" role="tab" aria-controls="contact" aria-selected="false">Medicina</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="nav-enfermeria-tab" data-toggle="tab" href="#nav-enfermeria" role="tab" aria-controls="contact" aria-selected="false">Enfermeria</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="nav-fisioterapia-tab" data-toggle="tab" href="#nav-fisioterapia" role="tab" aria-controls="contact" aria-selected="false">Fisioterapia</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="nav-gerontologia-tab" data-toggle="tab" href="#nav-geronlogia" role="tab" aria-controls="contact" aria-selected="false">Gerontologia</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="nav-Psiquiatria-tab" data-toggle="tab" href="#nav-Psiquiatria" role="tab" aria-controls="contact" aria-selected="false">Psiquiatria</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="nav-TrabajoSocial-tab" data-toggle="tab" href="#nav-TrabajoSocial" role="tab" aria-controls="contact" aria-selected="false">Trabajo Social</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="nav-TerapiaOcupacional-tab" data-toggle="tab" href="#nav-TerapiaOcupacional" role="tab" aria-controls="contact" aria-selected="false">Terapia Ocupacional</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="nav-test-tab" data-toggle="tab" href="#nav-test" role="tab" aria-controls="contact" aria-selected="false">Test</a>
						</li>
					</ul>
				</nav>
				<div class="tab-content" id="tabPrincipalContent">
					<div class="tab-pane fade show active m-2" id="nav-datosPaciente" role="tabpanel" aria-labelledby="home-tab">
						<div class="post clearfix">
							<div class="row">
								<div class="user-block col-sm-4 col-xs-6">
									<label>
										Fecha de ingreso
									</label>
									<p><?php if(empty($fetchPaciente[15])){echo "Sin dato";}else{echo $fetchPaciente[15];} ?></p>
								</div>

								<div class="user-block col-sm-4 col-xs-6">
									<label>
										Entidad(EPS)
									</label>
									<p><?php if(empty($fetchPaciente[32])){echo "Sin dato";}else{echo $fetchPaciente[32];} ?></p>
								</div>

								<div class="user-block col-sm-4 col-xs-6">
									<label>
										Perfil
									</label>
									<p><?php if(empty($fetchPaciente[6])){echo "Sin dato";}else{echo strtoupper($fetchPaciente[6]);}  ?></p>
								</div>

								<div class="user-block col-sm-4 col-xs-6">
									<label>
										Tipo de usuario
									</label>
									<p><?php echo $fetchPaciente[8]; ?></p>
								</div>

								<div class="user-block col-sm-4 col-xs-6">
									<label>
										Numero de cedula
									</label>
									<p><?php if(empty($fetchPaciente[9])){echo "Sin dato";}else{echo strtoupper($fetchPaciente[9]);} ?></p>
								</div>

								<div class="user-block col-sm-4 col-xs-6">
									<label>
										Expedida en
									</label>
									<p><?php if(empty($fetchPaciente[10])){echo "Sin dato";}else{echo strtoupper($fetchPaciente[10]);} ?></p>
								</div>

								<div class="user-block col-sm-4 col-xs-6">
									<label>
										Tipo de cedula
									</label>
									<p><?php if(empty($fetchPaciente[11])){echo "Sin dato";}else{echo $fetchPaciente[11];} ?></p>
								</div>
								<div class="user-block col-sm-4 col-xs-6">
									<label>
										Genero
									</label>
									<p><?php if(empty($fetchPaciente[12])){echo "Sin dato";}else{echo strtoupper($fetchPaciente[12]);} ?></p>
								</div>
								<div class="user-block col-sm-4 col-xs-6">
									<label>
										Estado civil
									</label>
									<p><?php if(empty($fetchPaciente[13])){echo "Sin dato";}else{echo strtoupper($fetchPaciente[13]);} ?></p>
								</div>

								<div class="user-block col-sm-4 col-xs-6">
									<label>
										Fecha de nacimiento
									</label>
									<p><?php if(empty($fetchPaciente[13])){echo "Sin dato";}else{echo $fetchPaciente[14];} ?></p>
								</div>

								<div class="user-block col-sm-4 col-xs-6">
									<label>
										Edad
									</label>
									<p><?php  echo $edad.' años'; ?></p>
								</div>

								<div class="user-block col-sm-4 col-xs-6">
									<?php 
									$sqlLugarNacimiento = "SELECT UPPER((CONVERT(CAST(CONVERT(mun.descripcion USING latin1) AS BINARY) USING utf8)))AS lugar_nacimiento
									FROM gddt_matricula_datos md
									INNER JOIN gddt_municipios mun ON  mun.id_municipio = $fetchPaciente[33]
									WHERE num_documento = '".$docPaciente."'";
									$queryLugarNacimiento = mysqli_query($conn, $sqlLugarNacimiento);							 
									$fetchLugarNacimiento = mysqli_fetch_row($queryLugarNacimiento);
									?>
									<label>
										Lugar de nacimiento
									</label>
									<p><?php if(empty($fetchLugarNacimiento[0])){echo "Sin dato";}else{echo $fetchLugarNacimiento[0];} ?></p>
								</div>

								<div class="user-block col-sm-4 col-xs-6">
									<label>
										Procedencia
									</label>
									<p><?php if(empty($fetchPaciente[16])){echo "Sin dato";}else{echo strtoupper($fetchPaciente[16]);} ?></p>
								</div>

								<div class="user-block col-sm-4 col-xs-6">
									<?php 
									$sqlMunicipioProcedencia = "SELECT UPPER((CONVERT(CAST(CONVERT(mun.descripcion USING latin1) AS BINARY) USING utf8)))AS lugar_nacimiento
									FROM gddt_matricula_datos md
									INNER JOIN gddt_municipios mun ON  mun.id_municipio = $fetchPaciente[34]
									WHERE num_documento = '".$docPaciente."'";
									$queryMunicipioProcedencia = mysqli_query($conn, $sqlMunicipioProcedencia);							 
									$fetchMunicipioProcedencia = mysqli_fetch_row($queryMunicipioProcedencia);
									?>
									<label>
										Municipio procedencia
									</label>
									<p><?php if(empty($fetchMunicipioProcedencia[0])){echo "Sin dato";}else{echo $fetchMunicipioProcedencia[0];} ?></p>
								</div>

								<div class="user-block col-sm-4 col-xs-6">
									<label>
										Motivo ingreso
									</label>
									<p><?php  if(empty($fetchPaciente[26])){echo "Sin dato";}else{echo $fetchPaciente[26];} ?></p>
								</div>

								<div class="user-block col-sm-4 col-xs-6">
									<label>
										Ficha remision ingreso
									</label>
									<p><?php if(empty($fetchPaciente[27])){echo "Sin dato";}else{echo $fetchPaciente[27];} ?></p>
								</div>

								<div class="user-block col-sm-4 col-xs-6">
									<label>
										Nombre de la madre
									</label>
									<p><?php if(empty($fetchPaciente[22])){echo "Sin dato";}else{echo $fetchPaciente[22];} ?></p>
								</div>
								<div class="user-block col-sm-4 col-xs-6">
									<label>
										Nombre de el padre
									</label>
									<p><?php if(empty($fetchPaciente[21])){echo "Sin dato";}else{echo $fetchPaciente[21];} ?></p>
								</div>



								<div class="user-block col-sm-4 col-xs-6">
									<?php 
									$sqlHijos = "SELECT hijos_vivos,hijos_fallecidos FROM gddt_relaciones_familiares
									WHERE id_matricula_dato = $fetchPaciente[0]";
									$queryHijos = mysqli_query($conn, $sqlHijos);							 
									$fetchHijos = mysqli_fetch_row($queryHijos);
									?>
									<label>
										Hijos vivos
									</label>
									<p><?php  if(empty($fetchHijos)){echo "Sin dato";}else{echo $fetchHijos[0];} ?></p>
								</div>

								<div class="user-block col-sm-4 col-xs-6">
									<label>
										Hijos fallecidos
									</label>
									<p><?php  if(empty($fetchHijos)){echo "Sin dato";}else{echo $fetchHijos[1];} ?></p>
								</div>

								<div class="user-block col-sm-4 col-xs-6">
									<?php 
									$sqlPatologias = "SELECT p.patologia 
									FROM gddt_patologias p
									INNER JOIN gddt_patologias_usuarios pu ON p.id_patologia = pu.id_patologia
									WHERE id_matricula_dato = $fetchPaciente[0]";
									$queryPatologias = mysqli_query($conn, $sqlPatologias);							 
									$fetchPatologias = mysqli_fetch_row($queryPatologias);
									?>
									<label>
										Patologias
									</label>
									<p><?php if(empty($fetchPatologias)){echo "Sin dato";}else{ echo $fetchPatologias[0];} ?></p>
								</div>

								<div class="user-block col-sm-4 col-xs-6">
									<?php 
									$sqlAntecedenteMental = "SELECT a.antecedente 
									FROM gddt_antecedentes a
									INNER JOIN gddt_antecedentes_personales am ON a.id_antecedente = am.id_antecedente 
									WHERE am.id_matricula_dato  = $fetchPaciente[0]";
									$queryAntecedenteMental = $db->query($sqlAntecedenteMental);
									$fetchAntecedenteMental = $queryAntecedenteMental->fetchAll(PDO::FETCH_OBJ);
									
									?>
									<label>
										Antecedentes mentales
									</label>
									<?php 
									if(empty($fetchAntecedenteMental)){
										echo "<p>Sin dato</p>";
									}else{
										foreach ($fetchAntecedenteMental as $antecedente) {
											echo "<p>{$antecedente->antecedente}</p>";
										}
									} 
									?>
								</div>

								<div class="user-block col-sm-4 col-xs-6">
									<?php 
									$sqlAntecedententesFamiliares = "SELECT a.antecedente
									FROM gddt_antecedentes a
									INNER JOIN gddt_antecedentes_familiares af ON a.id_antecedente = af.id_antecedente 
									WHERE af.id_matricula_dato = $fetchPaciente[0]";
									$queryAntecedententesFamiliares = $db->query($sqlAntecedententesFamiliares);
									$fetchAntecedententesFamiliares = $queryAntecedententesFamiliares->fetchAll(PDO::FETCH_OBJ);

									?>
									<label>
										Antecedentes de enfermedades materiales
									</label>
									<?php 
									if(empty($fetchAntecedententesFamiliares)){
										echo "<p>Sin dato</p>";
									}else{
										foreach ($fetchAntecedententesFamiliares as $antecedenteFamiliar) {
											echo "<p>{$antecedenteFamiliar->antecedente}</p>";
										}
									} 
									?>
								</div>

								<div class="user-block col-sm-4 col-xs-6">
									<?php 
									$sqlApoyoFamiliar = "SELECT af.apoyo_familiar 
									FROM gddt_apoyo_familiar af
									INNER JOIN gddt_apoyo_familiar_datos afd ON af.id_apoyo_familiar = afd.id_apoyo_familiar 
									WHERE afd.id_matricula_dato = $fetchPaciente[0]";
									$queryApoyoFamiliar = $db->query($sqlApoyoFamiliar);
									$fetchApoyoFamiliar = $queryApoyoFamiliar->fetchAll(PDO::FETCH_OBJ);
									?>
									<label>
										Apoyo socio familiar
									</label>
									<?php 
									if(empty($fetchApoyoFamiliar)){
										echo "<p>Sin dato</p>";
									}else{
										foreach ($fetchApoyoFamiliar as $apoyo) {
											echo "<p>{$apoyo->apoyo_familiar}</p>";
										}
									} 
									?>
								</div>

								<div class="user-block col-sm-4 col-xs-6">
									<label>
										Escolaridad
									</label>
									<p><?php echo $fetchPaciente[17]; ?></p>
								</div>

								<div class="user-block col-sm-4 col-xs-6">
									<label>
										Sabe escribir
									</label>
									<p><?php if($fetchPaciente[18]=='s'){echo 'Si';}else{echo 'No';} ?></p>
								</div>
								<div class="user-block col-sm-4 col-xs-6">
									<label>
										Sabe leer
									</label>
									<p><?php if($fetchPaciente[18]=='s'){echo 'Si';}else{echo 'No';} ?></p>
								</div>
								<div class="user-block col-sm-4 col-xs-6">
									<label>
										Antecedente laboral
									</label>
									<p><?php if(empty($fetchPaciente[25])){echo "Sin dato";}else{echo $fetchPaciente[25];} ?></p>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane fade m-2" id="nav-nutricion" role="tabpanel" aria-labelledby="nutricion-tab">
						<!-- Datatable side server -->
						<nav>
							<ul class="nav nav-tabs" id="tabNutricion" role="tablist">
								<li class="nav-item">
									<a class="nav-link active" id="nav-nutricion-tab" data-toggle="tab" href="#nav-InfoNutricion" role="tab" aria-controls="InfoNutricion" aria-selected="true">Informacion nutricional</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="nav-seguimientos-tab" data-toggle="tab" href="#nav-seguimientos" role="tab" aria-controls="seguimientos" aria-selected="true">Historial de seguimientos</a>
								</li>
							</ul>
						</nav>
						<div class="tab-content" id="tabNutricionContent">
							<div class="tab-pane fade show active m-2" id="nav-InfoNutricion" role="tabpanel" aria-labelledby="InfoNutricion-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Informacion Nutricional</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevaInformacionNutricional" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listInfoNutricional" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Id Nutricion</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade m-2"  id="nav-seguimientos" role="tabpanel" aria-labelledby="seguimientos-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Seguimientos Nutricionales</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevoSeguimientoNutricional" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listSeguimientoNutricional" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Id Seguimiento</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Tipo Seguimiento</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane fade m-2" id="nav-psicologia" role="tabpanel" aria-labelledby="psicologia-tab" aria-orientation="vertical">
						<nav>
							<ul class="nav nav-tabs" id="tabPsicologia" role="tablist">
								<li class="nav-item">
									<a class="nav-link active" id="nav-AntecedenteMental-tab" data-toggle="tab" href="#nav-AntecedenteMental" role="tab" aria-controls="InfoNutricion" aria-selected="true">Antecedentes Mentales</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="nav-enfermedadesFamiliar-tab" data-toggle="tab" href="#nav-enfermedadesFamiliar" role="tab" aria-controls="enfermedadesFamiliar" aria-selected="true">Enfermedades Familiares</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="nav-patologias-tab" data-toggle="tab" href="#nav-patologias" role="tab" aria-controls="patologias" aria-selected="true">Patologias</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="nav-relacionesFamiliares-tab" data-toggle="tab" href="#nav-relacionesFamiliar" role="tab" aria-controls="relacionesFamiliar" aria-selected="true">Relaciones Familiares</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="nav-apoyoFamiliar-tab" data-toggle="tab" href="#nav-apoyoFamiliar" role="tab" aria-controls="apoyoFamiliar" aria-selected="true">Apoyo Familiar</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="nav-relacionSocial-tab" data-toggle="tab" href="#nav-relacionSocial" role="tab" aria-controls="relacionSocial" aria-selected="true">Relaciones Sociales</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="nav-grupoSocial-tab" data-toggle="tab" href="#nav-grupoSocial" role="tab" aria-controls="grupoSocial" aria-selected="true">Grupos Sociales</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="nav-rasgosPersonalidad-tab" data-toggle="tab" href="#nav-rasgosPersonalidad" role="tab" aria-controls="rasgosPersonalidad" aria-selected="true">Rasgos Personalidad</a>
								</li>	
								<li class="nav-item">
									<a class="nav-link" id="nav-sintomasDepresion-tab" data-toggle="tab" href="#nav-sintomasDepresion" role="tab" aria-controls="sintomasDepresion" aria-selected="true">Sintomas de Depresion</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="nav-sintomasAnsiedad-tab" data-toggle="tab" href="#nav-sintomasAnsiedad" role="tab" aria-controls="sintomasAnsiedad" aria-selected="true">Sintomas de Ansiedad</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="nav-impresionDiagnostica-tab" data-toggle="tab" href="#nav-impresionDiagnostica" role="tab" aria-controls="impresionDiagnostica" aria-selected="true">Impresion Diagnostica</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="nav-evaluacionMultiaxial-tab" data-toggle="tab" href="#nav-evaluacionMultiaxial" role="tab" aria-controls="evaluacionMultiaxial" aria-selected="true">Evaluacion Multiaxial</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="nav-familiograma-tab" data-toggle="tab" href="#nav-familiograma" role="tab" aria-controls="familiograma" aria-selected="true">Familiograma</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="nav-planTratamiento-tab" data-toggle="tab" href="#nav-planTratamiento" role="tab" aria-controls="planTratamiento" aria-selected="true">Plan de tratamientos</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="nav-seguimientoPsicologia-tab" data-toggle="tab" href="#nav-seguimientoPsicologia" role="tab" aria-controls="seguimientoPsicologia" aria-selected="true">Seguimientos</a>
								</li>
							</ul>
						</nav>
						<div class="tab-content" id="tabPsicologia">
							<div class="tab-pane fade show active m-2" id="nav-AntecedenteMental" role="tabpanel" aria-labelledby="AntecedenteMental-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Antecedentes Personales</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevoAntecedenteMental" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listPsiAntcMental" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Id Antecedente</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Antecedente</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade m-2" id="nav-enfermedadesFamiliar" role="tabpanel" aria-labelledby="enfermedadesFamiliar-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Antecedentes de enfermedades familiares</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevoAntecedenteFamiliar" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listPsiAntcFamiliar" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Id Antecedente</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Antecedente</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade m-2" id="nav-patologias" role="tabpanel" aria-labelledby="patologias-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Patologias</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevoPsicologiaPatologias" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listPsiPatologias" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Id Patologia</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade m-2" id="nav-relacionesFamiliar" role="tabpanel" aria-labelledby="relacionesFamiliar-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Relaciones Familiares</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevoRelacionesFamiliares" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listPsiRelacionesFamiliar" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Id relacion familiar</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade m-2" id="nav-apoyoFamiliar" role="tabpanel" aria-labelledby="apoyoFamiliar-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Apoyo Familiar</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevoApoyoFamiliar" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listPsiApoyoFamiliar" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Id Apoyo Familiar</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Tipo de apoyo</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade m-2" id="nav-relacionSocial" role="tabpanel" aria-labelledby="relacionSocial-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Relaciones Sociales</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevoRelacionesSociales" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listPsiRelacionSocial" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Id Relacion social</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade m-2" id="nav-grupoSocial" role="tabpanel" aria-labelledby="grupoSocial-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Grupos Sociales</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevoGrupoSocial" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listPsiGrupoSocial" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Id Grupo Social</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Grupo Social</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade m-2" id="nav-rasgosPersonalidad" role="tabpanel" aria-labelledby="rasgosPersonalidad-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Rasgos  de la Personalidad</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevoRasgosPersonales" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listPsiRasgosPersonalidad" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Id Rasgos Personalidad</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade m-2" id="nav-sintomasDepresion" role="tabpanel" aria-labelledby="sintomasDepresion-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Sintomas de Depresion</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevoSintomasDepresion" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listPsiSintomasDepresion" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Id Rasgos Personalidad</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade m-2" id="nav-sintomasAnsiedad" role="tabpanel" aria-labelledby="sintomasAnsiedad-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Sintomas de Ansiedad</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevoSintomasAnsiedad" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listPsiSintomasAnsiedad" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Id Sintoma Ansiedad</th>
																	<th>Usuario Que Registra</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade m-2" id="nav-impresionDiagnostica" role="tabpanel" aria-labelledby="impresionDiagnostica-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Impresion Diagnostica</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevoImpresionDiagnostica" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listPsiImpresionDiagnostica" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Id Impresion Diagnostica</th>
																	<th>Usuario Que Registra</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade m-2" id="nav-evaluacionMultiaxial" role="tabpanel" aria-labelledby="evaluacionMultiaxial-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Evaluacion Multiaxial</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevoEvaluacionMultiaxial" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listPsiEvaluacionMultiaxial" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Id Evaluacion Multiaxial</th>
																	<th>Usuario Que Registra</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Numero de eje</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade m-2" id="nav-familiograma" role="tabpanel" aria-labelledby="familiograma-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Familiogramas</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevoFamiliograma" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listPsiFamiliograma" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Id Familiograma</th>
																	<th>Usuario Que Registra</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade m-2" id="nav-planTratamiento" role="tabpanel" aria-labelledby="planTratamiento-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Plan de tratamientos</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevoPlanTratamiento" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listPsiPlanTratamiento" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Id Tratamiento</th>
																	<th>Usuario Que Registra</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade m-2" id="nav-seguimientoPsicologia" role="tabpanel" aria-labelledby="seguimientoPsicologia-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Historial Seguimientos</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevoSeguimiento" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listPsiSeguimientoPsicologia" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Id Seguimiento</th>
																	<th>Usuario Que Registra</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane fade m-2" id="nav-medicina" role="tabpanel" aria-labelledby="medicina-tab">
						<nav>
							<ul class="nav nav-tabs" id="tabPsicologia" role="tablist">
								<li class="nav-item">
									<a class="nav-link active" id="nav-SignosVitales-tab" data-toggle="tab" href="#nav-SignosVitales" role="tab" aria-controls="SignosVitales" aria-selected="true">Signos Vitales</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="nav-MedicinaPatologias-tab" data-toggle="tab" href="#nav-MedicinaPatologias" role="tab" aria-controls="MedicinaPatologias" aria-selected="true">Patologias</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="nav-MedicinaConsultas-tab" data-toggle="tab" href="#nav-MedicinaConsultas" role="tab" aria-controls="MedicinaConsultas" aria-selected="true">Consultas</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="nav-MedicinaFormulasMedicas-tab" data-toggle="tab" href="#nav-MedicinaFormulasMedicas" role="tab" aria-controls="MedicinaFormulasMedicas" aria-selected="true">Formulas Medicas</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="nav-MedicinaRevisionFuncional-tab" data-toggle="tab" href="#nav-MedicinaRevisionFuncional" role="tab" aria-controls="MedicinaRevisionFuncional" aria-selected="true">Revision Funcional</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="nav-MedicinaAntecedentePersonal-tab" data-toggle="tab" href="#nav-MedicinaAntecedentePersonal" role="tab" aria-controls="MedicinaAntecedentePersonal" aria-selected="true">Antecedente Personal</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="nav-MedicinaImpresionDiagnostica-tab" data-toggle="tab" href="#nav-MedicinaImpresionDiagnostica" role="tab" aria-controls="MedicinaImpresionDiagnostica" aria-selected="true">Impresion Diagnostica</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="nav-MedicinaExamenFisico-tab" data-toggle="tab" href="#nav-MedicinaExamenFisico" role="tab" aria-controls="MedicinaExamenFisico" aria-selected="true">Examen Fisico</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="nav-MedicinaResultadoExamenes-tab" data-toggle="tab" href="#nav-MedicinaResultadoExamenes" role="tab" aria-controls="MedicinaResultadoExamenes" aria-selected="true">Resultado Examenes</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="nav-MedicinaOrdenMedica-tab" data-toggle="tab" href="#nav-MedicinaOrdenMedica" role="tab" aria-controls="MedicinaOrdenMedica" aria-selected="true">Ordenes Medicas</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="nav-MedicinaSeguimiento-tab" data-toggle="tab" href="#nav-MedicinaSeguimiento" role="tab" aria-controls="MedicinaSeguimiento" aria-selected="true">Historial de Seguimientos</a>
								</li>
								
							</ul>
						</nav>
						<div class="tab-content" id="tabMedicina">
							<div class="tab-pane fade show active m-2" id="nav-SignosVitales" role="tabpanel" aria-labelledby="SignosVitales-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Signos Vitales</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevoMedSignosVitales" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listMedSignosVitales" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Id Signo Vital</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade m-2" id="nav-MedicinaPatologias" role="tabpanel" aria-labelledby="MedicinaPatologias-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Patologias</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevoMedPatologias" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listMedPatologias" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Id Patologia</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade m-2" id="nav-MedicinaConsultas" role="tabpanel" aria-labelledby="MedicinaConsultas-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Consultas</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevoMedConsultas" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listMedConsultas" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Id Consulta</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade m-2" id="nav-MedicinaFormulasMedicas" role="tabpanel" aria-labelledby="MedicinaFormulasMedicas-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Formulas Medicas</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevoMedFormulasMedicas" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listMedFormulasMedicas" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Id Formula Medica</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade m-2" id="nav-MedicinaRevisionFuncional" role="tabpanel" aria-labelledby="MedicinaRevisionFuncional-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Revision Funcional</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevoMedRevisionFuncional" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listMedRevisionFuncional" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Id Revision</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade m-2" id="nav-MedicinaAntecedentePersonal" role="tabpanel" aria-labelledby="MedicinaAntecedentePersonal-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Antecedente personal</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevoMedAntecedentePersonal" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listMedAntecedentePersonal" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Id Antecedente</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade m-2" id="nav-MedicinaImpresionDiagnostica" role="tabpanel" aria-labelledby="MedicinaImpresionDiagnostica-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Impresion Diagnostica</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevoMedImpresionDiagnostica" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listMedImpresionDiagnostica" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Id Impresion Diagnostica</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade m-2" id="nav-MedicinaExamenFisico" role="tabpanel" aria-labelledby="MedicinaExamenFisico-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Examen Fisico</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevoMedExamenFisico" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listMedExamenFisico" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Id Examen</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade m-2" id="nav-MedicinaResultadoExamenes" role="tabpanel" aria-labelledby="MedicinaResultadoExamenes-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Resultado Examenes</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevoMedResultadoExamenes" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listMedResultadoExamen" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Identificador</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade m-2" id="nav-MedicinaOrdenMedica" role="tabpanel" aria-labelledby="MedicinaOrdenMedica-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Ordenes Medicas</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevoMedOrdenMedica" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listMedResultadoOrdenMedica" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Identificador</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade m-2" id="nav-MedicinaSeguimiento" role="tabpanel" aria-labelledby="MedicinaSeguimiento-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Examen Fisico</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevoMedSeguimientos" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listMedSeguimientos" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Id Seguimiento</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane fade m-2" id="nav-enfermeria" role="tabpanel" aria-labelledby="enfermeria-tab">
						<nav>
							<ul class="nav nav-tabs" id="tabPsicologia" role="tablist">
								<li class="nav-item">
									<a class="nav-link active" id="nav-EnfermeriaNotas-tab" data-toggle="tab" href="#nav-EnfermeriaNotas" role="tab" aria-controls="EnfermeriaNotas" aria-selected="true">Notas</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="nav-EnfermeriaJefe-tab" data-toggle="tab" href="#nav-EnfermeriaJefe" role="tab" aria-controls="EnfermeriaJefe" aria-selected="true">Jefe</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="nav-EnfermeriaCuidados-tab" data-toggle="tab" href="#nav-EnfermeriaCuidados" role="tab" aria-controls="EnfermeriaCuidados" aria-selected="true">Cuidados</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="nav-EnfermeriaBalanceLiquidos-tab" data-toggle="tab" href="#nav-EnfermeriaBalanceLiquidos" role="tab" aria-controls="EnfermeriaBalanceLiquidos" aria-selected="true">Balance de Liquidos</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="nav-EnfermeriaControlGlicemia-tab" data-toggle="tab" href="#nav-EnfermeriaControlGlicemia" role="tab" aria-controls="EnfermeriaControlGlicemia" aria-selected="true">Control de Glicemia</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="nav-EnfermeriaControlSuministro-tab" data-toggle="tab" href="#nav-EnfermeriaControlSuministro" role="tab" aria-controls="EnfermeriaControlSuministro" aria-selected="true">Control de Suministro y Tratamiento</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="nav-EnfermeriaControlCuraciones-tab" data-toggle="tab" href="#nav-EnfermeriaControlCuraciones" role="tab" aria-controls="EnfermeriaControlCuraciones" aria-selected="true">Control de Curaciones y Procedimientos</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="nav-EnfermeriaSignosVitales-tab" data-toggle="tab" href="#nav-EnfermeriaSignosVitales" role="tab" aria-controls="EnfermeriaSignosVitales" aria-selected="true">Signos Vitales</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="nav-AyudasDiagnostica-tab" data-toggle="tab" href="#nav-AyudasDiagnostica" role="tab" aria-controls="AyudasDiagnostica" aria-selected="true">Ayudas Diagnostica</a>
								</li>
							</ul>
						</nav>
						<div class="tab-content" id="tabEnfermeriaNotas">
							<div class="tab-pane fade show active m-2" id="nav-EnfermeriaNotas" role="tabpanel" aria-labelledby="EnfermeriaNotas-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Notas de Enfermeria</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevaEnfermeriaNota" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listEnfermeriaNotas" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Id Nota</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade m-2" id="nav-EnfermeriaJefe" role="tabpanel" aria-labelledby="EnfermeriaJefe-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Jefes de enfermeria</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevaEnfermeriaJefe" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listEnfermeriaJefe" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Id Jefe</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade m-2" id="nav-EnfermeriaCuidados" role="tabpanel" aria-labelledby="EnfermeriaCuidados-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Cuidados Enfermeria</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevaEnfermeriaCuidados" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listEnfermeriaCuidados" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Identificador</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade m-2" id="nav-EnfermeriaBalanceLiquidos" role="tabpanel" aria-labelledby="EnfermeriaBalanceLiquidos-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Balance de liquidos</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevaEnfermeriaBalanceLiquidos" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listEnfermeriaBalanceLiquidos" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Identificador</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade m-2" id="nav-EnfermeriaControlGlicemia" role="tabpanel" aria-labelledby="EnfermeriaControlGlicemia-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Control de glicemia</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevaEnfermeriaControlGlicemia" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listEnfermeriaControlGlicemia" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Identificador</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade m-2" id="nav-EnfermeriaControlSuministro" role="tabpanel" aria-labelledby="EnfermeriaControlSuministro-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Control de suministros</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevaEnfermeriaControlSuministros" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listEnfermeriaControlSuministro" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Identificador</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade m-2" id="nav-EnfermeriaControlCuraciones" role="tabpanel" aria-labelledby="EnfermeriaControlCuraciones-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Control de curaciones y procedimientos</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevaEnfermeriaControlCuraciones" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listEnfermeriaControlCuraciones" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Identificador</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade m-2" id="nav-EnfermeriaSignosVitales" role="tabpanel" aria-labelledby="EnfermeriaSignosVitales-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Signos vitales</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevaEnfermeriaSignosVitales" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listEnfermeriaSignosVitales" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Identificador</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane fade m-2" id="nav-fisioterapia" role="tabpanel" aria-labelledby="fisioterapia-tab">
						<nav>
							<ul class="nav nav-tabs" id="tabFisioterapia" role="tablist">
								<li class="nav-item">
									<a class="nav-link active" id="nav-FisioterapiaTonoMuscular-tab" data-toggle="tab" href="#nav-FisioterapiaTonoMuscular" role="tab" aria-controls="FisioterapiaTonoMuscular" aria-selected="true">Tono Muscular</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="nav-FisioterapiaReflejosPatologicos-tab" data-toggle="tab" href="#nav-FisioterapiaReflejosPatologicos" role="tab" aria-controls="FisioterapiaReflejosPatologicos" aria-selected="true">Reflejos Patologicos</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="nav-FisioterapiaFuncionesMentales-tab" data-toggle="tab" href="#nav-FisioterapiaFuncionesMentales" role="tab" aria-controls="FisioterapiaFuncionesMentales" aria-selected="true">Funciones Mentales</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="nav-FisioterapiaSensibilidad-tab" data-toggle="tab" href="#nav-FisioterapiaSensibilidad" role="tab" aria-controls="FisioterapiaSensibilidad" aria-selected="true">Sensibilidad</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="nav-FisioterapiaCoordinacion-tab" data-toggle="tab" href="#nav-FisioterapiaCoordinacion" role="tab" aria-controls="FisioterapiaCoordinacion" aria-selected="true">Coordinacion</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="nav-FisioterapiaEquilibrio-tab" data-toggle="tab" href="#nav-FisioterapiaEquilibrio" role="tab" aria-controls="FisioterapiaEquilibrio" aria-selected="true">Equilibrio</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="nav-FisioterapiaMotricidad-tab" data-toggle="tab" href="#nav-FisioterapiaMotricidad" role="tab" aria-controls="FisioterapiaMotricidad" aria-selected="true">Motricidad</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="nav-FisioterapiaDiagnosticoFisioterapeutico-tab" data-toggle="tab" href="#nav-FisioterapiaDiagnosticoFisioterapeutico" role="tab" aria-controls="FisioterapiaDiagnosticoFisioterapeutico" aria-selected="true">Diagnostico Fisioterapeutico</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="nav-FisioterapiaFuerzaMuscular-tab" data-toggle="tab" href="#nav-FisioterapiaFuerzaMuscular" role="tab" aria-controls="FisioterapiaFuerzaMuscular" aria-selected="true">Fuerza Muscular</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="nav-FisioterapiaPostura-tab" data-toggle="tab" href="#nav-FisioterapiaPostura" role="tab" aria-controls="FisioterapiaPostura" aria-selected="true">Postura</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="nav-FisioterapiaMarcha-tab" data-toggle="tab" href="#nav-FisioterapiaMarcha" role="tab" aria-controls="FisioterapiaMarcha" aria-selected="true">Marcha</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="nav-FisioterapiaSeguimiento-tab" data-toggle="tab" href="#nav-FisioterapiaSeguimiento" role="tab" aria-controls="FisioterapiaSeguimiento" aria-selected="true">Seguimientos</a>
								</li>
							</ul>
						</nav>
						<div class="tab-content" id="tabFisioterapia">
							<div class="tab-pane fade show active m-2" id="nav-FisioterapiaTonoMuscular" role="tabpanel" aria-labelledby="FisioterapiaTonoMuscular-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Tono Muscular</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevaFisioterapiaTonoMuscular" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listFisioterapiaTonoMuscular" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Identificador</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade m-2" id="nav-FisioterapiaReflejosPatologicos" role="tabpanel" aria-labelledby="FisioterapiaReflejosPatologicos-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Reflejos Patologicos</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevaFisioterapiaReflejoPatologico" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listFisioterapiaReflejosPatologicos" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Identificador</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade m-2" id="nav-FisioterapiaFuncionesMentales" role="tabpanel" aria-labelledby="FisioterapiaFuncionesMentales-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Funciones Mentales</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevaFisioterapiaFuncionMental" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listFisioterapiaFuncionesMentales" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Identificador</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade m-2" id="nav-FisioterapiaSensibilidad" role="tabpanel" aria-labelledby="FisioterapiaSensibilidad-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Sensibilidad</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevaFisioterapiaSensibilidad" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listFisioterapiaSensibilidad" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Identificador</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade m-2" id="nav-FisioterapiaCoordinacion" role="tabpanel" aria-labelledby="FisioterapiaCoordinacion-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Coordinacion</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevaFisioterapiaCoordinacion" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listFisioterapiaCoordinacion" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Identificador</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade m-2" id="nav-FisioterapiaEquilibrio" role="tabpanel" aria-labelledby="FisioterapiaEquilibrio-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Equilibrio</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevaFisioterapiaEquilibrio" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listFisioterapiaEquilibrio" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Identificador</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade m-2" id="nav-FisioterapiaMotricidad" role="tabpanel" aria-labelledby="FisioterapiaMotricidad-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Motricidad</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevaFisioterapiaMotricidad" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listFisioterapiaMotricidad" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Identificador</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade m-2" id="nav-FisioterapiaDiagnosticoFisioterapeutico" role="tabpanel" aria-labelledby="FisioterapiaDiagnosticoFisioterapeutico-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Diagnostico Fisioterapeutico</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevaFisioterapiaDiagnosticoFisioterapeutico" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listFisioterapiaDiagnosticoFisioterapeutico" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Identificador</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade m-2" id="nav-FisioterapiaFuerzaMuscular" role="tabpanel" aria-labelledby="FisioterapiaFuerzaMuscular-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Fuerza Muscular</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevaFisioterapiaFuerzaMuscular" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listFisioterapiaFuerzaMuscular" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Identificador</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade m-2" id="nav-FisioterapiaPostura" role="tabpanel" aria-labelledby="FisioterapiaPostura-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Postura</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevaFisioterapiaPostura" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listFisioterapiaPostura" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Identificador</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade m-2" id="nav-FisioterapiaMarcha" role="tabpanel" aria-labelledby="FisioterapiaMarcha-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Marcha</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevaFisioterapiaMarcha" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listFisioterapiaMarcha" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Identificador</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade m-2" id="nav-FisioterapiaSeguimiento" role="tabpanel" aria-labelledby="FisioterapiaSeguimiento-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Seguimientos</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevaFisioterapiaSeguimiento" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listFisioterapiaSeguimiento" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Identificador</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane fade m-2" id="nav-geronlogia" role="tabpanel" aria-labelledby="geronlogia-tab">
						<nav>
							<ul class="nav nav-tabs" id="tabGerontologia" role="tablist">
								<li class="nav-item">
									<a class="nav-link active" id="nav-GerontologiaAbcInstrumental-tab" data-toggle="tab" href="#nav-GerontologiaAbcInstrumental" role="tab" aria-controls="GerontologiaAbcInstrumental" aria-selected="true">ABC Instrumental</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="nav-GerontologiaActitudPersonal-tab" data-toggle="tab" href="#nav-GerontologiaActitudPersonal" role="tab" aria-controls="GerontologiaActitudPersonal" aria-selected="true">Actitud Personal</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="nav-GerontologiaAditamientos-tab" data-toggle="tab" href="#nav-GerontologiaAditamientos" role="tab" aria-controls="GerontologiaAditamientos" aria-selected="true">Aditamientos</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="nav-GerontologiaActividadesRealiza-tab" data-toggle="tab" href="#nav-GerontologiaActividadesRealiza" role="tab" aria-controls="GerontologiaActividadesRealiza" aria-selected="true">Actividades que realiza</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="nav-GerontologiaActividadesqueRealizaria-tab" data-toggle="tab" href="#nav-GerontologiaActividadesqueRealizaria" role="tab" aria-controls="GerontologiaActividadesqueRealizaria" aria-selected="true">Actividades que realizaria</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="nav-GerontologiaSeguimiento-tab" data-toggle="tab" href="#nav-GerontologiaSeguimiento" role="tab" aria-controls="GerontologiaSeguimiento" aria-selected="true">Seguimiento</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="nav-GerontologiaCalidadVida-tab" data-toggle="tab" href="#nav-GerontologiaCalidadVida" role="tab" aria-controls="GerontologiaCalidadVida" aria-selected="true">Calidad de vida</a>
								</li>
							</ul>
						</nav>
						<div class="tab-content" id="tabGerontologia">
							<div class="tab-pane fade show active m-2" id="nav-GerontologiaAbcInstrumental" role="tabpanel" aria-labelledby="GerontologiaAbcInstrumental-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">ABC Instrumental</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevaGerontologiaAbcIntrumental" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listGerontologiaAbcInstrumental" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Identificador</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade m-2" id="nav-GerontologiaActitudPersonal" role="tabpanel" aria-labelledby="GerontologiaActitudPersonal-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Actitud Personal</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevaGerontologiaActitudPersonal" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listGerontologiaActitudPersonal" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Identificador</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade m-2" id="nav-GerontologiaAditamientos" role="tabpanel" aria-labelledby="GerontologiaAditamientos-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Aditamentos</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevaGerontologiaAditamentos" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listGerontologiaAditamientos" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Identificador</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade m-2" id="nav-GerontologiaActividadesRealiza" role="tabpanel" aria-labelledby="GerontologiaActividadesRealiza-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Actividades que realiza</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevaGerontologiaActividadesRealiza" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listGerontologiaActividadesRealiza" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Identificador</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade m-2" id="nav-GerontologiaActividadesqueRealizaria" role="tabpanel" aria-labelledby="GerontologiaActividadesqueRealizaria-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Actividades que le gustaria realizar</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevaGerontologiaActividadesRealiza" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listGerontologiaActividadesQueRealizaria" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Identificador</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade m-2" id="nav-GerontologiaSeguimiento" role="tabpanel" aria-labelledby="GerontologiaSeguimiento-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Seguimiento</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevaGerontologiaSeguimiento" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listGerontologiaGerontologiaSeguimiento" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Identificador</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade m-2" id="nav-GerontologiaCalidadVida" role="tabpanel" aria-labelledby="GerontologiaCalidadVida-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Calidad de vida</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevaGerontologiaCalidadVida" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listGerontologiaGerontologiaCalidadVida" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Identificador</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane fade m-2" id="nav-Psiquiatria" role="tabpanel" aria-labelledby="Psiquiatria-tab">
						<nav>
							<ul class="nav nav-tabs" id="tabPsiquiatria" role="tablist">
								<li class="nav-item">
									<a class="nav-link active" id="nav-PsiquiatriaInformacion-tab" data-toggle="tab" href="#nav-PsiquiatriaInformacion" role="tab" aria-controls="PsiquiatriaInformacion" aria-selected="true">Informacion Psiquiatrica</a>
								</li>
							</ul>
						</nav>
						<div class="tab-content" id="tabPsiquiatria">
							<div class="tab-pane fade show active m-2" id="nav-PsiquiatriaInformacion" role="tabpanel" aria-labelledby="PsiquiatriaInformacion-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Informacion Psiquiatrica</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevaPsiquiatriaInformacion" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listPsiquiatriaInformacion" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Identificador</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane fade m-2" id="nav-TrabajoSocial" role="tabpanel" aria-labelledby="TrabajoSocial-tab">
						<nav>
							<ul class="nav nav-tabs" id="tabTrabajoSocial" role="tablist">
								<li class="nav-item">
									<a class="nav-link active" id="nav-TrabajoSocialInformacionGeneral-tab" data-toggle="tab" href="#nav-TrabajoSocialInformacionGeneral" role="tab" aria-controls="TrabajoSocialInformacionGeneral" aria-selected="true">Información General</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="nav-TrabajoSocialReingreso-tab" data-toggle="tab" href="#nav-TrabajoSocialReingreso" role="tab" aria-controls="TrabajoSocialReingreso" aria-selected="true">Reingreso</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="nav-TrabajoSocialVisitaDomiciliaria-tab" data-toggle="tab" href="#nav-TrabajoSocialVisitaDomiciliaria" role="tab" aria-controls="TrabajoSocialVisitaDomiciliaria" aria-selected="true">Visita Domiciliaria</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="nav-TrabajoSocialAspectoSocioEconomico-tab" data-toggle="tab" href="#nav-TrabajoSocialAspectoSocioEconomico" role="tab" aria-controls="TrabajoSocialAspectoSocioEconomico" aria-selected="true">Aspecto Socioeconomico</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="nav-TrabajoSocialSeguimiento-tab" data-toggle="tab" href="#nav-TrabajoSocialSeguimiento" role="tab" aria-controls="TrabajoSocialSeguimiento" aria-selected="true">Seguimiento</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="nav-TrabajoSocialRedApoyoFamiliar-tab" data-toggle="tab" href="#nav-TrabajoSocialRedApoyoFamiliar" role="tab" aria-controls="TrabajoSocialRedApoyoFamiliar" aria-selected="true">Red de apoyo familiar</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="nav-TrabajoSocialActividadesRealiza-tab" data-toggle="tab" href="#nav-TrabajoSocialActividadesRealiza" role="tab" aria-controls="TrabajoSocialActividadesRealiza" aria-selected="true">Actividades que realiza</a>
								</li>
							</ul>
						</nav>
						<div class="tab-content" id="tabTrabajoSocial">
							<div class="tab-pane fade show active m-2" id="nav-TrabajoSocialInformacionGeneral" role="tabpanel" aria-labelledby="TrabajoSocialInformacionGeneral-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Información general</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevoTrabajoSocialIformacion" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listTrabajoSocialInformacion" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Identificador</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade m-2" id="nav-TrabajoSocialReingreso" role="tabpanel" aria-labelledby="TrabajoSocialReingreso-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Reingreso</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevoTrabajoSocialReingreso" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listTrabajoSocialReingreso" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Identificador</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade m-2" id="nav-TrabajoSocialVisitaDomiciliaria" role="tabpanel" aria-labelledby="TrabajoSocialVisitaDomiciliaria-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Visita Domiciliaria</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevoTrabajoSocialVisitaDomiciliaria" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listTrabajoSocialVisitaDomiciliaria" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Identificador</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade m-2" id="nav-TrabajoSocialAspectoSocioEconomico" role="tabpanel" aria-labelledby="TrabajoSocialAspectoSocioEconomico-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Aspecto Socioeconomico</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevoTrabajoSocialAspectoSocioeconomico" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listTrabajoSocialAspectoSocioeconomico" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Identificador</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade m-2" id="nav-TrabajoSocialSeguimiento" role="tabpanel" aria-labelledby="TrabajoSocialSeguimiento-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Seguimiento</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevoTrabajoSocialSeguimiento" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listTrabajoSocialSeguimiento" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Identificador</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade m-2" id="nav-TrabajoSocialRedApoyoFamiliar" role="tabpanel" aria-labelledby="TrabajoSocialRedApoyoFamiliar-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Red de apoyo familiar</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevoTrabajoSocialRedApoyoFamiliar" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listTrabajoSocialRedApoyoFamiliar" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Identificador</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade m-2" id="nav-TrabajoSocialActividadesRealiza" role="tabpanel" aria-labelledby="TrabajoSocialActividadesRealiza-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Actividades que realiza</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevoTrabajoSocialActividadesRealiza" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listTrabajoSocialActividadesRealiza" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Identificador</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane fade m-2" id="nav-TerapiaOcupacional" role="tabpanel" aria-labelledby="TerapiaOcupacional-tab">
						<nav>
							<ul class="nav nav-tabs" id="tabTerapiaOcupacional" role="tablist">
								<li class="nav-item">
									<a class="nav-link active" id="nav-TerapiaOcupacionalEvaluacion-tab" data-toggle="tab" href="#nav-TerapiaOcupacionalEvaluacion" role="tab" aria-controls="TerapiaOcupacionalEvaluacion" aria-selected="true">Evaluacion Ocupacional</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="nav-TerapiaOcupacionaSeguimiento-tab" data-toggle="tab" href="#nav-TerapiaOcupacionaSeguimiento" role="tab" aria-controls="TerapiaOcupacionaSeguimiento" aria-selected="true">Seguimiento</a>
								</li>
							</ul>
						</nav>
						<div class="tab-content" id="tabTerapiaOcupacional">
							<div class="tab-pane fade show active m-2" id="nav-TerapiaOcupacionalEvaluacion" role="tabpanel" aria-labelledby="TerapiaOcupacionalEvaluacion-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Evaluacion Ocupacional</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevoTerapiaOcupacionalEvaluacion" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listTerapiaOcupacionalEvaluacion" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Identificador</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade m-2" id="nav-TerapiaOcupacionaSeguimiento" role="tabpanel" aria-labelledby="TerapiaOcupacionaSeguimiento-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Seguimiento</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevoTerapiaOcupacionalSeguimiento" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listTerapiaOcupacionalSeguimiento" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Identificador</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane fade m-2" id="nav-test" role="tabpanel" aria-labelledby="test-tab">
						<nav>
							<ul class="nav nav-tabs" id="tabTerapiaOcupacional" role="tablist">
								<li class="nav-item">
									<a class="nav-link active" id="nav-TestTestMinimental-tab" data-toggle="tab" href="#nav-TestTestMinimental" role="tab" aria-controls="TestTestMinimental" aria-selected="true">Test Minimental</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="nav-TestTestYesavage-tab" data-toggle="tab" href="#nav-TestTestYesavage" role="tab" aria-controls="TestTestYesavage" aria-selected="true">Test Yesavage</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="nav-TestTestBarthel-tab" data-toggle="tab" href="#nav-TestTestBarthel" role="tab" aria-controls="TestTestBarthel" aria-selected="true">Test Barthel</a>
								</li>
							</ul>
						</nav>
						<div class="tab-content" id="tabTerapiaOcupacional">
							<div class="tab-pane fade show active m-2" id="nav-TestTestMinimental" role="tabpanel" aria-labelledby="TestTestMinimental-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Test Minimental</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevoTestTestMinimental" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listTestTestMinimental" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Identificador</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade m-2" id="nav-TestTestYesavage" role="tabpanel" aria-labelledby="TestTestYesavage-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Test Yesavage</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevoTestTestYesavage" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listTestTestYesavage" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Identificador</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade m-2" id="nav-TestTestBarthel" role="tabpanel" aria-labelledby="TestTestBarthel-tab">
								<div class="post row">
									<div class="row m-2">
										<div class="col-md-12">
											<div class="box">
												<div class="box-header">
													<div class="row">
														<h5 class="box-title" style="margin-left: 15px">Test Barthel</h5>
														<div class="col-sm-2 col-sm-offset-10 ml-auto float-right" style="margin-right: 15px">
															<a href="JavaScript:void(0)" class="btn btn-info btn-block mb-4" data-toggle="modal" data-target="#modalNuevoTestTestBarthel" >
																<i class="fa fa-fw fa-plus" style="margin-right:15px;"></i><b>Nuevo</b>
															</a>
														</div>
													</div>
												</div>
												<div class="box-body">
													<div class="container">
														<table id="listTestTestbarthel" class="table-light table-bordered table-striped table-hover"  width="98%" cellspacing="0">
															<thead>
																<tr>
																	<th>N°</th>
																	<th>Identificador</th>
																	<th>Usuario</th>
																	<th>Fecha</th>
																	<th>Hora</th>
																	<th>Acciones</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- MODALES REGISTRAR NUTRICION -->
			<?php include_once('modalesRegistrar/nutricion.php'); ?>
			<!-- MODALES RESGISTRAR DE PSICOLOGIA -->
			<?php include_once('modalesRegistrar/psicologia.php'); ?>
			<!-- MODALES RESGISTRAR DE MEDICINA -->
			<?php include_once('modalesRegistrar/medicina.php'); ?>
			<!-- MODALES RESGISTRAR DE ENFERMERIA -->
			<?php include_once('modalesRegistrar/enfermeria.php'); ?>
			<!-- MODALES RESGISTRAR DE FISIOTERAPIA -->
			<?php include_once('modalesRegistrar/fisioterapia.php'); ?>
			<!-- MODALES RESGISTRAR DE GERONTOLOGIA -->
			<?php include_once('modalesRegistrar/gerontologia.php'); ?>
			<!-- MODALES RESGISTRAR DE PSIQUIATRIA -->
			<?php include_once('modalesRegistrar/psiquiatria.php'); ?>
			<!-- MODALES RESGISTRAR DE TERAPIA OCUPACIONAL -->
			<?php include_once('modalesRegistrar/terapiaOcupacional.php'); ?>
			<!-- MODALES RESGISTRAR DE TRABAJO SOCIAL -->
			<?php include_once('modalesRegistrar/trabajoSocial.php'); ?>
			<!-- MODALES RESGISTRAR DE TEST -->
			<?php include_once('modalesRegistrar/test.php'); ?>

			<script type="text/javascript">
				$.fn.dataTable.ext.errMode = 'none';
				var idMatricula = "idMatricula=<?php echo $fetchPaciente[0]?>";

				<?php 
				function cargarDataTables($divId,$namePage,$idPaciente){
					$string = '
					$("#'.$divId.'").DataTable({
						language: {
							"decimal": "",
							"emptyTable": "No hay información",
							"info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
							"infoEmpty": "Mostrando 0 de 0 Entradas",
							"infoFiltered": "(Filtrado de _MAX_ total entradas)",
							"infoPostFix": "",
							"thousands": ",",
							"lengthMenu": " Mostrar:_MENU_ Entradas",
							"loadingRecords": "Cargando...",
							"processing": "Procesando...",
							"search": "Buscar:",
							"zeroRecords": "Sin resultados encontrados",
							"paginate": {
								"first": "Primero",
								"last": "Ultimo",
								"next": "Siguiente",
								"previous": "Anterior"
							}
						},
						"processing": true,
						"serverSide": true,
						"ajax": {
							url: "'.$namePage.'.php",
							type: "post",
							data: {id: "' .$idPaciente. '"},
							error: function () {}
						}
					});
					';
					return $string;
				}
				?>
				/* Aqui cargamos los DataTables con el metodo cargarDataTables() */
				/* Nutricion */
				
				<?php  echo cargarDataTables('listInfoNutricional',	'nutricion/response/responseInformacionNutricional',$fetchPaciente[0]); ?>
				<?php  echo cargarDataTables('listSeguimientoNutricional','nutricion/response/responseSeguimientoNutricion',$fetchPaciente[0]); ?>
				/* Psicologia */
				<?php  echo cargarDataTables('listPsiAntcMental','psicologia/response/responsePsicologiaAntcMental',$fetchPaciente[0]); ?>
				<?php  echo cargarDataTables('listPsiAntcFamiliar','psicologia/response/responsePsicologiaAntcFamiliar',$fetchPaciente[0]); ?>
				<?php  echo cargarDataTables('listPsiPatologias','psicologia/response/responsePsicologiaPatologias',$fetchPaciente[0]); ?>
				<?php  echo cargarDataTables('listPsiRelacionesFamiliar','psicologia/response/responsePsicologiaRelacionFamiliar',$fetchPaciente[0]); ?>
				<?php  echo cargarDataTables('listPsiApoyoFamiliar','psicologia/response/responsePsicologiaApoyoFamiliar',$fetchPaciente[0]); ?>
				<?php  echo cargarDataTables('listPsiRelacionSocial','psicologia/response/responsePsicologiaRelacionSocial',$fetchPaciente[0]); ?>
				<?php  echo cargarDataTables('listPsiGrupoSocial','psicologia/response/responsePsicologiaGrupoSocial',$fetchPaciente[0]); ?>
				<?php  echo cargarDataTables('listPsiRasgosPersonalidad','psicologia/response/responsePsicologiaRasgosPersonalidad',$fetchPaciente[0]); ?>
				<?php  echo cargarDataTables('listPsiSintomasDepresion','psicologia/response/responsePsicologiaSintomasDepresion',$fetchPaciente[0]); ?>
				<?php  echo cargarDataTables('listPsiSintomasAnsiedad','psicologia/response/responsePsicologiaSintomaAnsiedad',$fetchPaciente[0]); ?>
				<?php  echo cargarDataTables('listPsiImpresionDiagnostica','psicologia/response/responsePsicologiaImpresionDiagnostica',$fetchPaciente[0]); ?>
				<?php  echo cargarDataTables('listPsiEvaluacionMultiaxial','psicologia/response/responsePsicologiaEvaluacionMultiaxial',$fetchPaciente[0]); ?>
				<?php  echo cargarDataTables('listPsiFamiliograma','psicologia/response/responsePsicologiaFamiliograma',$fetchPaciente[0]); ?>
				<?php  echo cargarDataTables('listPsiPlanTratamiento','psicologia/response/responsePsicologiaPlanTratamientos',$fetchPaciente[0]); ?>
				<?php  echo cargarDataTables('listPsiSeguimientoPsicologia','psicologia/response/responsePsicologiaSeguimiento',$fetchPaciente[0]); ?>
				/* Medicina */
				<?php  echo cargarDataTables('listMedSignosVitales','medicina/response/responseMedicinaSignosVitales',$fetchPaciente[0]); ?>
				<?php  echo cargarDataTables('listMedPatologias','medicina/response/responseMedicinaPatologias',$fetchPaciente[0]); ?>
				<?php  echo cargarDataTables('listMedConsultas','medicina/response/responseMedicinaConsultas',$fetchPaciente[0]); ?>
				<?php  echo cargarDataTables('listMedFormulasMedicas','medicina/response/responseMedicinaFormulasMedicas',$fetchPaciente[0]); ?>
				<?php  echo cargarDataTables('listMedRevisionFuncional','medicina/response/responseMedicinaRevisionFuncional',$fetchPaciente[0]); ?>
				<?php  echo cargarDataTables('listMedAntecedentePersonal','medicina/response/responseMedicinaAntecedentePersonal',$fetchPaciente[0]); ?>
				<?php  echo cargarDataTables('listMedImpresionDiagnostica','medicina/response/responseMedicinaImpresionDiagnostica',$fetchPaciente[0]); ?>
				<?php  echo cargarDataTables('listMedExamenFisico','medicina/response/responseMedicinaExamenFisico',$fetchPaciente[0]); ?>
				<?php  echo cargarDataTables('listMedSeguimientos','medicina/response/responseMedicinaSeguimientos',$fetchPaciente[0]); ?>
				<?php  echo cargarDataTables('listMedResultadoExamen','medicina/response/responseMedicinaResultadoExamen',$fetchPaciente[0]); ?>
				<?php  echo cargarDataTables('listMedResultadoOrdenMedica','medicina/response/responseMedicinaOrdenMedica',$fetchPaciente[0]); ?>

				/* Enfermeria */
				<?php  echo cargarDataTables('listEnfermeriaNotas','enfermeria/response/responseEnfermeriaNotas',$fetchPaciente[0]); ?>
				<?php  echo cargarDataTables('listEnfermeriaJefe','enfermeria/response/responseEnfermeriaJefe',$fetchPaciente[0]); ?>
				<?php  echo cargarDataTables('listEnfermeriaCuidados','enfermeria/response/responseEnfermeriaCuidados',$fetchPaciente[0]); ?>
				<?php  echo cargarDataTables('listEnfermeriaBalanceLiquidos','enfermeria/response/responseEnfermeriaBalanceLiquidos',$fetchPaciente[0]); ?>
				<?php  echo cargarDataTables('listEnfermeriaControlGlicemia','enfermeria/response/responseEnfermeriaControlGlicemia',$fetchPaciente[0]); ?>
				<?php  echo cargarDataTables('listEnfermeriaControlSuministro','enfermeria/response/responseEnfermeriaControlSuministro',$fetchPaciente[0]); ?>
				<?php  echo cargarDataTables('listEnfermeriaControlCuraciones','enfermeria/response/responseEnfermeriaControlCuraciones',$fetchPaciente[0]); ?>
				<?php  echo cargarDataTables('listEnfermeriaSignosVitales','enfermeria/response/responseEnfermeriaSignosVitales',$fetchPaciente[0]); ?>



				/* Fisioterapia */ 
				<?php  echo cargarDataTables('listFisioterapiaTonoMuscular','fisioterapia/response/responseFisioterapiaTonoMuscular',$fetchPaciente[0]); ?>
				<?php  echo cargarDataTables('listFisioterapiaReflejosPatologicos','fisioterapia/response/responseFisioterapiaReflejosPatologicos',$fetchPaciente[0]); ?>
				<?php  echo cargarDataTables('listFisioterapiaFuncionesMentales','fisioterapia/response/responseFisioterapiaFuncionesMentales',$fetchPaciente[0]); ?>
				<?php  echo cargarDataTables('listFisioterapiaSensibilidad','fisioterapia/response/responseFisioterapiaSensibilidad',$fetchPaciente[0]); ?>
				<?php  echo cargarDataTables('listFisioterapiaCoordinacion','fisioterapia/response/responseFisioterapiaCoordinacion',$fetchPaciente[0]); ?>
				<?php  echo cargarDataTables('listFisioterapiaEquilibrio','fisioterapia/response/responseFisioterapiaEquilibrio',$fetchPaciente[0]); ?>
				<?php  echo cargarDataTables('listFisioterapiaMotricidad','fisioterapia/response/responseFisioterapiaMotricidad',$fetchPaciente[0]); ?>
				<?php  echo cargarDataTables('listFisioterapiaDiagnosticoFisioterapeutico','fisioterapia/response/responseFisioterapiaDiagnosticoFisioterapeutico',$fetchPaciente[0]); ?>
				<?php  echo cargarDataTables('listFisioterapiaFuerzaMuscular','fisioterapia/response/responseFisioterapiaFuerzaMuscular',$fetchPaciente[0]); ?>
				<?php  echo cargarDataTables('listFisioterapiaPostura','fisioterapia/response/responseFisioterapiaPostura',$fetchPaciente[0]); ?>
				<?php  echo cargarDataTables('listFisioterapiaMarcha','fisioterapia/response/responseFisioterapiaMarcha',$fetchPaciente[0]); ?>
				<?php  echo cargarDataTables('listFisioterapiaSeguimiento','fisioterapia/response/responseFisioterapiaSeguimiento',$fetchPaciente[0]); ?>


				/* Gerontologia*/
				<?php  echo cargarDataTables('listGerontologiaAbcInstrumental','gerontologia/response/responseGerontologiaAbcInstrumental',$fetchPaciente[0]); ?>
				<?php  echo cargarDataTables('listGerontologiaActitudPersonal','gerontologia/response/responseGerontologiaActitudPersonal',$fetchPaciente[0]); ?>
				<?php  echo cargarDataTables('listGerontologiaAditamientos','gerontologia/response/responseGerontologiaAditamientos',$fetchPaciente[0]); ?>
				<?php  echo cargarDataTables('listGerontologiaActividadesRealiza','gerontologia/response/responseGerontolgiaActividadesRealiza',$fetchPaciente[0]); ?>
				<?php  echo cargarDataTables('listGerontologiaActividadesQueRealizaria','gerontologia/response/responseGerontologiaActividadesQueRealizaria',$fetchPaciente[0]); ?>
				<?php  echo cargarDataTables('listGerontologiaGerontologiaSeguimiento','gerontologia/response/responseGerontologiaSeguimiento',$fetchPaciente[0]); ?>
				<?php  echo cargarDataTables('listGerontologiaGerontologiaCalidadVida','gerontologia/response/responseGerontologiaCalidadVida',$fetchPaciente[0]); ?>


				/* Psiquiatria */
				<?php  echo cargarDataTables('listPsiquiatriaInformacion','psiquiatria/response/responsePsiquiatriaInformacion',$fetchPaciente[0]); ?>

				/* Terapia Ocupacional*/
				<?php  echo cargarDataTables('listTerapiaOcupacionalEvaluacion','terapiaOcupacional/response/responseTerapiaOcupacionalEvaluacion',$fetchPaciente[0]); ?>
				<?php  echo cargarDataTables('listTerapiaOcupacionalSeguimiento','terapiaOcupacional/response/responseTerapiaOcupacionalSeguimiento',$fetchPaciente[0]); ?>
				
				/*Trabajo Social*/
				<?php  echo cargarDataTables('listTrabajoSocialInformacion','trabajoSocial/response/responseTrabajoSocialInformacion',$fetchPaciente[0]); ?>
				<?php  echo cargarDataTables('listTrabajoSocialReingreso','trabajoSocial/response/responseTrabajoSocialReingreso',$fetchPaciente[0]); ?>
				<?php  echo cargarDataTables('listTrabajoSocialVisitaDomiciliaria','trabajoSocial/response/responseTrabajoSocialVisitaDomiciliaria',$fetchPaciente[0]); ?>
				<?php  echo cargarDataTables('listTrabajoSocialAspectoSocioeconomico','trabajoSocial/response/responseTrabajoSocialAspectoSocioeconomico',$fetchPaciente[0]); ?>
				<?php  echo cargarDataTables('listTrabajoSocialSeguimiento','trabajoSocial/response/responseTrabajoSocialSeguimiento',$fetchPaciente[0]); ?>
				<?php  echo cargarDataTables('listTrabajoSocialRedApoyoFamiliar','trabajoSocial/response/responseTrabajoSocialRedApoyoFamiliar',$fetchPaciente[0]); ?>
				<?php  echo cargarDataTables('listTrabajoSocialActividadesRealiza','trabajoSocial/response/responseTrabajoSocialActividadesRealiza',$fetchPaciente[0]); ?>
				
				
				/*TEST*/
				<?php  echo cargarDataTables('listTestTestMinimental','test/response/responseTestTestMinimental',$fetchPaciente[0]); ?>
				<?php  echo cargarDataTables('listTestTestYesavage','test/response/responseTestTestYesavage',$fetchPaciente[0]); ?>
				<?php  echo cargarDataTables('listTestTestbarthel','test/response/responseTestTestbarthel',$fetchPaciente[0]); ?>


				function calcularTranstornoEje(){
					var idTrastornoEje = document.getElementById("transtornoMultiaxial").value;
					$.ajax({
						type: 'post',
						url: 'traerEjeTrastorno.php',
						dataType: 'json',
						data: {id: idTrastornoEje},
						success: function(data){
							$("#numeroDelEje").val(data);
						}
					})
				}
				function calcularTranstornoEjeEditar(){
					var idTrastornoEjeEditar = document.getElementById("transtornoMultiaxialEditar").value;
					$.ajax({
						type: 'post',
						url: 'traerEjeTrastorno.php',
						dataType: 'json',
						data: {id: idTrastornoEjeEditar},
						success: function(data){
							$("#numeroDelEjeEditar").val(data);
						}
					})
				}
			</script>
		</section>

	<?php } ?>
