<?php require "../../../../session.php";



$sqlUsuario = "SELECT 
*,
(CONVERT(CAST(CONVERT(observacion USING latin1) AS BINARY) USING utf8))AS observacion
FROM gddt_abc_instrumental 
WHERE id_abc_instrumental =  ". $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);

?>
<form name="editarUsuario" method="POST" action="procesarPsicologiaAntceMental.php?tipo=update" enctype="multipart/form-data">
	<h4 class="text-center mb-4"> ABC Instrumental </h4>
	<div class="row">
		<div class="form-group col-md-4">
			<label for="recipient-name" class="form-control-label">Usuario que registra <i style="color: darkorange">*</i></label>
			<select class="form-control" name="usuario" >
				<option>Selecciona una opción</option>
				<?php
				$queryCuenta = mysqli_query($conn, "SELECT id_cuenta,
					UPPER((CONVERT(CAST(CONVERT(CONCAT(primer_nombre,' ',segundo_nombre,' ',primer_apellido,' ',segundo_apellido) USING latin1) AS BINARY) USING utf8))) AS nombreUsuario FROM gddt_cuentas ;");
				while ($fetchCuenta = mysqli_fetch_row($queryCuenta)) {
					if ($fetchCuenta[0] == $fetchUsuario[1]) {
						echo "<option selected value=" . $fetchCuenta[0] . ">" . $fetchCuenta[1] . "</option>";
					} else {
						echo "<option value=" . $fetchCuenta[0] . ">" . $fetchCuenta[1] . "</option>";
					}
				}
				?>
			</select>
		</div>
		<?php 
		if(strlen($fetchUsuario[3]) < 2){$mes = '0'.$fetchUsuario[3];}else{$mes = $fetchUsuario[3];}
		if(strlen($fetchUsuario[4]) < 2){$dia = '0'.$fetchUsuario[4];}else{$dia = $fetchUsuario[4];}
		$year = $fetchUsuario[2];
		$fecha = $year.'-'.$mes.'-'.$dia;
		?>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Fecha de modificacion <i style="color: darkorange">*</i></label>
			<input type="date" class="form-control" name="fechaMod" value="<?= $fecha ?>"/>
		</div>
		<div class="form-group col-md-4">
			<label for="recipient-name" class="form-control-label" >¿Usa el teléfono?<i style="color: darkorange">*</i></label>
			<?php 
			$select = array(
				0 => 'Si',
				1 => 'No'
			);
			?>
			<select class="form-control" name="usaTelefono" >
				<option>Selecciona una opción</option>
				<?php
				foreach ($select as $key) {
					if($key == $fetchUsuario[7]){
						echo '<option selected value='.$key.'>'.$key.'</option>';
					}else{
						echo '<option value='.$key.'>'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="recipient-name" class="form-control-label" >¿Maneja llaves?<i style="color: darkorange">*</i></label>
			<select class="form-control" name="majeaLlaves" >
				<option>Selecciona una opción</option>
				<?php
				foreach ($select as $key) {
					if($key == $fetchUsuario[12]){
						echo '<option selected value='.$key.'>'.$key.'</option>';
					}else{
						echo '<option value='.$key.'>'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="recipient-name" class="form-control-label" >¿Abre y cierra puertas?<i style="color: darkorange">*</i></label>
			<select class="form-control" name="abreCierraPuertas" >
				<option>Selecciona una opción</option>
				<?php
				foreach ($select as $key) {
					if($key == $fetchUsuario[13]){
						echo '<option selected value='.$key.'>'.$key.'</option>';
					}else{
						echo '<option value='.$key.'>'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="recipient-name" class="form-control-label" >¿Abre paquetes?<i style="color: darkorange">*</i></label>
			<select class="form-control" name="abrePaquetes" >
				<option>Selecciona una opción</option>
				<?php
				foreach ($select as $key) {
					if($key == $fetchUsuario[14]){
						echo '<option selected value='.$key.'>'.$key.'</option>';
					}else{
						echo '<option value='.$key.'>'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="recipient-name" class="form-control-label" >¿Usa radio y televisión?<i style="color: darkorange">*</i></label>
			<select class="form-control" name="usaRadioTelevision" >
				<option>Selecciona una opción</option>
				<?php
				foreach ($select as $key) {
					if($key == $fetchUsuario[15]){
						echo '<option selected value='.$key.'>'.$key.'</option>';
					}else{
						echo '<option value='.$key.'>'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="recipient-name" class="form-control-label" >¿Maneja dinero propio?<i style="color: darkorange">*</i></label>
			<select class="form-control" name="manejaDineroPropio" >
				<option>Selecciona una opción</option>
				<?php
				foreach ($select as $key) {
					if($key == $fetchUsuario[16]){
						echo '<option selected value='.$key.'>'.$key.'</option>';
					}else{
						echo '<option value='.$key.'>'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-12">
			<label for="recipient-name" class="form-control-label" >Observación<i style="color: darkorange">*</i></label>
			<textarea class="form-control" name="observacion" cols="40" rows="5" ><?= $fetchUsuario[22] ?></textarea>
		</div>
	</div>
	<!-- End Row -->


	<!-- Modal Footer -->
	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">
			Cancelar
		</button>
		<button type="submit" class="btn btn-primary" name="Actualizar">
			Actualizar
		</button>
	</div>
