<?php require "../../../../session.php";



$sqlUsuario = "SELECT 
*,
(CONVERT(CAST(CONVERT(otra_realizaria USING latin1) AS BINARY) USING utf8))AS otra_realizaria,
(CONVERT(CAST(CONVERT(observacion_realizaria USING latin1) AS BINARY) USING utf8))AS realizaria
FROM gddt_actividades_realizaria
WHERE id_actividad_realizaria =  ". $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);

?>
<form name="editarUsuario" method="POST" action="procesarPsicologiaAntceMental.php?tipo=update" enctype="multipart/form-data">
	<h4 class="text-center mb-4"> Actividades que realiza </h4>
	<div class="row">
		<div class="form-group col-md-4">
			<label for="recipient-name" class="form-control-label">Usuario que registra <i style="color: darkorange">*</i></label>
			<select class="form-control" name="usuario" >
				<option>Selecciona una opción</option>
				<?php
				$queryCuenta = mysqli_query($conn, "SELECT id_cuenta,
					UPPER((CONVERT(CAST(CONVERT(CONCAT(primer_nombre,' ',segundo_nombre,' ',primer_apellido,' ',segundo_apellido) USING latin1) AS BINARY) USING utf8))) AS nombreUsuario FROM gddt_cuentas ;");
				while ($fetchCuenta = mysqli_fetch_row($queryCuenta)) {
					if ($fetchCuenta[0] == $fetchUsuario[1]) {
						echo "<option selected value=" . $fetchCuenta[0] . ">" . $fetchCuenta[1] . "</option>";
					} else {
						echo "<option value=" . $fetchCuenta[0] . ">" . $fetchCuenta[1] . "</option>";
					}
				}
				?>
			</select>
		</div>
		<?php 
		if(strlen($fetchUsuario[3]) < 2){$mes = '0'.$fetchUsuario[3];}else{$mes = $fetchUsuario[3];}
		if(strlen($fetchUsuario[4]) < 2){$dia = '0'.$fetchUsuario[4];}else{$dia = $fetchUsuario[4];}
		$year = $fetchUsuario[2];
		$fecha = $year.'-'.$mes.'-'.$dia;
		?>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Fecha de modificacion <i style="color: darkorange">*</i></label>
			<input type="date" class="form-control" name="fechaMod" value="<?= $fecha ?>"/>
		</div>
		<div class="form-group col-md-4">
			<label for="recipient-name" class="form-control-label" >Juegos de mesa<i style="color: darkorange">*</i></label>
			<?php 
			$select = array(
				0 => 'Si',
				1 => 'No',
				2 => 'No aplica'
			);
			?>
			<select class="form-control" name="juegoMesa" >
				<option>Selecciona una opción</option>
				<?php
				foreach ($select as $key) {
					if($key == $fetchUsuario[7]){
						echo '<option selected value='.$key.'>'.$key.'</option>';
					}else{
						echo '<option value='.$key.'>'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="recipient-name" class="form-control-label" >Fiestas sociales<i style="color: darkorange">*</i></label>
			<select class="form-control" name="fiestasSociales" >
				<option>Selecciona una opción</option>
				<?php
				foreach ($select as $key) {
					if($key == $fetchUsuario[8]){
						echo '<option selected value='.$key.'>'.$key.'</option>';
					}else{
						echo '<option value='.$key.'>'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="recipient-name" class="form-control-label" >Lectura<i style="color: darkorange">*</i></label>
			<select class="form-control" name="lectura" >
				<option>Selecciona una opción</option>
				<?php
				foreach ($select as $key) {
					if($key == $fetchUsuario[9]){
						echo '<option selected value='.$key.'>'.$key.'</option>';
					}else{
						echo '<option value='.$key.'>'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="recipient-name" class="form-control-label" >Escritura<i style="color: darkorange">*</i></label>
			<select class="form-control" name="escritura" >
				<option>Selecciona una opción</option>
				<?php
				foreach ($select as $key) {
					if($key == $fetchUsuario[10]){
						echo '<option selected value='.$key.'>'.$key.'</option>';
					}else{
						echo '<option value='.$key.'>'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="recipient-name" class="form-control-label" >Manualidades<i style="color: darkorange">*</i></label>
			<select class="form-control" name="manualidades" >
				<option>Selecciona una opción</option>
				<?php
				foreach ($select as $key) {
					if($key == $fetchUsuario[11]){
						echo '<option selected value='.$key.'>'.$key.'</option>';
					}else{
						echo '<option value='.$key.'>'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="recipient-name" class="form-control-label" >Pintura<i style="color: darkorange">*</i></label>
			<select class="form-control" name="pintura" >
				<option>Selecciona una opción</option>
				<?php
				foreach ($select as $key) {
					if($key == $fetchUsuario[12]){
						echo '<option selected value='.$key.'>'.$key.'</option>';
					}else{
						echo '<option value='.$key.'>'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="recipient-name" class="form-control-label" >Música<i style="color: darkorange">*</i></label>
			<select class="form-control" name="musica" >
				<option>Selecciona una opción</option>
				<?php
				foreach ($select as $key) {
					if($key == $fetchUsuario[13]){
						echo '<option selected value='.$key.'>'.$key.'</option>';
					}else{
						echo '<option value='.$key.'>'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="recipient-name" class="form-control-label" >Gimnasia<i style="color: darkorange">*</i></label>
			<select class="form-control" name="gimnasia" >
				<option>Selecciona una opción</option>
				<?php
				foreach ($select as $key) {
					if($key == $fetchUsuario[14]){
						echo '<option selected value='.$key.'>'.$key.'</option>';
					}else{
						echo '<option value='.$key.'>'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="recipient-name" class="form-control-label" >Talleres dirigidos<i style="color: darkorange">*</i></label>
			<select class="form-control" name="talleresDirigidos" >
				<option>Selecciona una opción</option>
				<?php
				foreach ($select as $key) {
					if($key == $fetchUsuario[15]){
						echo '<option selected value='.$key.'>'.$key.'</option>';
					}else{
						echo '<option value='.$key.'>'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="recipient-name" class="form-control-label" >Orar<i style="color: darkorange">*</i></label>
			<select class="form-control" name="orar" >
				<option>Selecciona una opción</option>
				<?php
				foreach ($select as $key) {
					if($key == $fetchUsuario[16]){
						echo '<option selected value='.$key.'>'.$key.'</option>';
					}else{
						echo '<option value='.$key.'>'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="recipient-name" class="form-control-label" >Caminar<i style="color: darkorange">*</i></label>
			<select class="form-control" name="caminar" >
				<option>Selecciona una opción</option>
				<?php
				foreach ($select as $key) {
					if($key == $fetchUsuario[17]){
						echo '<option selected value='.$key.'>'.$key.'</option>';
					}else{
						echo '<option value='.$key.'>'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="recipient-name" class="form-control-label" >Pasear<i style="color: darkorange">*</i></label>
			<select class="form-control" name="pasear" >
				<option>Selecciona una opción</option>
				<?php
				foreach ($select as $key) {
					if($key == $fetchUsuario[18]){
						echo '<option selected value='.$key.'>'.$key.'</option>';
					}else{
						echo '<option value='.$key.'>'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>

		<div class="form-group col-md-12">
			<label for="recipient-name" class="form-control-label" >Otra<i style="color: darkorange">*</i></label>
			<input type="text" class="form-control" name="otra" value="<?= $fetchUsuario[21] ?>"/>
		</div>
		<div class="form-group col-md-12">
			<label for="recipient-name" class="form-control-label" >Observación<i style="color: darkorange">*</i></label>
			<textarea class="form-control" name="observacion" cols="40" rows="5" ><?= $fetchUsuario[22] ?></textarea>
		</div>
	</div>
	<!-- End Row -->


	<!-- Modal Footer -->
	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">
			Cancelar
		</button>
		<button type="submit" class="btn btn-primary" name="Actualizar">
			Actualizar
		</button>
	</div>
