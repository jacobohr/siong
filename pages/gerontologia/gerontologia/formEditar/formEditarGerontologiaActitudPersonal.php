<?php require "../../../../session.php";



$sqlUsuario = "SELECT 
*,
(CONVERT(CAST(CONVERT(observacion USING latin1) AS BINARY) USING utf8))AS observacion,
(CONVERT(CAST(CONVERT(tiempo_libre USING latin1) AS BINARY) USING utf8))AS tiempo_libre
FROM gddt_actitud_personal 
WHERE id_actitud_personal =   ". $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);

?>
<form name="editarUsuario" method="POST" action="procesarPsicologiaAntceMental.php?tipo=update" enctype="multipart/form-data">
	<h4 class="text-center mb-4"> Actitud Personal </h4>
	<div class="row">
		<div class="form-group col-md-4">
			<label for="recipient-name" class="form-control-label">Usuario que registra <i style="color: darkorange">*</i></label>
			<select class="form-control" name="usuario" >
				<option>Selecciona una opción</option>
				<?php
				$queryCuenta = mysqli_query($conn, "SELECT id_cuenta,
					UPPER((CONVERT(CAST(CONVERT(CONCAT(primer_nombre,' ',segundo_nombre,' ',primer_apellido,' ',segundo_apellido) USING latin1) AS BINARY) USING utf8))) AS nombreUsuario FROM gddt_cuentas ;");
				while ($fetchCuenta = mysqli_fetch_row($queryCuenta)) {
					if ($fetchCuenta[0] == $fetchUsuario[1]) {
						echo "<option selected value=" . $fetchCuenta[0] . ">" . $fetchCuenta[1] . "</option>";
					} else {
						echo "<option value=" . $fetchCuenta[0] . ">" . $fetchCuenta[1] . "</option>";
					}
				}
				?>
			</select>
		</div>
		<?php 
		if(strlen($fetchUsuario[3]) < 2){$mes = '0'.$fetchUsuario[3];}else{$mes = $fetchUsuario[3];}
		if(strlen($fetchUsuario[4]) < 2){$dia = '0'.$fetchUsuario[4];}else{$dia = $fetchUsuario[4];}
		$year = $fetchUsuario[2];
		$fecha = $year.'-'.$mes.'-'.$dia;
		?>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Fecha de modificacion <i style="color: darkorange">*</i></label>
			<input type="date" class="form-control" name="fechaMod" value="<?= $fecha ?>"/>
		</div>
		<div class="form-group col-md-4">
			<label for="recipient-name" class="form-control-label" >Autonomía<i style="color: darkorange">*</i></label>
			<?php 
			$selectAutonomia = array(
				0 => 'Total',
				1 => 'Parcial',
				2 => 'Dependiente'
			);
			?>
			<select class="form-control" name="autonomia" >
				<option>Selecciona una opción</option>
				<?php
				foreach ($selectAutonomia as $key) {
					if($key == $fetchUsuario[7]){
						echo '<option selected value='.$key.'>'.$key.'</option>';
					}else{
						echo '<option value='.$key.'>'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="recipient-name" class="form-control-label" >Autoestima<i style="color: darkorange">*</i></label>
			<?php 
			$selectAutoestimaAutoconceptoAutovaloración = array(
				0 => 'Alta',
				1 => 'Moderada',
				2 => 'Escasa',
				3 => 'No posee',
				4 => 'No aplica'
			);
			?>
			<select class="form-control" name="autoestima" >
				<option>Selecciona una opción</option>
				<?php
				foreach ($selectAutoestimaAutoconceptoAutovaloración as $key) {
					if($key == $fetchUsuario[8]){
						echo '<option selected value='.$key.'>'.$key.'</option>';
					}else{
						echo '<option value='.$key.'>'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="recipient-name" class="form-control-label" >Autoconcepto<i style="color: darkorange">*</i></label>
			<select class="form-control" name="autoconcepto" >
				<option>Selecciona una opción</option>
				<?php
				foreach ($selectAutoestimaAutoconceptoAutovaloración as $key) {
					if($key == $fetchUsuario[9]){
						echo '<option selected value='.$key.'>'.$key.'</option>';
					}else{
						echo '<option value='.$key.'>'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="recipient-name" class="form-control-label" >Autovaloración<i style="color: darkorange">*</i></label>
			<select class="form-control" name="autovaloracion" >
				<option>Selecciona una opción</option>
				<?php
				foreach ($selectAutoestimaAutoconceptoAutovaloración as $key) {
					if($key == $fetchUsuario[10]){
						echo '<option selected value='.$key.'>'.$key.'</option>';
					}else{
						echo '<option value='.$key.'>'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="recipient-name" class="form-control-label" >Estado de ánimo<i style="color: darkorange">*</i></label>
			<?php 
			$selectEstadoAnimo = array(
				0 => 'Dinamico',
				1 => 'Participativo',
				2 => 'Alegre',
				3 => 'Atento',
				4 => 'Apatico',
				5 => 'Agresivo',
				6 => 'Indiferente'
			);
			?>
			<select class="form-control" name="estadoDeAnimo" >
				<option>Selecciona una opción</option>
				<?php
				foreach ($selectEstadoAnimo as $key) {
					if($key == $fetchUsuario[11]){
						echo '<option selected value='.$key.'>'.$key.'</option>';
					}else{
						echo '<option value='.$key.'>'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="recipient-name" class="form-control-label" >Cuidado personal<i style="color: darkorange">*</i></label>
			<?php 
			$selectCuidadoPersonal = array(
				0 => 'Excelente',
				1 => 'Bueno',
				2 => 'Aceptable',
				3 => 'Deficiente'
			);
			?>
			<select class="form-control" name="cuidadoPersonal" >
				<option>Selecciona una opción</option>
				<?php
				foreach ($selectCuidadoPersonal as $key) {
					if($key == $fetchUsuario[12]){
						echo '<option selected value='.$key.'>'.$key.'</option>';
					}else{
						echo '<option value='.$key.'>'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="recipient-name" class="form-control-label" >Actitud ante la vejez<i style="color: darkorange">*</i></label>
			<?php 
			$selectActitudVejez = array(
				0 => 'Positiva',
				1 => 'Negativa',
				2 => 'Indiferente',
				3 => 'No aplica'
			);
			?>
			<select class="form-control" name="actitudVejez" >
				<option>Selecciona una opción</option>
				<?php
				foreach ($selectActitudVejez as $key) {
					if($key == $fetchUsuario[13]){
						echo '<option selected value='.$key.'>'.$key.'</option>';
					}else{
						echo '<option value='.$key.'>'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="recipient-name" class="form-control-label" >relaciones familiares<i style="color: darkorange">*</i></label>
			<?php 
			$selectRelacionesFamiliares = array(
				0 => 'Afectiva',
				1 => 'Regular',
				2 => 'Deficiente',
				3 => 'Sin familia',
				4 => 'Buena'
			);
			?>
			<select class="form-control" name="relacionesFamiliares" >
				<option>Selecciona una opción</option>
				<?php
				foreach ($selectRelacionesFamiliares as $key) {
					if($key == $fetchUsuario[14]){
						echo '<option selected value='.$key.'>'.$key.'</option>';
					}else{
						echo '<option value='.$key.'>'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="recipient-name" class="form-control-label" >relaciones con los compañeros<i style="color: darkorange">*</i></label>
			<?php 
			$selectRelacionesCompaneros = array(
				0 => 'Armoniosa',
				1 => 'Regular',
				2 => 'Deficiente',
				3 => 'Discordante',
				4 => 'Buena'
			);
			?>
			<select class="form-control" name="relacionesCompaneros" >
				<option>Selecciona una opción</option>
				<?php
				foreach ($selectRelacionesCompaneros as $key) {
					if($key == $fetchUsuario[15]){
						echo '<option selected value='.$key.'>'.$key.'</option>';
					}else{
						echo '<option value='.$key.'>'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="recipient-name" class="form-control-label" >relaciones con los funcionarios<i style="color: darkorange">*</i></label>
			<?php 
			$selectRelacionesFuncionarios = array(
				0 => 'Buena',
				1 => 'Regular',
				2 => 'Mala'
			);
			?>
			<select class="form-control" name="relacionesFuncionarios" >
				<option>Selecciona una opción</option>
				<?php
				foreach ($selectRelacionesFuncionarios as $key) {
					if($key == $fetchUsuario[16]){
						echo '<option selected value='.$key.'>'.$key.'</option>';
					}else{
						echo '<option value='.$key.'>'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-12">
			<label for="recipient-name" class="form-control-label" >Empleo del tiempo libre<i style="color: darkorange">*</i></label>
			<textarea class="form-control" name="tiempoLibre" cols="40" rows="5" ><?= $fetchUsuario[20] ?></textarea>
		</div>
		<div class="form-group col-md-12">
			<label for="recipient-name" class="form-control-label" >Observacionese<i style="color: darkorange">*</i></label>
			<textarea class="form-control" name="observaciones" cols="40" rows="5" ><?= $fetchUsuario[19] ?></textarea>
		</div>
	</div>
	<!-- End Row -->


	<!-- Modal Footer -->
	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">
			Cancelar
		</button>
		<button type="submit" class="btn btn-primary" name="Actualizar">
			Actualizar
		</button>
	</div>
