<?php
//include connection file 
require "../../../../session.php";

$sqlUsuario = "
SELECT 
UPPER((CONVERT(CAST(CONVERT(CONCAT(c.primer_nombre,' ',c.segundo_nombre,' ',c.primer_apellido,' ',c.segundo_apellido) USING latin1) AS BINARY) USING utf8))) AS nombreUsuario,
CONCAT(ap.ano_mod,'/',ap.mes_mod,'/',ap.dia_mod) AS fecha,  
ap.hora_mod,
ap.autonomia,
ap.autoestima,
ap.autoconcepto,
ap.autovaloracion,
ap.estado_animo,
ap.cuidado_personal,
ap.actitud_vejez,
ap.relacion_familiar,
ap.relacion_companero,
ap.relacion_funcionario,
(CONVERT(CAST(CONVERT(ap.observacion USING latin1) AS BINARY) USING utf8))AS observacion,
(CONVERT(CAST(CONVERT(ap.tiempo_libre USING latin1) AS BINARY) USING utf8))AS tiempo_libre
FROM gddt_actitud_personal ap
INNER JOIN gddt_cuentas c ON c.id_cuenta = ap.id_cuenta 
WHERE ap.id_actitud_personal =  " . $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);
$rows = mysqli_num_rows($queryUsuario);

if($rows == 0){
    echo "<p style='color:red;font-size:0.9em;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No se encontraron datos :(</p>";
}else{

    ?>

    <div class="content">
        <h6 class="text-center mb-4">Actualizado por <?= $fetchUsuario[0];?> el <?= $fetchUsuario[1]; ?> a las <?= $fetchUsuario[2]; ?></h6>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Autonomía
            </label>
            <p><?php if(empty($fetchUsuario[3])){echo "Sin dato";}else{echo $fetchUsuario[3];} ?></p>
        </div>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Autoestima
            </label>
            <p><?php if(empty($fetchUsuario[4])){echo "Sin dato";}else{echo $fetchUsuario[4];} ?></p>
        </div>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Autoconcepto
            </label>
            <p><?php if(empty($fetchUsuario[5])){echo "Sin dato";}else{echo $fetchUsuario[5];} ?></p>
        </div>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Autovaloración
            </label>
            <p><?php if(empty($fetchUsuario[6])){echo "Sin dato";}else{echo $fetchUsuario[6];} ?></p>
        </div>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Estado de ánimo
            </label>
            <p><?php if(empty($fetchUsuario[7])){echo "Sin dato";}else{echo $fetchUsuario[7];} ?></p>
        </div>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Cuidado personal 
            </label>
            <p><?php if(empty($fetchUsuario[8])){echo "Sin dato";}else{echo $fetchUsuario[8];} ?></p>
        </div>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Actitud ante la vejez
            </label>
            <p><?php if(empty($fetchUsuario[9])){echo "Sin dato";}else{echo $fetchUsuario[9];} ?></p>
        </div>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Manejo de las relaciones familiares
            </label>
            <p><?php if(empty($fetchUsuario[10])){echo "Sin dato";}else{echo $fetchUsuario[10];} ?></p>
        </div>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Manejo de las relaciones con los compañeros
            </label>
            <p><?php if(empty($fetchUsuario[11])){echo "Sin dato";}else{echo $fetchUsuario[11];} ?></p>
        </div>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Manejo de las relaciones con los funcionarios
            </label>
            <p><?php if(empty($fetchUsuario[12])){echo "Sin dato";}else{echo $fetchUsuario[12];} ?></p>
        </div>
        <div class="user-block col-sm-12 col-xs-12">
            <label>
                Empreo del tiempo libre  
            </label>
            <p><?php if(empty($fetchUsuario[14])){echo "Sin dato";}else{echo $fetchUsuario[14];} ?></p>
        </div>
         <div class="user-block col-sm-12 col-xs-12">
            <label>
                Observaciones
            </label>
            <p><?php if(empty($fetchUsuario[13])){echo "Sin dato";}else{echo $fetchUsuario[13];} ?></p>
        </div>

        <div class="user-block">&nbsp;</div>

    </div><!-- Fin del content -->
    <?php } ?>