<?php
//include connection file 
require "../../../../session.php";

$sqlUsuario = "
SELECT 
UPPER((CONVERT(CAST(CONVERT(CONCAT(c.primer_nombre,' ',c.segundo_nombre,' ',c.primer_apellido,' ',c.segundo_apellido) USING latin1) AS BINARY) USING utf8))) AS nombreUsuario,
CONCAT(ai.ano_mod,'/',ai.mes_mod,'/',ai.dia_mod) AS fecha,  
ai.hora_mod,
ai.uso_telefono,
ai.manejo_llaves,
ai.abrir_cerrar_puertas,
ai.abrir_paquetes,
ai.usar_radio_television,
ai.manejo_dinero_propio,
(CONVERT(CAST(CONVERT(ai.observacion USING latin1) AS BINARY) USING utf8))AS observacion
FROM gddt_abc_instrumental ai
INNER JOIN gddt_cuentas c ON c.id_cuenta = ai.id_cuenta 
WHERE ai.id_abc_instrumental =  " . $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);
$rows = mysqli_num_rows($queryUsuario);

if($rows == 0){
    echo "<p style='color:red;font-size:0.9em;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No se encontraron datos :(</p>";
}else{

    ?>

    <div class="content">
        <h6 class="text-center mb-4">Actualizado por <?= $fetchUsuario[0];?> el <?= $fetchUsuario[1]; ?> a las <?= $fetchUsuario[2]; ?></h6>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                ¿Usa el teléfono?
            </label>
            <p><?php if(empty($fetchUsuario[3])){echo "Sin dato";}else{echo $fetchUsuario[3];} ?></p>
        </div>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                ¿Maneja llaves?
            </label>
            <p><?php if(empty($fetchUsuario[4])){echo "Sin dato";}else{echo $fetchUsuario[4];} ?></p>
        </div>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                ¿Abre y cierra puertas?
            </label>
            <p><?php if(empty($fetchUsuario[5])){echo "Sin dato";}else{echo $fetchUsuario[5];} ?></p>
        </div>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                ¿Abre paquetes?
            </label>
            <p><?php if(empty($fetchUsuario[6])){echo "Sin dato";}else{echo $fetchUsuario[6];} ?></p>
        </div>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                ¿Usa radio y televisión?
            </label>
            <p><?php if(empty($fetchUsuario[7])){echo "Sin dato";}else{echo $fetchUsuario[7];} ?></p>
        </div>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                ¿Maneja dinero propio?  
            </label>
            <p><?php if(empty($fetchUsuario[8])){echo "Sin dato";}else{echo $fetchUsuario[8];} ?></p>
        </div>
        <div class="user-block col-sm-12 col-xs-12">
            <label>
                Observación  
            </label>
            <p><?php if(empty($fetchUsuario[9])){echo "Sin dato";}else{echo $fetchUsuario[9];} ?></p>
        </div>

        <div class="user-block">&nbsp;</div>

    </div><!-- Fin del content -->
    <?php } ?>