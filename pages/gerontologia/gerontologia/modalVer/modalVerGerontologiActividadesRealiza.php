<?php
//include connection file 
require "../../../../session.php";

$sqlUsuario = "
SELECT 
UPPER((CONVERT(CAST(CONVERT(CONCAT(c.primer_nombre,' ',c.segundo_nombre,' ',c.primer_apellido,' ',c.segundo_apellido) USING latin1) AS BINARY) USING utf8))) AS nombreUsuario,
CONCAT(ar.ano_mod,'/',ar.mes_mod,'/',ar.dia_mod) AS fecha,  
ar.hora_mod,
ar.juego_mesa_actividad,
ar.fiesta_social_actividad,
ar.lectura_actividad,
ar.escritura_actividad,
ar.manualidad_actividad,
ar.pintura_actividad,
ar.musica_actividad,
ar.gimnasia_actividad,
ar.taller_dirigido_actividad,
ar.television_actividad,
(CONVERT(CAST(CONVERT(ar.otra_realiza USING latin1) AS BINARY) USING utf8))AS otra_realiza,
(CONVERT(CAST(CONVERT(ar.observacion_realiza USING latin1) AS BINARY) USING utf8))AS observacion_realiza
FROM gddt_actividades_realiza ar
INNER JOIN gddt_cuentas c ON c.id_cuenta = ar.id_cuenta 
WHERE ar.id_actividad_realiza =  " . $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);
$rows = mysqli_num_rows($queryUsuario);

if($rows == 0){
    echo "<p style='color:red;font-size:0.9em;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No se encontraron datos :(</p>";
}else{

    ?>

    <div class="content">
        <h6 class="text-center mb-4">Actualizado por <?= $fetchUsuario[0];?> el <?= $fetchUsuario[1]; ?> a las <?= $fetchUsuario[2]; ?></h6>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Juegos de mesa
            </label>
            <p><?php if(empty($fetchUsuario[3])){echo "Sin dato";}else{echo $fetchUsuario[3];} ?></p>
        </div>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Fiestas sociales
            </label>
            <p><?php if(empty($fetchUsuario[4])){echo "Sin dato";}else{echo $fetchUsuario[4];} ?></p>
        </div>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Lectura
            </label>
            <p><?php if(empty($fetchUsuario[5])){echo "Sin dato";}else{echo $fetchUsuario[5];} ?></p>
        </div>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Escritura
            </label>
            <p><?php if(empty($fetchUsuario[6])){echo "Sin dato";}else{echo $fetchUsuario[6];} ?></p>
        </div>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Manualidades
            </label>
            <p><?php if(empty($fetchUsuario[7])){echo "Sin dato";}else{echo $fetchUsuario[7];} ?></p>
        </div>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Pintura
            </label>
            <p><?php if(empty($fetchUsuario[8])){echo "Sin dato";}else{echo $fetchUsuario[8];} ?></p>
        </div>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Música
            </label>
            <p><?php if(empty($fetchUsuario[9])){echo "Sin dato";}else{echo $fetchUsuario[9];} ?></p>
        </div>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Gimnasia
            </label>
            <p><?php if(empty($fetchUsuario[10])){echo "Sin dato";}else{echo $fetchUsuario[10];} ?></p>
        </div>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Talleres dirigidos
            </label>
            <p><?php if(empty($fetchUsuario[11])){echo "Sin dato";}else{echo $fetchUsuario[11];} ?></p>
        </div>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Ver televisión
            </label>
            <p><?php if(empty($fetchUsuario[12])){echo "Sin dato";}else{echo $fetchUsuario[12];} ?></p>
        </div>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Otra
            </label>
            <p><?php if(empty($fetchUsuario[13])){echo "Sin dato";}else{echo $fetchUsuario[13];} ?></p>
        </div>
        <div class="user-block col-sm-12 col-xs-12">
            <label>
                Observación 
            </label>
            <p><?php if(empty($fetchUsuario[14])){echo "Sin dato";}else{echo $fetchUsuario[14];} ?></p>
        </div>
        

        <div class="user-block">&nbsp;</div>

    </div><!-- Fin del content -->
    <?php } ?>