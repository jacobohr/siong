<?php 
include "Conexion.php";

class FacturaEscalaVida{

	private $idEncuesta;

	public function __construct($param){
		$this->idEncuesta = $param;
	}

	public function InicioHTML(){
		$html = '
		<!DOCTYPE html>
		<html>
		<head>
		<title></title>
		</head>
		<body>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">';
		return $html;
	}
	public function finHTML(){
		$html = '
		</body>
		</html>';
		return $html;
	}

	public function inicioTabla(){
		$html = '<table class="table table-sm table-bordered">';
		return $html;
	}
	public function finTabla(){
		$html = '</table>';
		return $html;
	}
	public function inicioThead(){
		$html = '<thead>';
		return $html;
	}
	public function finThead(){
		$html = '</thead>';
		return $html;
	}
	public function inicioTbody(){
		$html = '<tbody>';
		return $html;
	}
	public function finTbody(){
		$html = '</tbody>';
		return $html;
	}
	public function encabezadoTabla(){
		$arrayDatos = $this->traerDatosBasicos();


		$html = '
		<tr>
		<th class="text-center" colspan="5">CALIDAD DE VIDA</th>
		</tr>
		<tr>
		<td style="border-left: 1px solid white; border-right: 1px solid white;" colspan="5" ></td>
		</tr>
		<tr>
		<td class="text-center" colspan="5">DOCUMENTO: '.$arrayDatos[1].' NOMBRE: '.$arrayDatos[0].'</td>
		</tr>
		<tr >
		<td style="border-left: 1px solid white; border-right: 1px solid white;" colspan="5" ></td>
		</tr>';
		return $html;
	}
	public function elementosEvaluar(){
		$html ='
		<tr>
		<td class="text-center">ITEMS</td>
		<td class="text-center">CASI SIEMPRE</td>
		<td class="text-center">A MENUDO</td>
		<td class="text-center">ALGUNAS VECES</td>
		<td class="text-center">CASI NUNCA</td>
		</tr>';
		return $html;
	}
	public function sumaRespuestas(){
		$html = '
		<tr>
		<td></td>
		';
		return $html;
	}
	public function detalleReporte(){
		$preguntas = array(
			0 =>  '1. Realizo las actividades físicas con facilidad (ejercicios físicos, subir y bajar escaleras, agacharse y levantarse)',
			1 =>  '2. Puedo salir de compras normalmente',
			2 =>  '3. Mi familia me quiere y me respeta',
			3 =>  '4. Mi estado de ánimo es favorable',
			4 =>  '5. Me siento confiado y seguro frente al futuro',
			5 =>  '6. Con la jubilación mi vida perdió sentido',
			6 =>  '7. Mi estado de salud me permite realizar por mi mismo las actividades de la vida diaria (Bañarme, comer, limpiar, cocinar, lavar)',
			7 =>  '8. Puedo leer frecuentemente libros, revista o periódicos',
			8 =>  '9. Soy importante para mi familia.',
			9 => '10. Me siento solo y desamparado en la vida',
			10 => '11. Mi vida es aburrida y monótona',
			11 => '12. Camino con ayuda de alguien o de algo',
			12 => '13. Tengo facilidad para aprender nuevas cosas',
			13 => '14. Utilizo el transporte público con facilidad',
			14 => '15. Mi situación monetaria me permite resolver todas mis necesidades, de cualquier índole',
			15 => '16. Mi familia me ayuda a resolver los problemas que se me puedan presentar',
			16 => '17. He logrado realizar en mi vida mis aspiraciones',
			17 => '18. Estoy satisfecho con las condiciones económicas que tengo',
			18 => '19. Soy capaz de atenderme a mí mismo y cuidar de mi persona',
			19 => '20. Estoy nervioso e inquieto',
			20 => '21. Puedo ayudar en el cuidado y atención de mis nietos u otros niños que vivan en el hogar',
			21 => '22. Puedo expresar a mi familia lo que pienso y siento',
			22 => '23. Mis creencias me dan seguridad en el futuro',
			23 => '24. Mantengo relaciones con mis amigos y vecinos',
			24 => '25. Soy feliz con la familia que tengo',
			25 => '26.  Salgo a distraerme con frecuencia con mi familia y amigos',
			26 => '27.  Mi vivienda tiene buenas condiciones para vivir yo en ella',
			27 => '28. He pensado en quitarme la vida',
			28 => '29. Mi familia me tiene en cuenta para tomar decisiones en los problemas de hogar',
			29 => '30. Considero que todavía puedo ser una persona útil',
			30 => '31.Mi vivienda me resulta cómoda para mis necesidades',
			31 => '32. Mi estado de salud me permite disfrutar de la vida',
			32 => '33. Tengo aspiraciones y planes para el futuro',
			33 => '34. Soy feliz con la vida que llevo'
		);
		
		
		$arrayRespuestas = $this->traerDatosItems();

		$k = 0;
		$acumulador = 0;
		$html = '';

		foreach ($preguntas as $key) {
			$html ='<tr>';
			$html .= '<td>'.$key.'</td>';
			if($arrayRespuestas[$k]=='4'){$html .='<td class="text-center">X</td>';}else{$html .= '<td></td>';}
			if($arrayRespuestas[$k]=='3'){$html .='<td class="text-center">X</td>';}else{$html .= '<td></td>';}
			if($arrayRespuestas[$k]=='2'){$html .='<td class="text-center">X</td>';}else{$html .= '<td></td>';}
			if($arrayRespuestas[$k]=='1'){$html .='<td class="text-center">X</td>';}else{$html .= '<td></td>';}
			$html .= '</tr>';
			echo $html;
			$acumulador = $acumulador + $arrayRespuestas[$k];
			$k++;
			
		}
		$html ='<tr>';
		$html .='<td class="text-right" colspan="5">SUMA DE LAS RESPUESTAS => '.$acumulador.' </td>';
		$html .='</tr>';
		echo $html;
	}
	public function traerDatosItems(){
		$conexion = new Conexion();
		$id = $this->idEncuesta;
		$sql = 'SELECT respuestas FROM gddt_encuesta_calidad_de_vida WHERE id_encuesta = '.$id ;
		$query = mysqli_query($conexion->conn, $sql);
		$fetch = mysqli_fetch_row($query);
		$array = str_split($fetch[0]);
		return $array;
	}
	public function traerDatosBasicos(){
		$conexion = new Conexion();
		$id = $this->idEncuesta;
		$sql = 'SELECT 
		UPPER((CONVERT(CAST(CONVERT(CONCAT(c.primer_nombre," ",c.segundo_nombre," ",c.primer_apellido," ",c.segundo_apellido) USING latin1) AS BINARY) USING utf8))) AS nombreUsuario,
		c.num_documento
		FROM gddt_encuesta_calidad_de_vida cv
		INNER JOIN gddt_matricula_datos c ON cv.id_matricula_dato = c.id_matricula_dato
		WHERE cv.id_encuesta ='.$id ;
		$query = mysqli_query($conexion->conn, $sql);
		$fetch = mysqli_fetch_row($query);
		$array = array(
			0 => "$fetch[0]",
			1 => "$fetch[1]"
		);
		return $array;
	}
}
?>