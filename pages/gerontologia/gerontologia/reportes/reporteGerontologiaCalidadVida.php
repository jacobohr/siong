<?php
require_once __DIR__ . '/../../../../plugins/dompdf/autoload.inc.php';
require 'FacturaEscalaVida.php';
$id = $_REQUEST["idu"];
use Dompdf\Dompdf;
$dompdf = new Dompdf();
$factura = new FacturaEscalaVida($id);
ob_start();
echo $factura->InicioHTML();
echo $factura->inicioTabla();
echo $factura->inicioThead();
echo $factura->encabezadoTabla();
echo $factura->elementosEvaluar();
echo $factura->finThead();
echo $factura->inicioTbody();
$factura->detalleReporte();
echo $factura->finTbody();
echo $factura->finTabla();
echo $factura->finHTML();


$dompdf->loadHtml(ob_get_clean());
$dompdf->render();
$pdf = $dompdf->output();
$filename = "factura_venta";
file_put_contents($filename,$pdf);
$dompdf->stream($filename,  array("Attachment" => 0));
?>

