<?php 


class Conexion{    
	private $dbServer = 'localhost';
	private $dbUser = 'root';
	private $dbPassword = '';
	public $dbSchema = 'c0siong_gestor';
	public $conn;
	public function __construct(){
		try {
			$this->conn = mysqli_connect($this->dbServer,$this->dbUser, $this->dbPassword,$this->dbSchema) or die("Connection failed: " . mysqli_connect_error());
		} catch (PDOException  $e) {
			echo "ERROR: " . $e->getMessage();
		}
	}
}