<?php

//include connection file 
include_once(dirname(__FILE__) . '/../../session.php');

?>

<div class="content m-3">
	<form name="registroPacientes" id="registroPacientes" enctype="multipart/form-data">
		<div class="row">
			<div class="form-group col-md-4">
				<label for="recipient-name" class="form-control-label">Empresa <i style="color: darkorange">*</i></label>
				<select class="form-control" name="tipo_empresa" required>
					<option value="">Selecciona una opción</option>
					<?php
					$queryTipoEmpresa = mysqli_query($conn, "SELECT id_empresa, nom_empresa FROM gddt_empresas;");
					while($fetchTipoEmpresa = mysqli_fetch_row($queryTipoEmpresa)) {
						echo "<option value=".$fetchTipoEmpresa[0].">".(strtoupper($fetchTipoEmpresa[1]))."</option>";
					}
					?>
				</select>
			</div>

			<div class="form-group col-md-4">
				<label for="message-text" class="form-control-label">Nombre empresa</label>
				<input type="text" class="form-control" name="numero_empresa" required />
			</div>

			<div class="form-group col-md-4">
				<label for="message-text" class="form-control-label">Cargo </label>
				<input type="text" class="form-control" name="cargo" required style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();" />
			</div>

			<div class="form-group col-md-4">
				<label for="recipient-name" class="form-control-label">Cuenta <i style="color: darkorange">*</i></label>
				<select class="form-control" name="cuenta" required>
					<option value="">Selecciona una opción</option>
					<?php
					$queryCuenta = mysqli_query($conn, "SELECT id_cuenta,CONCAT(primer_nombre,' ',segundo_nombre,' ',primer_apellido,' ',segundo_apellido)AS nombre_completo FROM gddt_cuentas;");
					while($fetchCuenta = mysqli_fetch_row($queryCuenta)) {
						echo "<option value=".$fetchCuenta[0].">".(strtoupper($fetchCuenta[1]))."</option>";
					}
					?>
				</select>
			</div>

			<div class="form-group col-md-4">
				<label for="message-text" class="form-control-label">Estado <i style="color: darkorange">*</i></label>
				<select class="form-control" name="estado" required>
					<option value="">Selecciona una opción</option>
					<?php
					$queryEstado = mysqli_query($conn, "SELECT id_estado, descripcion FROM gddt_estados;");
					while($fetchEstado = mysqli_fetch_row($queryEstado)) {
						echo "<option value=".$fetchEstado[0].">".(strtoupper($fetchEstado[1]))."</option>";
					}
					?>
				</select>
			</div>

			<div class="form-group col-md-4">
				<label for="message-text" class="form-control-label">Fecha ingreso </label>
				<input type="text" class="form-control" name="fecha" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();" />
			</div>

			<div class="form-group col-md-4">
				<label for="recipient-name" class="form-control-label">Perfil <i style="color: darkorange">*</i></label>
				<select class="form-control" name="perfil" required>
					<option value="">Selecciona una opción</option>
					<?php
					$querPerfil = mysqli_query($conn, "SELECT id_perfil_usuario, descripcion FROM gddt_perfil_usuarios;");
					while($fetchPerfil = mysqli_fetch_row($querPerfil)) {
						echo "<option value=".$fetchPerfil[0].">".strtoupper($fetchPerfil[1])."</option>";
					}
					?>
				</select>
			</div>

			<div class="form-group col-md-4">
				<label for="message-text" class="form-control-label">Tipo de Usuario <i style="color: darkorange">*</i></label>
				<select class="form-control" name="tipo_usuario" required>
					<option value="">Selecciona una opción</option>
					<?php
					$querTipoUsuario = mysqli_query($conn, "SELECT id_tipo_usuario, descripcion FROM gddt_tipo_usuarios;");
					while($fetchTipoUsuario = mysqli_fetch_row($querTipoUsuario)) {
						echo "<option value=".$fetchTipoUsuario[0].">".strtoupper($fetchTipoUsuario[1])."</option>";
					}
					?>
				</select>
			</div>

			<div class="form-group col-md-4">
				<label for="message-text" class="form-control-label">Primer nombre <i style="color: darkorange">*</i></label>
				<input type="text" class="form-control" name="primer_nombre" required style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();" />
			</div>

			<div class="form-group col-md-4">
				<label for="message-text" class="form-control-label">Segundo nombre </label>
				<input type="text" class="form-control" name="segundo_nombre" required style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();" />
			</div>

			<div class="form-group col-md-4">
				<label for="message-text" class="form-control-label">Primer apellido <i style="color: darkorange">*</i></label>
				<input type="text" class="form-control" name="primer_apellido" required style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();" />
			</div>

			<div class="form-group col-md-4">
				<label for="message-text" class="form-control-label">Segundo apellido </label>
				<input type="text" class="form-control" name="segundo_apellido" required style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();" />
			</div>

			<div class="form-group col-md-4">
				<label for="message-text" class="form-control-label">Numero de documento <i style="color: darkorange">*</i></label>
				<input type="number" class="form-control" name="numero_documento" required style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();" />
			</div>

			<div class="form-group col-md-4">
				<label for="message-text" class="form-control-label">Fecha de nacimiento <i style="color: darkorange">*</i></label>
				<input type="date" class="form-control" name="fecha_nacimiento" />
			</div>

			<div class="form-group col-md-4">
				<label for="recipient-name" class="form-control-label">Lugar de expedicion  <i style="color: darkorange">*</i></label>
				<select class="form-control" name="lugar_expedicion" required>
					<option value="">Selecciona una opción</option>
					<?php
					$queryExpedicion = mysqli_query($conn, 
						"SELECT mun.id_municipio,(CONVERT(CAST(CONVERT(mun.descripcion USING latin1) AS BINARY) USING utf8)) AS descripcion
						FROM gddt_matricula_datos md
						INNER JOIN gddt_municipios mun
						ON  md.id_lugar_documento = mun.id_municipio
						GROUP BY mun.id_municipio ;");
					while ($fetchExpedicion = mysqli_fetch_row($queryExpedicion)) {
						echo "<option value=" . $fetchExpedicion[0] . ">" . $fetchExpedicion[1] . "</option>";
					}
					?>
				</select>
			</div>

			<div class="form-group col-md-4">
				<label for="recipient-name" class="form-control-label">Tipo de documento  <i style="color: darkorange">*</i></label>
				<select class="form-control" name="lugar_expedicion" required>
					<option value="">Selecciona una opción</option>
					<?php
					$queryTipoDocumento = mysqli_query($conn, "SELECT id_tipo_documento, descripcion FROM gddt_tipo_documento;");
					while ($fetchTipoDocumento = mysqli_fetch_row($queryTipoDocumento)) {
						echo "<option value=" . $fetchTipoDocumento[0] . ">" . $fetchTipoDocumento[1] . "</option>";
					}
					?>
				</select>
			</div>

			<div class="form-group col-md-4">
				<label for="recipient-name" class="form-control-label">Genero  <i style="color: darkorange">*</i></label>
				<select class="form-control" name="genero" required>
					<option value="">Selecciona una opción</option>
					<?php
					$queryGenero = mysqli_query($conn, "SELECT id_genero, nom_genero FROM gddt_generos;");
					while ($fetchGenero = mysqli_fetch_row($queryGenero)) {
						echo "<option value=" . $fetchGenero[0] . ">" . $fetchGenero[1] . "</option>";
					}
					?>
				</select>
			</div>

			<div class="form-group col-md-4">
				<label for="recipient-name" class="form-control-label">Estado civil  <i style="color: darkorange">*</i></label>
				<select class="form-control" name="estado_civil" required>
					<option value="">Selecciona una opción</option>
					<?php
					$queryEstadoCivil = mysqli_query($conn, "SELECT id_estado_civil, descripcion FROM gddt_estados_civiles;");
					while ($fetchEstadoCivil = mysqli_fetch_row($queryEstadoCivil)) {
						echo "<option value=" . $fetchEstadoCivil[0] . ">" . $fetchEstadoCivil[1] . "</option>";
					}
					?>
				</select>
			</div>

			<div class="form-group col-md-4 div-datos-usuario" style="display:none;">
				<label for="message-text" class="form-control-label">Fecha de nacimiento <i style="color: darkorange">*</i></label>
				<input type="text" class="form-control" name="fecha_nacimiento" required />
			</div>



			<div class="form-group col-md-4">
				<label for="recipient-name" class="form-control-label">Lugar de nacimiento  <i style="color: darkorange">*</i></label>
				<select class="form-control" name="lugar_nacimiento" required>
					<option value="">Selecciona una opción</option>
					<?php
					$queryLugarNacimiento = mysqli_query($conn, 
						"SELECT mun.id_municipio,(CONVERT(CAST(CONVERT(mun.descripcion USING latin1) AS BINARY) USING utf8)) AS descripcion
						FROM gddt_matricula_datos md
						INNER JOIN gddt_municipios mun
						ON  md.id_lugar_documento = mun.id_municipio
						GROUP BY mun.id_municipio ;");
					while ($fetchLugarNacimiento = mysqli_fetch_row($queryLugarNacimiento)) {
						echo "<option value=" . $fetchLugarNacimiento[0] . ">" . $fetchLugarNacimiento[1] . "</option>";
					}
					?>
				</select>
			</div>

			<div class="form-group col-md-4">
				<label for="recipient-name" class="form-control-label">Procedencia <i style="color: darkorange">*</i></label>
				<select class="form-control" name="procedencia" required>
					<option value="">Selecciona una opción</option>
					<?php
					$queryProcedencia = mysqli_query($conn, "SELECT id_procedencia, descripcion FROM gddt_procedencias;");
					while ($fetchProcedencia = mysqli_fetch_row($queryProcedencia)) {
						echo "<option value=" . $fetchProcedencia[0] . ">" . $fetchProcedencia[1] . "</option>";
					}
					?>
				</select>
			</div>

			<div class="form-group col-md-4">
				<label for="recipient-name" class="form-control-label">Procedencia municipio  <i style="color: darkorange">*</i></label>
				<select class="form-control" name="procedencia_municipio" required>
					<option value="">Selecciona una opción</option>
					<?php
					$queryProcedenciaMunicipio = mysqli_query($conn, 
						"SELECT mun.id_municipio,(CONVERT(CAST(CONVERT(mun.descripcion USING latin1) AS BINARY) USING utf8)) AS descripcion
						FROM gddt_matricula_datos md
						INNER JOIN gddt_municipios mun
						ON  md.id_lugar_documento = mun.id_municipio
						GROUP BY mun.id_municipio ;");
					while ($fetchProcedenciaMunicipio = mysqli_fetch_row($queryProcedenciaMunicipio)) {
						echo "<option value=" . $fetchProcedenciaMunicipio[0] . ">" . $fetchProcedenciaMunicipio[1] . "</option>";
					}
					?>
				</select>
			</div>

			<div class="form-group col-md-4">
				<label for="recipient-name" class="form-control-label">Nivel de estudios <i style="color: darkorange">*</i></label>
				<select class="form-control" name="nivel_estudios" required>
					<option value="">Selecciona una opción</option>
					<?php
					$queryNivelEstudios = mysqli_query($conn, "SELECT id_nivel_estudio, descripcion FROM gddt_nivel_estudios;");
					while ($fetchNivelEstudios = mysqli_fetch_row($queryNivelEstudios)) {
						echo "<option value=" . $fetchNivelEstudios[0] . ">" . $fetchNivelEstudios[1] . "</option>";
					}
					?>
				</select>
			</div>
			<div class="form-group col-md-4">
				<label for="recipient-name" class="form-control-label">Sabe leer </label>
				<select class="form-control" name="lectura" required>
					<option value="">Selecciona una opción</option>
					<option value="s">Si</option>
					<option value="n">No</option>
				</select>
			</div>
			<div class="form-group col-md-4">
				<label for="recipient-name" class="form-control-label">Sabe Escribir </label>
				<select class="form-control" name="lectura" required>
					<option value="">Selecciona una opción</option>
					<option value="s">Si</option>
					<option value="n">No</option>
				</select>
			</div>
			<div class="form-group col-md-4">
				<label for="message-text" class="form-control-label">Nombre de el padre</label>
				<input type="text" class="form-control" name="nombre_padre" required style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();" />
			</div>
			<div class="form-group col-md-4">
				<label for="message-text" class="form-control-label">Nombre de la madre</label>
				<input type="text" class="form-control" name="nombre_madre" required style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();" />
			</div>
			<div class="form-group col-md-4">
				<label for="message-text" class="form-control-label">Nombre de el acudiente</label>
				<input type="text" class="form-control" name="nombre_acudiente" required style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();" />
			</div>
			<div class="form-group col-md-4">
				<label for="message-text" class="form-control-label">Telefono de el acudiente</label>
				<input type="text" class="form-control" name="telefono_acudiente" required style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();" />
			</div>

			<div class="form-group col-md-4">
				<label for="message-text" class="form-control-label">Motivo ingreso <i style="color: darkorange">*</i></label>
				<input type="text" class="form-control" name="motivo_ingreso" required style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();" />
			</div>
			<div class="form-group col-md-4">
				<label for="message-text" class="form-control-label">Ficha remicion ingreso</label>
				<input type="text" class="form-control" name="ficha_emicion" required style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();" />
			</div>
			<div class="form-group col-md-4">
				<label for="message-text" class="form-control-label">Pais remicion ingreso</label>
				<input type="text" class="form-control" name="pais_remicion_ingreso" required style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();" />
			</div>
			<div class="form-group col-md-4">
				<label for="message-text" class="form-control-label">Aseguradora</label>
				<input type="text" class="form-control" name="aseguradora" required style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();" />
			</div>
			<div class="form-group col-md-4">
				<label for="message-text" class="form-control-label">Telefono usuario</label>
				<input type="text" class="form-control" name="telefono_usuario" required style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();" />
			</div>
			<div class="form-group col-md-4">
				<label for="message-text" class="form-control-label">Entidad</label>
				<input type="text" class="form-control" name="entidad" required style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();" />
			</div>
			<div class="form-group col-md-4">
				<label for="message-text" class="form-control-label">Diagnostico base <i style="color: darkorange">*</i></label>
				<textarea class="form-control" name="telefono_usuario" required style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();"></textarea>
			</div>
			<div class="form-group col-md-4">
				<label for="message-text" class="form-control-label">Antecendete laboral</label>
				<textarea class="form-control" name="antecendete_laboral" required style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();"></textarea>
			</div>

			<div class="modal-footer box-footer">
				
				<button type="submit" class="btn btn-primary" name="Guardar">
					Registrar
				</button>
			</div>
		</form>
	</div>