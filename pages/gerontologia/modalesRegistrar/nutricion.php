<!-- Modal Nueva Informacion Nutricional -->
<div id="modalNuevaInformacionNutricional" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-xl">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<div class="form-group col-md-12 text-center" style="margin-bottom: -8px;">
					<h4 class="modal-title">REGISTRAR NUEVA INFORMACION NUTRICIONAL</h4>
				</div>
			</div>
			<div class="modal-body">
				<div class="content">
					<div class="row">
						<form id="registrarInformacionNutricional" name="registrarInformacionNutricional" method="POST" action="<?php echo $base_url ?>pages/gerontologia/nutricion/procesarDatos/procesarInformacionNutricional.php?tipo=insert&idMatriculaDato=<?= $fetchPaciente[0] ?>" autocomplete="off">
							<div class="row">
								<div class="form-group col-md-4">
									<label for="message-text" class="form-control-label">Fecha<i style="color: darkorange">*</i></label>
									<input type="date" class="form-control" name="fecha"  required="true"/>
								</div>
								<div class="form-group col-md-4">
									<label for="message-text" class="form-control-label">Peso (En kilogramos) <i style="color: darkorange">*</i></label>
									<input type="number" class="form-control" name="peso" id="peso"  required="true"/>
								</div>
								<div class="form-group col-md-4">
									<label for="message-text" class="form-control-label">Estatura (En centimetros)<i style="color: darkorange">*</i></label>
									<input type="number" class="form-control" name="estatura" id="estatura" required="true"/>
								</div>
								<div class="form-group col-md-4">
									<label for="message-text" class="form-control-label">Circunferencia de la cintura <i style="color: darkorange">*</i></label>
									<input type="number" class="form-control" name="circunferenciaCintura" required="true" />
								</div>
								<div class="form-group col-md-4">
									<label for="message-text" class="form-control-label">Circunferencia de la cadera <i style="color: darkorange">*</i></label>
									<input type="number" class="form-control" name="circunferenciaCadera"  required="true"/>
								</div>
								<div class="form-group col-md-4">
									<label for="message-text" class="form-control-label">Riesgo cardiovascular <i style="color: darkorange">*</i></label>
									<input type="number" class="form-control" name="riesgoCardiovascular" step="any" required="true"/>
								</div>
								<div class="form-group col-md-4">
									<label for="message-text" class="form-control-label">Circunferencia de el brazo <i style="color: darkorange">*</i></label>
									<input type="number" class="form-control" name="circunferenciaBrazo"  required="true"/>
								</div>
								<div class="form-group col-md-4">
									<label for="message-text" class="form-control-label">Circunferencia de la muñeca <i style="color: darkorange">*</i></label>
									<input type="number" class="form-control" name="circunferenciaMuneca"  required="true"/>
								</div>
								<div class="form-group col-md-4">
									<label for="message-text" class="form-control-label">Circunferencia de la pantorrilla <i style="color: darkorange">*</i></label>
									<input type="number" class="form-control" name="circunferenciaPantorrilla" required="true" />
								</div>
								<div class="form-group col-md-4">
									<label for="message-text" class="form-control-label">Circunferencia tobillo rodilla <i style="color: darkorange">*</i></label>
									<input type="number" class="form-control" name="circunferenciaTobilloRodilla"  required="true"/>
								</div>
								<div class="form-group col-md-4">
									<label for="message-text" class="form-control-label">Complexion <i style="color: darkorange">*</i></label>
									<input type="number" class="form-control" name="complexion"  required="true"/>
								</div>
								<div class="form-group col-md-4">
									<label for="message-text" class="form-control-label">Peso Referencia <i style="color: darkorange">*</i></label>
									<input type="number" class="form-control" name="pesoReferencia"  required="true"/>
								</div>
								<div class="form-group col-md-4">
									<label for="recipient-name" class="form-control-label">Estado dental <i style="color: darkorange">*</i></label>
									<select class="form-control" name="estadoDental" id="estadoDental" required="true">
										<option value="">Selecciona una opción</option>
										<?php
										$queryAntecedente = mysqli_query($conn, "SELECT * FROM gddt_estados_dentales ;");
										while ($fetchAntecedente = mysqli_fetch_row($queryAntecedente)) {
											echo "<option value=" . $fetchAntecedente[0] . ">" . $fetchAntecedente[1] . "</option>";
										}
										?>
									</select>
								</div>
								<div class="form-group col-md-4">
									<label for="recipient-name" class="form-control-label">Clasificacion nutricional <i style="color: darkorange">*</i></label>
									<select class="form-control" name="clasificacionNutricional" id="clasificacionNutricional" required="true">
										<option value="">Selecciona una opción</option>
										<?php
										$queryEstadoDental = mysqli_query($conn, "SELECT id_clasif_nutricional, descripcion FROM gddt_clasif_nutricionales ;");
										while ($fetchEstadoDental = mysqli_fetch_row($queryEstadoDental)) {
											echo "<option value=" . $fetchEstadoDental[0] . ">" . $fetchEstadoDental[1] . "</option>";
										}
										?>
									</select>
								</div>
								<div class="form-group col-md-4">
									<label for="message-text" class="form-control-label">Imc <i style="color: darkorange">*</i></label>
									<input type="numeric" class="form-control" name="imc" id="imc" required="true" />
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">
									Cancelar
								</button>
								<button type="button" class="btn btn-info" onclick="calcularIMC()">
									Calcular IMC
								</button>
								<button type="submit" class="btn btn-primary" name="Guardar">
									Registrar
								</button>
							</div>
						</form>
					</div>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->




<!-- Modal Nueva Seguimiento Nutricional -->
<div id="modalNuevoSeguimientoNutricional" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<div class="form-group col-md-12 text-center" style="margin-bottom: -8px;">
					<h4 class="modal-title">REGISTRAR NUEVA INFORMACION NUTRICIONAL</h4>
				</div>
			</div>
			<div class="modal-body">
				<div class="content">
					<div class="row">
						<form id="registrarSeguimientoNutricional" method="POST" action="<?php echo $base_url ?>pages/gerontologia/nutricion/procesarDatos/procesarSeguimientoNutricional.php?tipo=insert&idMatriculaDato=<?= $fetchPaciente[0] ?>">
							<div class="row">
								<div class="form-group col-md-6">
									<label for="message-text" class="form-control-label">Fecha <i style="color: darkorange">*</i></label>
									<input type="date" class="form-control" name="fecha" id="fechaModRegistrar" required="true" />
								</div>
								<div class="form-group col-md-6">
									<label for="message-text" class="form-control-label">Tipo de seguimiento <i style="color: darkorange">*</i></label>
									<select  class="form-control" name="tipoSeguimiento" id="tipoSeguimientoRegistrar" required="true">
										<option value="">Selecciona una opción</option>
										<?php 
										$sqlTipoSeguimiento = "SELECT (CONVERT(CAST(CONVERT(seguimiento_tipo USING latin1) AS BINARY) USING utf8))AS seguimiento_tipo FROM gddt_seguimiento_nutricional GROUP BY seguimiento_tipo";
										$queryTipoSeguimiento = $db->query($sqlTipoSeguimiento);
										$fetchTipoSeguimiento = $queryTipoSeguimiento->fetchAll(PDO::FETCH_OBJ);
										foreach ($fetchTipoSeguimiento as $fetch) {
											echo '<option value='.$fetch->seguimiento_tipo.'>'.$fetch->seguimiento_tipo.'</option>';
										}
										?>
									</select>
								</div>
								<div class="form-group col-md-12">
									<label for="message-text" class="form-control-label">Descripcion <i style="color: darkorange">*</i></label>
									<textarea class="form-control" name="descripcion" id="descripcionRegistrar" cols="40" rows="5" required="true"></textarea>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">
									Cancelar
								</button>
								<button type="submit" class="btn btn-primary" name="Guardar">
									Registrar
								</button>
							</div>
						</form>
					</div>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->
<script type="text/javascript">

	


	function calcularIMC(){
		let peso = document.getElementById('peso');
		let estatura = document.getElementById('estatura');
		if(peso.value == "" || estatura.value == ""	){
			if(peso == ''){
				swal({
					title: "Porfavor ingrese el peso para poder continuar",
					icon: "warning",
					buttons: true,
					dangerMode: true,
				})
			}else{
				swal({
					title: "Porfavor ingrese la estatura para poder continuar",
					icon: "warning",
					buttons: true,
					dangerMode: true,
				})
			}
		}else{
			estatura = estatura.value;
			estatura = estatura /100;
			estatura = estatura * estatura;
			let resultado = peso.value / estatura;
			resultado = resultado.toFixed(10);
			document.getElementById('imc').value = resultado;
		}
	}
</script>

