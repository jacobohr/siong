<!-- Modal Registrar Fisioterapia Tono Muscular-->
<div id="modalNuevaFisioterapiaTonoMuscular" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<h4 class="modal-title text-center">REGISTRAR NUEVO TONO MUSCULAR</h4>
			</div>
			<div class="modal-body">
				<div class="content">
					<form id="registrarFisioterapiaTonoMuscular">
						<div class="row">
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Fecha <i style="color: darkorange">*</i></label>
								<input type="date" class="form-control" name="fechaMod" required="true" />
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Dominio cardiovascular y pulmonar <i style="color: darkorange">*</i></label>
								<input type="text" class="form-control" name="dominioCardiovascularPulmonar" required="true" />
							</div>
							<div class="form-group col-md-12">
								<label for="recipient-name" class="form-control-label" >Manipulacion  <i style="color: darkorange">*</i></label>
								<?php 
								$selectManipulacion = array(
									0 => 'Ninguno',
									1 => 'Rueda dentada',
									2 => 'Tubo plomo',
									3 => 'Navaja'
								);
								?>
								<select class="form-control" name="manipulacion" required="true" >
									<option value="">Selecciona una opción</option>
									<?php
									foreach ($selectManipulacion as $key) {
										echo '<option value='.$key.'>'.$key.'</option>';
									}
									?>
								</select>
							</div>
							<div class="form-group col-md-12">
								<label for="recipient-name" class="form-control-label" >Reflejos tonicos  <i style="color: darkorange">*</i></label>
								<?php 
								$selectReflejosTonicos = array(
									0 => 'Ninguno',
									1 => 'Tono cervical asimétrico',
									2 => 'Tono cervical simétrico'
								);
								?>
								<select class="form-control" name="reflejosTonico" required="true" >
									<option value="">Selecciona una opción</option>
									<?php
									foreach ($selectReflejosTonicos as $key) {
										echo '<option value='.$key.'>'.$key.'</option>';
									}
									?>
								</select>
							</div>
							<div class="form-group col-md-12">
								<label for="recipient-name" class="form-control-label" >Reflejos osteotendinoso  <i style="color: darkorange">*</i></label>
								<?php 
								$selectReflejosOsteotendinosos = array(
									0 => 'Arreflexia',
									1 => 'Eurreflexia',
									2 => 'Hiporreflexia',
									3 => 'Hiperreflexia'
								);
								?>
								<select class="form-control" name="reflejosOsteotedinosos" required="true" >
									<option value="">Selecciona una opción</option>
									<?php
									foreach ($selectReflejosOsteotendinosos as $key) {
										echo '<option value='.$key.'>'.$key.'</option>';
									}
									?>
								</select>
							</div>
							<div class="form-group col-md-12">
								<label for="recipient-name" class="form-control-label" >Inervación recíproca  <i style="color: darkorange">*</i></label>
								<?php 
								$selectReflejosOsteotendinoso = array(
									0 => 'Eutónico',
									1 => 'Hipotónico',
									2 => 'Flacidez',
									3 => 'Espasticidad',
									4 => 'Rigidez'
								);
								?>
								<select class="form-control" name="inervacionReciproca" required="true" >
									<option value="">Selecciona una opción</option>
									<?php
									foreach ($selectReflejosOsteotendinoso as $key) {
										echo '<option value='.$key.'>'.$key.'</option>';
									}
									?>
								</select>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Observaciones <i style="color: darkorange">*</i></label>
								<textarea class="form-control" name="observaciones" cols="40" rows="5" required="true" ></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								Cancelar
							</button>
							<button type="submit" class="btn btn-info" onClick="">
								Registrar
							</button>
						</div>
					</form>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->

<!-- Modal Registrar Fisioterapia Reflejo Patologico-->
<div id="modalNuevaFisioterapiaReflejoPatologico" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<h4 class="modal-title text-center">REGISTRAR NUEVO REFLEJO PATOLOGICO</h4>
			</div>
			<div class="modal-body">
				<div class="content">
					<form id="registrarFisioterapiaReflejoPatologico">
						<div class="row">
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Fecha de modificacion <i style="color: darkorange">*</i></label>
								<input type="date" class="form-control" name="fechaMod" required="true"/>
							</div>
							<div class="form-group col-md-12">
								<label for="recipient-name" class="form-control-label">Reflejo Patologico <i style="color: darkorange">*</i></label>
								<select class="form-control" name="grupoSocial" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									$queryReflejoPatologico = mysqli_query($conn, "SELECT id_reflejo_patologico,reflejo_patologico FROM gddt_reflejos_patologicos ;");
									while ($fetchReflejoPatologico = mysqli_fetch_row($queryReflejoPatologico)) {
										echo "<option value=" . $fetchReflejoPatologico[0] . ">" . $fetchReflejoPatologico[1] . "</option>";
									}
									?>
								</select>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								Cancelar
							</button>

							<button type="submit" class="btn btn-info" onClick="">
								Registrar
							</button>
						</div>
					</form>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->

<!-- Modal Registrar Fisioterapia Funcion Mental-->
<div id="modalNuevaFisioterapiaFuncionMental" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<h4 class="modal-title text-center">REGISTRAR NUEVA FUNCION MENTAL</h4>
			</div>
			<div class="modal-body">
				<div class="content">
					<form id="registrarFisioterapiaFuncionMental">
						<div class="row">
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Fecha de modificacion <i style="color: darkorange">*</i></label>
								<input type="date" class="form-control" name="fechaMod" required="true" />
							</div>
							<div class="form-group col-md-12">
								<label for="recipient-name" class="form-control-label">Esquema corporal <i style="color: darkorange">*</i></label>
								<?php 
								$selectHoraControl = array(
									0 => 'Conservado',
									1 => 'Alterado',
								);
								?>
								<select class="form-control" name="tipo" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									foreach ($selectHoraControl as $key) {
										echo '<option value='.$key.'>'.$key.'</option>';
									}
									?>
								</select>
							</div>
							<div class="form-group col-md-12" >
								<label for="message-text" class="form-control-label">Observaciones <i style="color: darkorange">*</i></label>
								<textarea class="form-control" name="observaciones" cols="40" rows="5" required="true"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								Cancelar
							</button>
							<button type="submit" class="btn btn-info" onClick="">
								Registrar
							</button>
						</div>
					</form>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->

<!-- Modal Registrar Fisioterapia Sensibilidad-->
<div id="modalNuevaFisioterapiaSensibilidad" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<h4 class="modal-title text-center">REGISTRAR NUEVA SENSIBILIDAD</h4>
			</div>
			<div class="modal-body">
				<div class="content">
					<form id="registrarFisioterapiaSensibilidad">
						<div class="row">
							<div class="form-group col-md-6">
								<label for="message-text" class="form-control-label">Fecha de modificacion <i style="color: darkorange">*</i></label>
								<input type="date" class="form-control" name="fechaMod" required="true"/>
							</div>
							<div class="form-group col-md-6">
								<label for="recipient-name" class="form-control-label" >Superficial  <i style="color: darkorange">*</i></label>
								<?php 
								$selectSuperficial = array(
									0 => 'conservado',
									1 => 'no conservado'
								);
								?>
								<select class="form-control" name="manipulacion" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									foreach ($selectSuperficial as $key) {
										echo '<option value='.$key.'>'.$key.'</option>';
									}
									?>
								</select>
							</div>
							<div class="form-group col-md-12">
								<textarea class="form-control" name="observacionesSuperficial" cols="40" rows="5" required="true"></textarea>
							</div>
							<div class="form-group col-md-12">
								<label for="recipient-name" class="form-control-label" >Profunda  <i style="color: darkorange">*</i></label>
								<?php 
								$selectProfunda = array(
									0 => 'conservado',
									1 => 'no conservado'
								);
								?>
								<select class="form-control" name="manipulacion" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									foreach ($selectProfunda as $key) {
										echo '<option value='.$key.'>'.$key.'</option>';
									}
									?>
								</select>
							</div>
							<div class="form-group col-md-12">
								<textarea class="form-control" name="observacionesProfunda" cols="40" rows="5" required="true"></textarea>
							</div>
							<div class="form-group col-md-12">
								<label for="recipient-name" class="form-control-label" >Cortical  <i style="color: darkorange">*</i></label>
								<?php 
								$selectCortical = array(
									0 => 'conservado',
									1 => 'no conservado'
								);
								?>
								<select class="form-control" name="manipulacion" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									foreach ($selectCortical as $key) {
										echo '<option value='.$key.'>'.$key.'</option>';
									}
									?>
								</select>
							</div>
							<div class="form-group col-md-12">
								<textarea class="form-control" name="observacionesCortical" cols="40" rows="5" required="true"></textarea>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Dolor <i style="color: darkorange">*</i></label>
								<textarea class="form-control" name="dolor" cols="40" rows="5" required="true"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								Cancelar
							</button>
							<button type="submit" class="btn btn-info" onClick="">
								Registrar
							</button>
						</div>
					</form>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->

<!-- Modal Registrar Fisioterapia Coordinacion-->
<div id="modalNuevaFisioterapiaCoordinacion" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<h4 class="modal-title text-center">REGISTRAR NUEVA COORDINACION</h4>
			</div>
			<div class="modal-body">
				<div class="content">
					<form id="registrarFisioterapiaCoordinacion">
						<div class="row">
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Fecha de modificacion <i style="color: darkorange">*</i></label>
								<input type="date" class="form-control" name="fechaMod" required="true"/>
							</div>
							<div class="form-group col-md-12">
								<label for="recipient-name" class="form-control-label" >Oculopédica  <i style="color: darkorange">*</i></label>
								<?php 
								$selectOculopedica = array(
									0 => 'conservado',
									1 => 'no conservado'
								);
								?>
								<select class="form-control" name="oculopedica" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									foreach ($selectOculopedica as $key) {
										echo '<option value='.$key.'>'.$key.'</option>';
									}
									?>
								</select>
							</div>
							<div class="form-group col-md-12">
								<textarea class="form-control" name="observacionesOculopedica" cols="40" rows="5" required="true"></textarea>
							</div>
							<div class="form-group col-md-12">
								<label for="recipient-name" class="form-control-label" >Oculomanual  <i style="color: darkorange">*</i></label>
								<?php 
								$selectOculomanual = array(
									0 => 'conservado',
									1 => 'no conservado'
								);
								?>
								<select class="form-control" name="oculomanual" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									foreach ($selectOculomanual as $key) {
										echo '<option value='.$key.'>'.$key.'</option>';
									}
									?>
								</select>
							</div>
							<div class="form-group col-md-12">
								<textarea class="form-control" name="observacionesOculopedica" cols="40" rows="5" required="true"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								Cancelar
							</button>
							<button type="submit" class="btn btn-info" onClick="">
								Registrar
							</button>
						</div>
					</form>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->

<!-- Modal Registrar Fisioterapia Equilibrio-->
<div id="modalNuevaFisioterapiaEquilibrio" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<h4 class="modal-title text-center">REGISTRAR NUEVO EQUILIBRIO</h4>
			</div>
			<div class="modal-body">
				<div class="content">
					
					<form id="registrarFisioterapiaEquilibrio">
						<div class="row">
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Fecha de modificacion <i style="color: darkorange">*</i></label>
								<input type="date" class="form-control" name="fechaMod" required="true"/>
							</div>
							<div class="form-group col-md-12">
								<label for="recipient-name" class="form-control-label" >Estática  <i style="color: darkorange">*</i></label>
								<?php 
								$selectEstatica = array(
									0 => 'conservado',
									1 => 'no conservado'
								);
								?>
								<select class="form-control" name="oculopedica" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									foreach ($selectEstatica as $key) {
										echo '<option value='.$key.'>'.$key.'</option>';
									}
									?>
								</select>
							</div>
							<div class="form-group col-md-12">
								<textarea class="form-control" name="observacionesEstatica" cols="40" rows="5" required="true"></textarea>
							</div>
							<div class="form-group col-md-12">
								<label for="recipient-name" class="form-control-label" >Dinámica  <i style="color: darkorange">*</i></label>
								<?php 
								$selectDinamica = array(
									0 => 'conservado',
									1 => 'no conservado'
								);
								?>
								<select class="form-control" name="oculomanual" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									foreach ($selectDinamica as $key) {
										echo '<option value='.$key.'>'.$key.'</option>';
									}
									?>
								</select>
							</div>
							<div class="form-group col-md-12">
								<textarea class="form-control" name="observacionesDinamica" cols="40" rows="5" required="true"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								Cancelar
							</button>
							<button type="submit" class="btn btn-info" onClick="">
								Registrar
							</button>
						</div>
					</form>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->

<!-- Modal Registrar Fisioterapia Motricidad-->
<div id="modalNuevaFisioterapiaMotricidad" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<h4 class="modal-title text-center">REGISTRAR NUEVO MOTRICIDAD</h4>
			</div>
			<div class="modal-body">
				<div class="content">
					<form id="registrarFisioterapiaMotricidad">
						<div class="row">
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Fecha de modificacion <i style="color: darkorange">*</i></label>
								<input type="date" class="form-control" name="fechaMod" required="true"/>
							</div>
							<div class="form-group col-md-12">
								<label for="recipient-name" class="form-control-label" >Fina  <i style="color: darkorange">*</i></label>
								<?php 
								$selectFina = array(
									0 => 'conservado',
									1 => 'no conservado'
								);
								?>
								<select class="form-control" name="oculopedica" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									foreach ($selectFina as $key) {
										echo '<option value='.$key.'>'.$key.'</option>';
									}
									?>
								</select>
							</div>
							<div class="form-group col-md-12">
								<textarea class="form-control" name="observacionesOculopedica" cols="40" rows="5" required="true"></textarea>
							</div>
							<div class="form-group col-md-12">
								<label for="recipient-name" class="form-control-label" >Gruesa  <i style="color: darkorange">*</i></label>
								<?php 
								$selectGruesa = array(
									0 => 'conservado',
									1 => 'no conservado'
								);
								?>
								<select class="form-control" name="oculomanual" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									foreach ($selectGruesa as $key) {
										echo '<option value='.$key.'>'.$key.'</option>';
									}
									?>
								</select>
							</div>
							<div class="form-group col-md-12">
								<textarea class="form-control" name="observacionesOculopedica" cols="40" rows="5" required="true"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								Cancelar
							</button>
							<button type="submit" class="btn btn-info" onClick="">
								Registrar
							</button>
						</div>
					</form>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->

<!-- Modal Registrar Fisioterapia Diagnostico Fisioterapeutico-->
<div id="modalNuevaFisioterapiaDiagnosticoFisioterapeutico" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<h4 class="modal-title text-center">REGISTRAR NUEVO DIAGNOSTICO FISIOTERAPEUTICO</h4>
			</div>
			<div class="modal-body">
				<div class="content">
					<form id="registrarFisioterapiaDiagnosticoFisioterapeutico">
						<div class="row">
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Fecha de modificacion <i style="color: darkorange">*</i></label>
								<input type="date" class="form-control" name="fechaMod" required="true"/>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Diagnóstico Fisioterapéutico <i style="color: darkorange">*</i></label>
								<textarea class="form-control" name="observacionesOculopedica" cols="40" rows="5" required="true"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								Cancelar
							</button>
							<button type="submit" class="btn btn-info" onClick="">
								Registrar
							</button>
						</div>
					</form>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->

<!-- Modal Registrar Fisioterapia Fuerza Muscular-->
<div id="modalNuevaFisioterapiaFuerzaMuscular" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<h4 class="modal-title text-center">REGISTRAR NUEVA FUERZA MUSCULAR</h4>
			</div>
			<div class="modal-body">
				<div class="content">
					<form id="registrarFisioterapiaFuerzaMuscular">
						<div class="row">
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Fecha de modificacion <i style="color: darkorange">*</i></label>
								<input type="date" class="form-control" name="fechaMod" required="true"/>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Fuerza Muscular <i style="color: darkorange">*</i></label>
								<textarea class="form-control" name="fuerzaMuscular" cols="40" rows="5" required="true"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								Cancelar
							</button>
							<button type="submit" class="btn btn-info" onClick="">
								Registrar
							</button>
						</div>
					</form>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->

<!-- Modal Registrar Fisioterapia Postura-->
<div id="modalNuevaFisioterapiaPostura" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<h4 class="modal-title text-center">REGISTRAR NUEVA POSTURA</h4>
			</div>
			<div class="modal-body">
				<div class="content">
					<form id="registrarFisioterapiaPostura">
						<div class="row">
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Fecha de modificacion <i style="color: darkorange">*</i></label>
								<input type="date" class="form-control" name="fechaMod" required="true"/>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Postura <i style="color: darkorange">*</i></label>
								<textarea class="form-control" name="postura" cols="40" rows="5" required="true"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								Cancelar
							</button>
							<button type="submit" class="btn btn-info" onClick="">
								Registrar
							</button>
						</div>
					</form>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->

<!-- Modal Registrar Fisioterapia Marcha-->
<div id="modalNuevaFisioterapiaMarcha" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<h4 class="modal-title text-center">REGISTRAR NUEVA MARCHA</h4>
			</div>
			<div class="modal-body">
				<div class="content">
					<form id="registrarFisioterapiaMarcha">
						<div class="row">
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Fecha de modificacion <i style="color: darkorange">*</i></label>
								<input type="date" class="form-control" name="fechaMod" required="true"/>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Marcha <i style="color: darkorange">*</i></label>
								<textarea class="form-control" name="marcha" cols="40" rows="5" required="true"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								Cancelar
							</button>
							<button type="submit" class="btn btn-info" onClick="">
								Registrar
							</button>
						</div>
					</form>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->

<!-- Modal Registrar Fisioterapia Seguimiento-->
<div id="modalNuevaFisioterapiaSeguimiento" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<h4 class="modal-title text-center">REGISTRAR NUEVO SEGUIMIENTO</h4>
			</div>
			<div class="modal-body">
				<div class="content">
					<form id="registrarFisioterapiaSeguimiento">
						<div class="row">
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Fecha de modificacion <i style="color: darkorange">*</i></label>
								<input type="date" class="form-control" name="fechaMod" required="true"/>
							</div>
							<div class="form-group col-md-12">
								<label for="recipient-name" class="form-control-label" >Tipo de seguimiento  <i style="color: darkorange">*</i></label>
								<?php 
								$selectFina = array(
									0 => 'Seguimiento al plan de Intervencion',
									1 => 'Plan de Intervencion',
									2 => 'Valoracion de Ingreso',
									3 => 'Atencion Individual',
									4 => 'Evento Adverso',
									5 => 'Nota de Egreso',
									6 => 'Nota Aclaratoria',
									7 => 'Intervencion',
									8 => 'Evaluacion',
									9 => 'Evolucion',
									10 => 'Control',
									11 => 'Nota Aclaratoria',
									12 => 'Nota Egreso',
									13 => 'Plan de Tratamiento',
									14 => 'Seguimiento',
									15 => 'Valoracion'
								);
								?>
								<select class="form-control" name="tipoSeguimiento" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									foreach ($selectFina as $key) {
										echo '<option value='.$key.'>'.$key.'</option>';
									}
									?>
								</select>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Descripcion <i style="color: darkorange">*</i></label>
								<textarea class="form-control" name="descripcion" cols="40" rows="5" required="true"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								Cancelar
							</button>
							<button type="submit" class="btn btn-info" onClick="">
								Registrar
							</button>
						</div>
					</form>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->