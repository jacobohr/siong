<!-- Modal Registrar Trabajo Social Informacion General-->
<div id="modalNuevoTrabajoSocialIformacion" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-xl">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<h4 class="modal-title text-center">REGISTRAR NUEVA INFORMACION GENERAL</h4>
			</div>
			<div class="modal-body">
				<div class="content">
					<form id="registrarTrabajoSocialIformacion">
						<div class="row">
							<div class="form-group col-md-6">
								<label for="message-text" class="form-control-label">Fecha de modificacion <i style="color: darkorange">*</i></label>
								<input type="date" class="form-control" name="fechaMod" required="true" />
							</div>
							<div class="form-group col-md-6">
								<label for="message-text" class="form-control-label">Adicciones <i style="color: darkorange">*</i></label>
								<input type="text" class="form-control" name="adicciones" required="true"/>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">¿Ha estado en alguna entidad de recuperación? ¿Cual? <i style="color: darkorange">*</i></label>
								<textarea class="form-control" name="descripcionSeguimiento" cols="40" rows="5" required="true"></textarea>
							</div>
							<div class="form-group col-md-4">
								<label for="recipient-name" class="form-control-label">Tiempo de indigencia <i style="color: darkorange">*</i></label>
								<select class="form-control" name="tiempoIndigencia" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									$queryTiempoindigencia = mysqli_query($conn, "SELECT id_tiempo_indigencia, tiempo_indigencia FROM gddt_tiempo_indigencia;");
									while ($fetchTiempoindigencia = mysqli_fetch_row($queryTiempoindigencia)) {
										echo "<option value=" . $fetchTiempoindigencia[0] . ">" . $fetchTiempoindigencia[1] . "</option>";
									}
									?>
								</select>
							</div>
							<div class="form-group col-md-4">
								<label for="message-text" class="form-control-label">Lugar que frecuenta <i style="color: darkorange">*</i></label>
								<input type="text" class="form-control" name="lugarFrecuenta" required="true"/>
							</div><div class="form-group col-md-4">
								<label for="message-text" class="form-control-label">Motivo que lo impulso a la calle <i style="color: darkorange">*</i></label>
								<input type="text" class="form-control" name="motivoCalle" required="true"/>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Situación familiar <i style="color: darkorange">*</i></label>
								<textarea class="form-control" name="situacionFamiliar" cols="40" rows="5" required="true"></textarea>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Tipologia <i style="color: darkorange">*</i></label>
								<textarea class="form-control" name="tipologia" cols="40" rows="5" required="true"></textarea>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Posibiliades de reintegro familiar <i style="color: darkorange">*</i></label>
								<textarea class="form-control" name="posibiliadesReintegroFamiliar" cols="40" rows="5" required="true"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								Cancelar
							</button>
							<button type="submit" class="btn btn-info">
								Registrar
							</button>
						</div>
					</form>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->
<!-- Modal Registrar Trabajo Social Reingreso -->
<div id="modalNuevoTrabajoSocialReingreso" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<h4 class="modal-title text-center">REGISTRAR NUEVO REINGRESO</h4>
			</div>
			<div class="modal-body">
				<div class="content">
					<form id="registrarTrabajoSocialReingreso">
						<div class="row">
							<div class="form-group col-md-6">
								<label for="message-text" class="form-control-label">Fecha de egreso <i style="color: darkorange">*</i></label>
								<input id="fechaEgresoNuevo" type="date" class="form-control" name="fecharEgreso"  onchange="validarFecha();" required="true"/>
							</div>
							<div class="form-group col-md-6">
								<label for="message-text" class="form-control-label">Fecha de reingreso <i style="color: darkorange">*</i></label>
								<input id="fechaIngresoNuevo" type="date" class="form-control" name="fechaReingreso" onchange="validarFecha();"  required="true"/>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Motivo de Reingreso <i style="color: darkorange">*</i></label>
								<textarea class="form-control" name="motivoReingreso" cols="40" rows="5" required="true"></textarea>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Remitido por <i style="color: darkorange">*</i></label>
								<textarea class="form-control" name="situacionFamiliar" cols="40" rows="5" required="true"></textarea>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Recomendaciones Especiales <i style="color: darkorange">*</i></label>
								<textarea class="form-control" name="tipologia" cols="40" rows="5" required="true"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								Cancelar
							</button>
							<button type="submit" class="btn btn-info">
								Registrar
							</button>
						</div>
					</form>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->

<!-- Modal Registrar Trabajo Social Visita Domiciliaria -->
<div id="modalNuevoTrabajoSocialVisitaDomiciliaria" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-xl">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<div class="form-group col-md-12 text-center" style="margin-bottom: -5px">
					<h4 class="modal-title text-center">REGISTRAR NUEVA VISITA DOMICILIARIA</h4>
				</div>
				
			</div>
			<div class="modal-body">
				<div class="content">
					<form id="registrarTrabajoSocialVisitaDomiciliaria">
						<div class="row">
							<div class="form-group col-md-3">
								<label for="message-text" class="form-control-label">Fecha <i style="color: darkorange">*</i></label>
								<input type="date" class="form-control" name="fechaMod" required="true"/>
							</div>
							<div class="form-group col-md-3">
								<label for="message-text" class="form-control-label">Barrio <i style="color: darkorange">*</i></label>
								<input type="text" class="form-control" name="barrio" required="true"/>
							</div>
							<div class="form-group col-md-3">
								<label for="message-text" class="form-control-label">Direccion <i style="color: darkorange">*</i></label>
								<input type="text" class="form-control" name="direccion" required="true"/>
							</div>
							<div class="form-group col-md-3">
								<label for="message-text" class="form-control-label">Telefono <i style="color: darkorange">*</i></label>
								<input type="number" class="form-control" name="telefono" required="true"/>
							</div>
							<div class="form-group col-md-12 text-center">
								<h5 >NOMBRE DE LAS PERSONAS ENTREVISTADAS Y NEXO</h5>
							</div>
							<div class="form-group col-md-12">
								<div class="row">
									<?php 
									$contador = 0;
									while($contador < 4){
										$contador++;
										echo '<div class="form-group col-md-6">';
										echo '<label for="message-text" class="form-control-label">Nombre </label>';
										echo '<input type="text" class="form-control" name="nombreEntrevistado'.$contador.'"/>';
										echo '</div>';
										echo '<div class="form-group col-md-6">';
										echo '<label for="recipient-name" class="form-control-label">Nexo</label>';
										echo '<select class="form-control" name="nexoEntrevistado'.$contador.'" >';
										echo '<option>Selecciona una opción</option>';
										$queryNexos = mysqli_query($conn, "SELECT id_nexo, nexo FROM gddt_nexos;");
										while ($fetchNexos = mysqli_fetch_row($queryNexos)) {
											echo "<option value=" . $fetchNexos[0] . ">" . $fetchNexos[1] . "</option>";
										}
										echo '</select>';
										echo '</div>';
									}
									?>
								</div>
							</div>
							<div class="form-group col-md-6">
								<label for="message-text" class="form-control-label">Objetivo de la entrevista <i style="color: darkorange">*</i></label>
								<textarea class="form-control" name="objetivoEntrevista" cols="40" rows="5" required="true"></textarea>
							</div>
							<div class="form-group col-md-6">
								<label for="message-text" class="form-control-label">Problemática familiar que se presenta <i style="color: darkorange">*</i></label>
								<textarea class="form-control" name="problematicaFamiliar" cols="40" rows="5" required="true"></textarea>
							</div>
							<div class="form-group col-md-12 text-center">
								<h5 >DATOS FAMILIARES</h5>
							</div>
							<div class="form-group col-md-12 text-center">
								<h5 >NOMBRE DE LAS PERSONAS ENTREVISTADAS Y NEXO</h5>
							</div>
							<table class="table table-bordered">
								<thead>
									<tr class="text-center">
										<th scope="col">Nombre</th>
										<th scope="col">Parentesco</th>
										<th scope="col">Edad</th>
										<th scope="col">Escolaridad</th>
										<th scope="col">Ocupacion</th>
									</tr>
								</thead>
								<tbody>
									<?php 
									$cont = 0;
									
									while($cont < 7){
										$cont++;
										echo '<tr>';
										echo '<th scope="row">';
										echo '<input type="text" class="form-control" name="nombreFamiliar'.$cont.'"/>';
										echo '</th>';
										echo '<td>';
										echo '<select class="form-control" name="parentescoFamiliar'.$cont.'" >';
										echo '<option>Selecciona una opcion</option>';
										$queryNexos = mysqli_query($conn, "SELECT id_nexo, nexo FROM gddt_nexos;");
										while ($fetchNexos = mysqli_fetch_row($queryNexos)) {
											echo "<option value=" . $fetchNexos[0] . ">" . $fetchNexos[1] . "</option>";
										}
										echo '</select>';
										echo '</td>';
										echo '<td>';
										echo '<input type="number" class="form-control" name="edadFamiliar'.$cont.'"  max="120" />';
										echo '</td>';
										echo '<td>';
										echo '<select class="form-control" name="nivelEstudioFamiliar'.$cont.'" >';
										echo '<option>Selecciona una opcion</option>';
										$queryNivelEstudios = mysqli_query($conn, "SELECT id_nivel_estudio, descripcion FROM gddt_nivel_estudios;");
										while ($fetchNivelEstudios = mysqli_fetch_row($queryNivelEstudios)) {
											echo "<option value=" . $fetchNivelEstudios[0] . ">" . $fetchNivelEstudios[1] . "</option>";
										}
										echo '</select>';
										echo '</td>';
										echo '<td>';
										echo '<input type="text" class="form-control" name="ocupacionFamiliar'.$cont.'"/>';
										echo '</td>';
										echo '</tr>';
									}
									?>
								</tbody>
							</table>
							<div class="form-group col-md-12">
								<h5>SITUACION FAMILIAR</h5>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Tipologia <i style="color: darkorange">*</i></label>
								<textarea class="form-control" name="tipologia" cols="40" rows="5" required="true"></textarea>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Elementos de La historia familiar <i style="color: darkorange">*</i></label>
								<textarea class="form-control" name="elementosHistoriaFamiliar" cols="40" rows="5" required="true"></textarea>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Relaciones entre subsistemas <i style="color: darkorange">*</i></label>
								<textarea class="form-control" name="relacionesSubsistemas" cols="40" rows="5" required="true"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								Cancelar
							</button>
							<button type="submit" class="btn btn-info">
								Registrar
							</button>
						</div>
					</form>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->

<!-- Modal Registrar Trabajo Social Aspecto Socioeconomico -->
<div id="modalNuevoTrabajoSocialAspectoSocioeconomico" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<div class="form-group col-md-12 text-center" style="margin-bottom: -5px">
					<h4 class="modal-title text-center">REGISTRAR NUEVA ASPECTO SOCIOECONOMICO</h4>
				</div>
			</div>
			<div class="modal-body">
				<div class="content">
					<form id="registrarTrabajoSocialAspectoSocioeconomico">
						<div class="row">
							<div class="form-group col-md-6">
								<label for="message-text" class="form-control-label">Fecha de modificacion <i style="color: darkorange">*</i></label>
								<input type="date" class="form-control" name="fechaMod" required="true"/>
							</div>
							<div class="form-group col-md-12">
								<h4 class="text-center"> ASPECTO SOCIO ECONOMICO </h4>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Características de la vivienda <i style="color: darkorange">*</i></label>
								<select class="form-control" name="caracteristicasVivienda" required="true">
									<option value="">Selecciona una opción</option>
									<option value="p">Propia</option>
									<option value="a">Alquilada</option>
									<option value="h">Hipotecada</option>
									<option value="c">Comodato</option>
									<option value="p">Pieza</option>
									<option value="t">Tugurio</option>
									<option value="i">Invación</option>
									<option value="ca">Calle</option>
								</select>
							</div>
							<div class="form-group col-md-12">
								<h4 class="text-center">MATERIAL DE CONSTRUCCION</h4>
							</div>
							<div class="form-group col-md-12">
								<div class="row">
									<div class="form-group col-md-6">
										<label for="message-text" class="form-control-label">Pisos <i style="color: darkorange">*</i></label>
										<input type="text" class="form-control" name="piso"  required="true"/>
									</div>
									<div class="form-group col-md-6">
										<label for="message-text" class="form-control-label">Techos <i style="color: darkorange">*</i></label>
										<input type="text" class="form-control" name="techo"  required="true"/>
									</div>
								</div>
							</div>
							<div class="form-group col-md-12">
								<div class="row">
									<div class="form-group col-md-6">
										<label for="message-text" class="form-control-label">Pared <i style="color: darkorange">*</i></label>
										<input type="text" class="form-control" name="pared"  required="true"/>
									</div>
									<div class="form-group col-md-6">
										<label for="message-text" class="form-control-label">Tendecia <i style="color: darkorange">*</i></label>
										<select class="form-control" name="tendecia" required="true">
											<option value="">Selecciona una opción</option>
											<option value="p">Propietario</option>
											<option value="a">Arrendatario</option>
											<option value="c">Comodato</option>
										</select>
									</div>
								</div>
								
							</div>
							
							<div class="form-group col-md-12 text-center">
								<?php 
								$arrayServicios = array(
									0 => 'Acueducto',
									1 => 'Alcantarillado',
									2 => 'Energia',
									3 => 'Baño',
									4 => 'Letrina',
									5 => 'Sanitario',
									6 => 'Telefono',
									7 => 'Gas'
								);
								foreach ($arrayServicios as $keyArrayServicio) {
									echo '<div class="custom-control custom-checkbox custom-control-inline" style="margin-right: 10px;">';
									echo '<input type="checkbox" class="custom-control-input" id="'.$keyArrayServicio.'" value="'.$keyArrayServicio.'">';
									echo '<label class="custom-control-label" for="'.$keyArrayServicio.'">'.$keyArrayServicio.'</label>';
									echo '</div>';
								}
								?>
							</div>
						</div>
						<div class="form-group col-md-12">
							<label for="message-text" class="form-control-label">Condiciones Higenicas <i style="color: darkorange">*</i></label>
							<textarea class="form-control" name="condicionesHigenicas" cols="40" rows="5" required="true"></textarea>
						</div>
						<div class="form-group col-md-12">
							<label for="message-text" class="form-control-label">Distribución de la Vivienda(# de alcobas,# de personas por pieza y cama) <i style="color: darkorange">*</i></label>
							<textarea class="form-control" name="distribuicionVivienda" cols="40" rows="5" required="true"></textarea>
						</div>
						<div class="form-group col-md-12">
							<label for="message-text" class="form-control-label">Acceso a la vivienda <i style="color: darkorange">*</i></label>
							<input type="text" class="form-control" name="accesoVivienda"  required="true"/>
						</div>
						<div class="form-group col-md-12">
							<label for="message-text" class="form-control-label">Ingreso Mensual de la Familia <i style="color: darkorange">*</i></label>
							<input type="text" class="form-control" name="ingresoMensualVivienda"  required="true"/>
						</div>
						<div class="form-group col-md-12">
							<label for="message-text" class="form-control-label">Proveedores económico de la familia <i style="color: darkorange">*</i></label>
							<input type="text" class="form-control" name="proveedorEconomico"  required="true"/>
						</div>
						<div class="form-group col-md-12">
							<label for="message-text" class="form-control-label">Observaciones <i style="color: darkorange">*</i></label>
							<textarea class="form-control" name="observaciones" cols="40" rows="5" required="true"></textarea>
						</div>
						<div class="form-group col-md-12">
							<label for="message-text" class="form-control-label">Recomendaciones <i style="color: darkorange">*</i></label>
							<textarea class="form-control" name="recomendaciones" cols="40" rows="5" required="true"></textarea>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">
							Cancelar
						</button>
						<button type="submit" class="btn btn-info">
							Registrar
						</button>
					</div>
				</form>
			</div>
		</div><!-- End of Modal body -->

		
	</div><!-- End of Modal content -->
</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->

<!-- Modal Registrar Trabajo Social Seguimiento -->
<div id="modalNuevoTrabajoSocialSeguimiento" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<div class="form-group col-md-12 text-center" style="margin-bottom: -5px">
					<h4 class="modal-title text-center">REGISTRAR NUEVO SEGUIMIENTO</h4>
				</div>
			</div>
			<div class="modal-body">
				<div class="content">
					<form id="registrarTrabajoSocialSeguimiento">
						<div class="row">
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Fecha de modificacion <i style="color: darkorange">*</i></label>
								<input type="date" class="form-control" name="fechaMod"  required="true"/>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Tipo de seguimiento <i style="color: darkorange">*</i></label>
								<select  class="form-control" name="tipoSeguimiento"  required="true">
									<option value="">Selecciona una opción</option>
									<?php 

									$sqlTipoSeguimiento = "SELECT (CONVERT(CAST(CONVERT(seguimiento_tipo USING latin1) AS BINARY) USING utf8))AS seguimiento_tipo FROM gddt_seguimiento_t_social GROUP BY seguimiento_tipo";
									$queryTipoSeguimiento = $db->query($sqlTipoSeguimiento);
									$fetchTipoSeguimiento = $queryTipoSeguimiento->fetchAll(PDO::FETCH_OBJ);
									foreach ($fetchTipoSeguimiento as $fetch) {
										echo '<option value='.$fetch->seguimiento_tipo.'>'.$fetch->seguimiento_tipo.'</option>';
									}
									?>
								</select>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Descripcion de el seguimiento <i style="color: darkorange">*</i></label>
								<textarea class="form-control" name="descripcionSeguimiento" cols="40" rows="5"  required="true"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								Cancelar
							</button>
							<button type="submit" class="btn btn-info">
								Registrar
							</button>
						</div>
					</form>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->
<!-- Modal Registrar Trabajo Social Red de apoyo familiar -->
<div id="modalNuevoTrabajoSocialRedApoyoFamiliar" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-xl">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<div class="form-group col-md-12 text-center" style="margin-bottom: -5px">
					<h4 class="modal-title text-center">REGISTRAR NUEVO APOYO FAMILIAR</h4>
				</div>
			</div>
			<div class="modal-body">
				<div class="content">
					<div class="row">
						<form id="registrarTrabajoSocialRedAopoyoFamiliar">
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Fecha <i style="color: darkorange">*</i></label>
								<input type="date" class="form-control" name="fechaMod" required="true"/>
							</div>
							<div class="form-group col-md-12">
								<table class="table table-sm table-bordered">
									<thead>
										<tr>
											<th scope="col">Nombre y apellido</th>
											<th scope="col">Parentesco primario</th>
											<th scope="col">Parentesco secundario</th>
											<th scope="col">Barrio</th>
											<th scope="col">Teléfono</th>
											<th scope="col">Tipo apoyo</th>
											<th scope="col">Red de apoyo</th>
										</tr>
									</thead>
									<tbody>
										<?php 
										$sqlUsuarioNexo = "SELECT id_nexo, nexo FROM gddt_nexos";
										$queryUsuarioNexo = $db->query($sqlUsuarioNexo);
										$fetchUsuarioNexo = $queryUsuarioNexo->fetchAll(PDO::FETCH_OBJ);

										$sqlUsuarioNexoAmigos = "SELECT id_nexo_amigo, nexo FROM gddt_nexos_amigos";
										$queryUsuarioNexoAmigos = $db->query($sqlUsuarioNexoAmigos);
										$fetchUsuarioNexoAmigos = $queryUsuarioNexoAmigos->fetchAll(PDO::FETCH_OBJ);

										$contador = 0;
										
										while($contador < 4){
											echo '<tr>';
											echo '<td><input type="text" class="form-control" name="nombre'.$contador.'" /></td>';
											echo '<td><select class="form-control" name="id_nexo'.$contador.'">';
											echo '<option>Selecciona una opción</option>';
											foreach ($fetchUsuarioNexo as $fetchNexo) {
												echo '<option value="'.$fetchNexo->id_nexo.'">'.$fetchNexo->nexo.'</option>';
											}
											echo '</select></td>';
											echo '<td><select class="form-control" name="id_nexo_amigo'.$contador.'">';
											echo '<option>Selecciona una opción</option>';
											foreach ($fetchUsuarioNexoAmigos as $fetchNexoAmigo) {
												echo '<option value="'.$fetchNexoAmigo->id_nexo_amigo.'">'.$fetchNexoAmigo->nexo.'</option>';
											}
											echo '</select></td>';
											echo '<td><input type="text" class="form-control" name="barrio'.$contador.'" /></td>';
											echo '<td><input type="number" class="form-control" name="telefono'.$contador.'" /></td>';
											echo '<td><input type="text" class="form-control" name="tipo_apoyo'.$contador.'" /></td>';
											echo '<td><input type="text" class="form-control" name="red_apoyo'.$contador.'" /></td>';
											echo '</tr>';
											$contador++;
										}
										?>
									</tbody>
								</table>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">
									Cancelar
								</button>
								<button type="submit" class="btn btn-info">
									Registrar
								</button>
							</div>
						</form>
					</div>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->

<!-- Modal Registrar Trabajo Social Actividades que realiza -->
<div id="modalNuevoTrabajoSocialActividadesRealiza" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<div class="form-group col-md-12 text-center" style="margin-bottom: -5px">
					<h4 class="modal-title text-center">REGISTRAR NUEVA ACTIVIDAD</h4>
				</div>
				
			</div>
			<div class="modal-body">
				<div class="content">
					<form id="registrarTrabajoSocialActividadesRealiza">
						<div class="row">
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Fecha <i style="color: darkorange">*</i></label>
								<input type="date" class="form-control" name="fechaReingreso" required="true"/>
							</div>
							<div class="form-group col-md-12">
								<label for="recipient-name" class="form-control-label">Participo? <i style="color: darkorange">*</i></label>
								<select class="form-control" name="participo" required="true">
									<option value="">Selecciona una opción</option>
									<?php 
									$selectSiNo = array (
										0 => 'Si',
										1 => 'No'
									);
									foreach ($selectSiNo as $key) {
										echo "<option value=" . $key . ">" . $key . "</option>";
									}
									?>
								</select>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Descripcion <i style="color: darkorange">*</i></label>
								<textarea class="form-control" name="descripcion" cols="40" rows="5" required="true"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								Cancelar
							</button>
							<button type="submit" class="btn btn-info">
								Registrar
							</button>
						</div>
					</form>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->

<script type="text/javascript">
	
	function validarFecha(){
		var fechaIngreso = document.getElementById('fechaIngresoNuevo').value;
		var fechaEgreso = document.getElementById('fechaEgresoNuevo').value;
		if(fechaIngreso > fechaEgreso){
			swal("Lo siento, la fecha de ingreso no puede ser mayor a la fecha de egreso");
			$("#fechaIngresoNuevo").val("");
			$("#fechaEgresoNuevo").val("");
		}else{
			if(fechaIngreso == fechaEgreso){
				swal("Lo siento, la fecha de ingreso no puede ser igual a la fecha de egreso");
				$("#fechaIngresoNuevo").val("");
				$("#fechaEgresoNuevo").val("");
			}
		}
	}


</script>