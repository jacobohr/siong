<!-- Modal Registrar AntecedenteMental-->
<div id="modalNuevoAntecedenteMental" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<h4 class="modal-title text-center">REGISTRAR NUEVO ANTECEDENTE MENTAL</h4>
			</div>
			<div class="modal-body">
				<div class="content">
					<form id="registrarPsicologiaAntecedenteMental"  method="POST" action="<?php echo $base_url ?>pages/gerontologia/psicologia/procesarDatos/procesarPsicologiaAntecedenteMental.php?tipo=insert&idMatriculaDato=<?= $fetchPaciente[0] ?>" autocomplete="off">
						<div class="row">
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Fecha <i style="color: darkorange">*</i></label>
								<input type="date" class="form-control" name="fecha" required="true"/>
							</div>
							<div class="form-group col-md-12">
								<label for="recipient-name" class="form-control-label">Antecedente <i style="color: darkorange">*</i></label>
								<select class="form-control" name="antecedente" id="Antecedente" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									$queryAntecedente = mysqli_query($conn, "SELECT id_antecedente, antecedente FROM gddt_antecedentes ;");
									while ($fetchAntecedente = mysqli_fetch_row($queryAntecedente)) {
										echo "<option value=" . $fetchAntecedente[0] . ">" . $fetchAntecedente[1] . "</option>";
									}
									?>
								</select>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								Cancelar
							</button>
							<button type="submit" class="btn btn-info" >
								Registrar
							</button>
						</div>
					</form>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->

<!-- Modal Registrar Antecedente Familiar-->
<div id="modalNuevoAntecedenteFamiliar" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<h4 class="modal-title text-center">REGISTRAR NUEVO ANTECEDENTE FAMILIAR</h4>
			</div>
			<div class="modal-body">
				<div class="content">
					<form id="registrarPsicologiaAntecedenteFamiliar" method="POST" action="<?php echo $base_url ?>pages/gerontologia/psicologia/procesarDatos/procesarPsicologiaAntecedenteFamiliar.php?tipo=insert&idMatriculaDato=<?= $fetchPaciente[0] ?>" autocomplete="off">
						<div class="row">
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Fecha <i style="color: darkorange">*</i></label>
								<input type="date" class="form-control" name="fecha" required="true" />
							</div>
							<div class="form-group col-md-12">
								<label for="recipient-name" class="form-control-label">Antecedente <i style="color: darkorange">*</i></label>
								<select class="form-control" name="antecedente" id="antecedenteFamiliar" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									$queryAntecedente = mysqli_query($conn, "SELECT id_antecedente, antecedente FROM gddt_antecedentes ;");
									while ($fetchAntecedente = mysqli_fetch_row($queryAntecedente)) {
										echo "<option value=" . $fetchAntecedente[0] . ">" . $fetchAntecedente[1] . "</option>";
									}
									?>
								</select>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								Cancelar
							</button>
							<button type="submit" class="btn btn-info" >
								Registrar
							</button>
						</div>
					</form>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->

<!-- Modal Registrar Patologias-->
<div id="modalNuevoPsicologiaPatologias" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<h4 class="modal-title text-center">REGISTRAR NUEVA PATOLOGIA</h4>
			</div>
			<div class="modal-body">
				<div class="content">
					<form id="registrarPsicologiaPatologias" method="POST" action="<?php echo $base_url ?>pages/gerontologia/psicologia/procesarDatos/procesarPsicologiaPatologia.php?tipo=insert&idMatriculaDato=<?= $fetchPaciente[0] ?>" autocomplete="off">
						<div class="row">
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Fecha<i style="color: darkorange">*</i></label>
								<input type="date" class="form-control" name="fecha" required="true" />
							</div>
							<div class="form-group col-md-12">
								<label for="recipient-name" class="form-control-label">Patologia <i style="color: darkorange">*</i></label>
								<select class="form-control" name="patologia" id="patologiaPsicologia" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									$queryPatologia = mysqli_query($conn, "SELECT id_patologia,patologia FROM gddt_patologias ;");
									while ($fetchPatologia = mysqli_fetch_row($queryPatologia)) {
										echo "<option value=" . $fetchPatologia[0] . ">" . $fetchPatologia[1] . "</option>";
									}
									?>
								</select>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Capitulo <i style="color: darkorange">*</i></label>
								<input type="number" class="form-control" name="capitulo" required="true"/>
							</div>
							<div class="form-group col-md-12">
								<label for="recipient-name" class="form-control-label">Enfermedad <i style="color: darkorange">*</i></label>
								<select class="form-control" name="enfermedad" id="enfermedadPsicologia" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									$queryEnfermedad = mysqli_query($conn, "SELECT id_tipo, des_enfermedad FROM gddt_enfermedades_cie ;");
									while ($fetchEnfermedad = mysqli_fetch_row($queryEnfermedad)) {
										echo "<option value=" . $fetchEnfermedad[0] . ">" . $fetchEnfermedad[1] . "</option>";
									}
									?>
								</select>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								Cancelar
							</button>
							<button type="submit" class="btn btn-info" >
								Registrar
							</button>
						</div>
					</form>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->

<!-- Modal Registrar Relaciones Familiar-->
<div id="modalNuevoRelacionesFamiliares" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<h4 class="modal-title text-center">REGISTRAR NUEVA RELACION FAMILIAR</h4>
			</div>
			<div class="modal-body">
				<div class="content">
					<div class="row">
						<form id="registrarPsicologiaRelacionesFamiliares" method="POST" action="<?php echo $base_url ?>pages/gerontologia/psicologia/procesarDatos/procesarPsicologiaRelacionFamiliar.php?tipo=insert&idMatriculaDato=<?= $fetchPaciente[0] ?>" autocomplete="off">
							<div class="row">
								<div class="form-group col-md-12">
									<label for="message-text" class="form-control-label">Fecha<i style="color: darkorange">*</i></label>
									<input type="date" class="form-control" name="fecha"  required="true"/>
								</div>
								<div class="form-group col-md-12">
									<label for="message-text" class="form-control-label">Hijos vivos <i style="color: darkorange">*</i></label>
									<input type="number" class="form-control" name="hijosVivos" minlength="1" maxlength="100" required="true"/>
								</div>
								<div class="form-group col-md-12">
									<label for="message-text" class="form-control-label">Hijos fallecidos <i style="color: darkorange">*</i></label>
									<input type="number" class="form-control" name="hijosFallecidos" minlength="1" maxlength="100" required="true"/>
								</div>
								<div class="form-group col-md-12">
									<label for="message-text" class="form-control-label">Relaciones familiares <i style="color: darkorange">*</i></label>
									<select name="relacionesFamiliares" id="relacionesFamiliares" class="form-control" required="true">
										<option value="">Selecciona una opción</option>
										<option value="Buenas">Buenas</option>
										<option value="Aceptables">Aceptables</option>
										<option value="Malas">Malas</option>
										<option value="No se dan">No se dan</option>
									</select>
								</div>
								<div class="form-group col-md-12">
									<label for="message-text" class="form-control-label">Observaciones familiares <i style="color: darkorange">*</i></label>
									<textarea class="form-control" name="observacionesFamiliares" cols="40" rows="5" required="true"></textarea>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">
									Cancelar
								</button>
								<button type="submit" class="btn btn-info" >
									Registrar
								</button>
							</div>
						</form>
					</div>
				</div>
			</div><!-- End of Modal body -->



		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->

<!-- Modal Registrar Relaciones Sociales-->
<div id="modalNuevoRelacionesSociales" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<h4 class="modal-title text-center">REGISTRAR NUEVA RELACION SOCIAL</h4>
			</div>
			<div class="modal-body">
				<div class="content">
					<form id="registrarPsicologiaRelacionesSociales" method="POST" action="<?php echo $base_url ?>pages/gerontologia/psicologia/procesarDatos/procesarPsicologiaRelacionSocial.php?tipo=insert&idMatriculaDato=<?= $fetchPaciente[0] ?>" autocomplete="off">
						<div class="row">
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Fecha<i style="color: darkorange">*</i></label>
								<input type="date" class="form-control" name="fecha" required="true"/>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Relaciones con los compañeros <i style="color: darkorange">*</i></label>
								<select name="relacionCompañeros" id="relacionCompañeros" class="form-control" required="true">
									<option value="">Selecciona una opción</option>
									<option value="Aceptación">Aceptación</option>
									<option value="Rechazo">Rechazo</option>
									<option value="Indiferencia">Indiferencia</option>
									<option value="No se dan">No se dan</option>
								</select>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Participa en actividades <i style="color: darkorange">*</i></label>
								<select name="participaActividades" id="participaActividades" class="form-control" required="true">
									<option value="">Selecciona una opción</option>
									<option value="si">Si</option>
									<option value="no">No</option>
									<option value="algunas veces">Algunas veces</option>
								</select>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Observaciones <i style="color: darkorange">*</i></label>
								<textarea class="form-control" name="observaciones" cols="40" rows="5" required="true"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								Cancelar
							</button>
							<button type="submit" class="btn btn-info">
								Registrar
							</button>
						</div>
					</form>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->

<!-- Modal Registrar Apoyo Familiar-->
<div id="modalNuevoApoyoFamiliar" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<h4 class="modal-title text-center">REGISTRAR NUEVO APOYO FAMILIAR</h4>
			</div>
			<div class="modal-body">
				<div class="content">
					<form id="registrarApoyoFamiliar" method="POST" action="<?php echo $base_url ?>pages/gerontologia/psicologia/procesarDatos/procesarPsicologiaApoyoFamiliar.php?tipo=insert&idMatriculaDato=<?= $fetchPaciente[0] ?>" autocomplete="off">
						<div class="row">
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Fecha <i style="color: darkorange">*</i></label>
								<input type="date" class="form-control" name="fecha" required="true"/>
							</div>
							<div class="form-group col-md-12">
								<label for="recipient-name" class="form-control-label">Tipo de apoyo <i style="color: darkorange">*</i></label>
								<select class="form-control" name="apoyo" id="tipoApoyo" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									$queryApoyoFamiliar = mysqli_query($conn, "SELECT id_apoyo_familiar, apoyo_familiar FROM gddt_apoyo_familiar ;");
									while ($fetchApoyoFamiliar = mysqli_fetch_row($queryApoyoFamiliar)) {
										echo "<option value=" . $fetchApoyoFamiliar[0] . ">" . $fetchApoyoFamiliar[1] . "</option>";
									}
									?>
								</select>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								Cancelar
							</button>
							<button type="submit" class="btn btn-info">
								Registrar
							</button>
						</div>
					</form>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->

<!-- Modal Registrar Grupo Social-->
<div id="modalNuevoGrupoSocial" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<h4 class="modal-title text-center">REGISTRAR NUEVO GRUPO SOCIAL</h4>
			</div>
			<div class="modal-body">
				<div class="content">
					<form id="registrarGrupoSocial" method="POST" action="<?php echo $base_url ?>pages/gerontologia/psicologia/procesarDatos/procesarPsicologiaGrupoSocial.php?tipo=insert&idMatriculaDato=<?= $fetchPaciente[0] ?>" autocomplete="off">
						<div class="row">
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Fecha<i style="color: darkorange">*</i></label>
								<input type="date" class="form-control" name="fecha" required="true"/>
							</div>
							<div class="form-group col-md-12">
								<label for="recipient-name" class="form-control-label">Grupo Social <i style="color: darkorange">*</i></label>
								<select class="form-control" name="grupoSocial" id="grupoSocial" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									$queryAntecedente = mysqli_query($conn, "SELECT id_grupo_social, grupo_social FROM gddt_grupos_sociales ;");
									while ($fetchAntecedente = mysqli_fetch_row($queryAntecedente)) {
										echo "<option value=" . $fetchAntecedente[0] . ">" . $fetchAntecedente[1] . "</option>";
									}
									?>
								</select>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								Cancelar
							</button>
							<button type="submit" class="btn btn-info">
								Registrar
							</button>
						</div>
					</form>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->

<!-- Modal Registrar Rasgos Personales-->
<div id="modalNuevoRasgosPersonales" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<h4 class="modal-title text-center">REGISTRAR NUEVO RASGO DE PERSONALIDAD</h4>
			</div>
			<div class="modal-body">
				<div class="content">
					<div class="row">
						<form id="registrarRasgosPersonales" method="POST" action="<?php echo $base_url ?>pages/gerontologia/psicologia/procesarDatos/procesarPsicologiaRasgosPersonalidad.php?tipo=insert&idMatriculaDato=<?= $fetchPaciente[0] ?>" autocomplete="off">
							<div class="row">
								<div class="form-group col-md-6">
									<label for="message-text" class="form-control-label">Fecha<i style="color: darkorange">*</i></label>
									<input type="date" class="form-control" name="fecha" required="true"/>
								</div>
								<div class="form-group col-md-6">
									<label for="message-text" class="form-control-label">Personalidad <i style="color: darkorange">*</i></label>
									<select name="personalidad" id="personalidad" class="form-control" required="true">
										<option value="">Selecciona una opción</option>
										<option value="Autoagresivo">Autoagresivo</option>
										<option value="Colérico">Colérico</option>
										<option value="Defensivo">Defensivo</option>
										<option value="Maduro">Maduro</option>
										<option value="Pasivo">Pasivo</option>
									</select>
								</div>
								<div class="form-group col-md-6">
									<label for="message-text" class="form-control-label">Lenguaje <i style="color: darkorange">*</i></label>
									<select name="lenguaje" id="lenguaje" class="form-control" required="true">
										<option value="">Selecciona una opción</option>
										<option value="Ausente">Ausente</option>
										<option value="Inconexo">Inconexo</option>
										<option value="Inintelegible">Inintelegible</option>
										<option value="Normal">Normal</option>
										<option value="Fluido">Fluido</option>
										<option value="Lento">Lento</option>
										<option value="Perseverante">Perseverante</option>
										<option value="Verborreico">Verborreico</option>
									</select>
								</div>
								<div class="form-group col-md-6">
									<label for="message-text" class="form-control-label">Principio de la realidad <i style="color: darkorange">*</i></label>
									<select name="principioRealidad" id="principioRealidad" class="form-control" required="true">
										<option value="">Selecciona una opción</option>
										<option value="Racional">Racional</option>
										<option value="Irracional">Irracional</option>
									</select>
								</div>
								<div class="form-group col-md-6">
									<label for="message-text" class="form-control-label"> Afectividad <i style="color: darkorange">*</i></label>
									<select name="afectividad"  id="afectividad" class="form-control" required="true"> 
										<option value="">Selecciona una opción</option>
										<option value="Equilibrada">Equilibrada</option>
										<option value="Eufórica">Eufórica</option>
										<option value="Irritable">Irritable</option>
										<option value="Ambivalente">Ambivalente</option>
										<option value="Depresiva">Depresiva</option>
										<option value="Tensionada">Tensionada</option>
										<option value="Angustiada">Angustiada</option>
									</select>
								</div>
								<div class="form-group col-md-6">
									<label for="message-text" class="form-control-label"> Autovaloracion <i style="color: darkorange">*</i></label>
									<select name="autovaloracion" id="autovaloracion" class="form-control" required="true">
										<option value="">Selecciona una opción</option>
										<option value="Se quiere a si mismo">Se quiere a si mismo</option>
										<option value="Se acepta parcialmente">Se acepta parcialmente</option>
										<option value="No se valora a si mismo">No se valora a si mismo</option>
									</select>
								</div>
								<div class="form-group col-md-6">
									<label for="message-text" class="form-control-label"> Autopercepcion salud <i style="color: darkorange">*</i></label>
									<select name="autopercepcion" id="autopercepcion" class="form-control" required="true">
										<option value="">Escoger autopercepción</option>
										<option value="Muy saludable">Muy saludable</option>
										<option value="Saludable">Saludable</option>
										<option value="Regular">Regular</option>
										<option value="Mal">Mal</option>
									</select>
								</div>
								<div class="form-group col-md-12">
									<label for="message-text" class="form-control-label"> Observaciones <i style="color: darkorange">*</i></label>
									<textarea class="form-control" name="observacionesPersonalidad" cols="40" rows="5" required="true"></textarea>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">
									Cancelar
								</button>
								<button type="submit" class="btn btn-info" >
									Registrar
								</button>
							</div>
						</form>
					</div>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->

<!-- Modal Registrar Sintomas de Depresion-->
<div id="modalNuevoSintomasDepresion" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<h4 class="modal-title text-center">REGISTRAR NUEVO SINTOMA DE DEPRESION</h4>
			</div>
			<div class="modal-body">
				<div class="content">
					<form id="registrarSintomasDepresion"  method="POST" action="<?php echo $base_url ?>pages/gerontologia/psicologia/procesarDatos/procesarPsicologiaSintomaDepresion.php?tipo=insert&idMatriculaDato=<?= $fetchPaciente[0] ?>" autocomplete="off">
						<div class="row">
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Fecha<i style="color: darkorange">*</i></label>
								<input type="date" class="form-control" name="fecha" required="true" />
							</div>
							<div class="form-group col-md-12">
								<label for="recipient-name" class="form-control-label">Sintoma <i style="color: darkorange">*</i></label>
								<select class="form-control" name="sintoma" id="sintoma" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									$querySintoma = mysqli_query($conn, "SELECT id_sintoma_depresion, sintoma_depresion FROM gddt_sintomas_depresion ;");
									while ($fetchSintoma = mysqli_fetch_row($querySintoma)) {
										echo "<option value=" . $fetchSintoma[0] . ">" . $fetchSintoma[1] . "</option>";
									}
									?>
								</select>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								Cancelar
							</button>
							<button type="submit" class="btn btn-info" >
								Registrar
							</button>
						</div>
					</form>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->

<!-- Modal Registrar Sintomas de ansiedad-->
<div id="modalNuevoSintomasAnsiedad" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<h4 class="modal-title text-center">REGISTRAR NUEVO SINTOMA DE ANSIEDAD</h4>
			</div>
			<div class="modal-body">
				<div class="content">
					<form id="registrarSintomasAnsiedad" method="POST" action="<?php echo $base_url ?>pages/gerontologia/psicologia/procesarDatos/procesarPsicologiaSintomaAnsiedad.php?tipo=insert&idMatriculaDato=<?= $fetchPaciente[0] ?>" autocomplete="off">
						<div class="row">
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Fecha<i style="color: darkorange">*</i></label>
								<input type="date" class="form-control" name="fecha" required="true" />
							</div>
							<div class="form-group col-md-12">
								<label for="recipient-name" class="form-control-label">Sintomas <i style="color: darkorange">*</i></label>
								<select class="form-control" name="sintoma" id="sintomaAnsiedad" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									$queryAnsiedad = mysqli_query($conn, "SELECT id_sintoma_ansiedad, sintoma_ansiedad FROM gddt_sintomas_ansiedad ;");
									while ($fetchAnsiedad = mysqli_fetch_row($queryAnsiedad)) {
										echo "<option value=" . $fetchAnsiedad[0] . ">" . $fetchAnsiedad[1] . "</option>";
									}
									?>
								</select>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								Cancelar
							</button>
							<button type="submit" class="btn btn-info">
								Registrar
							</button>
						</div>
					</form>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->

<!-- Modal Registrar Impresion Diagnostica-->
<div id="modalNuevoImpresionDiagnostica" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content modal-lg">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<h4 class="modal-title text-center">REGISTRAR NUEVA IMPRESION DIAGNOSTICA</h4>
			</div>
			<div class="modal-body">
				<div class="content">
					<div class="row">
						<form id="registrarImpresionDiagnostica" method="POST" action="<?php echo $base_url ?>pages/gerontologia/psicologia/procesarDatos/procesarPsicologiaImpresionDiagnostica.php?tipo=insert&idMatriculaDato=<?= $fetchPaciente[0] ?>" autocomplete="off">
							<div class="row">
								<div class="form-group col-md-12">
									<label for="message-text" class="form-control-label">Fecha<i style="color: darkorange">*</i></label>
									<input type="date" class="form-control" name="fecha" required="true" />
								</div>
								<div class="form-group col-md-12">
									<label for="message-text" class="form-control-label"> Impresion diagnostica <i style="color: darkorange">*</i></label>
									<textarea class="form-control" name="impresion" cols="40" rows="5" required="true"></textarea>
								</div>
								<div class="form-group col-md-12">
									<label for="message-text" class="form-control-label"> Observacion final psicologia <i style="color: darkorange">*</i></label>
									<textarea class="form-control" name="observacion" cols="40" rows="5" required="true"></textarea>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">
									Cancelar
								</button>
								<button type="submit" class="btn btn-info">
									Registrar
								</button>
							</div>
						</form>
					</div>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->

<!-- Modal Registrar Evaluacion Multiaxial-->
<div id="modalNuevoEvaluacionMultiaxial" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<h4 class="modal-title text-center">REGISTRAR NUEVA EVALUACION MULTIAXIAL</h4>
			</div>
			<div class="modal-body">
				<div class="content">
					<form id="registrarEvaluacionMultiaxial" method="POST" action="<?php echo $base_url ?>pages/gerontologia/psicologia/procesarDatos/procesarPsicologiaEvaluacionMultiaxial.php?tipo=insert&idMatriculaDato=<?= $fetchPaciente[0] ?>" autocomplete="off">
						<div class="row">
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Fecha<i style="color: darkorange">*</i></label>
								<input type="date" class="form-control" name="fecha" required="true"/>
							</div>
							<div class="form-group col-md-12">
								<label for="recipient-name" class="form-control-label">Transtorno <i style="color: darkorange">*</i></label>
								<select id="transtornoMultiaxial" class="form-control" name="transtorno" onchange="calcularTranstornoEje()" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									$queryAntecedente = mysqli_query($conn, "SELECT id_trastorno,trastorno FROM gddt_sel_trastornos ;");
									while ($fetchAntecedente = mysqli_fetch_row($queryAntecedente)) {
										echo "<option value=" . $fetchAntecedente[0] . ">" . $fetchAntecedente[1] . "</option>";
									}
									?>
								</select>
							</div>
							<div class="form-group col-md-4">
								<label for="message-text" class="form-control-label">N# del Eje <i style="color: darkorange">*</i></label>
								<input id="numeroDelEje" type="number" class="form-control" name="numeroEje" />
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								Cancelar
							</button>
							<button type="submit" class="btn btn-info" >
								Registrar
							</button>
						</div>
					</form>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->

<!-- Modal Registrar Familiograma-->
<div id="modalNuevoFamiliograma" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<h4 class="modal-title text-center">REGISTRAR NUEVO FAMILIOGRAMA</h4>
			</div>
			<div class="modal-body">
				<div class="content">
					<form id="registrarFamiliograma" method="POST" action="<?php echo $base_url ?>pages/gerontologia/psicologia/procesarDatos/procesarPsicologiaFamiliograma.php?tipo=insert&idMatriculaDato=<?= $fetchPaciente[0] ?>" autocomplete="off" enctype="multipart/form-data">
						<div class="row">
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Observaciones <i style="color: darkorange">*</i></label>
								<textarea class="form-control" name="observacion" cols="40" rows="5" required="true"></textarea>
							</div>
							<div class="form-group col-md-12">
								<p>
									Sube un archivo:
									<input type="file" class="form-control-file" name="fotoFamiliograma" accept="image/jpg" id="familiogramaFile" required="true"/>
								</p>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								Cancelar
							</button>
							<button type="submit" class="btn btn-info">
								Registrar
							</button>
						</div>
					</form>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->

<!-- Modal Registrar Plan de tratamiento-->
<div id="modalNuevoPlanTratamiento" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<h4 class="modal-title text-center">REGISTRAR NUEVO PLAN DE TRATAMIENTO</h4>
			</div>
			<div class="modal-body">
				<div class="content">
					<form id="registrarPlanTratamiento" method="POST" action="<?php echo $base_url ?>pages/gerontologia/psicologia/procesarDatos/procesarPsicologiaPlanTratamiento.php?tipo=insert&idMatriculaDato=<?= $fetchPaciente[0] ?>" autocomplete="off">
						<div class="row">
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Fecha<i style="color: darkorange">*</i></label>
								<input type="date" class="form-control" name="fecha" required="true"/>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label"> Tipo de tratamiento <i style="color: darkorange">*</i></label>
								<select id="tipoTratamiento" class="form-control" required="true" name="tipoTratamiento">
									<option value="">Selecciona una opción</option>
									<option value="Seguimiento">Seguimiento</option>
									<option value="Objetivo terapeutico">Objetivo terapeutico</option>
								</select>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Plan de tratamiento <i style="color: darkorange">*</i></label>
								<textarea class="form-control" name="planTratamiento" cols="40" rows="5" required="true"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								Cancelar
							</button>
							<button type="submit" class="btn btn-info" >
								Registrar
							</button>
						</div>
					</form>
				</div>
			</div><!-- End of Modal body -->



		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->

<!-- Modal Registrar Patologias-->
<div id="modalNuevoSeguimiento" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<h4 class="modal-title text-center">REGISTRAR NUEVO SEGUIMIENTO</h4>
			</div>
			<div class="modal-body">
				<div class="content">
					<div class="row">
						<form id="registrarSeguimiento" method="POST" action="<?php echo $base_url ?>pages/gerontologia/psicologia/procesarDatos/procesarPsicologiaSeguimiento.php?tipo=insert&idMatriculaDato=<?= $fetchPaciente[0] ?>" autocomplete="off">
							<div class="row">
								<div class="form-group col-md-6">
									<label for="message-text" class="form-control-label">Fecha<i style="color: darkorange">*</i></label>
									<input type="date" class="form-control" name="fecha" required="true"/>
								</div>
								<div class="form-group col-md-6">
									<label for="recipient-name" class="form-control-label">Tipo de Seguimiento <i style="color: darkorange">*</i></label>
									<select class="form-control" name="tipoSeguimiento" required="true">
										<option value="">Selecciona una opción</option>
										<?php
										$queryTipoSeguimiento = mysqli_query($conn, "SELECT (CONVERT(CAST(CONVERT(seguimiento_tipo USING latin1) AS BINARY) USING utf8))AS seguimiento_tipo FROM gddt_seguimiento_psicologico GROUP BY seguimiento_tipo;");
										while ($fetchTipoSeguimiento = mysqli_fetch_row($queryTipoSeguimiento)) {
											echo "<option value=" . $fetchTipoSeguimiento[0] . ">" . $fetchTipoSeguimiento[0] . "</option>";
										}
										?>
									</select>
								</div>
								<div class="form-group col-md-12">
									<label for="message-text" class="form-control-label">Descripcion de el seguimiento <i style="color: darkorange">*</i></label>
									<textarea class="form-control" name="descripcionSeguimiento" cols="40" rows="5" required="true"></textarea>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">
									Cancelar
								</button>
								<button type="submit" class="btn btn-info">
									Registrar
								</button>
							</div>
						</form>
					</div>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
			</div><!-- End of Modal -->