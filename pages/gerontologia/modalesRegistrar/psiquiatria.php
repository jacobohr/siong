<!-- Modal Registrar Psiquiatria Informacion-->
<div id="modalNuevaPsiquiatriaInformacion" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-xl">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<h4 class="modal-title text-center">REGISTRAR NUEVA PSIQUIATRIA</h4>
			</div>
			<div class="modal-body">
				<div class="content">
					<form id="registrarPsiquiatriaInformacion">
						<div class="row">
							<div class="form-group col-md-4">
								<label for="message-text" class="form-control-label">Fecha <i style="color: darkorange">*</i></label>
								<input type="date" class="form-control" name="fechaMod" required="true" />
							</div>
							<div class="form-group col-md-4">
								<label for="recipient-name" class="form-control-label" >Antecedentes personal<i style="color: darkorange">*</i></label>
								<?php 
								$selectAntecedentesPersonal = array(
									0 => 'Consumo de Toxicos',
									1 => 'Traumaticos',
									2 => 'Psiquiatricos',
									3 => 'Otros'
								);
								?>
								<select class="form-control" name="antecedentesPersonales" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									foreach ($selectAntecedentesPersonal as $key) {
										echo '<option value='.$key.'>'.$key.'</option>';
									}
									?>
								</select>
							</div>
							<div class="form-group col-md-4">
								<label for="recipient-name" class="form-control-label" >Antecedentes familiares<i style="color: darkorange">*</i></label>
								<?php 
								$select = array(
									0 => 'Enfermedad Psiquiatrica',
									1 => 'Otros'
								);
								?>
								<select class="form-control" name="antecedentesFamiliares" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									foreach ($select as $key) {
										echo '<option value='.$key.'>'.$key.'</option>';
									}
									?>
								</select>
							</div>
							<div class="form-group col-md-6">
								<label for="recipient-name" class="form-control-label" >Enfermedad actual<i style="color: darkorange">*</i></label>
								<textarea class="form-control" name="enfermedadActual" cols="40" rows="5" required="true" ></textarea>
							</div>
							
							<div class="form-group col-md-6">
								<label for="recipient-name" class="form-control-label" >Enfermedad mental<i style="color: darkorange">*</i></label>
								<textarea class="form-control" name="enfermedadMental" cols="40" rows="5" required="true"></textarea>
							</div>
							<div class="form-group col-md-6">
								<label for="recipient-name" class="form-control-label" >Diagnastico Psiquiatrico<i style="color: darkorange">*</i></label>
								<textarea class="form-control" name="diagnosticoPsiquiatrico" cols="40" rows="5" required="true"></textarea>
							</div>
							<div class="form-group col-md-6">
								<label for="recipient-name" class="form-control-label" >Analisis Psiquiatrico<i style="color: darkorange">*</i></label>
								<textarea class="form-control" name="AnalisisPsiquiatrico" cols="40" rows="5" required="true"></textarea>
							</div>
							<div class="form-group col-md-12">
								<label for="recipient-name" class="form-control-label" >Tratamiento<i style="color: darkorange">*</i></label>
								<textarea class="form-control" name="tratamiento" cols="40" rows="5" required="true"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								Cancelar
							</button>
							<button type="submit" class="btn btn-info" >
								Registrar
							</button>
						</div>
					</form>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->