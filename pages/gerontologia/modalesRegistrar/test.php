<!-- Modal Registrar Test Test Minimental-->
<div id="modalNuevoTestTestMinimental" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<h4 class="modal-title text-center">REGISTRAR NUEVO TEST MINIMENTAL</h4>
			</div>
			<div class="modal-body">
				<div class="content">
					<div class="row m-2">
						<form id="registrarTestTestMinimental">
							<div class="row">
								<div class="form-group col-md-12">
									<label for="message-text" class="form-control-label">Fecha <i style="color: darkorange">*</i></label>
									<input  type="date" class="form-control" name="fecha" required="true" />
								</div>
								<div class="form-group col-md-12" >
									<label for="message-text" class="form-control-label"></label>
									<h6><b>Orientación</b></h6>
								</div>

								<div class="form-group col-md-8">
									<label for="message-text" class="form-control-label">1. Temporal <i style="color: darkorange">*</i></label>
									<p>¿Que fecha, año, mes, dia de la semana, mañana o tarde es hoy?</p>
								</div>
								<div class="form-group col-md-4">
									<select class="form-control" name="preguntaUno" required="true">
										<option value="">Selecciona una opción</option>
										<?php 
										$cont = 0;
										while($cont <= 5){
											echo '<option value="'.$cont.'">'.$cont.'</option>';
											$cont++;
										}
										?>
									</select>
								</div>

								<div class="form-group col-md-8">
									<label for="message-text" class="form-control-label">2. Espacial <i style="color: darkorange">*</i></label>
									<p>¿Donde estamos? Pais, departamento, ciudad, barrio, lugar.</p>
								</div>
								<div class="form-group col-md-4">
									<select class="form-control" name="preguntaDos" required="true">
										<option value="">Selecciona una opción</option>
										<?php 
										$cont = 0;
										while($cont <= 5){
											echo '<option value="'.$cont.'">'.$cont.'</option>';
											$cont++;
										}
										?>
									</select>
								</div>

								<div class="form-group col-md-12" >
									<label for="message-text" class="form-control-label"></label>
									<h6><b>Fijacion o atención</b></h6>
								</div>
								<div class="form-group col-md-8">
									<label for="message-text" class="form-control-label">3. Memoria Sensorial <i style="color: darkorange">*</i></label>
									<p>Le voy a nombrar tres objetos: "Manzana, mesa, centavo" despues de que los haya nombrado, quiero que los repita. Acuerdese de ellos, le pedire que los nombre en unos minutos. Anote un punto por cada respuesta correcta. Continúe repitiendo los nombres hasta que los haya aprendido todos. Cuente las pruebas y anote.</p>
								</div>
								<div class="form-group col-md-4">
									<select class="form-control" name="preguntaTres" required="true">
										<option value="">Selecciona una opción</option>
										<?php 
										$cont = 0;
										while($cont <= 3){
											echo '<option value="'.$cont.'">'.$cont.'</option>';
											$cont++;
										}
										?>
									</select>
								</div>

								<div class="form-group col-md-12" >
									<label for="message-text" class="form-control-label"></label>
									<h6><b>Atención y cálculo</b></h6>
								</div>
								<div class="form-group col-md-8">
									<label for="message-text" class="form-control-label">4. Memoria Verbal de Trabajo<i style="color: darkorange">*</i></label>
									<p>Deletree "mundo" al revés. El puntaje es el número de letras en el órden correcto.</p>
								</div>
								<div class="form-group col-md-4">
									<select class="form-control" name="preguntaCuatro" required="true">
										<option value="">Selecciona una opción</option>
										<?php 
										$cont = 0;
										while($cont <= 5){
											echo '<option value="'.$cont.'">'.$cont.'</option>';
											$cont++;
										}
										?>
									</select>
								</div>

								<div class="form-group col-md-12" >
									<label for="message-text" class="form-control-label"></label>
									<h6><b>Memoria</b></h6>
								</div>
								<div class="form-group col-md-8">
									<label for="message-text" class="form-control-label">5. Memoria a Corto Plazo<i style="color: darkorange">*</i></label>
									<p>Pida al paciente que repita los tres nombres mencionados anteriormente. Anote un punto por cada respuesta correcta. Nota: no se puede hacer la prueba de memoria si el paciente no reodó los tres obejtos durante la prueba de atención.</p>
								</div>
								<div class="form-group col-md-4">
									<select class="form-control" name="preguntaCinco" required="true">
										<option value="">Selecciona una opción</option>
										<?php 
										$cont = 0;
										while($cont <= 3){
											echo '<option value="'.$cont.'">'.$cont.'</option>';
											$cont++;
										}
										?>
									</select>
								</div>

								<div class="form-group col-md-12" >
									<label for="message-text" class="form-control-label"></label>
									<h6><b>	Lenguaje y práxias</b></h6>
								</div>
								<div class="form-group col-md-8">
									<label for="message-text" class="form-control-label">6. Lenguaje<i style="color: darkorange">*</i></label>
									<p>Identifique un "reloj" y un "lapiz".</p>
								</div>
								<div class="form-group col-md-4">
									<select class="form-control" name="preguntaSeis" required="true">
										<option value="">Selecciona una opción</option>
										<?php 
										$cont = 0;
										while($cont <= 2){
											echo '<option value="'.$cont.'">'.$cont.'</option>';
											$cont++;
										}
										?>
									</select>
								</div>

								<div class="form-group col-md-8">
									<label for="message-text" class="form-control-label">7. Lenguaje<i style="color: darkorange">*</i></label>
									<p>Repita lo siguiente: "ni si, ni no, ni peros".</p>
								</div>
								<div class="form-group col-md-4">
									<select class="form-control" name="preguntaSiete" required="true">
										<option value="">Selecciona una opción</option>
										<?php 
										$cont = 0;
										while($cont <= 1){
											echo '<option value="'.$cont.'">'.$cont.'</option>';
											$cont++;
										}
										?>
									</select>
								</div>

								<div class="form-group col-md-8">
									<label for="message-text" class="form-control-label">8. Práxis<i style="color: darkorange">*</i></label>
									<p>Siga un comando en tres pasos: - Tome un papel con la mano derecha. - Doble el papel por la mitad. - Pongalo en el piso.</p>
								</div>
								<div class="form-group col-md-4">
									<select class="form-control" name="preguntaOcho" required="true">
										<option value="">Selecciona una opción</option>
										<?php 
										$cont = 0;
										while($cont <= 3){
											echo '<option value="'.$cont.'">'.$cont.'</option>';
											$cont++;
										}
										?>
									</select>
								</div>

								<div class="form-group col-md-8">
									<label for="message-text" class="form-control-label">9. Práxis.<i style="color: darkorange">*</i></label>
									<p>Cierre los ojos.</p>
								</div>
								<div class="form-group col-md-4">
									<select class="form-control" name="preguntaNueve" required="true">
										<option value="">Selecciona una opción</option>
										<?php 
										$cont = 0;
										while($cont <= 1){
											echo '<option value="'.$cont.'">'.$cont.'</option>';
											$cont++;
										}
										?>
									</select>
								</div>

								<div class="form-group col-md-8">
									<label for="message-text" class="form-control-label">10. Lenguaje<i style="color: darkorange">*</i></label>
									<p>Escríba una oración.	</p>
								</div>
								<div class="form-group col-md-4">
									<select class="form-control" name="preguntaDiez" required="true">
										<option value="">Selecciona una opción</option>
										<?php 
										$cont = 0;
										while($cont <= 1){
											echo '<option value="'.$cont.'">'.$cont.'</option>';
											$cont++;
										}
										?>
									</select>
								</div>

								<div class="form-group col-md-8">
									<label for="message-text" class="form-control-label">11. Práxis<i style="color: darkorange">*</i></label>
									<p>Copie el siguiente diseño:</p>
									<img src="<?= $base_url ?>images/test/minimental_figura.png" style="width: 160px;height: 117px;">
								</div>
								<div class="form-group col-md-4">
									<select class="form-control" name="usuario" required="true">
										<option value="">Selecciona una opción</option>
										<?php 
										$cont = 0;
										while($cont <= 1){
											echo '<option value="'.$cont.'">'.$cont.'</option>';
											$cont++;
										}
										?>
									</select>
								</div>

								<div class="form-group col-md-12">
									<label for="message-text" class="form-control-label">Observaciones test <i style="color: darkorange">*</i></label>
									<textarea class="form-control" name="observacionesTest" cols="40" rows="5" required="true"></textarea>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">
									Cancelar
								</button>
								<button type="submit" class="btn btn-info" >
									Registrar
								</button>
							</div>
						</form>
					</div>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->

<!-- Modal Registrar Test Test Yesavage-->
<div id="modalNuevoTestTestYesavage" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content modal-lg">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<h4 class="modal-title text-center">REGISTRAR NUEVO TEST YESAVAGE</h4>
			</div>
			<div class="modal-body">
				<div class="content">
					<div class="row m-2">
						<form id="registrarTestTestYesavage">
							<div class="row">
								<div class="form-group col-md-12">
									<label for="message-text" class="form-control-label">Fecha <i style="color: darkorange">*</i></label>
									<input id="fechaIngreso" type="date" class="form-control" name="fecha" required="true"/>
								</div>
								<div class="form-group col-md-8">
									<p>1. ¿Está basicamente satisfecho con su vida?</p>
								</div>
								<div class="form-group col-md-4">
									<select class="form-control" name="preguntaUno" required="true">
										<option value="">Selecciona una opción</option>
										<?php 
										echo '<option value="1">Si</option>';
										echo '<option value="0">No</option>';
										?>
									</select>
								</div>

								<div class="form-group col-md-8">
									<p>2. ¿Ha renunciado a muchas de sus actividades y pasatiempos?</p>
								</div>
								<div class="form-group col-md-4">
									<select class="form-control" name="preguntaDos" required="true">
										<option value="">Selecciona una opción</option>
										<?php 
										echo '<option value="1">Si</option>';
										echo '<option value="0">No</option>';
										?>
									</select>
								</div>

								<div class="form-group col-md-8">
									<p>	3. ¿Siente que su vida esta vacia?</p>
								</div>
								<div class="form-group col-md-4">
									<select class="form-control" name="preguntaTres" required="true">
										<option value="">Selecciona una opción</option>
										<?php 
										echo '<option value="1">Si</option>';
										echo '<option value="0">No</option>';
										?>
									</select>
								</div>

								<div class="form-group col-md-8">
									<p>4. ¿Se encuentra a menudo aburrido?</p>
								</div>
								<div class="form-group col-md-4">
									<select class="form-control" name="preguntaCuatro" required="true">
										<option value="">Selecciona una opción</option>
										<?php 
										echo '<option value="1">Si</option>';
										echo '<option value="0">No</option>';
										?>
									</select>
								</div>

								<div class="form-group col-md-8">
									<p>	5. ¿Se encuentra alegre y optimista, con buen animo casi todo el tiempo?</p>
								</div>
								<div class="form-group col-md-4">
									<select class="form-control" name="preguntaCinco"required="true" >
										<option value="">Selecciona una opción</option>
										<?php 
										echo '<option value="1">Si</option>';
										echo '<option value="0">No</option>';
										?>
									</select>
								</div>

								<div class="form-group col-md-8">
									<p>6. ¿Teme que vaya a pasar algo malo?</p>
								</div>
								<div class="form-group col-md-4">
									<select class="form-control" name="preguntaSeis"required="true" >
										<option value="">Selecciona una opción</option>
										<?php 
										echo '<option value="1">Si</option>';
										echo '<option value="0">No</option>';
										?>
									</select>
								</div>

								<div class="form-group col-md-8">
									<p>7. ¿Se siente feliz, contento la mayor parte del tiempo?</p>
								</div>
								<div class="form-group col-md-4">
									<select class="form-control" name="preguntaSiete" required="true">
										<option value="">Selecciona una opción</option>
										<?php 
										echo '<option value="1">Si</option>';
										echo '<option value="0">No</option>';
										?>
									</select>
								</div>

								<div class="form-group col-md-8">
									<p>	8. ¿Se siente a menudo desamparado, desvalido, indeciso?</p>
								</div>
								<div class="form-group col-md-4">
									<select class="form-control" name="preguntaOcho" required="true">
										<option value="">Selecciona una opción</option>
										<?php 
										echo '<option value="1">Si</option>';
										echo '<option value="0">No</option>';
										?>
									</select>
								</div>

								<div class="form-group col-md-8">
									<p>9. ¿Prefiere quedarse en casa que acaso salir y hacer cosas nuevas?</p>
								</div>
								<div class="form-group col-md-4">
									<select class="form-control" name="preguntaNueve" required="true">
										<option value="">Selecciona una opción</option>
										<?php 
										echo '<option value="1">Si</option>';
										echo '<option value="0">No</option>';
										?>
									</select>
								</div>

								<div class="form-group col-md-8">
									<p>10. ¿Le da impresion de que tiene mas fallos de memoria que los demas?</p>
								</div>
								<div class="form-group col-md-4">
									<select class="form-control" name="preguntaDiez" required="true">
										<option value="">Selecciona una opción</option>
										<?php 
										echo '<option value="1">Si</option>';
										echo '<option value="0">No</option>';
										?>
									</select>
								</div>

								<div class="form-group col-md-8">
									<p>11. ¿Cree que es agradable estar vivo?</p>
								</div>
								<div class="form-group col-md-4">
									<select class="form-control" name="preguntaOnce"required="true">
										<option value="">Selecciona una opción</option>
										<?php 
										echo '<option value="1">Si</option>';
										echo '<option value="0">No</option>';
										?>
									</select>
								</div>

								<div class="form-group col-md-8">
									<p>12. ¿Se le hace duro empezar nuevos proyectos?</p>
								</div>
								<div class="form-group col-md-4">
									<select class="form-control" name="preguntaDoce" required="true">
										<option value="">Selecciona una opción</option>
										<?php 
										echo '<option value="1">Si</option>';
										echo '<option value="0">No</option>';
										?>
									</select>
								</div>

								<div class="form-group col-md-8">
									<p>13. ¿Se siente lleno de energia?</p>
								</div>
								<div class="form-group col-md-4">
									<select class="form-control" name="preguntaTrece" required="true">
										<option value="">Selecciona una opción</option>
										<?php 
										echo '<option value="1">Si</option>';
										echo '<option value="0">No</option>';
										?>
									</select>
								</div>

								<div class="form-group col-md-8">
									<p>14. ¿Siente que su situacion es angustiosa, desesperada?</p>
								</div>
								<div class="form-group col-md-4">
									<select class="form-control" name="preguntaCatorce" required="true">
										<option value="">Selecciona una opción</option>
										<?php 
										echo '<option value="1">Si</option>';
										echo '<option value="0">No</option>';
										?>
									</select>
								</div>

								<div class="form-group col-md-8">
									<p>15. ¿Cree que la mayoria de la gente vive economicamente mejor que usted?</p>
								</div>
								<div class="form-group col-md-4">
									<select class="form-control" name="preguntaQuince"required="true" >
										<option value="">Selecciona una opción</option>
										<?php 
										echo '<option value="1">Si</option>';
										echo '<option value="0">No</option>';
										?>
									</select>
								</div>
								<div class="form-group col-md-12">
									<label for="message-text" class="form-control-label">Observaciones test <i style="color: darkorange">*</i></label>
									<textarea class="form-control" name="observacionesTest" cols="40" rows="5" required="true"></textarea>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">
									Cancelar
								</button>
								<button type="submit" class="btn btn-info">
									Registrar
								</button>
							</div>
						</form>
					</div>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->

<!-- Modal Registrar Test Test Barthel-->
<div id="modalNuevoTestTestBarthel" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content modal-lg">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<h4 class="modal-title text-center">REGISTRAR NUEVO TEST BARTHEL</h4>
			</div>
			<div class="modal-body">
				<div class="content">
					<div class="row">
						<form id="registrarTestTestBarthel">
							<div class="row m-2">
								<div class="form-group col-md-12">
									<label for="message-text" class="form-control-label">Fecha <i style="color: darkorange">*</i></label>
									<input type="date" class="form-control" name="fecha" required="true"/>
								</div>

								<div class="form-group col-md-8">
									<p>1. Alimentacion</p>
								</div>
								<div class="form-group col-md-4">
									<select class="form-control" name="preguntaUno" required="true">
										<option value="">Selecciona una opción</option>
										<option value="10">Independiente</option>
										<option value="5">Necesita ayuda</option>
										<option value="0">Dependiente</option>
									</select>
								</div>
								<div class="form-group col-md-8">
									<p>2. Lavarse (Baño)</p>
								</div>
								<div class="form-group col-md-4">
									<select class="form-control" name="preguntoDos" required="true">
										<option value="">Selecciona una opción</option>
										<option value="5">Independiente</option>
										<option value="0">Dependiente</option>
									</select>
								</div>
								<div class="form-group col-md-8">
									<p>3. Vestirse</p>
								</div>
								<div class="form-group col-md-4">
									<select class="form-control" name="preguntaTres" required="true">
										<option value="">Selecciona una opción</option>
										<option value="10">Independiente</option>
										<option value="5">Necesita ayuda</option>
										<option value="0">Dependiente</option>
									</select>
								</div>
								<div class="form-group col-md-8">
									<p>4. Arreglarse</p>
								</div>
								<div class="form-group col-md-4">
									<select class="form-control" name="preguntaCuatro" required="true">
										<option value="">Selecciona una opción</option>
										<option value="5">Independiente</option>
										<option value="0">Dependiente</option>
									</select>
								</div>
								<div class="form-group col-md-8">
									<p>	5. Deposición</p>
								</div>
								<div class="form-group col-md-4">
									<select class="form-control" name="preguntoCinco" required="true">
										<option value="">Selecciona una opción</option>
										<option value="10">Continente</option>
										<option value="5">Accidente ocasional</option>
										<option value="0">Incontinente</option>
									</select>
								</div>

								<div class="form-group col-md-8">
									<p>6. Micción</p>
								</div>
								<div class="form-group col-md-4">
									<select class="form-control" name="preguntaSeis" required="true">
										<option value="">Selecciona una opción</option>
										<option value="10">Continente</option>
										<option value="5">Accidente ocasional</option>
										<option value="0">Incontinente</option>
									</select>
								</div>

								<div class="form-group col-md-8">
									<p>7. Ir al baño</p>
								</div>
								<div class="form-group col-md-4">
									<select class="form-control" name="preguntaSiete" required="true">
										<option value="">Selecciona una opción</option>
										<option value="10">Independiente</option>
										<option value="5">Necesita ayuda</option>
										<option value="0">Dependiente</option>
									</select>
								</div>

								<div class="form-group col-md-8">
									<p>8. Traslado al sillón o cama</p>
								</div>
								<div class="form-group col-md-4">
									<select class="form-control" name="preguntaOcho" required="true">
										<option value="">Selecciona una opción</option>
										<option value="15">Independiente</option>
										<option value="10">Mímina ayuda</option>
										<option value="5">Gran ayuda</option>
										<option value="0">Dependiente</option>
									</select>
								</div>

								<div class="form-group col-md-8">
									<p>9. Deambulación</p>
								</div>
								<div class="form-group col-md-4">
									<select class="form-control" name="preguntaNueve" required="true">
										<option value="">Selecciona una opción</option>
										<option value="15">Independiente</option>
										<option value="10">Necesita ayuda</option>
										<option value="5">Independiente (Silla de ruedas)</option>
										<option value="0">Dependiente</option>
									</select>
								</div>

								<div class="form-group col-md-8">
									<p>10. Subir y bajar escaleras</p>
								</div>
								<div class="form-group col-md-4">
									<select class="form-control" name="preguntaDiez" required="true">
										<option value="">Selecciona una opción</option>
										<option value="10">Independiente</option>
										<option value="5">Necesita ayuda</option>
										<option value="0">Dependiente</option>
									</select>
								</div>

								<div class="form-group col-md-12">
									<label for="message-text" class="form-control-label">Observaciones test <i style="color: darkorange">*</i></label>
									<textarea class="form-control" name="observacionesTest" cols="40" rows="5" required="true"></textarea>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">
									Cancelar
								</button>
								<button type="submit" class="btn btn-info">
									Registrar
								</button>
							</div>
						</form>
					</div>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->