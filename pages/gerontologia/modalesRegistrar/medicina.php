<!-- Modal Registrar Signos Vitales-->
<div id="modalNuevoMedSignosVitales" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<h4 class="modal-title text-center">REGISTRAR NUEVO SIGNO VITAL</h4>
			</div>
			<div class="modal-body">
				<div class="content">
					<div class="row">
						<form id="registrarMedSignosVitales" method="POST" action="<?php echo $base_url ?>pages/gerontologia/medicina/procesarDatos/procesarMedicinaSignosVitales.php?tipo=insert&idMatriculaDato=<?= $fetchPaciente[0] ?>" autocomplete="off">
							<div class="row">
								<div class="form-group col-md-12">
									<label for="message-text" class="form-control-label">Fecha<i style="color: darkorange">*</i></label>
									<input type="date" class="form-control" name="fecha" required="true"/>
								</div>
								<div class="form-group col-md-12">
									<label for="message-text" class="form-control-label">Frecuencia cardiaca <i style="color: darkorange">*</i></label>
									<input type="number" class="form-control" name="frecuenciaCardiaca"   required="true"/>
								</div>
								<div class="form-group col-md-12">
									<label for="message-text" class="form-control-label">Presion arterial sistolica <i style="color: darkorange">*</i></label>
									<input type="number" class="form-control" name="presionSistolica"   required="true"/>
								</div>
								<div class="form-group col-md-12">
									<label for="message-text" class="form-control-label">Presion arterial diastolica <i style="color: darkorange">*</i></label>
									<input type="number" class="form-control" name="presionDiastolica"   required="true"/>
								</div>
								<div class="form-group col-md-12">
									<label for="message-text" class="form-control-label">Frecuencia respiratoria <i style="color: darkorange">*</i></label>
									<input type="number" class="form-control" name="frecuenciaRespiratoria"   required="true"/>
								</div>
								<div class="form-group col-md-12">
									<label for="message-text" class="form-control-label">Temperatura <i style="color: darkorange">*</i></label>
									<input type="number" class="form-control" name="temperatura"  required="true"/>
								</div>	
								<div class="form-group col-md-12">
									<label for="message-text" class="form-control-label">Peso </label>
									<input type="number" class="form-control" name="peso"  required="true" />
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">
									Cancelar
								</button>
								<button type="submit" class="btn btn-info" >
									Registrar
								</button>
							</div>
						</form>
					</div>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->

<!-- Modal Registrar Patologias-->
<div id="modalNuevoMedPatologias" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<h4 class="modal-title text-center">REGISTRAR NUEVA PATOLOGIA</h4>
			</div>
			<div class="modal-body">
				<div class="content">
					<div class="row">
						<form id="formularioMedicinaPatologia" method="POST" action="<?php echo $base_url ?>pages/gerontologia/medicina/procesarDatos/procesarMedicinaPatologias.php?tipo=insert&idMatriculaDato=<?= $fetchPaciente[0] ?>" autocomplete="off">
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Fecha<i style="color: darkorange">*</i></label>
								<input type="date" class="form-control" name="fecha" required="true" />
							</div>
							<div class="form-group col-md-12">
								<label for="recipient-name" class="form-control-label">Patologia <i style="color: darkorange">*</i></label>
								<select class="form-control" name="patologia" id="patologiaMedicina" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									$queryPatologia = mysqli_query($conn, "SELECT id_patologia,patologia FROM gddt_patologias ;");
									while ($fetchPatologia = mysqli_fetch_row($queryPatologia)) {
										echo "<option value=" . $fetchPatologia[0] . ">" . $fetchPatologia[1] . "</option>";
									}
									?>
								</select>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Capitulo <i style="color: darkorange">*</i></label>
								<input type="number" class="form-control" name="capitulo" required="true"/>
							</div>
							<div class="form-group col-md-12">
								<label for="recipient-name" class="form-control-label">Enfermedad <i style="color: darkorange">*</i></label>
								<select class="form-control" name="enfermedadMedicina" id="enfermedadMedicina" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									$queryEnfermedad = mysqli_query($conn, "SELECT id_enfermedad, des_enfermedad FROM gddt_enfermedades_cie ;");
									while ($fetchEnfermedad = mysqli_fetch_row($queryEnfermedad)) {
										echo "<option value=" . $fetchEnfermedad[0] . ">" . $fetchEnfermedad[1] . "</option>";
									}
									?>
								</select>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">
									Cancelar
								</button>
								<button type="submit" class="btn btn-info" >
									Registrar
								</button>
							</div>
						</form>
					</div>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->

<!-- Modal Registrar Consultas-->
<div id="modalNuevoMedConsultas" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-xl">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<div class="form-group col-md-12" style="margin-bottom: -8px"><h4 class="modal-title text-center">REGISTRAR NUEVO CONSULTA</h4></div>
			</div>
			<div class="modal-body">
				<div class="content">
					<form id="formularioMedicinaConsulta" method="POST" action="<?php echo $base_url ?>pages/gerontologia/medicina/procesarDatos/procesarMedicinaConsulta.php?tipo=insert&idMatriculaDato=<?= $fetchPaciente[0] ?>" autocomplete="off">
						<div class="row">
							<div class="form-group col-md-6">
								<label for="message-text" class="form-control-label"> Acompañante <i style="color: darkorange">*</i></label>
								<input type="text" class="form-control" name="acompanante" required="true" />
							</div>
							<div class="form-group col-md-6">
								<label for="message-text" class="form-control-label"> Telefono del Acompañante <i style="color: darkorange">*</i></label>
								<input type="number" class="form-control" name="telAcompanante" required="true"/>
							</div>
							<div class="form-group col-md-6">
								<label for="message-text" class="form-control-label"> Motivo de la consulta <i style="color: darkorange">*</i></label>
								<textarea class="form-control" name="motivoConsulta" cols="40" rows="5" required="true"></textarea>
							</div>	
							<div class="form-group col-md-6">
								<label for="message-text" class="form-control-label">Enfermedad actual <i style="color: darkorange">*</i></label>
								<textarea class="form-control" name="enfermedadActual" cols="40" rows="5" required="true"></textarea>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Descripcion profesional <i style="color: darkorange">*</i></label>
								<textarea class="form-control" name="descripcionProfesional" cols="40" rows="5" required="true"></textarea>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Antecedentes médicos personales <i style="color: darkorange">*</i></label>
								<?php 
								$selectNames = array(
									0 => 'HTA',
									1 => 'Diabetes',
									2 => 'Asma',
									3 => 'TB',
									4 => 'Cancer',
									5 => 'ITS',
									6 => 'EPOC',
									7 => 'Epilepsia',
									8 => 'Transt. Mental',
									9 => 'Transfuciones',
									10 => 'Hipertencion',
									11 => 'Dislipedimia',
									12 => 'Otros',
									13 => 'Ninguno'
								);
								?>
								<select class="form-control" name="antecedenteMedicoPersonal" id="antecedenteMedicoPersonal" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									foreach ($selectNames as $key) {
										echo '<option value='.$key.'>'.$key.'</option>';
									}
									?>
								</select>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Cuales </label>
								<textarea class="form-control" name="antecedenteMedicoPersonalCual" cols="40" rows="5" ></textarea>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Antecedentes médicos familiares <i style="color: darkorange">*</i></label>
								<select class="form-control" name="antecedenteMedicoFamiliares" id="antecedenteMedicoFamiliares" required="true">
									<option value="0">Selecciona una opción</option>
									<?php
									foreach ($selectNames as $key) {
										echo '<option value='.$key.'>'.$key.'</option>';
									}
									?>
								</select>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Cuales </label>
								<textarea class="form-control" name="antecedenteMedicoFamiliaresCual" cols="40" rows="5" ></textarea>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Antecedentes gineco obstétricos <i style="color: darkorange">*</i></label>
								<?php 
								$selectNames = array(
									0 => 'Gestas',
									1 => 'Partos',
									2 => 'Abortos',
									3 => 'TB',
									4 => 'Cesareas',
									5 => 'Vivos',
									6 => 'Menarquia',
									7 => 'Menopausia',
									8 => 'Ninguno'
								);
								$selectNamesAntcGineObste = array(
									0 => 'FUM',
									1 => 'FPP',
									2 => 'FUP',
									3 => 'Otros',
									4 => 'Ninguno'
								);
								?>
								<select class="form-control" name="antecedenteGinecoObstetricos" id="antecedenteGinecoObstetricos" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									foreach ($selectNames as $key) {
										echo '<option value='.$key.'>'.$key.'</option>';
									}
									?>
								</select>

							</div>
							<div class="form-group col-md-12">
								<select class="form-control" name="antecedenteGinecoObstetricos2" id="antecedenteGinecoObstetricos2" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									foreach ($selectNamesAntcGineObste as $key) {
										echo '<option value='.$key.'>'.$key.'</option>';
									}
									?>
								</select>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Cuales </label>
								<textarea class="form-control" name="AntecedentesGinecoObstétricosCuales" cols="40" rows="5" ></textarea>
							</div>
							<div class="form-group col-md-4">
								<label for="message-text" class="form-control-label">Fecha ultima citologia <i style="color: darkorange">*</i></label>
								<input type="date" class="form-control" name="fechaUltimaCitologia" required="true"/>
							</div>
							<div class="form-group col-md-4">
								<label for="message-text" class="form-control-label"> Periodo ciclo <i style="color: darkorange">*</i></label>
								<input type="number" class="form-control" name="periodoCiclo" required="true"/>
							</div>
							<div class="form-group col-md-4">
								<label for="message-text" class="form-control-label"> Metodo de planificacion <i style="color: darkorange">*</i></label>
								<input type="text" class="form-control" name="metodoPlanificacion" required="true"/>
							</div>
							<div class="form-group col-md-12" style="margin-bottom: -8px"><h4 class="modal-title text-center mb-4">ANTECEDENTES PEDIATRICOS</h4></div>
							<div class="form-group col-md-6">
								<label for="message-text" class="form-control-label"> Eruptiva <i style="color: darkorange">*</i></label>
								<select class="form-control" name="eruptiva" id="eruptiva" required="true">
									<option value="">Selecciona una opción</option>
									<option  value="SI">SI</option>
									<option  value="NO">NO</option>
								</select>
							</div>
							<div class="form-group col-md-6">
								<label for="message-text" class="form-control-label"> Cuales <i style="color: darkorange">*</i></label>
								<input type="text" class="form-control" name="eruptivaCual" />
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label"> Esquema evacuacion dia <i style="color: darkorange">*</i></label>
								<input type="text" class="form-control" name="esquemaEvacuacionDia" required="true"/>
							</div>
							<div class="form-group col-md-6">
								<label for="message-text" class="form-control-label"> Trastorno ortopedico <i style="color: darkorange">*</i></label>
								<select class="form-control" name="trastornoOrtopedico" id="trastornoOrtopedico" required="true">
									<option value="">Selecciona una opción</option>
									<option value="SI">SI</option>
									<option value="NO">NO</option>
								</select>
							</div>
							<div class="form-group col-md-6">
								<label for="message-text" class="form-control-label"> Cuales</label>
								<input type="text" class="form-control" name="cualTrastornoOrtopedico" />
							</div>
							<div class="form-group col-md-6">
								<label for="message-text" class="form-control-label"> Otro antecedente </label>
								<textarea class="form-control" name="otroAntecedente" cols="40" rows="5" ></textarea>
							</div>
							<div class="form-group col-md-6">
								<label for="message-text" class="form-control-label"> Alergia </label>
								<textarea class="form-control" name="alergia" cols="40" rows="5" ></textarea>
							</div>
							<div class="form-group col-md-6">
								<label for="message-text" class="form-control-label"> Quirurgico </label>
								<textarea class="form-control" name="quirurgico" cols="40" rows="5" ></textarea>
							</div>
							<div class="form-group col-md-6">
								<label for="message-text" class="form-control-label"> Droga sicoactiva </label>
								<textarea class="form-control" name="drogasicoactiva" cols="40" rows="5" ></textarea>
							</div>
							<div class="form-group col-md-6">
								<label for="message-text" class="form-control-label"> Cigarrillo <i style="color: darkorange">*</i></label>
								<select class="form-control" name="cigarrillo" id="cigarrillo" required="true">
									<option value="">Selecciona una opción</option>
									<option  value="SI">SI</option>
									<option  value="NO">NO</option>
								</select>
							</div>
							<div class="form-group col-md-6">
								<label for="message-text" class="form-control-label"> Cuanto al dia </label>
								<input type="text" class="form-control" name="cuantoCigarrillo" />
							</div>
							<div class="form-group col-md-6">
								<label for="message-text" class="form-control-label"> Licor <i style="color: darkorange">*</i></label>
								<select class="form-control" name="licor" id="licor" required="true">
									<option value="">Selecciona una opción</option>
									<option  value="SI">SI</option>
									<option  value="NO">NO</option>
								</select>
							</div>
							<div class="form-group col-md-6">
								<label for="message-text" class="form-control-label"> Frecuencia </label>
								<input type="text" class="form-control" name="frecuenciaLicor" />
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label"> Medicamento que consume </label>
								<textarea class="form-control" name="medicamentoConsume" cols="40" rows="5" ></textarea>
							</div>	
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								Cancelar
							</button>
							<button type="submit" class="btn btn-info">
								Registrar
							</button>
						</div>
					</form>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->

<!-- Modal Registrar Formulas Medicas-->
<div id="modalNuevoMedFormulasMedicas" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<h4 class="modal-title text-center">REGISTRAR NUEVA FORMULA MEDICA</h4>
			</div>
			<div class="modal-body">
				<div class="content">
					<form id="formularioMedicinaFormulasMedicas" method="POST" action="<?php echo $base_url ?>pages/gerontologia/medicina/procesarDatos/procesarMedicinaFormulasMedicas.php?tipo=insert&idMatriculaDato=<?= $fetchPaciente[0] ?>" autocomplete="off">
						<div class="row">
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Fecha<i style="color: darkorange">*</i></label>
								<input type="date" class="form-control" name="fecha" required="true" />
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Descripcion de la formula medica <i style="color: darkorange">*</i></label>
							</div>
							<?php for ($i=0; $i < 5; $i++) { ?>
								<div class="form-group col-md-12">
									<textarea class="form-control" name="planTratamiento[]" cols="40" rows="5" required="true"><?php echo ($i+1).'.'; ?></textarea>
								</div>
							<?php } ?>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								Cancelar
							</button>
							<button type="submit" class="btn btn-info">
								Registrar
							</button>
						</div>
					</form>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->

<!-- Modal Registrar Revision Funcional -->
<div id="modalNuevoMedRevisionFuncional" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<h4 class="modal-title text-center">REGISTRAR NUEVA REVISION FUNCIONAL</h4>
			</div>
			<div class="modal-body">
				<div class="content">
					<form id="registrarMedRevisionFuncional" method="POST" action="<?php echo $base_url ?>pages/gerontologia/medicina/procesarDatos/procesarMedicinaRevisionFuncional.php?tipo=insert&idMatriculaDato=<?= $fetchPaciente[0] ?>" autocomplete="off">
						<div class="row">
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Fecha<i style="color: darkorange">*</i></label>
								<input type="date" class="form-control" name="fecha" required="true" />
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label"> Descripcion de la revision funcional <i style="color: darkorange">*</i></label>
								<textarea class="form-control" name="observacionPsicologia" cols="40" rows="5" required="true"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								Cancelar
							</button>
							<button type="submit" class="btn btn-info">
								Registrar
							</button>
						</div>
					</form>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->

<!-- Modal Registrar Antecedente Personal -->
<div id="modalNuevoMedAntecedentePersonal" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<h4 class="modal-title text-center">REGISTRAR NUEVO ANTECEDENTE PERSONAL</h4>
			</div>
			<div class="modal-body">
				<div class="content">
					<form id="registrarMedAntecedentePersonal" method="POST" action="<?php echo $base_url ?>pages/gerontologia/medicina/procesarDatos/procesarMedicinaAntecedentePersonal.php?tipo=insert&idMatriculaDato=<?= $fetchPaciente[0] ?>" autocomplete="off">
						<div class="row">
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Fecha <i style="color: darkorange">*</i></label>
								<input type="date" class="form-control" name="fecha" required="true"/>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label"> Descripcion antecedente personal medica <i style="color: darkorange">*</i></label>
								<textarea class="form-control" name="desAntecedentePersonalMedica" cols="40" rows="5" required="true"></textarea>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label"> Personal </label>
								<textarea class="form-control" name="personal" cols="40" rows="5" required="true"></textarea>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label"> Familiar </label>
								<textarea class="form-control" name="familiar" cols="40" rows="5" required="true"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								Cancelar
							</button>
							<button type="submit" class="btn btn-info" >
								Registrar
							</button>
						</div>
					</form>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->

<!-- Modal Registrar Impresion Diagnostica -->
<div id="modalNuevoMedImpresionDiagnostica" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<h4 class="modal-title text-center">REGISTRAR NUEVA IMPRESION DIAGNOSTICA</h4>
			</div>
			<div class="modal-body">
				<div class="content">
					<form id="registrarMedImpresionDiagnostica" method="POST" action="<?php echo $base_url ?>pages/gerontologia/medicina/procesarDatos/procesarMedicinaImpresionDiagnostica.php?tipo=insert&idMatriculaDato=<?= $fetchPaciente[0] ?>" autocomplete="off">
						<div class="row">
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Fecha <i style="color: darkorange">*</i></label>
								<input type="date" class="form-control" name="fecha" required="true"/>
							</div>
							<div class="form-group col-md-12">
								<label for="recipient-name" class="form-control-label">Enfermedad <i style="color: darkorange">*</i></label>
								<select class="form-control" name="enfermedad" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									$queryCuenta = mysqli_query($conn, "SELECT id_enfermedad, des_enfermedad FROM gddt_enfermedades_cie_act;");
									while ($fetchCuenta = mysqli_fetch_row($queryCuenta)) {
										echo "<option value=" . $fetchCuenta[0] . ">" . $fetchCuenta[1] . "</option>";
									}
									?>
								</select>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Descripcion impresion diagnostica <i style="color: darkorange">*</i></label>
								<textarea class="form-control" name="planTratamiento" cols="40" rows="5" required="true"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								Cancelar
							</button>
							<button type="submit" class="btn btn-info">
								Registrar
							</button>
						</div>
					</form>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->

<!-- Modal Registrar Examen Fisico -->
<div id="modalNuevoMedExamenFisico" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-xl">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<h4 class="modal-title text-center">REGISTRAR NUEVA IMPRESION DIAGNOSTICA</h4>
			</div>
			<div class="modal-body">
				<div class="content">
					<form id="registrarMedExamenfisico" method="POST" action="<?php echo $base_url ?>pages/gerontologia/medicina/procesarDatos/procesarMedicinaExamenFisico.php?tipo=insert&idMatriculaDato=<?= $fetchPaciente[0] ?>" autocomplete="off">
						<div class="row">
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Fecha <i style="color: darkorange">*</i></label>
								<input type="date" class="form-control" name="fecha" required="true"/>
							</div>
							<div class="form-group col-md-4">
								<label for="message-text" class="form-control-label"> Cabeza <i style="color: darkorange">*</i></label>
								<textarea class="form-control" name="cabeza" cols="40" rows="5" required="true"></textarea>
							</div>
							<div class="form-group col-md-4">
								<label for="message-text" class="form-control-label"> Cuello <i style="color: darkorange">*</i></label>
								<textarea class="form-control" name="cuello" cols="40" rows="5" required="true"></textarea>
							</div>
							<div class="form-group col-md-4">
								<label for="message-text" class="form-control-label"> Torax <i style="color: darkorange">*</i></label>
								<textarea class="form-control" name="torax" cols="40" rows="5" required="true"></textarea>
							</div>
							<div class="form-group col-md-4">
								<label for="message-text" class="form-control-label"> Aparto respiratorio <i style="color: darkorange">*</i></label>
								<textarea class="form-control" name="aparatoRespiratorio" cols="40" rows="5" required="true"></textarea>
							</div>
							<div class="form-group col-md-4">
								<label for="message-text" class="form-control-label"> Aparto cardiovascular <i style="color: darkorange">*</i></label>
								<textarea class="form-control" name="aparatoCardiovascular" cols="40" rows="5" required="true"></textarea>
							</div>
							<div class="form-group col-md-4">
								<label for="message-text" class="form-control-label"> Abdomen <i style="color: darkorange">*</i></label>
								<textarea class="form-control" name="abdomen" cols="40" rows="5" required="true"></textarea>
							</div>
							<div class="form-group col-md-4">
								<label for="message-text" class="form-control-label"> Pelvis <i style="color: darkorange">*</i></label>
								<textarea class="form-control" name="pelvis" cols="40" rows="5" required="true" ></textarea>
							</div>
							<div class="form-group col-md-4">
								<label for="message-text" class="form-control-label"> Aparato genitourinario <i style="color: darkorange">*</i></label>
								<textarea class="form-control" name="apartoGenitourinario" cols="40" rows="5" required="true"></textarea>
							</div>
							<div class="form-group col-md-4">
								<label for="message-text" class="form-control-label"> Sistema nervioso <i style="color: darkorange">*</i></label>
								<textarea class="form-control" name="sistemaNervioso" cols="40" rows="5" required="true"></textarea>
							</div>
							<div class="form-group col-md-4">
								<label for="message-text" class="form-control-label"> Osteomuscular <i style="color: darkorange">*</i></label>
								<textarea class="form-control" name="osteomuscular" cols="40" rows="5" required="true"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								Cancelar
							</button>
							<button type="submit" class="btn btn-info">
								Registrar
							</button>
						</div>
					</form>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->

<!-- Modal Registrar Resultado Examenes  -->
<div id="modalNuevoMedResultadoExamenes" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-xl">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<h4 class="modal-title text-center">REGISTRAR NUEVO RESULTADO</h4>
			</div>
			<div class="modal-body">
				<div class="content">
					<form id="registrarMedResultadoExamenes" method="POST" action="<?php echo $base_url ?>pages/gerontologia/medicina/procesarDatos/procesarMedicinaResultadoExamenes.php?tipo=insert&idMatriculaDato=<?= $fetchPaciente[0] ?>" autocomplete="off">
						<div class="row">
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Fecha <i style="color: darkorange">*</i></label>
								<input type="date" class="form-control" name="fecha" required="true"/>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label"> Resulado <i style="color: darkorange">*</i></label>
								<textarea class="form-control" name="resultado" cols="40" rows="5" required="true"></textarea>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label"> Orden medica <i style="color: darkorange">*</i></label>
								<textarea class="form-control" name="ordenMedica" cols="40" rows="5" required="true"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								Cancelar
							</button>
							<button type="submit" class="btn btn-info">
								Registrar
							</button>
						</div>
					</form>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->

<!-- Modal Registrar Historial de Seguimientos -->
<div id="modalNuevoMedSeguimientos" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<h4 class="modal-title text-center">REGISTRAR NUEVO SEGUIMIENTO</h4>
			</div>
			<div class="modal-body">
				<div class="content">
					<form id="registrarMedSeguimientos" method="POST" action="<?php echo $base_url ?>pages/gerontologia/medicina/procesarDatos/procesarMedicinaSeguimiento.php?tipo=insert&idMatriculaDato=<?= $fetchPaciente[0] ?>" autocomplete="off">
						<div class="row">
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Fecha<i style="color: darkorange">*</i></label>
								<input type="date" class="form-control" name="fecha" required="true" />
							</div>
							<div class="form-group col-md-12">
								<label for="recipient-name" class="form-control-label">Tipo de seguimiento <i style="color: darkorange">*</i></label>
								<select class="form-control" name="tipoSeguimiento" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									$queryCuenta = mysqli_query($conn, "SELECT id_tipo_seguimiento, descripcion FROM gddt_tipo_seguimientos;");
									while ($fetchCuenta = mysqli_fetch_row($queryCuenta)) {
										echo "<option value=" . $fetchCuenta[0] . ">" . $fetchCuenta[1] . "</option>";
									}
									?>
								</select>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Descripcion del seguimiento <i style="color: darkorange">*</i></label>
								<textarea class="form-control" name="seguimientoDescripcion" cols="40" rows="5" required="true"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								Cancelar
							</button>
							<button type="submit" class="btn btn-info">
								Registrar
							</button>
						</div>
					</form>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->

<!-- Modal Registrar Orden Medica -->
<div id="modalNuevoMedOrdenMedica" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<h4 class="modal-title text-center">REGISTRAR NUEVA ORDEN MEDICA</h4>
			</div>
			<div class="modal-body">
				<div class="content">
					<form id="registrarMedOrdenMedica" method="POST" action="<?php echo $base_url ?>pages/gerontologia/medicina/procesarDatos/procesarMedicinaOrdenesMedicas.php?tipo=insert&idMatriculaDato=<?= $fetchPaciente[0] ?>" autocomplete="off">
						<div class="row">
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Fecha <i style="color: darkorange">*</i></label>
								<input type="date" class="form-control" name="fechaMod" required="true"/>
							</div>
							<div class="form-group col-md-12" id="ordenesMedicas">
								<label for="message-text" class="form-control-label">Orden Medica</label>
							</div>
							<?php 
							for ($i=1; $i <= 10; $i++) { ?>
								<textarea class="form-control mt-2" name="ordenesMedicas[]" cols="40" rows="5" required="true"><?php echo $i; ?>.</textarea>
							<?php } ?>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								Cancelar
							</button>
							<button type="submit" class="btn btn-info" >
								Registrar
							</button>
						</div>
					</form>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->