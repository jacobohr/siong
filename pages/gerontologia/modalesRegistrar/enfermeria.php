<!-- Modal Registrar Enfermeria Notas-->
<div id="modalNuevaEnfermeriaNota" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<h4 class="modal-title text-center">REGISTRAR NUEVA NOTA DE ENFERMERIA</h4>
			</div>
			<div class="modal-body">
				<div class="content">
					<form id="registrarEnfermeriaNotas" method="POST" action="<?php echo $base_url ?>pages/gerontologia/enfermeria/procesarDatos/procesarEnfermeriaNotas.php?tipo=insert&idMatriculaDato=<?= $fetchPaciente[0] ?>" autocomplete="off">
						<div class="row">
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Fecha <i style="color: darkorange">*</i></label>
								<input type="date" class="form-control" name="fecha" required="true" />
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Tipo <i style="color: darkorange">*</i></label>
								<select class="form-control" name="tipoNota" required="true">
									<option value="">Seleccione el tipo</option>
									<option value="Evaluacion de ingreso">Evaluacion de ingreso</option>
									<option value="Cita con medico General">Cita con medico General</option>
									<option value="Cita con Especialista">Cita con Especialista</option>
									<option value="Cirugia ">Cirugia</option>
									<option value="Remision a Urgencia">Remision a Urgencia</option>
									<option value="Control Hipertension con Medico">Control Hipertension con Medico</option>
									<option value="Control Hipertension con Enfermera">Control Hipertension con Enfermera</option>
									<option value="Control Diabetes">Control Diabetes</option>     
									<option value="Evolucion Auxiliares de Enfermeria">Evolucion Auxiliares de Enfermeria</option>
									<option value="Registro de egreso">Registro de egreso</option>
									<option value="Nota usuario hospitalizado">Nota usuario hospitalizado</option>
									<option value="Nota usuarios que regresa de hospitalizacion">Nota usuarios que regresa de hospitalizacion</option>
									<option value="Remision a ayudas diagnosticas">Remision a ayudas diagnosticas</option>
									<option value="Remision examenes laboratorio ">Remision Examenes de Laboratorio</option>
									<option value="Nota Egreso">Nota de Egreso</option>
									<option value="Nota Aclaratoria">Nota Aclaratoria</option>
									<option value="Evento adversos" >Evento Adverso</option>
								</select>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label"> Descripcion <i style="color: darkorange">*</i></label>
								<textarea class="form-control" name="descripcionNota" cols="40" rows="5" required="true"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								Cancelar
							</button>
							<button type="submit" class="btn btn-info">
								Registrar
							</button>
						</div>
					</form>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->

<!-- Modal Registrar Enfermeria Jefe-->
<div id="modalNuevaEnfermeriaJefe" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<h4 class="modal-title text-center">REGISTRAR NUEVO JEFE DE ENFERMERIA</h4>
			</div>
			<div class="modal-body">
				<div class="content">
					<form id="registrarEnfermeriaJefe">
						<div class="row">
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Fecha de modificacion <i style="color: darkorange">*</i></label>
								<input type="date" class="form-control" name="fechaMod" required="true"/>
							</div>
							<div class="form-group col-md-12">
								<label for="recipient-name" class="form-control-label">Tipo <i style="color: darkorange">*</i></label>
								<?php 
								$selectNames = array(
									0 => 'Valoracion de Ingreso de jefe enfermeria',
									1 => 'Valoracion cefalocaudal de jefe enfermeria',
									2 => 'Control despues de hospitalizacion jefe de enfermeria',
									3 => 'Control de patologias cronicas no transmisibles jefe de enfermeria',
									4 => 'Control de patologias cronicas transmisibles jefe de enfermeria',
									5 => 'Seguimiento al Plan de Intervencion de jefe enfermeria',
									6 => 'Nota de evolucion de jefe de enfermeria',
									7 => 'Nota de egreso de jefe de enfermeria',
									8 => 'Plan de Intervencion de jefe de enfermeria',
									9 => 'Atencion individual',
									10 => 'Nota Aclaratoria',
									11 => 'Evento Adverso',
									12 => 'Seguimiento al evento adverso',
									13 => 'Seguimiento de enfermeria'
								);
								?>
								<select class="form-control" name="tipo" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									foreach ($selectNames as $key) {
										echo '<option value='.$key.'>'.$key.'</option>';
									}
									?>
								</select>
							</div>
							<div class="form-group col-md-12">
								<?php 
								$sqlMatriculaDato = 'SELECT id_matricula_dato FROM gddt_matricula_datos WHERE num_documento ="'. $docPaciente.'"';
								$queryMatriculaDato = mysqli_query($conn, $sqlMatriculaDato);
								$fetchMatriculaDato = mysqli_fetch_row($queryMatriculaDato);


								$sqlImpresionDiagnostica = "
								SELECT *,(CONVERT(CAST(CONVERT(buscador_cie USING latin1) AS BINARY) USING utf8))AS buscador_cie 
								FROM gddt_jefe_enfermeria
								WHERE id_matricula_dato =" . $fetchMatriculaDato[0]." ORDER BY id_jefe_enfermeria DESC LIMIT 1 " ;
								$queryImpresionDiagnostica = mysqli_query($conn, $sqlImpresionDiagnostica);
								$fetchImpresionDiagnostica = mysqli_fetch_row($queryImpresionDiagnostica);

								if(empty($fetchImpresionDiagnostica)){$numeroDiagnostico = true;}else{$numeroDiagnostico = false;}

								?>
								<label for="message-text" class="form-control-label">Impresion diagnostica <i style="color: darkorange">*</i></label>
								<button type="button" class="btn btn-info float-right mb-2" style="height: 30px;padding-top: 2px;" onclick="cargaNuevoDiagnostico();">Agregar diagnostico secundario</button>
								<?php 
								if($numeroDiagnostico == false){
									?>
									<label for="message-text" class="form-control-label">Diagnostico 1</label>
									<input type="text" class="form-control" name="fechaMod" value="<?php if(empty($fetchImpresionDiagnostica[10])){echo 'Sin datos';}else{echo $fetchImpresionDiagnostica[10];} ?>" disabled/>
									<?php
								}?>
								<div id="divPruebaaa" class="mt-2">
								</div>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Descripcion <i style="color: darkorange">*</i></label>
								<textarea class="form-control" name="seguimientoDescripcion" cols="40" rows="5" required="true"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								Cancelar
							</button>

							<button type="submit" class="btn btn-info" onClick="">
								Registrar
							</button>
						</div>
					</form>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->

<!-- Modal Registrar Enfermeria Balance de liquidos-->
<div id="modalNuevaEnfermeriaBalanceLiquidos" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<h4 class="modal-title text-center">REGISTRAR NUEVO BALANCE DE LIQUIDOS</h4>
			</div>
			<div class="modal-body">
				<div class="content">
					
					<form id="registrarEnfermeriaBalanceLiquidos">
						<div class="row">
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Fecha <i style="color: darkorange">*</i></label>
								<input type="date" class="form-control" name="fechaMod" required="true"/>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Cantidad indicada <i style="color: darkorange">*</i></label>
								<input type="text" class="form-control" name="cantidadIndicada"required="true" />
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Cantidad por administrar <i style="color: darkorange">*</i></label>
								<input type="text" class="form-control" name="cantidadAdministrar"required="true" />
							</div>
							<div class="form-group col-md-12">
								<label for="recipient-name" class="form-control-label">Via de administracion <i style="color: darkorange">*</i></label>
								<?php 
								$selectViaAdministracion = array(
									0 => 'Sonda',
									1 => 'Oral',
									2 => 'Parenteral',
									3 => 'total'
								);
								?>
								<select class="form-control" name="tipo" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									foreach ($selectViaAdministracion as $key) {
										echo '<option value='.$key.'>'.$key.'</option>';
									}
									?>
								</select>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Total <i style="color: darkorange">*</i></label>
								<input type="text" class="form-control" name="total1" required="true"/>
							</div>
							<div class="form-group col-md-12">
								<label for="recipient-name" class="form-control-label">Eliminados <i style="color: darkorange">*</i></label>
								<?php 
								$selectEliminados = array(
									0 => 'Materia Fecal',
									1 => 'Orina',
									2 => 'Vómito',
									3 => 'Drenajes'
								);
								?>
								<select class="form-control" name="tipo" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									foreach ($selectEliminados as $key) {
										echo '<option value='.$key.'>'.$key.'</option>';
									}
									?>
								</select>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Total <i style="color: darkorange">*</i></label>
								<input type="text" class="form-control" name="total2" required="true"/>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								Cancelar
							</button>
							<button type="submit" class="btn btn-info" onClick="">
								Registrar
							</button>
						</div>
					</form>
					
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->

<!-- Modal Registrar Enfermeria Control de glicemia-->
<div id="modalNuevaEnfermeriaControlGlicemia" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<h4 class="modal-title text-center">REGISTRAR NUEVO CONTROL DE GLICEMIA</h4>
			</div>
			<div class="modal-body">
				<div class="content">
					
					<form id="registrarEnfermeriaControlGlicemia">
						<div class="row">
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Fecha de modificacion <i style="color: darkorange">*</i></label>
								<input type="date" class="form-control" name="fechaMod" required="true"/>
							</div>
							<div class="form-group col-md-12">
								<label for="recipient-name" class="form-control-label">Hora del control <i style="color: darkorange">*</i></label>
								<?php 
								$selectHoraControl = array(
									0 => '6:00 am',
									1 => '8:00 am',
									2 => '12:00 pm',
									3 => '6:00 pm',
									4 => '8:00 pm'

								);
								?>
								<select class="form-control" name="tipo" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									foreach ($selectHoraControl as $key) {
										echo '<option value='.$key.'>'.$key.'</option>';
									}
									?>
								</select>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label"> Sintomatología <i style="color: darkorange">*</i></label>
								<textarea class="form-control" name="sintomatologia" cols="40" rows="5" required="true"></textarea>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Valor glicemia <i style="color: darkorange">*</i></label>
								<input type="number" class="form-control" name="valorGlicemia" min="1" max="400" required="true"/>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								Cancelar
							</button>
							<button type="submit" class="btn btn-info" onClick="">
								Registrar
							</button>
						</div>
					</form>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->

<!-- Modal Registrar Enfermeria Control de suministros-->
<div id="modalNuevaEnfermeriaControlSuministros" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<h4 class="modal-title text-center">REGISTRAR NUEVO CONTROL DE SUMINISTROS</h4>
			</div>
			<div class="modal-body">
				<div class="content">
					<form id="registrarEnfermeriaControlSuministros">
						<div class="row">
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Fecha de modificacion <i style="color: darkorange">*</i></label>
								<input type="date" class="form-control" name="fechaMod" required="true"/>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Dosis <i style="color: darkorange">*</i></label>
								<input type="text" class="form-control" name="dosis" required="true"/>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label"> Medicamento <i style="color: darkorange">*</i></label>
								<textarea class="form-control" name="medicamento" cols="40" rows="5" required="true"></textarea>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Via <i style="color: darkorange">*</i></label>
								<input type="text" class="form-control" name="via" required="true"/>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								Cancelar
							</button>
							<button type="submit" class="btn btn-info" onClick="">
								Registrar
							</button>
						</div>
					</form>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->

<!-- Modal Registrar Enfermeria Control de Curaciones y procedimientos-->
<div id="modalNuevaEnfermeriaControlCuraciones" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<h4 class="modal-title text-center">REGISTRAR NUEVO CONTROL DE CURACIONES</h4>
			</div>
			<div class="modal-body">
				<div class="content">
					<form id="registrarEnfermeriaControlCuraciones">
						<div class="row">
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Fecha de modificacion <i style="color: darkorange">*</i></label>
								<input type="date" class="form-control" name="fechaMod" required="true"//>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Lugar anatomico <i style="color: darkorange">*</i></label>
								<input type="text" class="form-control" name="lugarAnatomico" required="true"/ />
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label"> Profundidad de tamaño<i style="color: darkorange">*</i></label>
								<input type="text" class="form-control" name="profundidadTamano" required="true"//>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Tipo de lesion <i style="color: darkorange">*</i></label>
								<input type="text" class="form-control" name="tipoLesion" required="true"//>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Bordes <i style="color: darkorange">*</i></label>
								<input type="text" class="form-control" name="bordes" required="true"//>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Caracteristicas <i style="color: darkorange">*</i></label>
								<textarea class="form-control" name="caracteristicas" cols="40" rows="5" required="true"/></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								Cancelar
							</button>
							<button type="submit" class="btn btn-info" onClick="">
								Registrar
							</button>
						</div>
					</form>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->

<!-- Modal Registrar Enfermeria Signos Vitales-->
<div id="modalNuevaEnfermeriaSignosVitales" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<h4 class="modal-title text-center">REGISTRAR NUEVO SIGNO VITAL</h4>
			</div>
			<div class="modal-body">
				<div class="content">
					<form id="registrarEnfermeriaSignosVitales">
						<div class="row">
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Fecha de modificacion <i style="color: darkorange">*</i></label>
								<input type="date" class="form-control" name="fechaMod" required="true"/>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Frecuencia cardiaca <i style="color: darkorange">*</i></label>
								<input type="number" class="form-control" name="frecuenciaCardiaca" required="true"/>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label"> Presión arterial sistólica<i style="color: darkorange">*</i></label>
								<input type="number" class="form-control" name="presionSistolica" required="true"/>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Presión arterial diastólica <i style="color: darkorange">*</i></label>
								<input type="number" class="form-control" name="presionDiastolica" required="true"/>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Frecuencia respiratoria <i style="color: darkorange">*</i></label>
								<input type="number" class="form-control" name="frecuenciaRespiratoria" required="true"/>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Temperatura <i style="color: darkorange">*</i></label>
								<input type="number" class="form-control" name="temperatura" required="true"/>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Peso <i style="color: darkorange">*</i></label>
								<input type="number" class="form-control" name="peso" required="true"/>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								Cancelar
							</button>
							<button type="submit" class="btn btn-info" onClick="">
								Registrar
							</button>
						</div>
					</form>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->


<script type="text/javascript">
	var cont = <?php if($numeroDiagnostico == false){echo 1;}else{echo 0;} ?>;
	function cargaNuevoDiagnostico() {
		cont++;

		var div = document.getElementById("divPruebaaa");
		var select = document.createElement("select");
		var label = document.createElement("label");
		var labelT = document.createTextNode("Diagnostico "+ cont);

		label.appendChild(labelT);


		select.setAttribute("class", "cargarEnfermedad form-control");
		document.getElementById("divPruebaaa").appendChild(label);;
		document.getElementById("divPruebaaa").appendChild(select);


		$('.cargarEnfermedad').select2({
			placeholder: "Digite el nombre de la enfermedad...",
			language: {
				noResults: function() {
					return "No hay resultado";        
				},
				searching: function() {
					return "Buscando...";
				},
				errorLoading: function(){
					return "Ningun registro encontrado";
				},
			},
			ajax:{
				url: 'autocomplete/traerEnfermedades.php',
				dataType: 'json',
				delay: 250,
				processResult: function(data){
					return{
						results:name
					};
				},
				cache: true
			}
		});	
	}
</script>
