	function formularioMedicinaPatologia(evt){
		evt.preventDefault();// Evitamos el submit en nuevos navegadores.
		let patologia = document.getElementById('patologiaMedicina');
		let enfermedad = document.getElementById('enfermedadMedicina');
		if (patologia.value == "0" || enfermedad.value == "0" )  {
			swal({
				title: "Faltan datos por completar",
				text: "Ingresa todos los campos requeridos",
				icon: "warning",
				buttons: true,
				dangerMode: true,
			})
			return true;
		}else{
			document.getElementById("formularioMedicinaPatologia").submit();
		}
	}
	function formularioMedicinaConsulta(evt){
		//evt.preventDefault();// Evitamos el submit en nuevos navegadores.
		let antecedenteMedicoPersonal = document.getElementById('antecedenteMedicoPersonal');
		let antecedenteMedicoFamiliares = document.getElementById('antecedenteMedicoFamiliares');
		let antecedenteGinecoObstetricos = document.getElementById('antecedenteGinecoObstetricos');
		let antecedenteGinecoObstetricos2 = document.getElementById('antecedenteGinecoObstetricos2');
		let eruptiva = document.getElementById('eruptiva');
		let cigarrillo = document.getElementById('cigarrillo');
		let licor = document.getElementById('licor');
		if (antecedenteMedicoPersonal.value == "0" || antecedenteMedicoFamiliares.value == "0" || antecedenteGinecoObstetricos2.value == "0" || eruptiva.value == "0" || cigarrillo.value == "0"  || licor.value == "0")  {
			swal({
				title: "Faltan datos por completar",
				text: "Ingresa todos los campos requeridos",
				icon: "warning",
				buttons: true,
				dangerMode: true,
			})
			return true;
		}else{
			document.getElementById("formularioMedicinaConsulta").submit();
		}
	}

