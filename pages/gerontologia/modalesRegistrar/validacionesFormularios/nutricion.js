 function registroSeguimientoNutricional(evt){
		evt.preventDefault();// Evitamos el submit en nuevos navegadores.
		let formulario = document.getElementById('registrarSeguimientoNutricional');
		let usuario = document.getElementById('usuarioRegistrar');
		let fecha = document.getElementById('fechaModRegistrar');
		let tipoSeguimiento = document.getElementById('tipoSeguimientoRegistrar');
		let descripcion = document.getElementById('descripcionRegistrar');
		if (usuario.value == "-1" || fecha.value == "0" || tipoSeguimiento.value == "0" || (!$(`#descripcionRegistrar`).val().length))  {
			swal({
				title: "Faltan datos por completar",
				text: "Ingresa todos los campos requeridos",
				icon: "warning",
				buttons: true,
				dangerMode: true,
			})
			return true;
		}else{
			document.registrarSeguimientoNutricional.submit()
		}
	}

	function registroInformacionNutrional(evt){
		evt.preventDefault();// Evitamos el submit en nuevos navegadores.
		let estadoDental = document.getElementById('estadoDental');
		let clasificacionNutricional = document.getElementById('clasificacionNutricional');
		let imc = document.getElementById('imc');
		if (estadoDental.value == "0" || clasificacionNutricional.value == "0" || imc.value == '')  {
			swal({
				title: "Faltan datos por completar",
				text: "Ingresa todos los campos requeridos",
				icon: "warning",
				buttons: true,
				dangerMode: true,
			})
			return true;
		}else{
			document.registrarInformacionNutricional.submit()
		}
	}

	function registroSeguimientoNutricionalEditar(evt){
		evt.preventDefault();// Evitamos el submit en nuevos navegadores.
		let tipoSeguimientoEditar = document.getElementById('tipoSeguimientoEditar');

		if (tipoSeguimientoEditar.value == "0" )  {
			swal({
				title: "Faltan datos por completar",
				text: "Ingresa todos los campos requeridos",
				icon: "warning",
				buttons: true,
				dangerMode: true,
			})
			return true;
		}else{
			document.registrarSeguimientoNutricionalEditar.submit()
		}
	}
	function registroInfoNutricionalEditar(evt){
		evt.preventDefault();// Evitamos el submit en nuevos navegadores.
		debugger;
		let clasificacionNutricionalEditar = document.getElementById('clasificacionNutricionalEditar');
		let estadoDentalEditar = document.getElementById('estadoDentalEditar');
		console.log(document.getElementById('clasificacionNutricionalEditar').value);
		console.log(document.getElementById('estadoDentalEditar').value);
		let imc = document.getElementById('imcEditar');
		if (clasificacionNutricionalEditar.value == '0' || estadoDentalEditar.value == '0' || imc.value == '')  {
			swal({
				title: "Faltan datos por completar",
				text: "Ingresa todos los campos requeridos",
				icon: "warning",
				buttons: true,
				dangerMode: true,
			})
			return true;
		}else{
			document.registrarSeguimientoNutricionalEditar.submit()
		}
	}

	function calcularIMCEDITAR(){
		let peso = document.getElementById('pesoEditar');
		let estatura = document.getElementById('estaturaEditar');
		if(peso.value == "" || estatura.value == ""	){
			if(peso == ''){
				swal({
					title: "Porfavor ingrese el peso para poder continuar",
					icon: "warning",
					buttons: true,
					dangerMode: true,
				})
			}else{
				swal({
					title: "Porfavor ingrese la estatura para poder continuar",
					icon: "warning",
					buttons: true,
					dangerMode: true,
				})
			}
		}else{
			estatura = estatura.value;
			estatura = estatura /100;
			estatura = estatura * estatura;
			let resultado = peso.value / estatura;
			resultado = resultado.toFixed(10);
			document.getElementById('imcEditar').value = resultado;
		}
	}
	