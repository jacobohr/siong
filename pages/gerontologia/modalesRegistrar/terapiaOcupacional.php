<!-- Modal Registrar Terapia Ocupacional Evaluacion-->
<div id="modalNuevoTerapiaOcupacionalEvaluacion" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-xl">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<h4 class="modal-title text-center">REGISTRAR NUEVA EVALUACION OCUPACIONAL</h4>
			</div>
			<div class="modal-body">
				<div class="content">
					<div class="row">
						<form id="registrarTerapiaOcupacionalEvaluacion">
							<div class="row">
								<div class="form-group col-md-12">
									<label for="message-text" class="form-control-label">Fecha de modificacion <i style="color: darkorange">*</i></label>
									<input type="date" class="form-control" name="fechaMod" required="true" />
								</div>
							</div>
							<div class="form-group col-md-12 mb-1">
								<h5 class="text-center mb-4"> 	INFORMACIÓN DIAGNOSTICA </h5>
							</div>
							<div class="row">
								<div class="form-group col-md-6">
									<label for="recipient-name" class="form-control-label" >Tipo discapacidad?<i style="color: darkorange">*</i></label>
									<?php 
									$selectTieneDiscapacidad = array(
										0 => 'Si',
										1 => 'No'
									);
									?>
									<select id="selectTieneDiscapacidadNuevo" class="form-control" name="tieneDiscapacidad" onchange="tieneDiscapacidadCual();" required="true">
										<option value="">Selecciona una opción</option>
										<?php
										foreach ($selectTieneDiscapacidad as $key) {
											echo '<option value='.$key.'>'.$key.'</option>';
										}
										?>
									</select>
								</div>
								<div class="form-group col-md-6" id="divTienediscapacidadCualNuevo" >
									<label id="labelTienediscapacidadCualNuevo" for="message-text" class="form-control-label" style="visibility: hidden;">Cual <i style="color: darkorange">*</i></label>
								</div>


								<div class="form-group col-md-12 mb-1">
									<h5 class="text-center mb-4">AUTONOMIA </h5>
								</div>

								<div class="form-group col-md-4">
									<label for="message-text" class="form-control-label">Se desplaza por sí solo <i style="color: darkorange">*</i></label>
									<?php 
									$selectAutonomia = array(
										0 => 'Si',
										1 => 'No'
									);
									?>
									<select class="form-control" name="autonomiaDesplaza" required="true">
										<option value="">Selecciona una opción</option>
										<?php
										foreach ($selectAutonomia as $key) {
											echo '<option value='.$key.'>'.$key.'</option>';
										}
										?>
									</select>
								</div>
								<div class="form-group col-md-4">
									<label for="message-text" class="form-control-label">Permanece sentado <i style="color: darkorange">*</i></label>
									<select class="form-control" name="autonomiaSentado" required="true">
										<option value="">Selecciona una opción</option>
										<?php
										foreach ($selectAutonomia as $key) {
											echo '<option value='.$key.'>'.$key.'</option>';
										}
										?>
									</select>
								</div>
								<div class="form-group col-md-4">
									<label for="message-text" class="form-control-label">Permanece en cama <i style="color: darkorange">*</i></label>
									<select class="form-control" name="autonomiaSentado" required="true">
										<option value="">Selecciona una opción</option>
										<?php
										foreach ($selectAutonomia as $key) {
											echo '<option value='.$key.'>'.$key.'</option>';
										}
										?>
									</select>
								</div>

								<div class="form-group col-md-12 mb-1">
									<h5 class="text-center mb-4">ACTIVIDADES DE AUTO CUIDADO </h5>
								</div>
								<div class="form-group col-md-4">
									<label for="message-text" class="form-control-label">Higiene<i style="color: darkorange">*</i></label>
									<?php 
									$selectAutoCuidado = array(
										0 => 'Independiente',
										1 => 'Semidependiente',
										2 => 'Dependiente'
									);
									?>
									<select class="form-control" name="autocuidadoHigiene" required="true">
										<option value="">Selecciona una opción</option>
										<?php
										foreach ($selectAutoCuidado as $key) {
											echo '<option value='.$key.'>'.$key.'</option>';
										}
										?>
									</select>
								</div>
								<div class="form-group col-md-4">
									<label for="message-text" class="form-control-label">Vestido<i style="color: darkorange">*</i></label>
									<select class="form-control" name="autocuidadoVestido" required="true">
										<option value="">Selecciona una opción</option>
										<?php
										foreach ($selectAutoCuidado as $key) {
											echo '<option value='.$key.'>'.$key.'</option>';
										}
										?>
									</select>
								</div>
								<div class="form-group col-md-4">
									<label for="message-text" class="form-control-label">Alimentación<i style="color: darkorange">*</i></label>
									<select class="form-control" name="autocuidadoAlimentacion" required="true">
										<option value="">Selecciona una opción</option>
										<?php
										foreach ($selectAutoCuidado as $key) {
											echo '<option value='.$key.'>'.$key.'</option>';
										}
										?>
									</select>
								</div>
								<div class="form-group col-md-12 mb-1">
									<h5 class="text-center mb-4">CONTROL DE ESFINTER </h5>
								</div>
								<div class="form-group col-md-6">
									<label for="recipient-name" class="form-control-label" >Vesical<i style="color: darkorange">*</i></label>
									<?php 
									$selectControlEsfinter = array(
										0 => 'Si',
										1 => 'No'
									);
									?>
									<select class="form-control" name="controlEsfinterVesical"required="true" >
										<option value="">Selecciona una opción</option>
										<?php
										foreach ($selectControlEsfinter as $key) {
											echo '<option value='.$key.'>'.$key.'</option>';
										}
										?>
									</select>
								</div>
								<div class="form-group col-md-6">
									<label for="recipient-name" class="form-control-label" >Anal<i style="color: darkorange">*</i></label>
									<select class="form-control" name="controlEsfinterAnal" required="true">
										<option value="">Selecciona una opción</option>
										<?php
										foreach ($selectControlEsfinter as $key) {
											echo '<option value='.$key.'>'.$key.'</option>';
										}
										?>
									</select>
								</div>
								<div class="form-group col-md-4"></div>
								<div class="form-group col-md-12 mb-1">
									<h5 class="text-center mb-4">COMUNICACIÓN</h5>
								</div>
								<div class="form-group col-md-12 text-center">
									<div class="custom-control custom-checkbox custom-control-inline">
										<input type="checkbox" class="custom-control-input" id="chkOralNuevo" value="Oral" >
										<label class="custom-control-label" for="chkOralNuevo">Oral</label>
									</div>
									<!-- Default inline 2-->
									<div class="custom-control custom-checkbox custom-control-inline">
										<input type="checkbox" class="custom-control-input" id="chkEscritaNeuvo" value="Escrita" >
										<label class="custom-control-label" for="chkEscritaNeuvo">Escrita</label>
									</div>
									<!-- Default inline 3-->
									<div class="custom-control custom-checkbox custom-control-inline">
										<input type="checkbox" class="custom-control-input" id="chkGestualNuevo" value="Gestual" >
										<label class="custom-control-label" for="chkGestualNuevo">Gestual</label>
									</div>
									<!-- Default inline 4-->
									<div class="custom-control custom-checkbox custom-control-inline">
										<input type="checkbox" class="custom-control-input" id="chkOtraNuevo" value="Otra" >
										<label class="custom-control-label" for="chkOtraNuevo">Otra</label>
									</div>
								</div>
								<div id="divComunicacionOtroCualNuevo" class="form-group col-md-12" style="visibility: hidden;">
								</div>
								<div class="form-group col-md-12 mb-1" >
									<h5 class="text-center mb-4">FORMACIÓN ACADÉMICA</h5>
								</div>
								<div class="form-group col-md-12" >
									<table class="table table-bordered">
										<thead>
											<tr>
												<th scope="col" class="text-center">Nivel Educativo</th>
												<th scope="col" class="text-center">Último Grado Cursado</th>
												<th scope="col" class="text-center">Observaciones</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<th scope="row" class="text-center"></th>
												<td align="center">
													<div class="custom-control custom-checkbox">
														<input type="checkbox" class="custom-control-input" id="DesescolarizadoNuevo" >
														<label class="custom-control-label" for="DesescolarizadoNuevo">Desescolarizado</label>
													</div>
												</td>
												<td><input type="text" name="desescolarizadoObervacion" class="form-control" ></td>
											</tr>
											<tr>
												<th scope="row" class="text-center">Primaria</th>
												<td><input type="text" name="primariaUltimoGradoCursado" class="form-control"  ></td>
												<td><input type="text" name="primariaObservacion" class="form-control"  ></td>
											</tr>
											<tr>
												<th scope="row" class="text-center">Secudaria</th>
												<td><input type="text" name="SecudariaUltimoGradoCursado" class="form-control" ></td>
												<td><input type="text" name="SecudariaObservacion" class="form-control"  ></td>
											</tr>
											<tr>
												<th scope="row" class="text-center">Técnico</th>
												<td><input type="text" name="TecnicoUltimoGradoCursado" class="form-control" ></td>
												<td><input type="text" name="TecnicoObservacion" class="form-control"></td>
											</tr>
											<tr>
												<th scope="row" class="text-center">Tecnólogo</th>
												<td><input type="text" name="TecnologoUltimoGradoCursado" class="form-control" ></td>
												<td><input type="text" name="TecnologoObservacion" class="form-control" ></td>
											</tr>
											<tr>
												<th scope="row" class="text-center">Superior</th>
												<td><input type="text" name="SuperiorUltimoGradoCursado" class="form-control" ></td>
												<td><input type="text" name="SuperiorObservacion" class="form-control" ></td>
											</tr>
											<tr>
												<th scope="row" class="text-center">Otro</th>
												<td><input type="text" name="OtroUltimoGradoCursado" class="form-control" ></td>
												<td><input type="text" name="OtroObservacion" class="form-control" ></td>
											</tr>
										</tbody>
									</table>
								</div>
								<div class="form-group col-md-12 mb-1" >
									<h5 class="text-center mb-4">COORDINACIÓN MOTORA GRUESA</h5>
								</div>
								<div class="form-group col-md-12" >
									<?php 
									$selectCordinacion = array(
										0 => 'Se desplaza sola',
										1 => 'Silla de ruedas',
										2 => 'Caminador',
										3 => 'Baston',
										4 => 'Muletas',
										5 => 'Con la ayuda de otra persona'
									);
									?>
									<label for="recipient-name" class="form-control-label">Se desplaza con la ayuda de <i style="color: darkorange">*</i></label>
									<select class="form-control" name="coordinacionSeDesplazaConAyuda" required="true">
										<option value="">Selecciona una opción</option>
										<?php
										foreach ($selectCordinacion as $key) {
											echo '<option value='.$key.'>'.$key.'</option>';
										}
										?>
									</select>
								</div>
								<div class="form-group col-md-12 mb-1" >
									<h5 class="text-center mb-4">COORDINACIÓN MOTORA FINA</h5>
								</div>
								<div class="form-group col-md-4" >
									<?php 
									$selectCordinacionDominancia = array(
										0 => 'Diestra',
										1 => 'Izquierda'
									);
									?>
									<label for="recipient-name" class="form-control-label">Dominancia <i style="color: darkorange">*</i></label>
									<select class="form-control" name="coordinacionDominancia" required="true">
										<option value="">Selecciona una opción</option>
										<?php
										foreach ($selectCordinacionDominancia as $key) {
											echo '<option value='.$key.'>'.$key.'</option>';
										}
										?>
									</select>
								</div>
								<div class="form-group col-md-4" >
									<?php 
									$selectCordinacionAgarreManos = array(
										0 => 'Llena',
										1 => 'Cilindrico',
										2 => 'Pinza Fina',
										3 => 'Pinza Trípode'
									);
									?>
									<label for="recipient-name" class="form-control-label">Hace agarres mano <i style="color: darkorange">*</i></label>
									<select class="form-control" name="coordinacionAgarreManos" required="true">
										<option value="">Selecciona una opción</option>
										<?php
										foreach ($selectCordinacionAgarreManos as $key) {
											echo '<option value='.$key.'>'.$key.'</option>';
										}
										?>
									</select>
								</div>
								<div class="form-group col-md-4" >
									<?php 
									$selectCordinacionNivelesFlexion = array(
										0 => 'Total',
										1 => 'Parcial',
										2 => 'Pinza Fina',
										3 => 'Nula'
									);
									?>
									<label for="recipient-name" class="form-control-label">Niveles de flexión y extensión <i style="color: darkorange">*</i></label>
									<select class="form-control" name="coordinacionAgarreManos" required="true">
										<option value="">Selecciona una opción</option>
										<?php
										foreach ($selectCordinacionNivelesFlexion as $key) {
											echo '<option value='.$key.'>'.$key.'</option>';
										}
										?>
									</select>
								</div>
								<div class="form-group col-md-12">
									<label for="message-text" class="form-control-label"> Movilidad miembros superiores <i style="color: darkorange">*</i></label>
									<textarea class="form-control" name="movilidadMiembrosSuperiores" cols="40" rows="5" required="true"></textarea>
								</div>
								<div class="form-group col-md-12">
									<label for="message-text" class="form-control-label"> Historia laboral <i style="color: darkorange">*</i></label>
									<textarea class="form-control" name="historialaboral" cols="40" rows="5"required="true" ></textarea>
								</div>
								<div class="form-group col-md-12">
									<label for="message-text" class="form-control-label"> Actividades ocupacionales en las que participa <i style="color: darkorange">*</i></label>
									<textarea class="form-control" name="actividadesOcupacionalesParticipa" cols="40" rows="5" required="true"></textarea>
								</div>
								<div class="form-group col-md-12">
									<label for="message-text" class="form-control-label"> Observaciones y recomendaciones <i style="color: darkorange">*</i></label>
									<textarea class="form-control" name="observacionesRecomendaciones" cols="40" rows="5" required="true"></textarea>
								</div>
								<div class="form-group col-md-12" >
									<?php 
									$selectAreaOcupacional = array(
										0 => 'Traperas',
										1 => 'Bolsas',
										2 => 'Limpiones',
										3 => 'Jardineria',
										4 => 'Domesticas',
										5 => 'Otras'
									);
									?>
									<label for="recipient-name" class="form-control-label">Area ocupacional a la cual se remite<i style="color: darkorange">*</i></label>
									<select class="form-control" name="areaOcupacional" required="true">
										<option value="">Selecciona una opción</option>
										<?php
										foreach ($selectAreaOcupacional as $key) {
											echo '<option value='.$key.'>'.$key.'</option>';
										}
										?>
									</select>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">
									Cancelar
								</button>
								<button type="submit" class="btn btn-info">
									Registrar
								</button>
							</div>
						</form>
					</div>
				</div>
			</div><!-- End of Modal body -->

			

		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->
<!-- Modal Registrar Terapia Ocupacional Seguimiento-->
<div id="modalNuevoTerapiaOcupacionalSeguimiento" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<h4 class="modal-title text-center">REGISTRAR NUEVO SEGUIMIENTO</h4>
			</div>
			<div class="modal-body">
				<div class="content">
					<form id="registrarTerapiaOcupacionalSeguimiento">
						<div class="row">
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Fecha <i style="color: darkorange">*</i></label>
								<input type="date" class="form-control" name="fechaMod" required="true"/>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Tipo de seguimiento <i style="color: darkorange">*</i></label>
								<?php 
								$selectTieneDiscapacidad = array(
									0 => 'Control',
									1 => 'Evaluacion',
									2 => 'Plan de Tratamiento'
								);
								?>
								<select  class="form-control" name="tipoSeguimiento" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									foreach ($selectTieneDiscapacidad as $key) {
										echo '<option value='.$key.'>'.$key.'</option>';
									}
									?>
								</select>
							</div>
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Descripcion de el seguimiento <i style="color: darkorange">*</i></label>
								<textarea class="form-control" name="descripcionSeguimiento" cols="40" rows="5" required="true"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								Cancelar
							</button>
							<button type="submit" class="btn btn-info">
								Registrar
							</button>
						</div>
					</form>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->

<script type="text/javascript">
	function tieneDiscapacidadCual(){
		var selectValue = document.getElementById('selectTieneDiscapacidadNuevo').value;
		var div = document.getElementById('divTienediscapacidadCualNuevo');
		var label = document.getElementById('labelTienediscapacidadCualNuevo');
		if(selectValue == "Si"){
			if(document.getElementById('inputTieneDispacadiadCualNuevo')){
				div.style.visibility = "visible";
				input = document.getElementById('inputTieneDispacadiadCualNuevo');
				input.value = "";
				label.style.visibility = "visible";
			}else{
				div.style.visibility = "visible";
				label.style.visibility = "visible";
				var input  = document.createElement("input");
				input.setAttribute("id", "inputTieneDispacadiadCualNuevo");
				input.setAttribute("class", "form-control");
				input.setAttribute("type", "text");
				input.setAttribute("name", "TipoDiscapacidadCualNuevo");
				document.getElementById("divTienediscapacidadCualNuevo").appendChild(input);
			}
			
		}else{
			if(document.getElementById('inputTieneDispacadiadCualNuevo')){
				div.style.visibility = "hidden";
				label.style.visibility = "hidden";
			}
		}
	}
	$("#chkOtraNuevo").change(function (){
		var divOtro = document.getElementById('divComunicacionOtroCualNuevo');
		if($(this).prop('checked') == true){
			if(document.getElementById('inputComunicacionOtroCualNuevo')){
				divOtro.style.visibility = "visible";
			}else{
				divOtro.style.visibility = "visible";
				var input  = document.createElement("input");

				input.setAttribute("id", "inputComunicacionOtroCualNuevo");
				input.setAttribute("class", "form-control");
				input.setAttribute("type", "text");
				input.setAttribute("name", "comunicacionOtraCualNuevo");

				document.getElementById("divComunicacionOtroCualNuevo").appendChild(input);
			}
		}else{
			if(document.getElementById('inputComunicacionOtroCualNuevo')){
				divOtro.style.visibility = "hidden";
			}
		}
	});
</script>