<!-- Modal Registrar Gerontologia ABC Instrumental-->
<div id="modalNuevaGerontologiaAbcIntrumental" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<h4 class="modal-title text-center">REGISTRAR NUEVO ABC INSTRUMENTAL</h4>
			</div>
			<div class="modal-body">
				<div class="content">
					<form id="registrarGerontologiaAbcInstrumental">
						<div class="row">
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Fecha <i style="color: darkorange">*</i></label>
								<input type="date" class="form-control" name="fechaMod" required="true" />
							</div>
							<div class="form-group col-md-12">
								<label for="recipient-name" class="form-control-label" >¿Usa el teléfono?<i style="color: darkorange">*</i></label>
								<?php 
								$select = array(
									0 => 'Si',
									1 => 'No'
								);
								?>
								<select class="form-control" name="usaTelefono" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									foreach ($select as $key) {
										echo '<option value='.$key.'>'.$key.'</option>';
									}
									?>
								</select>
							</div>
							<div class="form-group col-md-12">
								<label for="recipient-name" class="form-control-label" >¿Maneja llaves?<i style="color: darkorange">*</i></label>
								<select class="form-control" name="majeaLlaves" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									foreach ($select as $key) {
										echo '<option value='.$key.'>'.$key.'</option>';
									}
									?>
								</select>
							</div>
							<div class="form-group col-md-12">
								<label for="recipient-name" class="form-control-label" >¿Abre y cierra puertas?<i style="color: darkorange">*</i></label>
								<select class="form-control" name="abreCierraPuertas" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									foreach ($select as $key) {
										echo '<option value='.$key.'>'.$key.'</option>';
									}
									?>
								</select>
							</div>
							<div class="form-group col-md-12">
								<label for="recipient-name" class="form-control-label" >¿Abre paquetes?<i style="color: darkorange">*</i></label>
								<select class="form-control" name="abrePaquetes" required="true">
									<option value="">Selecciona una opción</option>
									<?php 
									foreach ($select as $key) {
										echo '<option value='.$key.'>'.$key.'</option>';
									}
									?>
								</select>
							</div>
							<div class="form-group col-md-12">
								<label for="recipient-name" class="form-control-label" >¿Usa radio y televisión?<i style="color: darkorange">*</i></label>
								<select class="form-control" name="usaRadioTelevision" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									foreach ($select as $key) {
										echo '<option value='.$key.'>'.$key.'</option>';
									}
									?>
								</select>
							</div>
							<div class="form-group col-md-12">
								<label for="recipient-name" class="form-control-label" >¿Maneja dinero propio?<i style="color: darkorange">*</i></label>
								<select class="form-control" name="manejaDineroPropio" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									foreach ($select as $key) {
										echo '<option value='.$key.'>'.$key.'</option>';
									}
									?>
								</select>
							</div>
							<div class="form-group col-md-12">
								<label for="recipient-name" class="form-control-label" >Observación<i style="color: darkorange">*</i></label>
								<textarea class="form-control" name="observacion" cols="40" rows="5" required="true"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								Cancelar
							</button>
							<button type="submit" class="btn btn-info">
								Registrar
							</button>
						</div>
					</form>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->

<!-- Modal Registrar Gerontologia Actitud Personal-->
<div id="modalNuevaGerontologiaActitudPersonal" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<h4 class="modal-title text-center">REGISTRAR NUEVA ACTITUD PERSONAL</h4>
			</div>
			<div class="modal-body">
				<div class="content">
					<form id="registrarGerontologiActitudPersonal">
						<div class="row">
							<div class="form-group col-md-4">
								<label for="message-text" class="form-control-label">Fecha <i style="color: darkorange">*</i></label>
								<input type="date" class="form-control" name="fechaMod" required="true"/>
							</div>
							<div class="form-group col-md-4">
								<label for="recipient-name" class="form-control-label" >Autonomía<i style="color: darkorange">*</i></label>
								<?php 
								$selectAutonomia = array(
									0 => 'Total',
									1 => 'Parcial',
									2 => 'Dependiente'
								);
								?>
								<select class="form-control" name="autonomia" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									foreach ($selectAutonomia as $key) {
										echo '<option value='.$key.'>'.$key.'</option>';
									}
									?>
								</select>
							</div>
							<div class="form-group col-md-4">
								<label for="recipient-name" class="form-control-label" >Autoestima<i style="color: darkorange">*</i></label>
								<?php 
								$selectAutoestimaAutoconceptoAutovaloración = array(
									0 => 'Alta',
									1 => 'Moderada',
									2 => 'Escasa',
									3 => 'No posee',
									4 => 'No aplica'
								);
								?>
								<select class="form-control" name="autoestima" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									foreach ($selectAutoestimaAutoconceptoAutovaloración as $key) {
										echo '<option value='.$key.'>'.$key.'</option>';
									}
									?>
								</select>
							</div>
							<div class="form-group col-md-4">
								<label for="recipient-name" class="form-control-label" >Autoconcepto<i style="color: darkorange">*</i></label>
								<select class="form-control" name="autoconcepto" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									foreach ($selectAutoestimaAutoconceptoAutovaloración as $key) {
										echo '<option value='.$key.'>'.$key.'</option>';
									}
									?>
								</select>
							</div>
							<div class="form-group col-md-4">
								<label for="recipient-name" class="form-control-label" >Autovaloración<i style="color: darkorange">*</i></label>
								<select class="form-control" name="autovaloracion" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									foreach ($selectAutoestimaAutoconceptoAutovaloración as $key) {
										echo '<option value='.$key.'>'.$key.'</option>';
									}
									?>
								</select>
							</div>
							<div class="form-group col-md-4">
								<label for="recipient-name" class="form-control-label" >Estado de ánimo<i style="color: darkorange">*</i></label>
								<?php 
								$selectEstadoAnimo = array(
									0 => 'Dinamico',
									1 => 'Participativo',
									2 => 'Alegre',
									3 => 'Atento',
									4 => 'Apatico',
									5 => 'Agresivo',
									6 => 'Indiferente'
								);
								?>
								<select class="form-control" name="estadoDeAnimo" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									foreach ($selectEstadoAnimo as $key) {
										echo '<option value='.$key.'>'.$key.'</option>';
									}
									?>
								</select>
							</div>
							<div class="form-group col-md-4">
								<label for="recipient-name" class="form-control-label" >Cuidado personal<i style="color: darkorange">*</i></label>
								<?php 
								$selectCuidadoPersonal = array(
									0 => 'Excelente',
									1 => 'Bueno',
									2 => 'Aceptable',
									3 => 'Deficiente'
								);
								?>
								<select class="form-control" name="cuidadoPersonal" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									foreach ($selectCuidadoPersonal as $key) {
										echo '<option value='.$key.'>'.$key.'</option>';
									}
									?>
								</select>
							</div>
							<div class="form-group col-md-4">
								<label for="recipient-name" class="form-control-label" >Actitud ante la vejez<i style="color: darkorange">*</i></label>
								<?php 
								$selectActitudVejez = array(
									0 => 'Positiva',
									1 => 'Negativa',
									2 => 'Indiferente',
									3 => 'No aplica'
								);
								?>
								<select class="form-control" name="actitudVejez" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									foreach ($selectActitudVejez as $key) {
										echo '<option value='.$key.'>'.$key.'</option>';
									}
									?>
								</select>
							</div>
							<div class="form-group col-md-4">
								<label for="recipient-name" class="form-control-label" >relaciones familiares<i style="color: darkorange">*</i></label>
								<?php 
								$selectRelacionesFamiliares = array(
									0 => 'Afectiva',
									1 => 'Regular',
									2 => 'Deficiente',
									3 => 'Sin familia',
									4 => 'Buena'
								);
								?>
								<select class="form-control" name="relacionesFamiliares" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									foreach ($selectRelacionesFamiliares as $key) {
										echo '<option value='.$key.'>'.$key.'</option>';
									}
									?>
								</select>
							</div>
							<div class="form-group col-md-4">
								<label for="recipient-name" class="form-control-label" >relaciones con los compañeros<i style="color: darkorange">*</i></label>
								<?php 
								$selectRelacionesCompaneros = array(
									0 => 'Armoniosa',
									1 => 'Regular',
									2 => 'Deficiente',
									3 => 'Discordante',
									4 => 'Buena'
								);
								?>
								<select class="form-control" name="relacionesCompaneros" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									foreach ($selectRelacionesCompaneros as $key) {
										echo '<option value='.$key.'>'.$key.'</option>';
									}
									?>
								</select>
							</div>
							<div class="form-group col-md-4">
								<label for="recipient-name" class="form-control-label" >relaciones con los funcionarios<i style="color: darkorange">*</i></label>
								<?php 
								$selectRelacionesFuncionarios = array(
									0 => 'Buena',
									1 => 'Regular',
									2 => 'Mala'
								);
								?>
								<select class="form-control" name="relacionesFuncionarios" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									foreach ($selectRelacionesFuncionarios as $key) {
										echo '<option value='.$key.'>'.$key.'</option>';

									}
									?>
								</select>
							</div>
							<div class="form-group col-md-12">
								<label for="recipient-name" class="form-control-label" >Empleo del tiempo libre<i style="color: darkorange">*</i></label>
								<textarea class="form-control" name="tiempoLibre" cols="40" rows="5" required="true"></textarea>
							</div>
							<div class="form-group col-md-12">
								<label for="recipient-name" class="form-control-label" >Observacionese<i style="color: darkorange">*</i></label>
								<textarea class="form-control" name="observaciones" cols="40" rows="5" required="true"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								Cancelar
							</button>
							<button type="submit" class="btn btn-info" >
								Registrar
							</button>
						</div>
					</form>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->

<!-- Modal Registrar Gerontologia Aditamentos-->
<div id="modalNuevaGerontologiaAditamentos" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<h4 class="modal-title text-center">REGISTRAR NUEVO ADITAMENTO</h4>
			</div>
			<div class="modal-body">
				<div class="content">
					<form id="registrarGerontologiAditamentos">
						<div class="row">
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Fecha <i style="color: darkorange">*</i></label>
								<input type="date" class="form-control" name="fechaMod" required="true"/>
							</div>
							<div class="form-group col-md-12">
								<label for="recipient-name" class="form-control-label" >Aditamento<i style="color: darkorange">*</i></label>
								<select class="form-control" name="usuario" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									$queryAditamento = mysqli_query($conn, "SELECT id_aditamento,aditamento FROM gddt_aditamentos;");
									while ($fetchAditamento = mysqli_fetch_row($queryAditamento)) {
										echo "<option value=" . $fetchAditamento[0] . ">" . $fetchAditamento[1] . "</option>";
									}
									?>
								</select>
							</div>
							<div class="form-group col-md-12">
								<label for="recipient-name" class="form-control-label" >Posesión del aditamento<i style="color: darkorange">*</i></label>
								<?php 
								$selectPosesionAditamento = array(
									0 => 'En prestamo',
									1 => 'Propio'
								);
								?>
								<select class="form-control" name="autoestima" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									foreach ($selectPosesionAditamento as $key) {
										echo '<option value='.$key.'>'.$key.'</option>';
									}
									?>
								</select>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								Cancelar
							</button>
							<button type="submit" class="btn btn-info">
								Registrar
							</button>
						</div>
					</form>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->

<!-- Modal Registrar Gerontologia Actividades que realzia-->
<div id="modalNuevaGerontologiaActividadesRealiza" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<h4 class="modal-title text-center">REGISTRAR NUEVA ACTIVIDAD QUE REALIZA</h4>
			</div>
			<div class="modal-body">
				<div class="content">
					<form id="registrarGerontologiActividadRealiza">
						<div class="row">
							<div class="form-group col-md-4">
								<label for="message-text" class="form-control-label">Fecha de modificacion <i style="color: darkorange">*</i></label>
								<input type="date" class="form-control" name="fechaMod" required="true"/>
							</div>
							<div class="form-group col-md-4">
								<label for="recipient-name" class="form-control-label" >Juegos de mesa<i style="color: darkorange">*</i></label>
								<?php 
								$select = array(
									0 => 'Si',
									1 => 'No'
								);
								?>
								<select class="form-control" name="juegoMesa" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									foreach ($select as $key) {
										echo '<option value='.$key.'>'.$key.'</option>';
									}
									?>
								</select>
							</div>
							<div class="form-group col-md-4">
								<label for="recipient-name" class="form-control-label" >Fiestas sociales<i style="color: darkorange">*</i></label>
								<select class="form-control" name="fiestasSociales" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									foreach ($select as $key) {
										echo '<option value='.$key.'>'.$key.'</option>';
									}
									?>
								</select>
							</div>
							<div class="form-group col-md-4">
								<label for="recipient-name" class="form-control-label" >Lectura<i style="color: darkorange">*</i></label>
								<select class="form-control" name="lectura"required="true" >
									<option value="">Selecciona una opción</option>
									<?php
									foreach ($select as $key) {
										echo '<option value='.$key.'>'.$key.'</option>';
									}
									?>
								</select>
							</div>
							<div class="form-group col-md-4">
								<label for="recipient-name" class="form-control-label" >Escritura<i style="color: darkorange">*</i></label>
								<select class="form-control" name="escritura" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									foreach ($select as $key) {
										echo '<option value='.$key.'>'.$key.'</option>';
									}
									?>
								</select>
							</div>
							<div class="form-group col-md-4">
								<label for="recipient-name" class="form-control-label" >Manualidades<i style="color: darkorange">*</i></label>
								<select class="form-control" name="manualidades" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									foreach ($select as $key) {
										echo '<option value='.$key.'>'.$key.'</option>';
									}
									?>
								</select>
							</div>
							<div class="form-group col-md-4">
								<label for="recipient-name" class="form-control-label" >Pintura<i style="color: darkorange">*</i></label>
								<select class="form-control" name="pintura" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									foreach ($select as $key) {
										echo '<option value='.$key.'>'.$key.'</option>';
									}
									?>
								</select>
							</div>
							<div class="form-group col-md-4">
								<label for="recipient-name" class="form-control-label" >Música<i style="color: darkorange">*</i></label>
								<select class="form-control" name="musica" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									foreach ($select as $key) {
										echo '<option value='.$key.'>'.$key.'</option>';
									}
									?>
								</select>
							</div>
							<div class="form-group col-md-4">
								<label for="recipient-name" class="form-control-label" >Gimnasia<i style="color: darkorange">*</i></label>
								<select class="form-control" name="gimnasia" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									foreach ($select as $key) {
										echo '<option value='.$key.'>'.$key.'</option>';
									}
									?>
								</select>
							</div>
							<div class="form-group col-md-4">
								<label for="recipient-name" class="form-control-label" >Talleres dirigidos<i style="color: darkorange">*</i></label>
								<select class="form-control" name="talleresDirigidos" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									foreach ($select as $key) {
										echo '<option value='.$key.'>'.$key.'</option>';
									}
									?>
								</select>
							</div>
							<div class="form-group col-md-4">
								<label for="recipient-name" class="form-control-label" >Ver televisión<i style="color: darkorange">*</i></label>
								<select class="form-control" name="verTelevision" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									foreach ($select as $key) {
										echo '<option value='.$key.'>'.$key.'</option>';
									}
									?>
								</select>
							</div>
							<div class="form-group col-md-12">
								<label for="recipient-name" class="form-control-label" >Otra<i style="color: darkorange">*</i></label>
								<input type="text" class="form-control" name="otra" required="true"/>
							</div>
							<div class="form-group col-md-12">
								<label for="recipient-name" class="form-control-label" >Observación<i style="color: darkorange">*</i></label>
								<textarea class="form-control" name="observacion" cols="40" rows="5" required="true"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								Cancelar
							</button>
							<button type="submit" class="btn btn-info">
								Registrar
							</button>
						</div>
					</form>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->

<!-- Modal Registrar Gerontologia Seguimiento-->
<div id="modalNuevaGerontologiaSeguimiento" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<h4 class="modal-title text-center">REGISTRAR NUEVO SEGUIMIENTO</h4>
			</div>
			<div class="modal-body">
				<div class="content">
					<form id="registrarGerontologiSeguimiento">
						<div class="row">
							<div class="form-group col-md-12">
								<label for="message-text" class="form-control-label">Fecha <i style="color: darkorange">*</i></label>
								<input type="date" class="form-control" name="fechaMod" required="true"/>
							</div>
							<div class="form-group col-md-12">
								<label for="recipient-name" class="form-control-label" >Tipo<i style="color: darkorange">*</i></label>
								<select class="form-control" name="tipo" required="true">
									<option value="">Selecciona una opción</option>
									<?php
									$queryTipo = mysqli_query($conn, "SELECT (CONVERT(CAST(CONVERT(seguimiento_tipo USING latin1) AS BINARY) USING utf8))AS seguimiento_tipo
										FROM gddt_seguimiento_gerontologico WHERE seguimiento_tipo <> '' GROUP BY seguimiento_tipo;");
									while ($fetchTipo  = mysqli_fetch_row($queryTipo)) {
										echo "<option value=" . $fetchTipo[0] . ">" . $fetchTipo[0] . "</option>";
									}
									?>
								</select>
							</div>
							<div class="form-group col-md-12">
								<label for="recipient-name" class="form-control-label" >Descripcion<i style="color: darkorange">*</i></label>
								<textarea class="form-control" name="descripcion" cols="40" rows="5" required="true"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								Cancelar
							</button>
							<button type="submit" class="btn btn-info">
								Registrar
							</button>
						</div>
					</form>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->

<!-- Modal Registrar Gerontologia Calidad de vida-->
<div id="modalNuevaGerontologiaCalidadVida" class="modal fade bd-example"  role="dialog"aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-xl">
		<div class="modal-content">
			<div class="modal-header header-modal-sany" style="background-color: #138496">
				<h4 class="modal-title text-center">REGISTRAR NUEVA CALIDAD DE VIDA</h4>
			</div>
			<div class="modal-body">
				<div class="content">
					<form id="registrarGerontologiaCalidadVida">
						<div class="row">
							<?php 
							$preguntas = array(
								0 =>  '1. Realizo las actividades físicas con facilidad (ejercicios físicos, subir y bajar escaleras, agacharse y levantarse)',
								1 =>  '2. Puedo salir de compras normalmente',
								2 =>  '3. Mi familia me quiere y me respeta',
								3 =>  '4. Mi estado de ánimo es favorable',
								4 =>  '5. Me siento confiado y seguro frente al futuro',
								5 =>  '6. Con la jubilación mi vida perdió sentido',
								6 =>  '7. Mi estado de salud me permite realizar por mi mismo las actividades de la vida diaria (Bañarme, comer, limpiar, cocinar, lavar)',
								7 =>  '8. Puedo leer frecuentemente libros, revista o periódicos',
								8 =>  '9. Soy importante para mi familia.',
								9 => '10. Me siento solo y desamparado en la vida',
								10 => '11. Mi vida es aburrida y monótona',
								11 => '12. Camino con ayuda de alguien o de algo',
								12 => '13. Tengo facilidad para aprender nuevas cosas',
								13 => '14. Utilizo el transporte público con facilidad',
								14 => '15. Mi situación monetaria me permite resolver todas mis necesidades, de cualquier índole',
								15 => '16. Mi familia me ayuda a resolver los problemas que se me puedan presentar',
								16 => '17. He logrado realizar en mi vida mis aspiraciones',
								17 => '18. Estoy satisfecho con las condiciones económicas que tengo',
								18 => '19. Soy capaz de atenderme a mí mismo y cuidar de mi persona',
								19 => '20. Estoy nervioso e inquieto',
								20 => '21. Puedo ayudar en el cuidado y atención de mis nietos u otros niños que vivan en el hogar',
								21 => '22. Puedo expresar a mi familia lo que pienso y siento',
								22 => '23. Mis creencias me dan seguridad en el futuro',
								23 => '24. Mantengo relaciones con mis amigos y vecinos',
								24 => '25. Soy feliz con la familia que tengo',
								25 => '26.  Salgo a distraerme con frecuencia con mi familia y amigos',
								26 => '27.  Mi vivienda tiene buenas condiciones para vivir yo en ella',
								27 => '28. He pensado en quitarme la vida',
								28 => '29. Mi familia me tiene en cuenta para tomar decisiones en los problemas de hogar',
								29 => '30. Considero que todavía puedo ser una persona útil',
								30 => '31.Mi vivienda me resulta cómoda para mis necesidades',
								31 => '32. Mi estado de salud me permite disfrutar de la vida',
								32 => '33. Tengo aspiraciones y planes para el futuro',
								33 => '34. Soy feliz con la vida que llevo'
							);
							$cont = 1;
							foreach ($preguntas as $key) {
								echo '<div class="form-group col-md-6">';
								echo '<label for="recipient-name" class="form-control-label" >'.$key.'<i style="color: darkorange">*</i></label>';
								echo '<select class="form-control" name="pregunta'.$cont.'" required="true">';
								echo '<option value="">Selecciona una opción</option>';
								echo '<option value="4"> CASI SIEMPRE</option>';
								echo '<option value="3">A MENUDO</option>';
								echo '<option value="2">ALGUNAS VECES</option>';
								echo '<option value="1">CASI NUNCA</option>';
								echo '</select>';
								echo '</div>';
								$cont++;
							}
							?>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								Cancelar
							</button>
							<button type="submit" class="btn btn-info">
								Registrar
							</button>
						</div>
					</form>
				</div>
			</div><!-- End of Modal body -->
		</div><!-- End of Modal content -->
	</div><!-- End of Modal dialog -->
</div><!-- End of Modal -->

