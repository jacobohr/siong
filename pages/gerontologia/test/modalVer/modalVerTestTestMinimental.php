<?php

//include connection file 
require "../../../../session.php";

$sqlUsuario = "
SELECT 
UPPER((CONVERT(CAST(CONVERT(CONCAT(c.primer_nombre,' ',c.segundo_nombre,' ',c.primer_apellido,' ',c.segundo_apellido) USING latin1) AS BINARY) USING utf8))) AS nombreUsuario,
CONCAT(tm.ano_mod,'/',tm.mes_mod,'/',tm.dia_mod) AS fecha, 
tm.hora_mod,
tm.p1,tm.p2,tm.p3,tm.p4,tm.p5,tm.p6,tm.p7,tm.p8,tm.p9,tm.p10,tm.p11,
(CONVERT(CAST(CONVERT(tm.observaciones_minimental USING latin1) AS BINARY) USING utf8))AS observaciones_minimental 
FROM gddt_test_minimental tm 
INNER JOIN gddt_cuentas c ON c.id_cuenta = tm.id_cuenta 
WHERE tm.id_minimental = " . $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);
$rows = mysqli_num_rows($queryUsuario);

if($rows == 0){
    echo "<p style='color:red;font-size:0.9em;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No se encontraron datos :(</p>";
}else{

    ?>

    <div class="content">
        <h6 class="text-center mb-4">Actualizado por <?= $fetchUsuario[0];?> el <?= $fetchUsuario[1]; ?> a las <?= $fetchUsuario[2]; ?></h6>
        
        <div class="user-block col-sm-12 col-xs-12"><h6><b>Orientacion</b></h6></div>
        <div class="user-block col-sm-12 col-xs-12"><h7><b>1. Temporal</b></h7></div>
        <div class="user-block col-sm-10 col-xs-10">
            <p>
                ¿Que fecha, año, mes, dia de la semana, mañana o tarde es hoy?
            </p>
        </div>
        <div class="user-block col-sm-2 col-xs-2"><?php echo $fetchUsuario[3]; ?></div>

        <div class="user-block col-sm-12 col-xs-12"><h7><b>2. Espacial</b></h7></div>
        <div class="user-block col-sm-10 col-xs-10">
            <p>
                ¿Donde estamos? Pais, departamento, ciudad, barrio, lugar.
            </p>
        </div>
        <div class="user-block col-sm-2 col-xs-2"><?php echo $fetchUsuario[4]; ?></div>
        
        <div class="user-block col-sm-12 col-xs-12"><h6><b>Fijacion o atención</b></h6></div>
        <div class="user-block col-sm-12 col-xs-12"><h7><b>3. Memoria Sensorial</b></h7></div>
        <div class="user-block col-sm-10 col-xs-10">
            <p>
                Le voy a nombrar tres objetos: "Manzana, mesa, centavo" despues de que los haya nombrado, quiero que los repita. Acuerdese de ellos, le pedire que los nombre en unos minutos. Anote un punto por cada respuesta correcta. Continúe repitiendo los nombres hasta que los haya aprendido todos. Cuente las pruebas y anote.
            </p>
        </div>
        <div class="user-block col-sm-2 col-xs-2"><?php echo $fetchUsuario[5]; ?></div>

        <div class="user-block col-sm-12 col-xs-12"><h6><b>Atención y cálculo</b></h6></div>
        <div class="user-block col-sm-12 col-xs-12"><h7><b>4. Memoria Verbal de Trabajo</b></h7></div>
        <div class="user-block col-sm-10 col-xs-10">
            <p>
                Deletree "mundo" al revés. El puntaje es el número de letras en el órden correcto.
            </p>
        </div>
        <div class="user-block col-sm-2 col-xs-2"><?php echo $fetchUsuario[6]; ?></div>

        <div class="user-block col-sm-12 col-xs-12"><h6><b>Memoria</b></h6></div>
        <div class="user-block col-sm-12 col-xs-12"><h7><b>5. Memoria a Corto Plazo</b></h7></div>
        <div class="user-block col-sm-10 col-xs-10">
            <p>
                Pida al paciente que repita los tres nombres mencionados anteriormente. Anote un punto por cada respuesta correcta. Nota: no se puede hacer la prueba de memoria si el paciente no reodó los tres obejtos durante la prueba de atención.
            </p>
        </div>
        <div class="user-block col-sm-2 col-xs-2"><?php echo $fetchUsuario[7]; ?></div>

        <div class="user-block col-sm-12 col-xs-12"><h6><b>Lenguaje y práxias</b></h6></div>
        <div class="user-block col-sm-12 col-xs-12"><h7><b>6. Lenguaje</b></h7></div>
        <div class="user-block col-sm-10 col-xs-10">
            <p>
                Identifique un "reloj" y un "lapiz".
            </p>
        </div>
        <div class="user-block col-sm-2 col-xs-2"><?php echo $fetchUsuario[8]; ?></div>

        <div class="user-block col-sm-12 col-xs-12"><h7><b>7. Lenguaje</b></h7></div>
        <div class="user-block col-sm-10 col-xs-10">
            <p>
                Repita lo siguiente: "ni si, ni no, ni peros".
            </p>
        </div>
        <div class="user-block col-sm-2 col-xs-2"><?php echo $fetchUsuario[9]; ?></div>

        <div class="user-block col-sm-12 col-xs-12"><h7><b>8. Práxis</b></h7></div>
        <div class="user-block col-sm-10 col-xs-10">
            <p> Siga un comando en tres pasos: </p>
            <p>    - Tome un papel con la mano derecha. </p>
            <p>    - Doble el papel por la mitad. </p>
            <p>    - Pongalo en el piso. </p>
        </div>
        <div class="user-block col-sm-2 col-xs-2"><?php echo $fetchUsuario[10]; ?></div>

        <div class="user-block col-sm-12 col-xs-12"><h7><b>9. Práxis</b></h7></div>
        <div class="user-block col-sm-10 col-xs-10">
            <p> Cierre los ojos. </p>
        </div>
        <div class="user-block col-sm-2 col-xs-2"><?php echo $fetchUsuario[11]; ?></div>

        <div class="user-block col-sm-12 col-xs-12"><h7><b>10. Lenguaje</b></h7></div>
        <div class="user-block col-sm-10 col-xs-10">
            <p> Escríba una oración.</p>
        </div>
        <div class="user-block col-sm-2 col-xs-2"><?php echo $fetchUsuario[12]; ?></div>

        <div class="user-block col-sm-12 col-xs-12"><h7><b>11. Práxis</b></h7></div>
        <div class="user-block col-sm-10 col-xs-10">
            <p> Copie el siguiente diseño:</p>
            <img src="<?= $base_url ?>images/test/minimental_figura.png" style="width: 160px;height: 117px;">
        </div>
        <div class="user-block col-sm-2 col-xs-2"><?php echo $fetchUsuario[13]; ?></div>

        <?php 
        $p1 = $fetchUsuario[3];
        $p2 = $fetchUsuario[4];
        $p3 = $fetchUsuario[5];
        $p4 = $fetchUsuario[6];
        $p5 = $fetchUsuario[7];
        $p6 = $fetchUsuario[8];
        $p7 = $fetchUsuario[9];
        $p8 = $fetchUsuario[10];
        $p9 = $fetchUsuario[11];
        $p10 = $fetchUsuario[12];
        $p11 = $fetchUsuario[13];
        $puntaje_minimental = $p1+$p2+$p3+$p4+$p5+$p6+$p7+$p8+$p9+$p10+$p11;
        if ($puntaje_minimental >= 24){
            $puntaje_minimental_texto = "Sin deterioro cognitivo";
        }
        if (($puntaje_minimental <= 23) && ($puntaje_minimental >= 13)){
            $puntaje_minimental_texto = "Deterioro congnitivo leve a moderado";
        }
        if ($puntaje_minimental <= 12){
            $puntaje_minimental_texto = "Deterioro cognitivo severo";
        }
        ?>
        <div class="user-block col-sm-12 col-xs-12"><h7><b>Observaciones del test</b></h7></div>
        <div class="user-block col-sm-12 col-xs-12"><p><?php if(empty($fetchUsuario[14])){echo "Sin dato";}else{echo $fetchUsuario[14];} ?></p></div>

        <div class="user-block col-sm-12 col-xs-12"><h7><b>Resultado del test</b></h7></div>
        <div class="user-block col-sm-10 col-xs-10">
            <p> Puntaje</p>
            <p><?php echo $puntaje_minimental; ?></p>
            <p> Interpretación</p>
            <p><?php echo $puntaje_minimental_texto; ?></p>
        </div>

        <div class="user-block">&nbsp;</div>

    </div><!-- Fin del content -->
    <?php } ?>