<?php

//include connection file 
require "../../../../session.php";

$sqlUsuario = "
SELECT 
UPPER((CONVERT(CAST(CONVERT(CONCAT(c.primer_nombre,' ',c.segundo_nombre,' ',c.primer_apellido,' ',c.segundo_apellido) USING latin1) AS BINARY) USING utf8))) AS nombreUsuario,
CONCAT(tb.ano_mod,'/',tb.mes_mod,'/',tb.dia_mod) AS fecha,  
tb.hora_mod,
tb.b1,tb.b2,tb.b3,tb.b4,tb.b5,tb.b6,tb.b7,tb.b8,tb.b9,tb.b10, 
(CONVERT(CAST(CONVERT(tb.observacion USING latin1) AS BINARY) USING utf8))AS observacion 
FROM gddt_test_barthel tb
INNER JOIN gddt_cuentas c ON c.id_cuenta = tb.id_cuenta 
WHERE tb.id_barthel = " . $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);
$rows = mysqli_num_rows($queryUsuario);

if($rows == 0){
    echo "<p style='color:red;font-size:0.9em;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No se encontraron datos :(</p>";
}else{

    ?>

    <div class="content">
        <h6 class="text-center mb-4">Actualizado por <?= $fetchUsuario[0];?> el <?= $fetchUsuario[1]; ?> a las <?= $fetchUsuario[2]; ?></h6>
        
        <div class="user-block col-sm-10 col-xs-10">
            <p>
                1. Alimentacion
            </p>
        </div>
        <div class="user-block col-sm-2 col-xs-2">
            <?php 
            if($fetchUsuario[3] == 10){
                echo 'Independiente';
            }else{
                if($fetchUsuario[3] == 5){
                    echo 'Necesita ayuda';
                }else{
                    echo 'Dependiente';
                }
            } ?>
        </div>


        <div class="user-block col-sm-10 col-xs-10">
            <p>
                2. Lavarse (Baño)
            </p>
        </div>
        <div class="user-block col-sm-2 col-xs-2">
            <?php 
            if($fetchUsuario[4] == 10){
                echo 'Independiente';
            }else{
                if($fetchUsuario[4] == 5){
                    echo 'Necesita ayuda';
                }else{
                    echo 'Dependiente';
                }
            } ?>
        </div>

        <div class="user-block col-sm-10 col-xs-10">
            <p>3. Vestirse</p>
        </div>
        <div class="user-block col-sm-2 col-xs-2">
            <?php 
            if($fetchUsuario[5] == 10){
                echo 'Independiente';
            }else{
                if($fetchUsuario[5] == 5){
                    echo 'Necesita ayuda';
                }else{
                    echo 'Dependiente';
                }
            } ?>
        </div>

        <div class="user-block col-sm-10 col-xs-10">
            <p>4. Arreglarse</p>
        </div>
        <div class="user-block col-sm-2 col-xs-2">
            <?php 
            if($fetchUsuario[6] == 10){
                echo 'Independiente';
            }else{
                if($fetchUsuario[6] == 5){
                    echo 'Necesita ayuda';
                }else{
                    echo 'Dependiente';
                }
            } ?>
        </div>

        <div class="user-block col-sm-10 col-xs-10">
            <p>5. Deposición</p>
        </div>
        <div class="user-block col-sm-2 col-xs-2">
            <?php 
            if($fetchUsuario[7] == 10){
                echo 'Independiente';
            }else{
                if($fetchUsuario[7] == 5){
                    echo 'Necesita ayuda';
                }else{
                    echo 'Dependiente';
                }
            } ?>
        </div>

        <div class="user-block col-sm-10 col-xs-10">
            <p> 6. Micción</p>
        </div>
        <div class="user-block col-sm-2 col-xs-2">
            <?php 
            if($fetchUsuario[8] == 10){
                echo 'Independiente';
            }else{
                if($fetchUsuario[8] == 5){
                    echo 'Necesita ayuda';
                }else{
                    echo 'Dependiente';
                }
            } ?>
        </div>

        <div class="user-block col-sm-10 col-xs-10">
            <p> 7. Ir al baño</p>
        </div>
        <div class="user-block col-sm-2 col-xs-2">
            <?php 
            if($fetchUsuario[9] == 10){
                echo 'Independiente';
            }else{
                if($fetchUsuario[9] == 5){
                    echo 'Necesita ayuda';
                }else{
                    echo 'Dependiente';
                }
            } ?>
        </div>

        <div class="user-block col-sm-10 col-xs-10">
            <p> 8. Traslado al sillón o cama </p>
        </div>
        <div class="user-block col-sm-2 col-xs-2">
            <?php 
            if($fetchUsuario[10] == 10){
                echo 'Independiente';
            }else{
                if($fetchUsuario[10] == 5){
                    echo 'Necesita ayuda';
                }else{
                    echo 'Dependiente';
                }
            } ?>
        </div>

        <div class="user-block col-sm-10 col-xs-10">
            <p> 9. Deambulación </p>
        </div>
        <div class="user-block col-sm-2 col-xs-2">
            <?php 
            if($fetchUsuario[11] == 10){
                echo 'Independiente';
            }else{
                if($fetchUsuario[11] == 5){
                    echo 'Necesita ayuda';
                }else{
                    echo 'Dependiente';
                }
            } ?>
        </div>

        <div class="user-block col-sm-10 col-xs-10">
            <p>10. Subir y bajar escaleras</p>
        </div>
        <div class="user-block col-sm-2 col-xs-2">
            <?php 
            if($fetchUsuario[12] == 10){
                echo 'Independiente';
            }else{
                if($fetchUsuario[12] == 5){
                    echo 'Necesita ayuda';
                }else{
                    echo 'Dependiente';
                }
            } ?>
        </div>



        <?php 
        $b1 = $fetchUsuario[3];
        $b2 = $fetchUsuario[4];
        $b3 = $fetchUsuario[5];
        $b4 = $fetchUsuario[6];
        $b5 = $fetchUsuario[7];
        $b6 = $fetchUsuario[8];
        $b7 = $fetchUsuario[9];
        $b8 = $fetchUsuario[10];
        $b9 = $fetchUsuario[11];
        $b10 = $fetchUsuario[12];

        $puntaje_barthel = $b1+$b2+$b3+$b4+$b5+$b6+$b7+$b8+$b9+$b10;
        if (($puntaje_barthel >= 0) && ($puntaje_barthel <= 59)){$puntaje_barthel_texto = "dependiente menor";}
        if (($puntaje_barthel >= 60) && ($puntaje_barthel <= 99)){$puntaje_barthel_texto = "semidependiente";}   
        if ($puntaje_barthel == 100){$puntaje_barthel_texto = "independiente";}

        ?>
        <div class="user-block col-sm-12 col-xs-12"><h7><b>Observaciones del test</b></h7></div>
        <div class="user-block col-sm-12 col-xs-12"><p><?php if(empty($fetchUsuario[13])){echo "Sin dato";}else{echo $fetchUsuario[13];} ?></p></div>

        <div class="user-block col-sm-12 col-xs-12"><h7><b>Resultado del test</b></h7></div>
        <div class="user-block col-sm-10 col-xs-10">
            <b><p> Puntaje</p></b>
            <p><?php echo $puntaje_barthel; ?></p>
            <b><p> Interpretación</p></b>
            <p><?php echo $puntaje_barthel_texto; ?></p>
        </div>

        <div class="user-block">&nbsp;</div>

    </div><!-- Fin del content -->
    <?php } ?>