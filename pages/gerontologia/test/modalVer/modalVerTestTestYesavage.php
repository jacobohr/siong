<?php

//include connection file 
require "../../../../session.php";

$sqlUsuario = "
SELECT 
UPPER((CONVERT(CAST(CONVERT(CONCAT(c.primer_nombre,' ',c.segundo_nombre,' ',c.primer_apellido,' ',c.segundo_apellido) USING latin1) AS BINARY) USING utf8))) AS nombreUsuario,
CONCAT(ty.ano_mod,'/',ty.mes_mod,'/',ty.dia_mod) AS fecha,  
ty.hora_mod,
ty.y1,ty.y2,ty.y3,ty.y4,ty.y5,ty.y6,ty.y7,ty.y8,ty.y9,ty.y10,ty.y11,ty.y12,ty.y13,ty.y14,ty.y15,
(CONVERT(CAST(CONVERT(ty.Observaciones_yesavage USING latin1) AS BINARY) USING utf8))AS Observaciones_yesavage 
FROM gddt_test_yesavage ty
INNER JOIN gddt_cuentas c ON c.id_cuenta = ty.id_cuenta 
WHERE ty.id_yesavage = " . $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);
$rows = mysqli_num_rows($queryUsuario);

if($rows == 0){
    echo "<p style='color:red;font-size:0.9em;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No se encontraron datos :(</p>";
}else{

    ?>

    <div class="content">
        <h6 class="text-center mb-4">Actualizado por <?= $fetchUsuario[0];?> el <?= $fetchUsuario[1]; ?> a las <?= $fetchUsuario[2]; ?></h6>
        
        <div class="user-block col-sm-10 col-xs-10">
            <p>
                1. ¿Está basicamente satisfecho con su vida?
            </p>
        </div>
        <div class="user-block col-sm-2 col-xs-2"><?php if($fetchUsuario[3] == 1){echo 'Si';}else{echo 'No';} ?></div>


        <div class="user-block col-sm-10 col-xs-10">
            <p>
                2. ¿Ha renunciado a muchas de sus actividades y pasatiempos?
            </p>
        </div>
        <div class="user-block col-sm-2 col-xs-2"><?php if($fetchUsuario[4] == 1){echo 'Si';}else{echo 'No';} ?></div>
        

        <div class="user-block col-sm-10 col-xs-10">
            <p> 3. ¿Siente que su vida esta vacia?</p>
        </div>
        <div class="user-block col-sm-2 col-xs-2"><?php if($fetchUsuario[5] == 1){echo 'Si';}else{echo 'No';} ?></div>

        <div class="user-block col-sm-10 col-xs-10">
            <p>4. ¿Se encuentra a menudo aburrido?</p>
        </div>
        <div class="user-block col-sm-2 col-xs-2"><?php if($fetchUsuario[6] == 1){echo 'Si';}else{echo 'No';} ?></div>

        <div class="user-block col-sm-10 col-xs-10">
            <p>5. ¿Se encuentra alegre y optimista, con buen animo casi todo el tiempo?</p>
        </div>
        <div class="user-block col-sm-2 col-xs-2"><?php if($fetchUsuario[7] == 1){echo 'Si';}else{echo 'No';} ?></div>

        <div class="user-block col-sm-10 col-xs-10">
            <p> 6. ¿Teme que vaya a pasar algo malo?</p>
        </div>
        <div class="user-block col-sm-2 col-xs-2"><?php if($fetchUsuario[8] == 1){echo 'Si';}else{echo 'No';} ?></div>

        <div class="user-block col-sm-10 col-xs-10">
            <p>7. ¿Se siente feliz, contento la mayor parte del tiempo?</p>
        </div>
        <div class="user-block col-sm-2 col-xs-2"><?php if($fetchUsuario[9] == 1){echo 'Si';}else{echo 'No';} ?></div>

        <div class="user-block col-sm-10 col-xs-10">
            <p>     8. ¿Se siente a menudo desamparado, desvalido, indeciso? </p>
        </div>
        <div class="user-block col-sm-2 col-xs-2"><?php if($fetchUsuario[10] == 1){echo 'Si';}else{echo 'No';}; ?></div>

        <div class="user-block col-sm-10 col-xs-10">
            <p> 9. ¿Prefiere quedarse en casa que acaso salir y hacer cosas nuevas? </p>
        </div>
        <div class="user-block col-sm-2 col-xs-2"><?php if($fetchUsuario[11] == 1){echo 'Si';}else{echo 'No';} ?></div>

        <div class="user-block col-sm-10 col-xs-10">
            <p> 10. ¿Le da impresion de que tiene mas fallos de memoria que los demas?</p>
        </div>
        <div class="user-block col-sm-2 col-xs-2"><?php if($fetchUsuario[12] == 1){echo 'Si';}else{echo 'No';} ?></div>

        <div class="user-block col-sm-10 col-xs-10">
            <p> 11. ¿Cree que es agradable estar vivo?</p>
        </div>
        <div class="user-block col-sm-2 col-xs-2"><?php if($fetchUsuario[13] == 1){echo 'Si';}else{echo 'No';} ?></div>


        <div class="user-block col-sm-10 col-xs-10">
            <p> 12. ¿Se le hace duro empezar nuevos proyectos?</p>
        </div>
        <div class="user-block col-sm-2 col-xs-2"><?php if($fetchUsuario[14] == 1){echo 'Si';}else{echo 'No';} ?></div>

        <div class="user-block col-sm-10 col-xs-10">
            <p>13. ¿Se siente lleno de energia?</p>
        </div>
        <div class="user-block col-sm-2 col-xs-2"><?php if($fetchUsuario[15] == 1){echo 'Si';}else{echo 'No';} ?></div>

        <div class="user-block col-sm-10 col-xs-10">
            <p>14. ¿Siente que su situacion es angustiosa, desesperada?</p>
        </div>
        <div class="user-block col-sm-2 col-xs-2"><?php if($fetchUsuario[16] == 1){echo 'Si';}else{echo 'No';} ?></div>

        <div class="user-block col-sm-10 col-xs-10">
            <p> 15. ¿Cree que la mayoria de la gente vive economicamente mejor que usted?</p>
        </div>
        <div class="user-block col-sm-2 col-xs-2"><?php if($fetchUsuario[17] == 1){echo 'Si';}else{echo 'No';} ?></div>

        <?php 
        $y1 = $fetchUsuario[3];
        $y2 = $fetchUsuario[4];
        $y3 = $fetchUsuario[5];
        $y4 = $fetchUsuario[6];
        $y5 = $fetchUsuario[7];
        $y6 = $fetchUsuario[8];
        $y7 = $fetchUsuario[9];
        $y8 = $fetchUsuario[10];
        $y9 = $fetchUsuario[11];
        $y10 = $fetchUsuario[12];
        $y11 = $fetchUsuario[13];
        $y12 = $fetchUsuario[14];
        $y13 = $fetchUsuario[15];
        $y14 = $fetchUsuario[16];
        $y15 = $fetchUsuario[17];
        $puntaje_yesavage = $y1 + $y2 + $y3 + $y4 + $y5 + $y6 + $y7 + $y8 + $y9 + $y10 + $y11 + $y12 + $y13 + $y14 + $y15;
        if ($puntaje_yesavage >= 11){$puntaje_yesavage_texto = "Depresion severa";}
        if (($puntaje_yesavage <= 10) && ($puntaje_yesavage >= 6)){$puntaje_yesavage_texto = "Depresion moderada";}
        if ($puntaje_yesavage <= 5){$puntaje_yesavage_texto = "Normal";}

        ?>
        <div class="user-block col-sm-12 col-xs-12"><h7><b>Observaciones del test</b></h7></div>
        <div class="user-block col-sm-12 col-xs-12"><p><?php if(empty($fetchUsuario[18])){echo "Sin dato";}else{echo $fetchUsuario[18];} ?></p></div>

        <div class="user-block col-sm-12 col-xs-12"><h7><b>Resultado del test</b></h7></div>
        <div class="user-block col-sm-10 col-xs-10">
            <p> Puntaje</p>
            <p><?php echo $puntaje_yesavage; ?></p>
            <p> Interpretación</p>
            <p><?php echo $puntaje_yesavage_texto; ?></p>
        </div>

        <div class="user-block">&nbsp;</div>

    </div><!-- Fin del content -->
    <?php } ?>