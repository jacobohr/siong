<?php require "../../../../session.php";



$sqlUsuario = "
SELECT *, (CONVERT(CAST(CONVERT(observaciones_minimental USING latin1) AS BINARY) USING utf8))AS observaciones_minimental FROM gddt_test_minimental WHERE id_minimental = ". $_REQUEST["idu"];
$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);


$arrayNumeros = array (
	0 => '1',
	1 => '2',
	2 => '3',
	3 => '4',
	4 => '5'
)

?>
<form name="editarUsuario" method="POST" action="procesarPsicologiaAntceMental.php?tipo=update" enctype="multipart/form-data">
	<h4 class="text-center mb-4"> Test Minimental </h4>
	<div class="row">
		<div class="form-group col-md-12">
			<label for="recipient-name" class="form-control-label">Usuario que registra <i style="color: darkorange">*</i></label>
			<select class="form-control" name="usuario" >
				<option>Selecciona una opción</option>
				<?php
				$queryCuenta = mysqli_query($conn, "SELECT id_cuenta,
					UPPER((CONVERT(CAST(CONVERT(CONCAT(primer_nombre,' ',segundo_nombre,' ',primer_apellido,' ',segundo_apellido) USING latin1) AS BINARY) USING utf8))) AS nombreUsuario FROM gddt_cuentas ;");
				while ($fetchCuenta = mysqli_fetch_row($queryCuenta)) {
					if ($fetchCuenta[0] == $fetchUsuario[1]) {
						echo "<option selected value=" . $fetchCuenta[0] . ">" . $fetchCuenta[1] . "</option>";
					} else {
						echo "<option value=" . $fetchCuenta[0] . ">" . $fetchCuenta[1] . "</option>";
					}
				}
				?>
			</select>
		</div>
		<?php 
		if(strlen($fetchUsuario[3]) < 2){$mes = '0'.$fetchUsuario[3];}else{$mes = $fetchUsuario[3];}
		if(strlen($fetchUsuario[4]) < 2){$dia = '0'.$fetchUsuario[4];}else{$dia = $fetchUsuario[4];}
		$year = $fetchUsuario[2];
		$fecha = $year.'-'.$mes.'-'.$dia;
		?>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label">Fecha <i style="color: darkorange">*</i></label>
			<input id="fechaIngreso" type="date" class="form-control" name="fecha" value="<?= $fecha ?>"/>
		</div>
		<div class="form-group col-md-12" >
			<label for="message-text" class="form-control-label"></label>
			<h6><b>Orientación</b></h6>
		</div>

		<div class="form-group col-md-8">
			<label for="message-text" class="form-control-label">1. Temporal <i style="color: darkorange">*</i></label>
			<p>¿Que fecha, año, mes, dia de la semana, mañana o tarde es hoy?</p>
		</div>
		<div class="form-group col-md-4">
			<select class="form-control" name="preguntaUno" >
				<option>Selecciona una opción</option>
				<?php 
				$cont = 0;
				while($cont <= 5){
					if($cont == $fetchUsuario[7]){
						echo '<option selected value="'.$cont.'">'.$cont.'</option>';
					}else{
						echo '<option value="'.$cont.'">'.$cont.'</option>';
					}
					$cont++;
				}
				?>
			</select>
		</div>

		<div class="form-group col-md-8">
			<label for="message-text" class="form-control-label">2. Espacial <i style="color: darkorange">*</i></label>
			<p>¿Donde estamos? Pais, departamento, ciudad, barrio, lugar.</p>
		</div>
		<div class="form-group col-md-4">
			<select class="form-control" name="preguntaDos" >
				<option>Selecciona una opción</option>
				<?php 
				$cont = 0;
				while($cont <= 5){
					if($cont == $fetchUsuario[8]){
						echo '<option selected value="'.$cont.'">'.$cont.'</option>';
					}else{
						echo '<option value="'.$cont.'">'.$cont.'</option>';
					}
					$cont++;
				}
				?>
			</select>
		</div>

		<div class="form-group col-md-12" >
			<label for="message-text" class="form-control-label"></label>
			<h6><b>Fijacion o atención</b></h6>
		</div>
		<div class="form-group col-md-8">
			<label for="message-text" class="form-control-label">3. Memoria Sensorial <i style="color: darkorange">*</i></label>
			<p>Le voy a nombrar tres objetos: "Manzana, mesa, centavo" despues de que los haya nombrado, quiero que los repita. Acuerdese de ellos, le pedire que los nombre en unos minutos. Anote un punto por cada respuesta correcta. Continúe repitiendo los nombres hasta que los haya aprendido todos. Cuente las pruebas y anote.</p>
		</div>
		<div class="form-group col-md-4">
			<select class="form-control" name="preguntaTres" >
				<option>Selecciona una opción</option>
				<?php 
				$cont = 0;
				while($cont <= 3){
					if($cont == $fetchUsuario[9]){
						echo '<option selected value="'.$cont.'">'.$cont.'</option>';
					}else{
						echo '<option value="'.$cont.'">'.$cont.'</option>';
					}
					$cont++;
				}
				?>
			</select>
		</div>

		<div class="form-group col-md-12" >
			<label for="message-text" class="form-control-label"></label>
			<h6><b>Atención y cálculo</b></h6>
		</div>
		<div class="form-group col-md-8">
			<label for="message-text" class="form-control-label">4. Memoria Verbal de Trabajo<i style="color: darkorange">*</i></label>
			<p>Deletree "mundo" al revés. El puntaje es el número de letras en el órden correcto.</p>
		</div>
		<div class="form-group col-md-4">
			<select class="form-control" name="preguntaCuatro" >
				<option>Selecciona una opción</option>
				<?php 
				$cont = 0;
				while($cont <= 5){
					if($cont == $fetchUsuario[10]){
						echo '<option selected value="'.$cont.'">'.$cont.'</option>';
					}else{
						echo '<option value="'.$cont.'">'.$cont.'</option>';
					}
					$cont++;
				}
				?>
			</select>
		</div>

		<div class="form-group col-md-12" >
			<label for="message-text" class="form-control-label"></label>
			<h6><b>Memoria</b></h6>
		</div>
		<div class="form-group col-md-8">
			<label for="message-text" class="form-control-label">5. Memoria a Corto Plazo<i style="color: darkorange">*</i></label>
			<p>Pida al paciente que repita los tres nombres mencionados anteriormente. Anote un punto por cada respuesta correcta. Nota: no se puede hacer la prueba de memoria si el paciente no reodó los tres obejtos durante la prueba de atención.</p>
		</div>
		<div class="form-group col-md-4">
			<select class="form-control" name="preguntaCinco" >
				<option>Selecciona una opción</option>
				<?php 
				$cont = 0;
				while($cont <= 3){
					if($cont == $fetchUsuario[11]){
						echo '<option selected value="'.$cont.'">'.$cont.'</option>';
					}else{
						echo '<option value="'.$cont.'">'.$cont.'</option>';
					}
					$cont++;
				}
				?>
			</select>
		</div>

		<div class="form-group col-md-12" >
			<label for="message-text" class="form-control-label"></label>
			<h6><b>	Lenguaje y práxias</b></h6>
		</div>
		<div class="form-group col-md-8">
			<label for="message-text" class="form-control-label">6. Lenguaje<i style="color: darkorange">*</i></label>
			<p>Identifique un "reloj" y un "lapiz".</p>
		</div>
		<div class="form-group col-md-4">
			<select class="form-control" name="preguntaSeis" >
				<option>Selecciona una opción</option>
				<?php 
				$cont = 0;
				while($cont <= 2){
					if($cont == $fetchUsuario[12]){
						echo '<option selected value="'.$cont.'">'.$cont.'</option>';
					}else{
						echo '<option value="'.$cont.'">'.$cont.'</option>';
					}
					$cont++;
				}
				?>
			</select>
		</div>

		<div class="form-group col-md-8">
			<label for="message-text" class="form-control-label">7. Lenguaje<i style="color: darkorange">*</i></label>
			<p>Repita lo siguiente: "ni si, ni no, ni peros".</p>
		</div>
		<div class="form-group col-md-4">
			<select class="form-control" name="preguntaSiete" >
				<option>Selecciona una opción</option>
				<?php 
				$cont = 0;
				while($cont <= 1){
					if($cont == $fetchUsuario[13]){
						echo '<option selected value="'.$cont.'">'.$cont.'</option>';
					}else{
						echo '<option value="'.$cont.'">'.$cont.'</option>';
					}
					$cont++;
				}
				?>
			</select>
		</div>

		<div class="form-group col-md-8">
			<label for="message-text" class="form-control-label">8. Práxis<i style="color: darkorange">*</i></label>
			<p>Siga un comando en tres pasos: - Tome un papel con la mano derecha. - Doble el papel por la mitad. - Pongalo en el piso.</p>
		</div>
		<div class="form-group col-md-4">
			<select class="form-control" name="preguntaOcho" >
				<option>Selecciona una opción</option>
				<?php 
				$cont = 0;
				while($cont <= 3){
					if($cont == $fetchUsuario[14]){
						echo '<option selected value="'.$cont.'">'.$cont.'</option>';
					}else{
						echo '<option value="'.$cont.'">'.$cont.'</option>';
					}
					$cont++;
				}
				?>
			</select>
		</div>

		<div class="form-group col-md-8">
			<label for="message-text" class="form-control-label">9. Práxis.<i style="color: darkorange">*</i></label>
			<p>Cierre los ojos.</p>
		</div>
		<div class="form-group col-md-4">
			<select class="form-control" name="preguntaNueve" >
				<option>Selecciona una opción</option>
				<?php 
				$cont = 0;
				while($cont <= 1){
					if($cont == $fetchUsuario[15]){
						echo '<option selected value="'.$cont.'">'.$cont.'</option>';
					}else{
						echo '<option value="'.$cont.'">'.$cont.'</option>';
					}
					$cont++;
				}
				?>
			</select>
		</div>

		<div class="form-group col-md-8">
			<label for="message-text" class="form-control-label">10. Lenguaje<i style="color: darkorange">*</i></label>
			<p>Escríba una oración.	</p>
		</div>
		<div class="form-group col-md-4">
			<select class="form-control" name="preguntaDiez" >
				<option>Selecciona una opción</option>
				<?php 
				$cont = 0;
				while($cont <= 1){
					if($cont == $fetchUsuario[16]){
						echo '<option selected value="'.$cont.'">'.$cont.'</option>';
					}else{
						echo '<option value="'.$cont.'">'.$cont.'</option>';
					}
					$cont++;
				}
				?>
			</select>
		</div>

		<div class="form-group col-md-8">
			<label for="message-text" class="form-control-label">11. Práxis<i style="color: darkorange">*</i></label>
			<p>Copie el siguiente diseño:</p>
			<img src="<?= $base_url ?>images/test/minimental_figura.png" style="width: 160px;height: 117px;">
		</div>
		<div class="form-group col-md-4">
			<select class="form-control" name="usuario" >
				<option>Selecciona una opción</option>
				<?php 
				$cont = 0;
				while($cont <= 1){
					if($cont == $fetchUsuario[17]){
						echo '<option selected value="'.$cont.'">'.$cont.'</option>';
					}else{
						echo '<option value="'.$cont.'">'.$cont.'</option>';
					}
					$cont++;
				}
				?>
			</select>
		</div>

		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label">Observaciones test <i style="color: darkorange">*</i></label>
			<textarea class="form-control" name="observacionesTest" cols="40" rows="5" ><?= $fetchUsuario[19] ?></textarea>
		</div>
	</div>
	<!-- End Row -->


	<!-- Modal Footer -->
	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">
			Cancelar
		</button>
		<button type="submit" class="btn btn-primary" name="Actualizar">
			Actualizar
		</button>
	</div>
</div>    
