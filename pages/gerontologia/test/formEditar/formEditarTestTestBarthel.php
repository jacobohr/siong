<?php require "../../../../session.php";



$sqlUsuario = "
SELECT *,(CONVERT(CAST(CONVERT(observacion USING latin1) AS BINARY) USING utf8))AS observacion FROM gddt_test_barthel
WHERE id_barthel = ". $_REQUEST["idu"];
$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);

function selects($numero){
	
}

?>
<form name="editarUsuario" method="POST" action="procesarPsicologiaAntceMental.php?tipo=update" enctype="multipart/form-data">
	<h4 class="text-center mb-4"> Test Barthel </h4>
	<div class="row">
		<div class="form-group col-md-12">
			<label for="recipient-name" class="form-control-label">Usuario que registra <i style="color: darkorange">*</i></label>
			<select class="form-control" name="usuario" >
				<option>Selecciona una opción</option>
				<?php
				$queryCuenta = mysqli_query($conn, "SELECT id_cuenta,
					UPPER((CONVERT(CAST(CONVERT(CONCAT(primer_nombre,' ',segundo_nombre,' ',primer_apellido,' ',segundo_apellido) USING latin1) AS BINARY) USING utf8))) AS nombreUsuario FROM gddt_cuentas ;");
				while ($fetchCuenta = mysqli_fetch_row($queryCuenta)) {
					if ($fetchCuenta[0] == $fetchUsuario[1]) {
						echo "<option selected value=" . $fetchCuenta[0] . ">" . $fetchCuenta[1] . "</option>";
					} else {
						echo "<option value=" . $fetchCuenta[0] . ">" . $fetchCuenta[1] . "</option>";
					}
				}
				?>
			</select>
		</div>
		<?php 
		if(strlen($fetchUsuario[3]) < 2){$mes = '0'.$fetchUsuario[3];}else{$mes = $fetchUsuario[3];}
		if(strlen($fetchUsuario[4]) < 2){$dia = '0'.$fetchUsuario[4];}else{$dia = $fetchUsuario[4];}
		$year = $fetchUsuario[2];
		$fecha = $year.'-'.$mes.'-'.$dia;
		?>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label">Fecha <i style="color: darkorange">*</i></label>
			<input type="date" class="form-control" name="fecha" value="<?= $fecha ?>"/>
		</div>

		<div class="form-group col-md-8">
			<p>1. Alimentacion</p>
		</div>
		<div class="form-group col-md-4">
			<select class="form-control" name="preguntaUno" >
				<option>Selecciona una opción</option>
				<?php 
				if($fetchUsuario[7] == 10){
					echo '<option selected value="10">Independiente</option>';
					echo '<option value="5">Necesita ayuda</option>';
					echo '<option value="0">Dependiente</option>';
				}elseif($fetchUsuario[7] == 5){
					echo '<option value="10">Independiente</option>';
					echo '<option selected value="5">Necesita ayuda</option>';
					echo '<option value="0">Dependiente</option>';
				}elseif($fetchUsuario[7] == 0){
					echo '<option value="10">Independiente</option>';
					echo '<option value="5">Necesita ayuda</option>';
					echo '<option selected value="0">Dependiente</option>';
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-8">
			<p>2. Lavarse (Baño)</p>
		</div>
		<div class="form-group col-md-4">
			<select class="form-control" name="preguntoDos" >
				<option>Selecciona una opción</option>
				<?php 
				if($fetchUsuario[8] == 5){
					echo '<option selected value="5">Independiente</option>';
					echo '<option value="0">Dependiente</option>';
				}elseif($fetchUsuario[8] == 0){
					echo '<option value="5">Independiente</option>';
					echo '<option selected value="0">Dependiente</option>';
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-8">
			<p>3. Vestirse</p>
		</div>
		<div class="form-group col-md-4">
			<select class="form-control" name="preguntaTres" >
				<option>Selecciona una opción</option>
				<?php 
				if($fetchUsuario[9] == 10){
					echo '<option selected value="10">Independiente</option>';
					echo '<option value="5">Necesita ayuda</option>';
					echo '<option value="0">Dependiente</option>';
				}elseif($fetchUsuario[9] == 5){
					echo '<option value="10">Independiente</option>';
					echo '<option selected value="5">Necesita ayuda</option>';
					echo '<option value="0">Dependiente</option>';
				}elseif($fetchUsuario[9] == 0){
					echo '<option value="10">Independiente</option>';
					echo '<option value="5">Necesita ayuda</option>';
					echo '<option selected value="0">Dependiente</option>';
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-8">
			<p>4. Arreglarse</p>
		</div>
		<div class="form-group col-md-4">
			<select class="form-control" name="preguntaCuatro" >
				<option>Selecciona una opción</option>
				<?php 
				if($fetchUsuario[10] == 5){
					echo '<option selected value="5">Independiente</option>';
					echo '<option value="0">Dependiente</option>';
				}elseif($fetchUsuario[10] == 0){
					echo '<option value="5">Independiente</option>';
					echo '<option selected value="0">Dependiente</option>';
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-8">
			<p>	5. Deposición</p>
		</div>
		<div class="form-group col-md-4">
			<select class="form-control" name="preguntoCinco" >
				<option>Selecciona una opción</option>
				<?php 
				if($fetchUsuario[11] == 10){
					echo '<option selected value="10">Continente</option>';
					echo '<option value="5">Accidente ocasional</option>';
					echo '<option value="0">Incontinente</option>';
				}elseif($fetchUsuario[11] == 5){
					echo '<option value="10">Continente</option>';
					echo '<option selected value="5">Accidente ocasional</option>';
					echo '<option value="0">Incontinente</option>';
				}elseif($fetchUsuario[11] == 0){
					echo '<option value="10">Continente</option>';
					echo '<option value="5">Accidente ocasional</option>';
					echo '<option selected value="0">Incontinente</option>';
				}
				?>
			</select>
		</div>

		<div class="form-group col-md-8">
			<p>6. Micción</p>
		</div>
		<div class="form-group col-md-4">
			<select class="form-control" name="preguntaSeis" >
				<option>Selecciona una opción</option>
				<?php 
				if($fetchUsuario[12] == 10){
					echo '<option selected value="10">Continente</option>';
					echo '<option value="5">Accidente ocasional</option>';
					echo '<option value="0">Incontinente</option>';
				}elseif($fetchUsuario[12] == 5){
					echo '<option value="10">Continente</option>';
					echo '<option selected value="5">Accidente ocasional</option>';
					echo '<option value="0">Incontinente</option>';
				}elseif($fetchUsuario[12] == 0){
					echo '<option value="10">Continente</option>';
					echo '<option value="5">Accidente ocasional</option>';
					echo '<option selected value="0">Incontinente</option>';
				}
				?>
			</select>
		</div>

		<div class="form-group col-md-8">
			<p>7. Ir al baño</p>
		</div>
		<div class="form-group col-md-4">
			<select class="form-control" name="preguntaSiete" >
				<option>Selecciona una opción</option>
				<?php 
				if($fetchUsuario[13] == 10){
					echo '<option selected value="10">Independiente</option>';
					echo '<option value="5">Necesita ayuda</option>';
					echo '<option value="0">Dependiente</option>';
				}elseif($fetchUsuario[13] == 5){
					echo '<option value="10">Independiente</option>';
					echo '<option selected value="5">Necesita ayuda</option>';
					echo '<option value="0">Dependiente</option>';
				}elseif($fetchUsuario[13] == 0){
					echo '<option value="10">Independiente</option>';
					echo '<option value="5">Necesita ayuda</option>';
					echo '<option selected value="0">Dependiente</option>';
				}
				?>
			</select>
		</div>

		<div class="form-group col-md-8">
			<p>8. Traslado al sillón o cama</p>
		</div>
		<div class="form-group col-md-4">
			<select class="form-control" name="preguntaOcho" >
				<option>Selecciona una opción</option>
				<?php 
				if($fetchUsuario[14] == 15){
					echo '<option selected value="15">Independiente</option>';
					echo '<option value="10">Mímina ayuda</option>';
					echo '<option value="5">Gran ayuda</option>';
					echo '<option value="0">Dependiente</option>';
				}elseif($fetchUsuario[14] == 10){
					echo '<option value="15">Independiente</option>';
					echo '<option selected value="10">Mímina ayuda</option>';
					echo '<option value="5">Gran ayuda</option>';
					echo '<option value="0">Dependiente</option>';
				}elseif($fetchUsuario[14] == 5){
					echo '<option value="15">Independiente</option>';
					echo '<option value="10">Mímina ayuda</option>';
					echo '<option selected value="5">Gran ayuda</option>';
					echo '<option value="0">Dependiente</option>';
				}elseif($fetchUsuario[14] == 0){
					echo '<option value="15">Independiente</option>';
					echo '<option value="10">Mímina ayuda</option>';
					echo '<option value="5">Gran ayuda</option>';
					echo '<option selected value="0">Dependiente</option>';
				}
				?>
			</select>
		</div>

		<div class="form-group col-md-8">
			<p>9. Deambulación</p>
		</div>
		<div class="form-group col-md-4">
			<select class="form-control" name="preguntaNueve" >
				<option>Selecciona una opción</option>
				<?php 
				if($fetchUsuario[15] == 15){
					echo '<option selected value="15">Independiente</option>';
					echo '<option value="10">Necesita ayuda</option>';
					echo '<option value="5">Independiente (Silla de ruedas)</option>';
					echo '<option value="0">Dependiente</option>';
				}elseif($fetchUsuario[15] == 10){
					echo '<option value="15">Independiente</option>';
					echo '<option selected value="10">Necesita ayuda</option>';
					echo '<option value="5">Independiente (Silla de ruedas)</option>';
					echo '<option value="0">Dependiente</option>';
				}elseif($fetchUsuario[15] == 5){
					echo '<option value="15">Independiente</option>';
					echo '<option value="10">Necesita ayuda</option>';
					echo '<option selected value="5">Independiente (Silla de ruedas)</option>';
					echo '<option value="0">Dependiente</option>';
				}elseif($fetchUsuario[15] == 0){
					echo '<option value="15">Independiente</option>';
					echo '<option value="10">Necesita ayuda</option>';
					echo '<option value="5">Independiente (Silla de ruedas)</option>';
					echo '<option selected value="0">Dependiente</option>';
				}
				?>
			</select>
		</div>

		<div class="form-group col-md-8">
			<p>10. Subir y bajar escaleras</p>
		</div>
		<div class="form-group col-md-4">
			<select class="form-control" name="preguntaDiez" >
				<option>Selecciona una opción</option>
				<?php 
				selects($fetchUsuario[16]);
				if($fetchUsuario[16] == 10){
					echo '<option selected value="10">Independiente</option>';
					echo '<option value="5">Necesita ayuda</option>';
					echo '<option value="0">Dependiente</option>';
				}elseif($fetchUsuario[16] == 5){
					echo '<option value="10">Independiente</option>';
					echo '<option selected value="5">Necesita ayuda</option>';
					echo '<option value="0">Dependiente</option>';
				}elseif($fetchUsuario[16] == 0){
					echo '<option value="10">Independiente</option>';
					echo '<option value="5">Necesita ayuda</option>';
					echo '<option selected value="0">Dependiente</option>';
				}
				?>
			</select>
		</div>

		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label">Observaciones test <i style="color: darkorange">*</i></label>
			<textarea class="form-control" name="observacionesTest" cols="40" rows="5" ><?= $fetchUsuario[18] ?></textarea>
		</div>
	</div>
	<!-- End Row -->


	<!-- Modal Footer -->
	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">
			Cancelar
		</button>
		<button type="submit" class="btn btn-primary" name="Actualizar">
			Actualizar
		</button>
	</div>
</div>    
