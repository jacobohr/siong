<?php require "../../../../session.php";



$sqlUsuario = "
SELECT *,(CONVERT(CAST(CONVERT(Observaciones_yesavage USING latin1) AS BINARY) USING utf8))AS Observaciones_yesavage FROM gddt_test_yesavage WHERE id_yesavage = ". $_REQUEST["idu"];
$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);
?>
<form name="editarUsuario" method="POST" action="procesarPsicologiaAntceMental.php?tipo=update" enctype="multipart/form-data">
	<h4 class="text-center mb-4"> Test Yesavage </h4>
	<div class="row">
		<div class="form-group col-md-12">
			<label for="recipient-name" class="form-control-label">Usuario que registra <i style="color: darkorange">*</i></label>
			<select class="form-control" name="usuario" >
				<option>Selecciona una opción</option>
				<?php
				$queryCuenta = mysqli_query($conn, "SELECT id_cuenta,
					UPPER((CONVERT(CAST(CONVERT(CONCAT(primer_nombre,' ',segundo_nombre,' ',primer_apellido,' ',segundo_apellido) USING latin1) AS BINARY) USING utf8))) AS nombreUsuario FROM gddt_cuentas ;");
				while ($fetchCuenta = mysqli_fetch_row($queryCuenta)) {
					if ($fetchCuenta[0] == $fetchUsuario[1]) {
						echo "<option selected value=" . $fetchCuenta[0] . ">" . $fetchCuenta[1] . "</option>";
					} else {
						echo "<option value=" . $fetchCuenta[0] . ">" . $fetchCuenta[1] . "</option>";
					}
				}
				?>
			</select>
		</div>
		<?php 
		if(strlen($fetchUsuario[3]) < 2){$mes = '0'.$fetchUsuario[3];}else{$mes = $fetchUsuario[3];}
		if(strlen($fetchUsuario[4]) < 2){$dia = '0'.$fetchUsuario[4];}else{$dia = $fetchUsuario[4];}
		$year = $fetchUsuario[2];
		$fecha = $year.'-'.$mes.'-'.$dia;
		?>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label">Fecha <i style="color: darkorange">*</i></label>
			<input id="fechaIngreso" type="date" class="form-control" name="fecha" value="<?= $fecha ?>"/>
		</div>

		<div class="form-group col-md-8">
			<p>1. ¿Está basicamente satisfecho con su vida?</p>
		</div>
		<div class="form-group col-md-4">
			<select class="form-control" name="preguntaUno" >
				<option>Selecciona una opción</option>
				<?php 
				if($fetchUsuario[7] == 1){
					echo '<option selected value="1">Si</option>';
					echo '<option value="0">No</option>';
				}else{
					echo '<option value="1">Si</option>';
					echo '<option selected value="0">No</option>';
				}
				?>
			</select>
		</div>

		<div class="form-group col-md-8">
			<p>2. ¿Ha renunciado a muchas de sus actividades y pasatiempos?</p>
		</div>
		<div class="form-group col-md-4">
			<select class="form-control" name="preguntaDos" >
				<option>Selecciona una opción</option>
				<?php 
				if($fetchUsuario[8] == 1){
					echo '<option selected value="1">Si</option>';
					echo '<option value="0">No</option>';
				}else{
					echo '<option value="1">Si</option>';
					echo '<option selected value="0">No</option>';
				}
				?>
			</select>
		</div>

		<div class="form-group col-md-8">
			<p>	3. ¿Siente que su vida esta vacia?</p>
		</div>
		<div class="form-group col-md-4">
			<select class="form-control" name="preguntaTres" >
				<option>Selecciona una opción</option>
				<?php 
				if($fetchUsuario[9] == 1){
					echo '<option selected value="1">Si</option>';
					echo '<option value="0">No</option>';
				}else{
					echo '<option value="1">Si</option>';
					echo '<option selected value="0">No</option>';
				}
				?>
			</select>
		</div>

		<div class="form-group col-md-8">
			<p>4. ¿Se encuentra a menudo aburrido?</p>
		</div>
		<div class="form-group col-md-4">
			<select class="form-control" name="preguntaCuatro" >
				<option>Selecciona una opción</option>
				<?php 
				if($fetchUsuario[10] == 1){
					echo '<option selected value="1">Si</option>';
					echo '<option value="0">No</option>';
				}else{
					echo '<option value="1">Si</option>';
					echo '<option selected value="0">No</option>';
				}
				?>
			</select>
		</div>

		<div class="form-group col-md-8">
			<p>	5. ¿Se encuentra alegre y optimista, con buen animo casi todo el tiempo?</p>
		</div>
		<div class="form-group col-md-4">
			<select class="form-control" name="preguntaCinco" >
				<option>Selecciona una opción</option>
				<?php 
				if($fetchUsuario[11] == 1){
					echo '<option selected value="1">Si</option>';
					echo '<option value="0">No</option>';
				}else{
					echo '<option value="1">Si</option>';
					echo '<option selected value="0">No</option>';
				}
				?>
			</select>
		</div>

		<div class="form-group col-md-8">
			<p>6. ¿Teme que vaya a pasar algo malo?</p>
		</div>
		<div class="form-group col-md-4">
			<select class="form-control" name="preguntaSeis" >
				<option>Selecciona una opción</option>
				<?php 
				if($fetchUsuario[12] == 1){
					echo '<option selected value="1">Si</option>';
					echo '<option value="0">No</option>';
				}else{
					echo '<option value="1">Si</option>';
					echo '<option selected value="0">No</option>';
				}
				?>
			</select>
		</div>

		<div class="form-group col-md-8">
			<p>7. ¿Se siente feliz, contento la mayor parte del tiempo?</p>
		</div>
		<div class="form-group col-md-4">
			<select class="form-control" name="preguntaSiete" >
				<option>Selecciona una opción</option>
				<?php 
				if($fetchUsuario[13] == 1){
					echo '<option selected value="1">Si</option>';
					echo '<option value="0">No</option>';
				}else{
					echo '<option value="1">Si</option>';
					echo '<option selected value="0">No</option>';
				}
				?>
			</select>
		</div>

		<div class="form-group col-md-8">
			<p>	8. ¿Se siente a menudo desamparado, desvalido, indeciso?</p>
		</div>
		<div class="form-group col-md-4">
			<select class="form-control" name="preguntaOcho" >
				<option>Selecciona una opción</option>
				<?php 
				if($fetchUsuario[14] == 1){
					echo '<option selected value="1">Si</option>';
					echo '<option value="0">No</option>';
				}else{
					echo '<option value="1">Si</option>';
					echo '<option selected value="0">No</option>';
				}
				?>
			</select>
		</div>

		<div class="form-group col-md-8">
			<p>9. ¿Prefiere quedarse en casa que acaso salir y hacer cosas nuevas?</p>
		</div>
		<div class="form-group col-md-4">
			<select class="form-control" name="preguntaNueve" >
				<option>Selecciona una opción</option>
				<?php 
				if($fetchUsuario[15] == 1){
					echo '<option selected value="1">Si</option>';
					echo '<option value="0">No</option>';
				}else{
					echo '<option value="1">Si</option>';
					echo '<option selected value="0">No</option>';
				}
				?>
			</select>
		</div>

		<div class="form-group col-md-8">
			<p>10. ¿Le da impresion de que tiene mas fallos de memoria que los demas?</p>
		</div>
		<div class="form-group col-md-4">
			<select class="form-control" name="preguntaDiez" >
				<option>Selecciona una opción</option>
				<?php 
				if($fetchUsuario[16] == 1){
					echo '<option selected value="1">Si</option>';
					echo '<option value="0">No</option>';
				}else{
					echo '<option value="1">Si</option>';
					echo '<option selected value="0">No</option>';
				}
				?>
			</select>
		</div>

		<div class="form-group col-md-8">
			<p>11. ¿Cree que es agradable estar vivo?</p>
		</div>
		<div class="form-group col-md-4">
			<select class="form-control" name="preguntaOnce" >
				<option>Selecciona una opción</option>
				<?php 
				if($fetchUsuario[17] == 1){
					echo '<option selected value="1">Si</option>';
					echo '<option value="0">No</option>';
				}else{
					echo '<option value="1">Si</option>';
					echo '<option selected value="0">No</option>';
				}
				?>
			</select>
		</div>

		<div class="form-group col-md-8">
			<p>12. ¿Se le hace duro empezar nuevos proyectos?</p>
		</div>
		<div class="form-group col-md-4">
			<select class="form-control" name="preguntaDoce" >
				<option>Selecciona una opción</option>
				<?php 
				if($fetchUsuario[18] == 1){
					echo '<option selected value="1">Si</option>';
					echo '<option value="0">No</option>';
				}else{
					echo '<option value="1">Si</option>';
					echo '<option selected value="0">No</option>';
				}
				?>
			</select>
		</div>

		<div class="form-group col-md-8">
			<p>13. ¿Se siente lleno de energia?</p>
		</div>
		<div class="form-group col-md-4">
			<select class="form-control" name="preguntaTrece" >
				<option>Selecciona una opción</option>
				<?php 
				if($fetchUsuario[19] == 1){
					echo '<option selected value="1">Si</option>';
					echo '<option value="0">No</option>';
				}else{
					echo '<option value="1">Si</option>';
					echo '<option selected value="0">No</option>';
				}
				?>
			</select>
		</div>

		<div class="form-group col-md-8">
			<p>14. ¿Siente que su situacion es angustiosa, desesperada?</p>
		</div>
		<div class="form-group col-md-4">
			<select class="form-control" name="preguntaCatorce" >
				<option>Selecciona una opción</option>
				<?php 
				if($fetchUsuario[20] == 1){
					echo '<option selected value="1">Si</option>';
					echo '<option value="0">No</option>';
				}else{
					echo '<option value="1">Si</option>';
					echo '<option selected value="0">No</option>';
				}
				?>
			</select>
		</div>

		<div class="form-group col-md-8">
			<p>15. ¿Cree que la mayoria de la gente vive economicamente mejor que usted?</p>
		</div>
		<div class="form-group col-md-4">
			<select class="form-control" name="preguntaQuince" >
				<option>Selecciona una opción</option>
				<?php 
				if($fetchUsuario[21] == 1){
					echo '<option selected value="1">Si</option>';
					echo '<option value="0">No</option>';
				}else{
					echo '<option value="1">Si</option>';
					echo '<option selected value="0">No</option>';
				}
				?>
			</select>
		</div>

		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label">Observaciones test <i style="color: darkorange">*</i></label>
			<textarea class="form-control" name="observacionesTest" cols="40" rows="5" ><?= $fetchUsuario[23] ?></textarea>
		</div>
	</div>
	<!-- End Row -->


	<!-- Modal Footer -->
	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">
			Cancelar
		</button>
		<button type="submit" class="btn btn-primary" name="Actualizar">
			Actualizar
		</button>
	</div>
</div>    
