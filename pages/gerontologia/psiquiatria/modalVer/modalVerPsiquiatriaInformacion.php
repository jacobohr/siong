<?php
//include connection file 
require "../../../../session.php";

$sqlUsuario = "
SELECT 
UPPER((CONVERT(CAST(CONVERT(CONCAT(c.primer_nombre,' ',c.segundo_nombre,' ',c.primer_apellido,' ',c.segundo_apellido) USING latin1) AS BINARY) USING utf8))) AS nombreUsuario,
CONCAT(nd.ano_mod,'/',nd.mes_mod,'/',nd.dia_mod) AS fecha,  
nd.hora_mod,
(CONVERT(CAST(CONVERT(nd.enfermedad_actual USING latin1) AS BINARY) USING utf8))AS enfermedad_actual,
nd.antecedentes_personales,
nd.antecedentes_familiares,
(CONVERT(CAST(CONVERT(nd.enfermedad_mental USING latin1) AS BINARY) USING utf8))AS enfermedad_mental,
(CONVERT(CAST(CONVERT(nd.diagnostico_psiquiatrico USING latin1) AS BINARY) USING utf8))AS diagnostico_psiquiatrico,
(CONVERT(CAST(CONVERT(nd.analisis_psiquiatrico USING latin1) AS BINARY) USING utf8))AS analisis_psiquiatrico,
(CONVERT(CAST(CONVERT(nd.tratamiento USING latin1) AS BINARY) USING utf8))AS tratamiento
FROM gddt_psiquiatria_datos nd
INNER JOIN gddt_cuentas c ON c.id_cuenta = nd.id_cuenta 
WHERE nd.id_psiquiatria_datos =  " . $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);
$rows = mysqli_num_rows($queryUsuario);

if($rows == 0){
    echo "<p style='color:red;font-size:0.9em;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No se encontraron datos :(</p>";
}else{
    ?>
    <div class="content">
        <h6 class="text-center mb-4">Actualizado por <?= $fetchUsuario[0];?> el <?= $fetchUsuario[1]; ?> a las <?= $fetchUsuario[2]; ?></h6>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Antecedentes personales
            </label>
            <p><?php if(empty($fetchUsuario[4])){echo "Sin dato";}else{echo $fetchUsuario[4];} ?></p>
        </div>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Antecedentes familiares
            </label>
            <p><?php if(empty($fetchUsuario[5])){echo "Sin dato";}else{echo $fetchUsuario[5];} ?></p>
        </div>
        <div class="user-block col-sm-12 col-xs-12">
            <label>
                Enfermedad actual
            </label>
            <p><?php if(empty($fetchUsuario[3])){echo "Sin dato";}else{echo $fetchUsuario[3];} ?></p>
        </div>
        <div class="user-block col-sm-12 col-xs-12">
            <label>
                Enfermedad Metal
            </label>
            <p><?php if(empty($fetchUsuario[6])){echo "Sin dato";}else{echo $fetchUsuario[6];} ?></p>
        </div>
        <div class="user-block col-sm-12 col-xs-12">
            <label>
                Diagnastico Psiquiatrico
            </label>
            <p><?php if(empty($fetchUsuario[7])){echo "Sin dato";}else{echo $fetchUsuario[7];} ?></p>
        </div>
        <div class="user-block col-sm-12 col-xs-12">
            <label>
                Analisis Psiquiatrico
            </label>
            <p><?php if(empty($fetchUsuario[8])){echo "Sin dato";}else{echo $fetchUsuario[8];} ?></p>
        </div>
        <div class="user-block col-sm-12 col-xs-12">
            <label>
                Tratamiento
            </label>
            <p><?php if(empty($fetchUsuario[9])){echo "Sin dato";}else{echo $fetchUsuario[9];} ?></p>
        </div>
        <div class="user-block">&nbsp;</div>

    </div><!-- Fin del content -->
    <?php } ?>