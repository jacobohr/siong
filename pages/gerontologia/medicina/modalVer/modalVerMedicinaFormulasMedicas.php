<?php

//include connection file 
require "../../../../session.php";

$sqlUsuario = "
SELECT 
(CONVERT(CAST(CONVERT(CONCAT(UPPER(c.primer_nombre),' ',UPPER(c.segundo_nombre),' ',UPPER(c.primer_apellido),' ',UPPER(c.segundo_apellido)) USING latin1) AS BINARY) USING utf8)) AS nombreUsuario,
CONCAT(fm.dia_mod,'/',fm.mes_mod,'/',fm.ano_mod) fecha,  
fm.hora_mod, 
fm.des_formula_medica 
FROM gddt_formulas_medicas fm 
INNER JOIN gddt_cuentas c ON c.id_cuenta = fm.id_cuenta 
WHERE fm.id_formula_medica =" . $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);
$rows = mysqli_num_rows($queryUsuario);


$queryFormulas = $db->query($sqlUsuario);
$fetchFormulas = $queryFormulas->fetchAll(PDO::FETCH_OBJ);


if($rows == 0){
	echo "<p style='color:red;font-size:0.9em;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No se encontraron datos :(</p>";
}else{
	?>
	<div class="content">
		<h6 class="text-center mb-4">Actualizado por <?= $fetchUsuario[0];?> el <?= $fetchUsuario[1]; ?> a las <?= $fetchUsuario[2]; ?></h6>
		
		<div class="user-block col-sm-12 col-xs-12">
			<label>
				Formulas medicas
			</label>
		</div>
		<?php foreach ($fetchFormulas as $key) { ?>
			<p><?php echo $key->des_formula_medica; ?></p>
		<?php } ?>

		<div class="user-block">&nbsp;</div>
	</div><!-- Fin del content -->
	<?php } ?>