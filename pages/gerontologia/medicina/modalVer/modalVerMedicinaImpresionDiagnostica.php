<?php

//include connection file 
require "../../../../session.php";

$sqlUsuario = "
	SELECT 
	(CONVERT(CAST(CONVERT(CONCAT(UPPER(c.primer_nombre),' ',UPPER(c.segundo_nombre),' ',UPPER(c.primer_apellido),' ',UPPER(c.segundo_apellido)) USING latin1) AS BINARY) USING utf8)) AS nombreUsuario,
	CONCAT(idm.dia_mod,'/',idm.mes_mod,'/',idm.ano_mod) fecha,   
	idm.hora_mod,
	idm.des_impresion_diagnostica_medica,
	eca.des_enfermedad, 
	eca.codigo
	FROM gddt_impresiones_diagnosticas_medica idm
	INNER JOIN gddt_cuentas c ON c.id_cuenta = idm.id_cuenta 
	INNER JOIN gddt_enfermedades_cie_act eca ON eca.id_enfermedad = idm.id_enfermedad 
	WHERE idm.id_impresion_diagnostica_medica = " . $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);
$rows = mysqli_num_rows($queryUsuario);


if($rows == 0){
    echo "<p style='color:red;font-size:0.9em;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No se encontraron datos :(</p>";
}else{


?>

<div class="content">
<h6 class="text-center mb-4">Actualizado por <?= $fetchUsuario[0];?> el <?= $fetchUsuario[1]; ?> a las <?= $fetchUsuario[2]; ?></h6>
 <div class="user-block col-sm-4 col-xs-6">
    <label>
        Impresion diagnostica
    </label>
    <p><?php  if(empty($fetchUsuario[3])){echo 'Sin datos';}else{ echo $fetchUsuario[3];} ?></p>
</div>

 <div class="user-block col-sm-4 col-xs-6">
    <label>
        Enfermedad
    </label>
    <p><?php  if(empty($fetchUsuario[4])){echo 'Sin datos';}else{ echo $fetchUsuario[4];} ?></p>
</div>
 <div class="user-block col-sm-4 col-xs-6">
    <label>
        Codigo
    </label>
    <p><?php  if(empty($fetchUsuario[5])){echo 'Sin datos';}else{ echo $fetchUsuario[5];} ?></p>
</div>


<div class="user-block">&nbsp;</div>

</div><!-- Fin del content -->
<?php } ?>