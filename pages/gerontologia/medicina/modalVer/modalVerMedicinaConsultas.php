<?php

//include connection file 
require "../../../../session.php";

$sqlUsuario = "
SELECT 
(CONVERT(CAST(CONVERT(CONCAT(UPPER(c.primer_nombre),' ',UPPER(c.segundo_nombre),' ',UPPER(c.primer_apellido),' ',UPPER(c.segundo_apellido)) USING latin1) AS BINARY) USING utf8)) AS nombreUsuario,
CONCAT(co.dia_mod,'/',co.mes_mod,'/',co.ano_mod) fecha, 
co.hora_mod,
co.acompanante,
co.tel_acompanante,
co.fecha_ultima_citologia,
periodo_ciclo,
co.metodo_planificacion,
co.esquema_evacuacion_dia,
co.trastorno_ortopedico,
co.cual2,
co.otro_antecedente,
co.alergia,
co.quirurgico,
co.droga_sicoactiva,
co.cigarrillo,
co.cuanto,
co.licor,
co.frecuencia,
(CONVERT(CAST(CONVERT(co.medicamento_consume USING latin1) AS BINARY) USING utf8))AS medicamento_consume,
co.motivo_consulta, 
(CONVERT(CAST(CONVERT(co.enfermedad_actual USING latin1) AS BINARY) USING utf8))AS enfermedad_actual,
co.descripcion_profesional
FROM gddt_consultas co 
INNER JOIN gddt_cuentas c ON c.id_cuenta = co.id_cuenta 
AND co.id_consulta = " . $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);
$rows = mysqli_num_rows($queryUsuario);

if($rows == 0){
    echo "<p style='color:red;font-size:0.9em;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No se encontraron datos :(</p>";
}else{



?>

<div class="content">
<h6 class="text-center mb-4">Actualizado por <?= $fetchUsuario[0];?> el <?= $fetchUsuario[1]; ?> a las <?= $fetchUsuario[2]; ?></h6>
 <div class="user-block col-sm-4 col-xs-6">
    <label>
        Acompañante
    </label>
    <p><?php if(empty($fetchUsuario[3])){echo "Sin dato";}else{echo $fetchUsuario[3];} ?></p>
</div>
 <div class="user-block col-sm-4 col-xs-6">
    <label>
        Telefono del acompañante
    </label>
    <p><?php if(empty($fetchUsuario[4])){echo "Sin dato";}else{echo $fetchUsuario[4];} ?></p>
</div>
 <div class="user-block col-sm-4 col-xs-6">
    <label>
       Descripcion Profesional
    </label>
    <p><?php if(empty($fetchUsuario[22])){echo "Sin dato";}else{echo $fetchUsuario[22];} ?></p>
</div>
 <div class="user-block col-sm-4 col-xs-6">
    <label>
        Motivo de la consulta
    </label>
    <p><?php if(empty($fetchUsuario[20])){echo "Sin dato";}else{echo $fetchUsuario[20];} ?></p>
</div>
 <div class="user-block col-sm-4 col-xs-6">
    <label>
        Fecha de la ultima citologia
    </label>
    <p><?php if(empty($fetchUsuario[5])){echo "Sin dato";}else{echo $fetchUsuario[5];} ?></p>
</div>
 <div class="user-block col-sm-4 col-xs-6">
    <label>
        Periodo ciclo
    </label>
    <p><?php if(empty($fetchUsuario[6])){echo "Sin dato";}else{echo $fetchUsuario[6];} ?></p>
</div>
 <div class="user-block col-sm-4 col-xs-6">
    <label>
        Metodo de planificacion
    </label>
    <p><?php if(empty($fetchUsuario[7])){echo "Sin dato";}else{echo $fetchUsuario[7];} ?></p>
</div>
 <div class="user-block col-sm-4 col-xs-6">
    <label>
        Esquema evacuacion dia
    </label>
    <p><?php if(empty($fetchUsuario[8])){echo "Sin dato";}else{echo $fetchUsuario[8];} ?></p>
</div>
 <div class="user-block col-sm-4 col-xs-6">
    <label>
        Transtorno ortopedico
    </label>
    <p><?php if(empty($fetchUsuario[9])){echo "Sin dato";}else{echo $fetchUsuario[9];} ?></p>
</div>
 <div class="user-block col-sm-4 col-xs-6">
    <label>
        Cual
    </label>
    <p><?php if(empty($fetchUsuario[10])){echo "Sin dato";}else{echo $fetchUsuario[10];} ?></p>
</div>
 <div class="user-block col-sm-4 col-xs-6">
    <label>
        Otro antecedente
    </label>
    <p><?php if(empty($fetchUsuario[11])){echo "Sin dato";}else{echo $fetchUsuario[11];} ?></p>
</div>
 <div class="user-block col-sm-4 col-xs-6">
    <label>
        Alergia
    </label>
    <p><?php if(empty($fetchUsuario[12])){echo "Sin dato";}else{echo $fetchUsuario[12];} ?></p>
</div>
 <div class="user-block col-sm-4 col-xs-6">
    <label>
        Quirurgico
    </label>
    <p><?php if(empty($fetchUsuario[13])){echo "Sin dato";}else{echo $fetchUsuario[13];} ?></p>
</div>
 <div class="user-block col-sm-4 col-xs-6">
    <label>
        Droga sicoactiva
    </label>
    <p><?php if(empty($fetchUsuario[14])){echo "Sin dato";}else{echo $fetchUsuario[14];} ?></p>
</div>
 <div class="user-block col-sm-4 col-xs-6">
    <label>
        Cigarrillo
    </label>
    <p><?php if(empty($fetchUsuario[15])){echo "Sin dato";}else{echo $fetchUsuario[15];} ?></p>
</div>
 <div class="user-block col-sm-4 col-xs-6">
    <label>
        Cuanto al dia
    </label>
    <p><?php if(empty($fetchUsuario[16])){echo "Sin dato";}else{echo $fetchUsuario[16];} ?></p>
</div>
 <div class="user-block col-sm-4 col-xs-6">
    <label>
        Licor
    </label>
    <p><?php if(empty($fetchUsuario[17])){echo "Sin dato";}else{echo $fetchUsuario[17];} ?></p>
</div>
 <div class="user-block col-sm-4 col-xs-6">
    <label>
        Frecuencia
    </label>
    <p><?php if(empty($fetchUsuario[18])){echo "Sin dato";}else{echo $fetchUsuario[18];} ?></p>
</div>
 <div class="user-block col-sm-4 col-xs-6">
    <label>
        Medicamento que consume
    </label>
    <p><?php if(empty($fetchUsuario[19])){echo "Sin dato";}else{echo $fetchUsuario[19];} ?></p>
</div>

 <div class="user-block col-sm-4 col-xs-6">
    <label>
        Enfermedad actual
    </label>
    <p><?php if(empty($fetchUsuario[21])){echo "Sin dato";}else{echo $fetchUsuario[21];} ?></p>
</div>


<div class="user-block">&nbsp;</div>

</div><!-- Fin del content -->
<?php } ?>