<?php

//include connection file 
require "../../../../session.php";

$sqlUsuario = "
SELECT 
(CONVERT(CAST(CONVERT(CONCAT(UPPER(c.primer_nombre),' ',UPPER(c.segundo_nombre),' ',UPPER(c.primer_apellido),' ',UPPER(c.segundo_apellido)) USING latin1) AS BINARY) USING utf8)) AS nombreUsuario,
CONCAT(ef.dia_mod,'/',ef.mes_mod,'/',ef.ano_mod) fecha,    
ef.hora_mod,
ef.cabeza, 
ef.cuello, 
ef.torax, 
ef.aparato_respiratorio, 
ef.aparato_cardiovascular, 
ef.abdomen, 
ef.pelvis, 
ef.aparato_genitourinario, 
ef.sistema_nervioso, 
ef.osteomuscular 
FROM gddt_examen_fisico ef 
INNER JOIN gddt_cuentas c ON c.id_cuenta = ef.id_cuenta 
WHERE ef.id_examen_fisico =" . $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);
$rows = mysqli_num_rows($queryUsuario);


if($rows == 0){
    echo "<p style='color:red;font-size:0.9em;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No se encontraron datos :(</p>";
}else{


?>

<div class="content">
<h6 class="text-center mb-4">Actualizado por <?= $fetchUsuario[0];?> el <?= $fetchUsuario[1]; ?> a las <?= $fetchUsuario[2]; ?></h6>
 <div class="user-block col-sm-4 col-xs-6">
    <label>
        Cabeza
    </label>
    <p><?php  if(empty($fetchUsuario[3])){echo 'Sin datos';}else{ echo $fetchUsuario[3];} ?></p>
</div>
 <div class="user-block col-sm-4 col-xs-6">
    <label>
        Cuello
    </label>
    <p><?php  if(empty($fetchUsuario[4])){echo 'Sin datos';}else{ echo $fetchUsuario[4];} ?></p>
</div>
 <div class="user-block col-sm-4 col-xs-6">
    <label>
        Torax
    </label>
    <p><?php  if(empty($fetchUsuario[5])){echo 'Sin datos';}else{ echo $fetchUsuario[5];} ?></p>
</div>
 <div class="user-block col-sm-4 col-xs-6">
    <label>
        Aparato respiratorio
    </label>
    <p><?php  if(empty($fetchUsuario[6])){echo 'Sin datos';}else{ echo $fetchUsuario[6];} ?></p>
</div>
 <div class="user-block col-sm-4 col-xs-6">
    <label>
        Aparato cardiovascular
    </label>
    <p><?php  if(empty($fetchUsuario[7])){echo 'Sin datos';}else{ echo $fetchUsuario[7];} ?></p>
</div>
 <div class="user-block col-sm-4 col-xs-6">
    <label>
        Abdomen
    </label>
    <p><?php  if(empty($fetchUsuario[8])){echo 'Sin datos';}else{ echo $fetchUsuario[8];} ?></p>
</div>
 <div class="user-block col-sm-4 col-xs-6">
    <label>
        Pelvis
    </label>
    <p><?php  if(empty($fetchUsuario[9])){echo 'Sin datos';}else{ echo $fetchUsuario[9];} ?></p>
</div>
 <div class="user-block col-sm-4 col-xs-6">
    <label>
        Aparato genitourinario
    </label>
    <p><?php  if(empty($fetchUsuario[10])){echo 'Sin datos';}else{ echo $fetchUsuario[10];} ?></p>
</div>
 <div class="user-block col-sm-4 col-xs-6">
    <label>
        Sistema nervioso
    </label>
    <p><?php  if(empty($fetchUsuario[11])){echo 'Sin datos';}else{ echo $fetchUsuario[11];} ?></p>
</div>
 <div class="user-block col-sm-4 col-xs-6">
    <label>
        Osteomuscular
    </label>
    <p><?php  if(empty($fetchUsuario[12])){echo 'Sin datos';}else{ echo $fetchUsuario[12];} ?></p>
</div>


<div class="user-block">&nbsp;</div>

</div><!-- Fin del content -->
<?php } ?>