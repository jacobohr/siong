<?php

//include connection file 
require "../../../../session.php";

$sqlUsuario = "
SELECT
UPPER((CONVERT(CAST(CONVERT(CONCAT(c.primer_nombre,' ',c.segundo_nombre,' ',c.primer_apellido,' ',c.segundo_apellido) USING latin1) AS BINARY) USING utf8))) AS nombreUsuario,
CONCAT(om.ano_mod,'/',om.mes_mod,'/',om.dia_mod) AS fecha, 
om.hora_mod,
des_orden_medica descripcion
FROM gddt_ordenes_medicas om
INNER JOIN gddt_cuentas c ON c.id_cuenta = om.id_cuenta 
AND om.id_orden_medica = " . $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);
$rows = mysqli_num_rows($queryUsuario);

$sqlDatosUsuarioPDO = "
SELECT
UPPER((CONVERT(CAST(CONVERT(CONCAT(c.primer_nombre,' ',c.segundo_nombre,' ',c.primer_apellido,' ',c.segundo_apellido) USING latin1) AS BINARY) USING utf8))) AS nombreUsuario,
CONCAT(om.ano_mod,'/',om.mes_mod,'/',om.dia_mod) AS fecha, 
om.hora_mod,
(CONVERT(CAST(CONVERT(des_orden_medica USING latin1) AS BINARY) USING utf8))AS descripcion
FROM gddt_ordenes_medicas om
INNER JOIN gddt_cuentas c ON c.id_cuenta = om.id_cuenta 
WHERE om.id_orden_medica = " . $_REQUEST["idu"];
$queryDatosUsuarioPDO = $db->query($sqlDatosUsuarioPDO);
$fetchDatosUsuarioPDO = $queryDatosUsuarioPDO->fetchAll(PDO::FETCH_OBJ);


if($rows == 0){
	echo "<p style='color:red;font-size:0.9em;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No se encontraron datos :(</p>";
}else{


	?>

	<div class="content">
		<h6 class="text-center mb-4">Actualizado por <?= $fetchUsuario[0];?> el <?= $fetchUsuario[1]; ?> a las <?= $fetchUsuario[2]; ?></h6>
		<div class="user-block col-sm-12 col-xs-12">
			<label>
				Orden Medica
			</label>
			<?php 
			if(empty($fetchDatosUsuarioPDO)){
				echo "<p>Sin dato</p>";
			}else{
				foreach ($fetchDatosUsuarioPDO as $fetch) {
					echo "<p>{$fetch->descripcion}</p>";
				}
			}
			?>
		</div>


		<div class="user-block">&nbsp;</div>

	</div><!-- Fin del content -->
	<?php } ?>