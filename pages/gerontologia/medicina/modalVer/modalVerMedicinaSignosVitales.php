<?php

//include connection file 
require "../../../../session.php";

$sqlUsuario = "
SELECT 
(CONVERT(CAST(CONVERT(CONCAT(UPPER(c.primer_nombre),' ',UPPER(c.segundo_nombre),' ',UPPER(c.primer_apellido),' ',UPPER(c.segundo_apellido)) USING latin1) AS BINARY) USING utf8)) AS nombreUsuario,
CONCAT(sv.dia_mod,'/',sv.mes_mod,'/',sv.ano_mod) fecha,
sv.hora_mod, 
sv.frecuencia_cardiaca, 
sv.presion_arterial_sistolica, 
sv.presion_arterial_diastolica, 
sv.frecuencia_respiratoria, sv.temperatura 
FROM gddt_signos_vitales sv
INNER JOIN gddt_cuentas c ON c.id_cuenta = sv.id_cuenta 
WHERE sv.id_signos_vitales = " . $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);
$rows = mysqli_num_rows($queryUsuario);

if($rows == 0){
    echo "<p style='color:red;font-size:0.9em;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No se encontraron datos :(</p>";
}else{

?>

<div class="content">
<h6 class="text-center mb-4">Actualizado por <?= $fetchUsuario[0];?> el <?= $fetchUsuario[1]; ?> a las <?= $fetchUsuario[2]; ?></h6>
 <div class="user-block col-sm-4 col-xs-6">
    <label>
        Frecuencia cardiaca
    </label>
    <p><?php if(empty($fetchUsuario[3])){echo "Sin dato";}else{echo $fetchUsuario[3];} ?></p>
</div>

 <div class="user-block col-sm-4 col-xs-6">
    <label>
        Presión arterial sistólica
    </label>
    <p><?php  if(empty($fetchUsuario[4])){echo "Sin dato";}else{echo $fetchUsuario[4];} ?></p>
</div>
 <div class="user-block col-sm-4 col-xs-6">
    <label>
        Presión arterial diastólica
    </label>
    <p><?php  if(empty($fetchUsuario[5])){echo "Sin dato";}else{echo $fetchUsuario[5];} ?></p>
</div>
 <div class="user-block col-sm-4 col-xs-6">
    <label>
        Frecuencia respiratoria
    </label>
    <p><?php  if(empty($fetchUsuario[6])){echo "Sin dato";}else{echo $fetchUsuario[6];} ?></p>
</div>
 <div class="user-block col-sm-4 col-xs-6">
    <label>
        Temperatura
    </label>
    <p><?php  if(empty($fetchUsuario[7])){echo "Sin dato";}else{echo $fetchUsuario[7];} ?></p>
</div>	

<div class="user-block">&nbsp;</div>

</div><!-- Fin del content -->
<?php } ?>