<?php require "../../../../session.php";



$sqlUsuario = "SELECT * FROM gddt_antecedentes_personales_medica WHERE id_antecedente_personal_medica = ". $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);

?>
<form name="editarUsuario" method="POST" method="POST" action="<?php echo $base_url ?>pages/gerontologia/medicina/procesarDatos/procesarMedicinaAntecedentePersonal.php?tipo=update&idRegistro=<?= $_REQUEST["idu"] ?>&idMatriculaDato=<?= $fetchUsuario[6] ?>" autocomplete="off"> 
	<h4 class="text-center mb-4"> Antecedente Personal</h4>
	<div class="row">
		<?php 
		if(strlen($fetchUsuario[3]) < 2){$mes = '0'.$fetchUsuario[3];}else{$mes = $fetchUsuario[3];}
		if(strlen($fetchUsuario[4]) < 2){$dia = '0'.$fetchUsuario[4];}else{$dia = $fetchUsuario[4];}
		$year = $fetchUsuario[2];
		$fecha = $year.'-'.$mes.'-'.$dia;
		?>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label">Fecha<i style="color: darkorange">*</i></label>
			<input type="date" class="form-control" name="fecha" value="<?= $fecha ?>" />
		</div>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label"> Descripcion antecedente personal medica <i style="color: darkorange">*</i></label>
			<textarea class="form-control" name="desAntecedentePersonalMedica" cols="40" rows="5" ><?= $fetchUsuario[7] ?></textarea>
		</div>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label"> Personal </label>
			<textarea class="form-control" name="personal" cols="40" rows="5" ><?= $fetchUsuario[8] ?></textarea>
		</div>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label"> Familiar </label>
			<textarea class="form-control" name="familiar" cols="40" rows="5" ><?= $fetchUsuario[9] ?></textarea>
		</div>
	</div>
	<!-- End Row -->
	<!-- Modal Footer -->
	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">
			Cancelar
		</button>
		<button type="submit" class="btn btn-primary" name="Actualizar">
			Actualizar
		</button>
	</div>   
</form>