<?php require "../../../../session.php";



$sqlUsuario = "SELECT *,
(CONVERT(CAST(CONVERT(resultado USING latin1) AS BINARY) USING utf8))AS resultado,
(CONVERT(CAST(CONVERT(orden_medica USING latin1) AS BINARY) USING utf8))AS orden_medica
FROM gddt_resultado_examenes 
WHERE id_resultado_examen = ". $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);

?>
<form name="editarUsuario" method="POST" method="POST" action="<?php echo $base_url ?>pages/gerontologia/medicina/procesarDatos/procesarMedicinaResultadoExamenes.php?tipo=update&idRegistro=<?= $_REQUEST["idu"] ?>&idMatriculaDato=<?= $fetchUsuario[1] ?>" autocomplete="off">
	<h4 class="text-center mb-4"> Resultado Examen</h4>
	<div class="row">
		<?php 
		if(strlen($fetchUsuario[4]) < 2){$mes = '0'.$fetchUsuario[4];}else{$mes = $fetchUsuario[4];}
		if(strlen($fetchUsuario[3]) < 2){$dia = '0'.$fetchUsuario[3];}else{$dia = $fetchUsuario[3];}
		$year = $fetchUsuario[5];
		$fecha = $year.'-'.$mes.'-'.$dia;
		?>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label">Fecha <i style="color: darkorange">*</i></label>
			<input type="date" class="form-control" name="fecha" value="<?= $fecha ?>"/>
		</div>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label"> Resultado <i style="color: darkorange">*</i></label>
			<textarea class="form-control" name="resultado" cols="40" rows="5" ><?= $fetchUsuario[9] ?></textarea>
		</div>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label"> Orden medica <i style="color: darkorange">*</i></label>
			<textarea class="form-control" name="ordenMedica" cols="40" rows="5" ><?= $fetchUsuario[10] ?></textarea>
		</div>
	</div>
	<!-- End Row -->
	<!-- Modal Footer -->
	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">
			Cancelar
		</button>
		<button type="submit" class="btn btn-primary" name="Actualizar">
			Actualizar
		</button>
	</div>
</form>    