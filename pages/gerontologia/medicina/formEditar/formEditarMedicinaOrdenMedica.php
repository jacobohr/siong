<?php require "../../../../session.php";



$sqlUsuario = "SELECT
*,
(CONVERT(CAST(CONVERT(des_orden_medica USING latin1) AS BINARY) USING utf8))AS descripcion
FROM gddt_ordenes_medicas 
WHERE id_orden_medica = ". $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);

$queryOrdenMedica = $db->query($sqlUsuario);
$fetchOrdenMedica = $queryOrdenMedica->fetchAll(PDO::FETCH_OBJ);
$contador = 0;
?>
<form name="editarUsuario" method="POST" method="POST" action="<?php echo $base_url ?>pages/gerontologia/medicina/procesarDatos/procesarMedicinaOrdenesMedicas.php?tipo=update&idRegistro=<?= $_REQUEST["idu"] ?>&idMatriculaDato=<?= $fetchUsuario[6] ?>" autocomplete="off"> 
	<h4 class="text-center mb-4"> Orden Medica</h4>
	<div class="row">
		<?php 
		if(strlen($fetchUsuario[3]) < 2){$mes = '0'.$fetchUsuario[3];}else{$mes = $fetchUsuario[3];}
		if(strlen($fetchUsuario[4]) < 2){$dia = '0'.$fetchUsuario[4];}else{$dia = $fetchUsuario[4];}
		$year = $fetchUsuario[2];
		$fecha = $year.'-'.$mes.'-'.$dia;
		?>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label">Fecha<i style="color: darkorange">*</i></label>
			<input type="date" class="form-control" name="fecha" value="<?= $fecha ?>"/>
		</div>
		<?php foreach ($fetchOrdenMedica as $key) { $contador++;?>
			<div class="form-group col-md-12">
				<label for="message-text" class="form-control-label">Descripcion de la Orden Medica </label>
				<textarea class="form-control" name="ordenesMedicas[]" cols="40" rows="5" ><?php echo $key->descripcion; ?></textarea>
			</div>
		<?php } ?>
		<?php 
		$contador++;
		if($contador <= 10){
			while($contador <= 10){
				echo '<div class="form-group col-md-12">';
				echo '<label for="message-text" class="form-control-label">Descripcion de la Orden Medica </label>';
				echo '<textarea class="form-control" name="ordenesMedicas[]" cols="40" rows="5" >'. $contador.'.'.'</textarea>';
				echo '</div>';
				$contador++; 
			} 
		} 
		?>
	</div>
	<!-- End Row -->


	<!-- Modal Footer -->
	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">
			Cancelar
		</button>
		<button type="submit" class="btn btn-primary" name="Actualizar">
			Actualizar
		</button>
	</div>
</div>    