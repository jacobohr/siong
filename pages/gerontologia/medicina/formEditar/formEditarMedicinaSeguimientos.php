<?php require "../../../../session.php";



$sqlUsuario = "SELECT *,(CONVERT(CAST(CONVERT(seguimiento_descripcion USING latin1) AS BINARY) USING utf8))AS seguimiento_descripcion FROM gddt_seguimiento_medico WHERE id_seguimiento_medico =  ". $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);

?>
<form name="editarUsuario" method="POST" method="POST" action="<?php echo $base_url ?>pages/gerontologia/medicina/procesarDatos/procesarMedicinaSeguimiento.php?tipo=update&idRegistro=<?= $_REQUEST["idu"] ?>&idMatriculaDato=<?= $fetchUsuario[6] ?>" autocomplete="off"> 
	<h4 class="text-center mb-4"> Seguimientos</h4>
	<div class="row">
		<?php 
		if(strlen($fetchUsuario[3]) < 2){$mes = '0'.$fetchUsuario[3];}else{$mes = $fetchUsuario[3];}
		if(strlen($fetchUsuario[4]) < 2){$dia = '0'.$fetchUsuario[4];}else{$dia = $fetchUsuario[4];}
		$year = $fetchUsuario[2];
		$fecha = $year.'-'.$mes.'-'.$dia;
		?>
		<div class="form-group col-md-6">
			<label for="message-text" class="form-control-label">Fecha <i style="color: darkorange">*</i></label>
			<input type="date" class="form-control" name="fecha" value="<?= $fecha ?>"/>
		</div>
		<div class="form-group col-md-6">
			<label for="recipient-name" class="form-control-label">Tipo de seguimiento <i style="color: darkorange">*</i></label>
			<select class="form-control" name="tipoSeguimiento" required="">
				<option value="">Selecciona una opción</option>
				<?php
				$queryCuenta = mysqli_query($conn, "SELECT id_tipo_seguimiento, descripcion FROM gddt_tipo_seguimientos;");
				while ($fetchCuenta = mysqli_fetch_row($queryCuenta)) {
					if ($fetchCuenta[0] == $fetchUsuario[7]) {
						echo "<option selected value=" . $fetchCuenta[0] . ">" . $fetchCuenta[1] . "</option>";
					} else {
						echo "<option value=" . $fetchCuenta[0] . ">" . $fetchCuenta[1] . "</option>";
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label">Descripcion del seguimiento <i style="color: darkorange">*</i></label>
			<textarea class="form-control" name="seguimientoDescripcion" cols="40" rows="5" required="true"><?= $fetchUsuario[9] ?></textarea>
		</div>
	</div>
	<!-- End Row -->


	<!-- Modal Footer -->
	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">
			Cancelar
		</button>
		<button type="submit" class="btn btn-primary" name="Actualizar">
			Actualizar
		</button>
	</div>
</div>    