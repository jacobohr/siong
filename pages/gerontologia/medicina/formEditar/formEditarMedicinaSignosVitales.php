<?php require "../../../../session.php";



$sqlUsuario = "SELECT * FROM gddt_signos_vitales WHERE id_signos_vitales = ". $_REQUEST["idu"];
$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);

?>
<form id="formEditarMedicinaSignosVitales" method="POST" action="<?php echo $base_url ?>pages/gerontologia/medicina/procesarDatos/procesarMedicinaSignosVitales.php?tipo=update&idRegistro=<?= $_REQUEST["idu"] ?>&idMatriculaDato=<?= $fetchUsuario[6] ?>" autocomplete="off"> 
	<h4 class="text-center mb-4"> Signos Vitales</h4>
	<div class="row">
		<?php 
		if(strlen($fetchUsuario[3]) < 2){$mes = '0'.$fetchUsuario[3];}else{$mes = $fetchUsuario[3];}
		if(strlen($fetchUsuario[4]) < 2){$dia = '0'.$fetchUsuario[4];}else{$dia = $fetchUsuario[4];}
		$year = $fetchUsuario[2];
		$fecha = $year.'-'.$mes.'-'.$dia;
		?>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Fecha<i style="color: darkorange">*</i></label>
			<input type="date" class="form-control" name="fecha" value="<?= $fecha ?>" required="true"/>
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Frecuencia cardiaca <i style="color: darkorange">*</i></label>
			<input type="number" class="form-control" name="frecuenciaCardiaca" value="<?= $fetchUsuario[7]; ?>"  required="true"/>
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Presion arterial sistolica <i style="color: darkorange">*</i></label>
			<input type="number" class="form-control" name="presionSistolica" value="<?= $fetchUsuario[8]; ?>"  required="true"/>
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Presion arterial diastolica <i style="color: darkorange">*</i></label>
			<input type="number" class="form-control" name="presionDiastolica" value="<?= $fetchUsuario[9]; ?>"  required="true"/>
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Frecuencia respiratoria <i style="color: darkorange">*</i></label>
			<input type="number" class="form-control" name="frecuenciaRespiratoria" value="<?= $fetchUsuario[10]; ?>"  required="true"/>
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Temperatura <i style="color: darkorange">*</i></label>
			<input type="number" class="form-control" name="temperatura" value="<?= $fetchUsuario[11]; ?>"  required="true"/>
		</div>	
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Peso </label>
			<input type="number" class="form-control" name="peso" value="<?= $fetchUsuario[12]; ?>"  required="true"/>
		</div>	
	</div>
	<!-- End Row -->
	<!-- Modal Footer -->
	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">
			Cancelar
		</button>
		<button type="submit" class="btn btn-primary">
			Actualizar
		</button>
	</div>
</form>