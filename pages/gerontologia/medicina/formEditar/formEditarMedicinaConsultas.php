<?php require "../../../../session.php";



$sqlUsuario = "SELECT * FROM gddt_consultas WHERE id_consulta = ". $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);

?>
<form name="editarUsuario" method="POST" method="POST" action="<?php echo $base_url ?>pages/gerontologia/medicina/procesarDatos/procesarMedicinaConsulta.php?tipo=update&idRegistro=<?= $_REQUEST["idu"] ?>&idMatriculaDato=<?= $fetchUsuario[6] ?>" autocomplete="off"> 
	<h4 class="text-center mb-4"> Consultas</h4>
	<div class="row">
		<?php 
		if(strlen($fetchUsuario[3]) < 2){$mes = '0'.$fetchUsuario[3];}else{$mes = $fetchUsuario[3];}
		if(strlen($fetchUsuario[4]) < 2){$dia = '0'.$fetchUsuario[4];}else{$dia = $fetchUsuario[4];}
		$year = $fetchUsuario[2];
		$fecha = $year.'-'.$mes.'-'.$dia;
		?>
		<div class="form-group col-md-6">
			<label for="message-text" class="form-control-label"> Acompañante <i style="color: darkorange">*</i></label>
			<input type="text" class="form-control" name="acompanante" required="true" value="<?php echo $fetchUsuario[7];?>" />
		</div>
		<div class="form-group col-md-6">
			<label for="message-text" class="form-control-label"> Telefono del Acompañante <i style="color: darkorange">*</i></label>
			<input type="number" class="form-control" name="telAcompanante" required="true" value="<?php echo $fetchUsuario[8];?>"/>
		</div>
		<div class="form-group col-md-6">
			<label for="message-text" class="form-control-label"> Motivo de la consulta <i style="color: darkorange">*</i></label>
			<textarea class="form-control" name="motivoConsulta" cols="40" rows="5" required="true"><?php echo $fetchUsuario[27]; ?></textarea>
		</div>	
		<div class="form-group col-md-6">
			<label for="message-text" class="form-control-label">Enfermedad actual <i style="color: darkorange">*</i></label>
			<textarea class="form-control" name="enfermedadActual" cols="40" rows="5" required="true"><?php echo $fetchUsuario[28]; ?></textarea>
		</div>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label">Descripcion profesional <i style="color: darkorange">*</i></label>
			<textarea class="form-control" name="descripcionProfesional" cols="40" rows="5" required="true"><?php echo $fetchUsuario[28]; ?></textarea>
		</div>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label">Antecedentes médicos personales <i style="color: darkorange">*</i></label>
			<?php 
			$selectNames = array(
				0 => 'HTA',
				1 => 'Diabetes',
				2 => 'Asma',
				3 => 'TB',
				4 => 'Cancer',
				5 => 'ITS',
				6 => 'EPOC',
				7 => 'Epilepsia',
				8 => 'Transt. Mental',
				9 => 'Transfuciones',
				10 => 'Hipertencion',
				11 => 'Dislipedimia',
				12 => 'Otros',
				13 => 'Ninguno'
			);
			$sqlAntcMedPersonal = "SELECT des_antecedente_medico_personal, otros FROM gddt_consultas_antecedentes_medicos_personales WHERE id_consulta = ". $_REQUEST["idu"]. ' LIMIT 1;';
			$queryAntcMedPersonal = $db->query($sqlAntcMedPersonal);
			$fetchAntcMedPersonal = $queryAntcMedPersonal->fetchAll(PDO::FETCH_OBJ);
			$valor = '';
			$otros = '';
			foreach ($fetchAntcMedPersonal as $key) {
				$valor = $key->des_antecedente_medico_personal;
				$otros = $key->otros;
			}
			?>
			<select class="form-control" name="antecedenteMedicoPersonal" id="antecedenteMedicoPersonal" required="true">
				<option value="">Selecciona una opción</option>
				<?php
				foreach ($selectNames as $key) {
					if($key == $valor){
						echo '<option selected value='.$key.'>'.$key.'</option>';
					}else{
						echo '<option value='.$key.'>'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label">Cuales </label>
			<textarea class="form-control" name="antecedenteMedicoPersonalCual" cols="40" rows="5" ><?php echo $otros;	 ?></textarea>
		</div>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label">Antecedentes médicos familiares</label>
			<select class="form-control" name="antecedenteMedicoFamiliares" id="antecedenteMedicoFamiliares" required="true">
				<option value="0">Selecciona una opción</option>
				<?php
				$sqlAntcMedFamiliar = "SELECT des_antecedente_medico_familiar, otros FROM gddt_cosultas_antecentes_medicos_familiares WHERE id_consulta = ". $_REQUEST["idu"]. ' LIMIT 1;';
				$queryAntcMedFamiliar = $db->query($sqlAntcMedFamiliar);
				$fetchAntcMedFamiliar = $queryAntcMedFamiliar->fetchAll(PDO::FETCH_OBJ);
				$valorFamiliar = '';
				$otrosFamiliar = '';
				foreach ($fetchAntcMedFamiliar as $key) {
					$valorFamiliar = $key->des_antecedente_medico_familiar;
					$otrosFamiliar = $key->otros;
				}
				foreach ($selectNames as $key) {
					if($key == $valorFamiliar){
						echo '<option selected value='.$key.'>'.$key.'</option>';
					}else{
						echo '<option value='.$key.'>'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label">Cuales </label>
			<textarea class="form-control" name="antecedenteMedicoFamiliaresCual" cols="40" rows="5" ><?php echo $otrosFamiliar; ?></textarea>
		</div>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label">Antecedentes gineco obstétricos</label>
			<?php 
			$selectNames = array(
				0 => 'Gestas',
				1 => 'Partos',
				2 => 'Abortos',
				3 => 'TB',
				4 => 'Cesareas',
				5 => 'Vivos',
				6 => 'Menarquia',
				7 => 'Menopausia',
				8 => 'Ninguno'
			);
			$selectNamesAntcGineObste = array(
				0 => 'FUM',
				1 => 'FPP',
				2 => 'FUP',
				3 => 'Otros',
				4 => 'Ninguno'
			);
			$sqlAntcMedGinObsterico = "SELECT des_antecedente_gineco_obstetrico FROM gddt_consultas_antecedentes_gineco_obstetricos WHERE id_consulta =  ". $_REQUEST["idu"]. ' LIMIT 1;';
			$queryAntcMedGinObsterico = $db->query($sqlAntcMedGinObsterico);
			$fetchAntcMedGinObsterico = $queryAntcMedGinObsterico->fetchAll(PDO::FETCH_OBJ);
			$valorGinObsterico = '';
			foreach ($fetchAntcMedGinObsterico as $key) {
				$valorGinObsterico = $key->des_antecedente_gineco_obstetrico;
			}
			?>
			<select class="form-control" name="antecedenteGinecoObstetricos" id="antecedenteGinecoObstetricos" required="true">
				<option value="">Selecciona una opción</option>
				<?php
				foreach ($selectNames as $key) {
					if($key == $valorGinObsterico){
						echo '<option selected value='.$key.'>'.$key.'</option>';
					}else{
						echo '<option value='.$key.'>'.$key.'</option>';
					}
					
				}
				?>
			</select>

		</div>
		<div class="form-group col-md-12">
			<select class="form-control" name="antecedenteGinecoObstetricos2" id="antecedenteGinecoObstetricos2" required="true">
				<option value="">Selecciona una opción</option>
				<?php
				$sqlAntcMedGinObstericoDos = "SELECT des_antecedente_gineco_obstetrico_2, otros FROM gddt_consultas_antecedentes_gineco_obstetricos_2 WHERE id_consulta =". $_REQUEST["idu"]. ' LIMIT 1;';
				$queryAntcMedGinObstericoDos = $db->query($sqlAntcMedGinObstericoDos);
				$fetchAntcMedGinObstericoDos = $queryAntcMedGinObstericoDos->fetchAll(PDO::FETCH_OBJ);
				$valorGinObstericoDos = '';
				$otrosGinObstericoDos = '';
				foreach ($fetchAntcMedGinObstericoDos as $key) {
					$valorGinObstericoDos = $key->des_antecedente_gineco_obstetrico_2;
					$otrosGinObstericoDos = $key->otros;
				}
				foreach ($selectNamesAntcGineObste as $key) {
					if($key == $valorGinObstericoDos){
						echo '<option selected value='.$key.'>'.$key.'</option>';
					}else{
						echo '<option value='.$key.'>'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label">Cuales </label>
			<textarea class="form-control" name="AntecedentesGinecoObstétricosCuales" cols="40" rows="5" ><?php echo $otrosGinObstericoDos; ?></textarea>
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Fecha ultima citologia <i style="color: darkorange">*</i></label>
			<input type="date" class="form-control" name="fechaUltimaCitologia" required="true" value="<?php echo $fetchUsuario[9]; ?>" />
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label"> Periodo ciclo <i style="color: darkorange">*</i></label>
			<input type="number" class="form-control" name="periodoCiclo" required="true" value="<?php echo $fetchUsuario[10]; ?>"/>
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label"> Metodo de planificacion <i style="color: darkorange">*</i></label>
			<input type="text" class="form-control" name="metodoPlanificacion" required="true" value="<?php echo $fetchUsuario[11]; ?>"/>
		</div>
		<div class="form-group col-md-12" style="margin-bottom: -8px"><h4 class="modal-title text-center mb-4">ANTECEDENTES PEDIATRICOS</h4></div>
		<div class="form-group col-md-6">
			<label for="message-text" class="form-control-label"> Eruptiva <i style="color: darkorange">*</i></label>
			<select class="form-control" name="eruptiva" id="eruptiva" required="true">
				<option value="">Selecciona una opción</option>
				<?php 
				if($fetchUsuario[12] == 'SI'){
					echo '<option selected value="SI">SI</option>';
					echo '<option  value="NO">NO</option>';
				}else if($fetchUsuario[12] == 'NO'){
					echo '<option value="SI">SI</option>';
					echo '<option selected value="NO">NO</option>';
				}else{
					echo '<option value="SI">SI</option>';
					echo '<option value="NO">NO</option>';
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-6">
			<label for="message-text" class="form-control-label"> Cuales</label>
			<input type="text" class="form-control" name="eruptivaCual" value="<?php echo $fetchUsuario[13]; ?>"/>
		</div>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label"> Esquema evacuacion dia <i style="color: darkorange">*</i></label>
			<input type="text" class="form-control" name="esquemaEvacuacionDia" value="<?php echo $fetchUsuario[14]; ?>" required="true"/>
		</div>
		<div class="form-group col-md-6">
			<label for="message-text" class="form-control-label"> Trastorno ortopedico <i style="color: darkorange">*</i></label>
			<select class="form-control" name="trastornoOrtopedico" id="trastornoOrtopedico" required="true">
				<option value="">Selecciona una opción</option>
				<?php 
				if($fetchUsuario[15] == 'SI'){
					echo '<option selected value="SI">SI</option>';
					echo '<option  value="NO">NO</option>';
				}else if($fetchUsuario[15] == 'NO'){
					echo '<option value="SI">SI</option>';
					echo '<option selected value="NO">NO</option>';
				}else{
					echo '<option value="SI">SI</option>';
					echo '<option value="NO">NO</option>';
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-6">
			<label for="message-text" class="form-control-label"> Cuales <i style="color: darkorange">*</i></label>
			<input type="text" class="form-control" name="cualTrastornoOrtopedico" value="<?php echo $fetchUsuario[16]; ?>"/>
		</div>
		<div class="form-group col-md-6">
			<label for="message-text" class="form-control-label"> Otro antecedente <i style="color: darkorange">*</i></label>
			<textarea class="form-control" name="otroAntecedente" cols="40" rows="5" ><?php echo $fetchUsuario[17]; ?></textarea>
		</div>
		<div class="form-group col-md-6">
			<label for="message-text" class="form-control-label"> Alergia <i style="color: darkorange">*</i></label>
			<textarea class="form-control" name="alergia" cols="40" rows="5" ><?php echo $fetchUsuario[18]; ?></textarea>
		</div>
		<div class="form-group col-md-6">
			<label for="message-text" class="form-control-label"> Quirurgico <i style="color: darkorange">*</i></label>
			<textarea class="form-control" name="quirurgico" cols="40" rows="5" ><?php echo $fetchUsuario[19]; ?></textarea>
		</div>
		<div class="form-group col-md-6">
			<label for="message-text" class="form-control-label"> Droga sicoactiva <i style="color: darkorange">*</i></label>
			<textarea class="form-control" name="drogasicoactiva" cols="40" rows="5" ><?php echo $fetchUsuario[20]; ?></textarea>
		</div>
		<div class="form-group col-md-6">
			<label for="message-text" class="form-control-label"> Cigarrillo <i style="color: darkorange">*</i></label>
			<select class="form-control" name="cigarrillo" id="cigarrillo" required="true">
				<option value="">Selecciona una opción</option>
				<?php 
				if($fetchUsuario[21] == 'SI'){
					echo '<option selected value="SI">SI</option>';
					echo '<option  value="NO">NO</option>';
				}else if($fetchUsuario[21] == 'NO'){
					echo '<option value="SI">SI</option>';
					echo '<option selected value="NO">NO</option>';
				}else{
					echo '<option value="SI">SI</option>';
					echo '<option value="NO">NO</option>';
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-6">
			<label for="message-text" class="form-control-label"> Cuanto al dia <i style="color: darkorange">*</i></label>
			<input type="text" class="form-control" name="cuantoCigarrillo" value="<?php echo $fetchUsuario[22]; ?>"/>
		</div>
		<div class="form-group col-md-6">
			<label for="message-text" class="form-control-label"> Licor <i style="color: darkorange">*</i></label>
			<select class="form-control" name="licor" id="licor" required="true">
				<option value="">Selecciona una opción</option>
				<?php 
				if($fetchUsuario[23] == 'SI'){
					echo '<option selected value="SI">SI</option>';
					echo '<option  value="NO">NO</option>';
				}else if($fetchUsuario[23] == 'NO'){
					echo '<option value="SI">SI</option>';
					echo '<option selected value="NO">NO</option>';
				}else{
					echo '<option value="SI">SI</option>';
					echo '<option value="NO">NO</option>';
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-6">
			<label for="message-text" class="form-control-label"> Frecuencia <i style="color: darkorange">*</i></label>
			<input type="text" class="form-control" name="frecuenciaLicor" value="<?php echo $fetchUsuario[24]; ?>"/>
		</div>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label"> Medicamento que consume <i style="color: darkorange">*</i></label>
			<textarea class="form-control" name="medicamentoConsume" cols="40" rows="5" ><?php echo $fetchUsuario[25]; ?></textarea>
		</div>	
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">
			Cancelar
		</button>
		<button type="submit" class="btn btn-info">
			Actualizar
		</button>
	</div>
</form>