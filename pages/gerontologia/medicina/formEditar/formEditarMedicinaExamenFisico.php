<?php require "../../../../session.php";



$sqlUsuario = "SELECT * FROM gddt_examen_fisico WHERE id_examen_fisico  = ". $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);

?>
<form name="editarUsuario" method="POST" method="POST" action="<?php echo $base_url ?>pages/gerontologia/medicina/procesarDatos/procesarMedicinaExamenFisico.php?tipo=update&idRegistro=<?= $_REQUEST["idu"] ?>&idMatriculaDato=<?= $fetchUsuario[6] ?>" autocomplete="off"> 
	<h4 class="text-center mb-4"> Examen Fisico</h4>
	<div class="row">
		<?php 
		if(strlen($fetchUsuario[3]) < 2){$mes = '0'.$fetchUsuario[3];}else{$mes = $fetchUsuario[3];}
		if(strlen($fetchUsuario[4]) < 2){$dia = '0'.$fetchUsuario[4];}else{$dia = $fetchUsuario[4];}
		$year = $fetchUsuario[2];
		$fecha = $year.'-'.$mes.'-'.$dia;
		?>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label">Fecha <i style="color: darkorange">*</i></label>
			<input type="date" class="form-control" name="fecha" value="<?= $fecha ?>"/>
		</div>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label"> Cabeza <i style="color: darkorange">*</i></label>
			<textarea class="form-control" name="cabeza" cols="40" rows="5" ><?= $fetchUsuario[7] ?></textarea>
		</div>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label"> Cuello <i style="color: darkorange">*</i></label>
			<textarea class="form-control" name="cuello" cols="40" rows="5" ><?= $fetchUsuario[8] ?></textarea>
		</div>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label"> Torax <i style="color: darkorange">*</i></label>
			<textarea class="form-control" name="torax" cols="40" rows="5" ><?= $fetchUsuario[9] ?></textarea>
		</div>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label"> Aparto respiratorio <i style="color: darkorange">*</i></label>
			<textarea class="form-control" name="aparatoRespiratorio" cols="40" rows="5" ><?= $fetchUsuario[10] ?></textarea>
		</div>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label"> Aparto cardiovascular <i style="color: darkorange">*</i></label>
			<textarea class="form-control" name="aparatoCardiovascular" cols="40" rows="5" ><?= $fetchUsuario[11] ?></textarea>
		</div>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label"> Abdomen <i style="color: darkorange">*</i></label>
			<textarea class="form-control" name="abdomen" cols="40" rows="5" ><?= $fetchUsuario[12] ?></textarea>
		</div>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label"> Pelvis <i style="color: darkorange">*</i></label>
			<textarea class="form-control" name="pelvis" cols="40" rows="5" ><?= $fetchUsuario[13] ?></textarea>
		</div>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label"> Aparato genitourinario <i style="color: darkorange">*</i></label>
			<textarea class="form-control" name="apartoGenitourinario" cols="40" rows="5" ><?= $fetchUsuario[14] ?></textarea>
		</div>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label"> Sistema nervioso <i style="color: darkorange">*</i></label>
			<textarea class="form-control" name="sistemaNervioso" cols="40" rows="5" ><?= $fetchUsuario[15] ?></textarea>
		</div>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label"> Osteomuscular <i style="color: darkorange">*</i></label>
			<textarea class="form-control" name="osteomuscular" cols="40" rows="5" ><?= $fetchUsuario[16] ?></textarea>
		</div>
	</div>
	<!-- End Row -->


	<!-- Modal Footer -->
	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">
			Cancelar
		</button>
		<button type="submit" class="btn btn-primary" name="Actualizar">
			Actualizar
		</button>
	</div>
</div>    