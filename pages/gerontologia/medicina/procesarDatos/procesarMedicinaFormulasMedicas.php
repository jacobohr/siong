<?php 
require "../../../../session.php";
error_reporting(E_ALL);
$tipo = $_REQUEST["tipo"];
$idMatriculaDato = $_REQUEST["idMatriculaDato"];
$idRegistro = $_REQUEST["idRegistro"];

$planTratamiento = $_POST['planTratamiento'];

$fecha = strtotime($_POST['fecha']);
$ano = date("Y",$fecha);
$dia = date("d",$fecha);
$mes = date("m",$fecha);
$hora = date("H:i");

$sqlCedula = "SELECT num_documento FROM gddt_matricula_datos WHERE id_matricula_dato =".$idMatriculaDato;
$queryCedula = mysqli_query($conn, $sqlCedula);
$fetchCedula = mysqli_fetch_row($queryCedula);

switch ($tipo) {
	case 'insert':
		/** TRAER ID DE LA OBRA**/
		$sqlIdFormulaMedica = "SELECT IFNULL(MAX(id_formula_medica)+1,1) maximo FROM gddt_formulas_medicas";
		$queryIdFormulaMedica = $db->query($sqlIdFormulaMedica);
		$fetchIdFormulaMedica = $queryIdFormulaMedica->fetchAll(PDO::FETCH_OBJ);
		foreach ($fetchIdFormulaMedica as $fetch) {
			$IdFormulaMedica = $fetch->maximo;
		}

		$arrayplanTratamiento = $_POST["planTratamiento"];
		$sizeArray = sizeof($arrayplanTratamiento);
		$i = 0;
		while($i < $sizeArray){
			if(substr($arrayplanTratamiento[$i],4) != ""){
				$insert = $db->prepare("INSERT INTO gddt_formulas_medicas (id_formula_medica, id_cuenta, ano_mod, mes_mod, dia_mod, hora_mod, id_matricula_dato, des_formula_medica) VALUES (:id_formula_medica, :id_cuenta, :ano_mod, :mes_mod, :dia_mod, :hora_mod, :id_matricula_dato, :des_formula_medica);");
				$insert->bindParam(':id_formula_medica', $IdFormulaMedica);
				$insert->bindParam(':id_cuenta', $_SESSION['ID_CUENTA']);
				$insert->bindParam(':ano_mod', $ano);
				$insert->bindParam(':mes_mod', $mes);
				$insert->bindParam(':dia_mod', $dia);
				$insert->bindParam(':hora_mod', $hora);
				$insert->bindParam(':id_matricula_dato', $idMatriculaDato);	
				$insert->bindParam(':des_formula_medica', $arrayplanTratamiento[$i]);
				$insert->execute();
			}
			$i++;
		}
		header('location:'.$base_url.'pages/gerontologia/pacientes.php?param='.$fetchCedula[0]);
		break;
	case 'update':
		$delete = $db->prepare("DELETE FROM gddt_formulas_medicas WHERE id_formula_medica='".$idRegistro."';");
		$delete->execute();

		$arrayplanTratamiento = $_POST["planTratamiento"];
		$sizeArray = sizeof($arrayplanTratamiento);
		$i = 0;
		while($i < $sizeArray){
			if(substr($arrayplanTratamiento[$i],4) != ""){
				$insert = $db->prepare("INSERT INTO gddt_formulas_medicas (id_formula_medica, id_cuenta, ano_mod, mes_mod, dia_mod, hora_mod, id_matricula_dato, des_formula_medica) VALUES (:id_formula_medica, :id_cuenta, :ano_mod, :mes_mod, :dia_mod, :hora_mod, :id_matricula_dato, :des_formula_medica);");
				$insert->bindParam(':id_formula_medica', $idRegistro);
				$insert->bindParam(':id_cuenta', $_SESSION['ID_CUENTA']);
				$insert->bindParam(':ano_mod', $ano);
				$insert->bindParam(':mes_mod', $mes);
				$insert->bindParam(':dia_mod', $dia);
				$insert->bindParam(':hora_mod', $hora);
				$insert->bindParam(':id_matricula_dato', $idMatriculaDato);	
				$insert->bindParam(':des_formula_medica', $arrayplanTratamiento[$i]);
				$insert->execute();
			}
			$i++;
		}
		header('location:'.$base_url.'pages/gerontologia/pacientes.php?param='.$fetchCedula[0]);
		break;
	case 'delete':
		$sqlBuscarCedula = "SELECT md.num_documento FROM gddt_formulas_medicas nd INNER JOIN gddt_matricula_datos md ON md.id_matricula_dato = nd.id_matricula_dato WHERE nd.id_formula_medica=".$idRegistro;
		$queryBuscarCedula = mysqli_query($conn, $sqlBuscarCedula);
		$fetchBuscarCedula = mysqli_fetch_row($queryBuscarCedula);
		
		$delete = $db->prepare("DELETE FROM gddt_formulas_medicas WHERE id_formula_medica='".$idRegistro."';");
		$delete->execute();
		
		header('location:'.$base_url.'pages/gerontologia/pacientes.php?param='.$fetchBuscarCedula[0]);
		break;

	break;

	default:
		# code...
	break;
}