<?php 
require "../../../../session.php";

$tipo = $_REQUEST["tipo"];
$idMatriculaDato = $_REQUEST["idMatriculaDato"];
$idRegistro = $_REQUEST["idRegistro"];

$desAntecedentePersonalMedica = $_POST['desAntecedentePersonalMedica'];
$personal = $_POST['personal'];
$familiar = $_POST['familiar'];

$fecha = strtotime($_POST['fecha']);
$ano = date("Y",$fecha);
$dia = date("d",$fecha);
$mes = date("m",$fecha);
$hora = date("H:i");

$sqlCedula = "SELECT num_documento FROM gddt_matricula_datos WHERE id_matricula_dato =".$idMatriculaDato;
$queryCedula = mysqli_query($conn, $sqlCedula);
$fetchCedula = mysqli_fetch_row($queryCedula);

switch ($tipo) {
	case 'insert':
		$insert = $db->prepare("INSERT INTO gddt_antecedentes_personales_medica (id_cuenta, ano_mod, mes_mod, dia_mod, hora_mod, id_matricula_dato, des_antecedente_personal_medica, personal,  familiar) VALUES (:id_cuenta, :ano_mod, :mes_mod, :dia_mod, :hora_mod, :id_matricula_dato, :des_antecedente_personal_medica, :personal, :familiar);");
		$insert->bindParam(':id_cuenta', $_SESSION['ID_CUENTA']);
		$insert->bindParam(':ano_mod', $ano);
		$insert->bindParam(':mes_mod', $mes);
		$insert->bindParam(':dia_mod', $dia);
		$insert->bindParam(':hora_mod', $hora);
		$insert->bindParam(':id_matricula_dato', $idMatriculaDato);	
		$insert->bindParam(':des_antecedente_personal_medica', $desAntecedentePersonalMedica);
		$insert->bindParam(':personal', $personal);
		$insert->bindParam(':familiar', $familiar);

		$insert->execute();
		header('location:'.$base_url.'pages/gerontologia/pacientes.php?param='.$fetchCedula[0]);
		break;
	case 'update':
		$update = $db->prepare("UPDATE gddt_antecedentes_personales_medica SET id_cuenta='".$_SESSION['ID_CUENTA']."', ano_mod='".$ano."', mes_mod='".$mes."', dia_mod='".$dia."', hora_mod='".$hora."', des_antecedente_personal_medica='".$desAntecedentePersonalMedica."', personal='".$personal."', familiar='".$familiar."'  WHERE id_antecedente_personal_medica ='".$idRegistro."';");
		$update->execute();

		header('location:'.$base_url.'pages/gerontologia/pacientes.php?param='.$fetchCedula[0]);
		break;
	case 'delete':
		$sqlBuscarCedula = "SELECT md.num_documento FROM gddt_antecedentes_personales_medica nd INNER JOIN gddt_matricula_datos md ON md.id_matricula_dato = nd.id_matricula_dato WHERE nd.id_antecedente_personal_medica=".$idRegistro;
		$queryBuscarCedula = mysqli_query($conn, $sqlBuscarCedula);
		$fetchBuscarCedula = mysqli_fetch_row($queryBuscarCedula);
		
		$delete = $db->prepare("DELETE FROM gddt_antecedentes_personales_medica WHERE id_antecedente_personal_medica='".$idRegistro."';");
		$delete->execute();
		
		header('location:'.$base_url.'pages/gerontologia/pacientes.php?param='.$fetchBuscarCedula[0]);
		break;

	break;

	default:
		# code...
	break;
}