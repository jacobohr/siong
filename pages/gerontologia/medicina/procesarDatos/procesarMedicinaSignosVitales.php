<?php 
require "../../../../session.php";

$tipo = $_REQUEST["tipo"];
$idMatriculaDato = $_REQUEST["idMatriculaDato"];
$idRegistro = $_REQUEST["idRegistro"];

$frecuenciaCardiaca = $_POST['frecuenciaCardiaca'];
$presionSistolica = $_POST['presionSistolica'];
$presionDiastolica = $_POST['presionDiastolica'];
$frecuenciaRespiratoria = $_POST['frecuenciaRespiratoria'];
$temperatura = $_POST['temperatura'];
$peso = $_POST['peso'];

$fecha = strtotime($_POST['fecha']);
$ano = date("Y",$fecha);
$dia = date("d",$fecha);
$mes = date("m",$fecha);
$hora = date("H:i");

$sqlCedula = "SELECT num_documento FROM gddt_matricula_datos WHERE id_matricula_dato =".$idMatriculaDato;
$queryCedula = mysqli_query($conn, $sqlCedula);
$fetchCedula = mysqli_fetch_row($queryCedula);

switch ($tipo) {
	case 'insert':
		$insert = $db->prepare("INSERT INTO gddt_signos_vitales (id_cuenta, ano_mod, mes_mod, dia_mod, hora_mod, id_matricula_dato, frecuencia_cardiaca, presion_arterial_sistolica,presion_arterial_diastolica, frecuencia_respiratoria, temperatura, peso) VALUES (:id_cuenta, :ano_mod, :mes_mod, :dia_mod, :hora_mod, :id_matricula_dato, :frecuencia_cardiaca, :presion_arterial_sistolica, :presion_arterial_diastolica, :frecuencia_respiratoria, :temperatura, :peso);");
		$insert->bindParam(':id_cuenta', $_SESSION['ID_CUENTA']);
		$insert->bindParam(':ano_mod', $ano);
		$insert->bindParam(':mes_mod', $mes);
		$insert->bindParam(':dia_mod', $dia);
		$insert->bindParam(':hora_mod', $hora);
		$insert->bindParam(':id_matricula_dato', $idMatriculaDato);	
		$insert->bindParam(':frecuencia_cardiaca', $frecuenciaCardiaca);
		$insert->bindParam(':presion_arterial_sistolica', $presionSistolica);
		$insert->bindParam(':presion_arterial_diastolica', $presionDiastolica);
		$insert->bindParam(':frecuencia_respiratoria', $frecuenciaRespiratoria);
		$insert->bindParam(':temperatura', $temperatura);
		$insert->bindParam(':peso', $peso);


		$insert->execute();
		header('location:'.$base_url.'pages/gerontologia/pacientes.php?param='.$fetchCedula[0]);
		break;
	case 'update':
		$update = $db->prepare("UPDATE gddt_signos_vitales SET id_cuenta='".$_SESSION['ID_CUENTA']."',dia_mod='".$dia."',mes_mod='".$mes."',ano_mod='".$ano."',hora_mod='".$hora."', frecuencia_cardiaca='".$frecuenciaCardiaca."', presion_arterial_sistolica='".$presionSistolica."',presion_arterial_diastolica='".$presionDiastolica."', frecuencia_respiratoria='".$frecuenciaRespiratoria."', temperatura='".$temperatura."', peso='".$peso."' WHERE id_signos_vitales ='".$idRegistro."';");
		$update->execute();
		header('location:'.$base_url.'pages/gerontologia/pacientes.php?param='.$fetchCedula[0]);
		break;
	case 'delete':
		$sqlBuscarCedula = "SELECT md.num_documento FROM gddt_signos_vitales nd INNER JOIN gddt_matricula_datos md ON md.id_matricula_dato = nd.id_matricula_dato WHERE nd.id_signos_vitales=".$idRegistro;
		$queryBuscarCedula = mysqli_query($conn, $sqlBuscarCedula);
		$fetchBuscarCedula = mysqli_fetch_row($queryBuscarCedula);
		
		$delete = $db->prepare("DELETE FROM gddt_signos_vitales WHERE id_signos_vitales='".$idRegistro."';");
		$delete->execute();
		
		header('location:'.$base_url.'pages/gerontologia/pacientes.php?param='.$fetchBuscarCedula[0]);
		break;

	break;

	default:
		# code...
	break;
}