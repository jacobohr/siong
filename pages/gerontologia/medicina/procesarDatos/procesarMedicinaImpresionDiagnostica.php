<?php 
require "../../../../session.php";

$tipo = $_REQUEST["tipo"];
$idMatriculaDato = $_REQUEST["idMatriculaDato"];
$idRegistro = $_REQUEST["idRegistro"];

$enfermedad = $_POST['enfermedad'];
$planTratamiento = $_POST['planTratamiento'];

$fecha = strtotime($_POST['fecha']);
$ano = date("Y",$fecha);
$dia = date("d",$fecha);
$mes = date("m",$fecha);
$hora = date("H:i");

$sqlCedula = "SELECT num_documento FROM gddt_matricula_datos WHERE id_matricula_dato =".$idMatriculaDato;
$queryCedula = mysqli_query($conn, $sqlCedula);
$fetchCedula = mysqli_fetch_row($queryCedula);

switch ($tipo) {
	case 'insert':
		$insert = $db->prepare("INSERT INTO gddt_impresiones_diagnosticas_medica (id_cuenta, ano_mod, mes_mod, dia_mod, hora_mod, id_matricula_dato, des_impresion_diagnostica_medica, id_enfermedad) VALUES (:id_cuenta, :ano_mod, :mes_mod, :dia_mod, :hora_mod, :id_matricula_dato, :des_impresion_diagnostica_medica, :id_enfermedad);");
		$insert->bindParam(':id_cuenta', $_SESSION['ID_CUENTA']);
		$insert->bindParam(':ano_mod', $ano);
		$insert->bindParam(':mes_mod', $mes);
		$insert->bindParam(':dia_mod', $dia);
		$insert->bindParam(':hora_mod', $hora);
		$insert->bindParam(':id_matricula_dato', $idMatriculaDato);
		$insert->bindParam(':des_impresion_diagnostica_medica', $planTratamiento);
		$insert->bindParam(':id_enfermedad', $enfermedad);

		$insert->execute();
		header('location:'.$base_url.'pages/gerontologia/pacientes.php?param='.$fetchCedula[0]);
		break;
	case 'update':
		$update = $db->prepare("UPDATE gddt_impresiones_diagnosticas_medica SET id_cuenta='".$_SESSION['ID_CUENTA']."', ano_mod='".$ano."', mes_mod='".$mes."', dia_mod='".$dia."', hora_mod='".$hora."', des_impresion_diagnostica_medica='".$planTratamiento."', id_enfermedad='".$enfermedad."'  WHERE id_impresion_diagnostica_medica ='".$idRegistro."';");
		$update->execute();

		header('location:'.$base_url.'pages/gerontologia/pacientes.php?param='.$fetchCedula[0]);
		break;
	case 'delete':
		$sqlBuscarCedula = "SELECT md.num_documento FROM gddt_impresiones_diagnosticas_medica nd INNER JOIN gddt_matricula_datos md ON md.id_matricula_dato = nd.id_matricula_dato WHERE nd.id_impresion_diagnostica_medica=".$idRegistro;
		$queryBuscarCedula = mysqli_query($conn, $sqlBuscarCedula);
		$fetchBuscarCedula = mysqli_fetch_row($queryBuscarCedula);
		
		$delete = $db->prepare("DELETE FROM gddt_impresiones_diagnosticas_medica WHERE id_impresion_diagnostica_medica='".$idRegistro."';");
		$delete->execute();
		
		header('location:'.$base_url.'pages/gerontologia/pacientes.php?param='.$fetchBuscarCedula[0]);
		break;

	break;

	default:
		# code...
	break;
}