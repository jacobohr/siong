<?php 
require "../../../../session.php";

$tipo = $_REQUEST["tipo"];
$idMatriculaDato = $_REQUEST["idMatriculaDato"];
$idRegistro = $_REQUEST["idRegistro"];

$cabeza = $_POST['cabeza'];
$cuello = $_POST['cuello'];
$torax = $_POST['torax'];
$aparatoRespiratorio = $_POST['aparatoRespiratorio'];
$aparatoCardiovascular = $_POST['aparatoCardiovascular'];
$abdomen = $_POST['abdomen'];
$pelvis = $_POST['pelvis'];
$apartoGenitourinario = $_POST['apartoGenitourinario'];
$sistemaNervioso = $_POST['sistemaNervioso'];
$osteomuscular = $_POST['osteomuscular'];

$fecha = strtotime($_POST['fecha']);
$ano = date("Y",$fecha);
$dia = date("d",$fecha);
$mes = date("m",$fecha);
$hora = date("H:i");

$sqlCedula = "SELECT num_documento FROM gddt_matricula_datos WHERE id_matricula_dato =".$idMatriculaDato;
$queryCedula = mysqli_query($conn, $sqlCedula);
$fetchCedula = mysqli_fetch_row($queryCedula);

switch ($tipo) {
	case 'insert':
		$insert = $db->prepare("INSERT INTO gddt_examen_fisico (id_cuenta, ano_mod, mes_mod, dia_mod, hora_mod, id_matricula_dato, cabeza, cuello, torax, aparato_respiratorio, aparato_cardiovascular, abdomen, pelvis, aparato_genitourinario, sistema_nervioso, osteomuscular) VALUES (:id_cuenta, :ano_mod, :mes_mod, :dia_mod, :hora_mod, :id_matricula_dato, :cabeza, :cuello, :torax, :aparato_respiratorio, :aparato_cardiovascular, :abdomen, :pelvis, :aparato_genitourinario, :sistema_nervioso, :osteomuscular);");
		$insert->bindParam(':id_cuenta', $_SESSION['ID_CUENTA']);
		$insert->bindParam(':ano_mod', $ano);
		$insert->bindParam(':mes_mod', $mes);
		$insert->bindParam(':dia_mod', $dia);
		$insert->bindParam(':hora_mod', $hora);
		$insert->bindParam(':id_matricula_dato', $idMatriculaDato);
		$insert->bindParam(':cabeza', $cabeza);
		$insert->bindParam(':cuello', $cuello);
		$insert->bindParam(':torax', $torax);
		$insert->bindParam(':aparato_respiratorio', $aparatoRespiratorio);
		$insert->bindParam(':aparato_cardiovascular', $aparatoCardiovascular);
		$insert->bindParam(':abdomen', $abdomen);
		$insert->bindParam(':pelvis', $pelvis);
		$insert->bindParam(':aparato_genitourinario', $apartoGenitourinario);
		$insert->bindParam(':sistema_nervioso', $sistemaNervioso);
		$insert->bindParam(':osteomuscular', $osteomuscular);

		$insert->execute();
		header('location:'.$base_url.'pages/gerontologia/pacientes.php?param='.$fetchCedula[0]);
		break;
	case 'update':
		$update = $db->prepare("UPDATE gddt_examen_fisico SET id_cuenta='".$_SESSION['ID_CUENTA']."', ano_mod='".$ano."', mes_mod='".$mes."', dia_mod='".$dia."', hora_mod='".$hora."', cabeza='".$cabeza."', cuello='".$cuello."', torax='".$torax."', aparato_respiratorio='".$aparatoRespiratorio."', aparato_cardiovascular='".$aparatoCardiovascular."', abdomen='".$abdomen."', pelvis='".$pelvis."', aparato_genitourinario='".$apartoGenitourinario."', sistema_nervioso='".$sistemaNervioso."', osteomuscular='". $osteomuscular."' WHERE id_examen_fisico ='".$idRegistro."';");
		$update->execute();

		header('location:'.$base_url.'pages/gerontologia/pacientes.php?param='.$fetchCedula[0]);
		break;
	case 'delete':
		$sqlBuscarCedula = "SELECT md.num_documento FROM gddt_examen_fisico nd INNER JOIN gddt_matricula_datos md ON md.id_matricula_dato = nd.id_matricula_dato WHERE nd.id_examen_fisico=".$idRegistro;
		$queryBuscarCedula = mysqli_query($conn, $sqlBuscarCedula);
		$fetchBuscarCedula = mysqli_fetch_row($queryBuscarCedula);
		
		$delete = $db->prepare("DELETE FROM gddt_examen_fisico WHERE id_examen_fisico='".$idRegistro."';");
		$delete->execute();
		
		header('location:'.$base_url.'pages/gerontologia/pacientes.php?param='.$fetchBuscarCedula[0]);
		break;

	break;

	default:
		# code...
	break;
}