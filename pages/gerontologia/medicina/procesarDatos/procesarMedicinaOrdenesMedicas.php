<?php 
require "../../../../session.php";
error_reporting(E_ALL);
$tipo = $_REQUEST["tipo"];
$idMatriculaDato = $_REQUEST["idMatriculaDato"];
$idRegistro = $_REQUEST["idRegistro"];

$ordenesMedicas = $_POST['ordenesMedicas'];

$fecha = strtotime($_POST['fecha']);
$ano = date("Y",$fecha);
$dia = date("d",$fecha);
$mes = date("m",$fecha);
$hora = date("H:i");

$sqlCedula = "SELECT num_documento FROM gddt_matricula_datos WHERE id_matricula_dato =".$idMatriculaDato;
$queryCedula = mysqli_query($conn, $sqlCedula);
$fetchCedula = mysqli_fetch_row($queryCedula);

switch ($tipo) {
	case 'insert':
		/** TRAER ID DE LA OBRA**/
		$sqlIdOrdenMedica = "SELECT IFNULL(MAX(id_orden_medica)+1,1) maximo FROM gddt_ordenes_medicas";
		$queryIdOrdenMedica = $db->query($sqlIdOrdenMedica);
		$fetchIdOrdenMedica = $queryIdOrdenMedica->fetchAll(PDO::FETCH_OBJ);
		foreach ($fetchIdOrdenMedica as $fetch) {
			$IdOrdenMedica = $fetch->maximo;
		}

		$arrayOrdenesMedicas = $_POST["ordenesMedicas"];
		$sizeArray = sizeof($arrayOrdenesMedicas);
		$i = 0;
		while($i < $sizeArray){
			if(substr($arrayOrdenesMedicas[$i],4) != ""){
				$insert = $db->prepare("INSERT INTO gddt_ordenes_medicas (id_orden_medica, id_cuenta, ano_mod, mes_mod, dia_mod, hora_mod, id_matricula_dato, des_orden_medica) VALUES (:id_orden_medica, :id_cuenta, :ano_mod, :mes_mod, :dia_mod, :hora_mod, :id_matricula_dato, :des_orden_medica);");
				$insert->bindParam(':id_orden_medica', $IdOrdenMedica);
				$insert->bindParam(':id_cuenta', $_SESSION['ID_CUENTA']);
				$insert->bindParam(':ano_mod', $ano);
				$insert->bindParam(':mes_mod', $mes);
				$insert->bindParam(':dia_mod', $dia);
				$insert->bindParam(':hora_mod', $hora);
				$insert->bindParam(':id_matricula_dato', $idMatriculaDato);	
				$insert->bindParam(':des_orden_medica', $arrayOrdenesMedicas[$i]);
				$insert->execute();
			}
			$i++;
		}
		header('location:'.$base_url.'pages/gerontologia/pacientes.php?param='.$fetchCedula[0]);
		break;
	case 'update':
		$delete = $db->prepare("DELETE FROM gddt_ordenes_medicas WHERE id_orden_medica='".$idRegistro."';");
		$delete->execute();

		$arrayOrdenesMedicas = $_POST["ordenesMedicas"];
		$sizeArray = sizeof($arrayOrdenesMedicas);
		$i = 0;
		while($i < $sizeArray){
			if(substr($arrayOrdenesMedicas[$i],4) != ""){
				$insert = $db->prepare("INSERT INTO gddt_ordenes_medicas (id_orden_medica, id_cuenta, ano_mod, mes_mod, dia_mod, hora_mod, id_matricula_dato, des_orden_medica) VALUES (:id_orden_medica, :id_cuenta, :ano_mod, :mes_mod, :dia_mod, :hora_mod, :id_matricula_dato, :des_orden_medica);");
				$insert->bindParam(':id_orden_medica', $idRegistro);
				$insert->bindParam(':id_cuenta', $_SESSION['ID_CUENTA']);
				$insert->bindParam(':ano_mod', $ano);
				$insert->bindParam(':mes_mod', $mes);
				$insert->bindParam(':dia_mod', $dia);
				$insert->bindParam(':hora_mod', $hora);
				$insert->bindParam(':id_matricula_dato', $idMatriculaDato);	
				$insert->bindParam(':des_orden_medica', $arrayOrdenesMedicas[$i]);
				$insert->execute();
			}
			$i++;
		}
		header('location:'.$base_url.'pages/gerontologia/pacientes.php?param='.$fetchCedula[0]);
		break;
	case 'delete':
		$sqlBuscarCedula = "SELECT md.num_documento FROM gddt_ordenes_medicas nd INNER JOIN gddt_matricula_datos md ON md.id_matricula_dato = nd.id_matricula_dato WHERE nd.id_orden_medica=".$idRegistro;
		$queryBuscarCedula = mysqli_query($conn, $sqlBuscarCedula);
		$fetchBuscarCedula = mysqli_fetch_row($queryBuscarCedula);
		
		$delete = $db->prepare("DELETE FROM gddt_ordenes_medicas WHERE id_orden_medica='".$idRegistro."';");
		$delete->execute();
		
		header('location:'.$base_url.'pages/gerontologia/pacientes.php?param='.$fetchBuscarCedula[0]);
		break;

	break;

	default:
		# code...
	break;
}