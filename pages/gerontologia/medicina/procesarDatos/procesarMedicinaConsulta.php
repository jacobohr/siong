<?php 
require "../../../../session.php";
error_reporting(E_ALL);
$tipo = $_REQUEST["tipo"];
$idMatriculaDato = $_REQUEST["idMatriculaDato"];
$idRegistro = $_REQUEST["idRegistro"];

$acompanante = $_POST['acompanante'];
$telAcompanante = $_POST['telAcompanante'];
$motivoConsulta = $_POST['motivoConsulta'];
$enfermedadActual = $_POST['enfermedadActual'];
$descripcionProfesional = $_POST['descripcionProfesional'];
$antecedenteMedicoPersonal = $_POST['antecedenteMedicoPersonal'];
$antecedenteMedicoPersonalCual = $_POST['antecedenteMedicoPersonalCual'];
$antecedenteMedicoFamiliares = $_POST['antecedenteMedicoFamiliares'];
$antecedenteMedicoFamiliaresCual = $_POST['antecedenteMedicoFamiliaresCual'];
$antecedenteGinecoObstetricos = $_POST['antecedenteGinecoObstetricos'];
$antecedenteGinecoObstetricos2 = $_POST['antecedenteGinecoObstetricos2'];
$AntecedentesGinecoObstétricosCuales = $_POST['AntecedentesGinecoObstétricosCuales'];
$fechaUltimaCitologia = $_POST['fechaUltimaCitologia'];
$periodoCiclo = $_POST['periodoCiclo'];
$metodoPlanificacion = $_POST['metodoPlanificacion'];
$eruptiva = $_POST['eruptiva'];
$eruptivaCual = $_POST['eruptivaCual'];
$esquemaEvacuacionDia = $_POST['esquemaEvacuacionDia'];
$trastornoOrtopedico = $_POST['trastornoOrtopedico'];
$cualTrastornoOrtopedico = $_POST['cualTrastornoOrtopedico'];
$otroAntecedente = $_POST['otroAntecedente'];
$alergia = $_POST['alergia'];
$quirurgico = $_POST['quirurgico'];
$drogasicoactiva = $_POST['drogasicoactiva'];
$cigarrillo = $_POST['cigarrillo'];
$cuantoCigarrillo = $_POST['cuantoCigarrillo'];
$licor = $_POST['licor'];
$frecuenciaLicor = $_POST['frecuenciaLicor'];
$medicamentoConsume = $_POST['medicamentoConsume'];



//$fecha = strtotime($_POST['fecha']);
$ano = date("Y");
$dia = date("d");
$mes = date("m");
$hora = date("H:i");

$sqlCedula = "SELECT num_documento FROM gddt_matricula_datos WHERE id_matricula_dato =".$idMatriculaDato;
$queryCedula = mysqli_query($conn, $sqlCedula);
$fetchCedula = mysqli_fetch_row($queryCedula);

switch ($tipo) {
	case 'insert':
	$sql = "SELECT IFNULL(MAX(id_consulta)+1,1) maximo FROM gddt_consultas";
	$query = mysqli_query($conn, $sql);
	$fetchIdConsulta = mysqli_fetch_row($query);

	$insert = $db->prepare("INSERT INTO gddt_consultas (id_consulta, id_cuenta, ano_mod, mes_mod, dia_mod, hora_mod, id_matricula_dato, acompanante, tel_acompanante, fecha_ultima_citologia, periodo_ciclo, metodo_planificacion, eruptiva, cual, esquema_evacuacion_dia, trastorno_ortopedico, cual2, otro_antecedente, alergia, quirurgico, droga_sicoactiva, cigarrillo, cuanto, licor, frecuencia, medicamento_consume, motivo_consulta, enfermedad_actual, descripcion_profesional) 
		VALUES (:id_consulta, :id_cuenta, :ano_mod, :mes_mod, :dia_mod, :hora_mod, :id_matricula_dato, 
		:acompanante, :tel_acompanante, :fecha_ultima_citologia, :periodo_ciclo, :metodo_planificacion, :eruptiva, 
		:cual, :esquema_evacuacion_dia, :trastorno_ortopedico, :cual2, :otro_antecedente, :alergia, :quirurgico,
		:droga_sicoactiva, :cigarrillo, :cuanto, :licor, :frecuencia, :medicamento_consume, :motivo_consulta, :enfermedad_actual, :descripcion_profesional);");
	$insert->bindParam(':id_consulta', $fetchIdConsulta[0]);
	$insert->bindParam(':id_cuenta', $_SESSION['ID_CUENTA']);
	$insert->bindParam(':ano_mod', $ano);
	$insert->bindParam(':mes_mod', $mes);
	$insert->bindParam(':dia_mod', $dia);
	$insert->bindParam(':hora_mod', $hora);
	$insert->bindParam(':id_matricula_dato', $idMatriculaDato);
	$insert->bindParam(':acompanante', $acompanante);	
	$insert->bindParam(':tel_acompanante', $telAcompanante);
	$insert->bindParam(':fecha_ultima_citologia', $fechaUltimaCitologia);
	$insert->bindParam(':periodo_ciclo', $periodoCiclo);
	$insert->bindParam(':metodo_planificacion', $metodoPlanificacion);
	$insert->bindParam(':eruptiva', $eruptiva);
	$insert->bindParam(':cual', $eruptivaCual);
	$insert->bindParam(':esquema_evacuacion_dia', $esquemaEvacuacionDia);
	$insert->bindParam(':trastorno_ortopedico', $trastornoOrtopedico);
	$insert->bindParam(':cual2', $cualTrastornoOrtopedico);
	$insert->bindParam(':otro_antecedente', $otroAntecedente);
	$insert->bindParam(':alergia', $alergia);
	$insert->bindParam(':quirurgico', $quirurgico);
	$insert->bindParam(':droga_sicoactiva', $drogasicoactiva);
	$insert->bindParam(':cigarrillo', $cigarrillo);
	$insert->bindParam(':cuanto', $cuantoCigarrillo);
	$insert->bindParam(':licor', $licor);
	$insert->bindParam(':frecuencia', $frecuenciaLicor);
	$insert->bindParam(':medicamento_consume', $medicamentoConsume);
	$insert->bindParam(':motivo_consulta', $motivoConsulta);
	$insert->bindParam(':enfermedad_actual', $enfermedadActual);
	$insert->bindParam(':descripcion_profesional', $descripcionProfesional);
	$insert->execute();

	$insertAntcMedicPersonales = $db->prepare("INSERT INTO gddt_consultas_antecedentes_medicos_personales(id_consulta,des_antecedente_medico_personal,otros) VALUES(:id_consulta, :des_antecedente_medico_personal, :otros);");
	$insertAntcMedicPersonales->bindParam(':id_consulta', $fetchIdConsulta[0]);	
	$insertAntcMedicPersonales->bindParam(':des_antecedente_medico_personal', $antecedenteMedicoPersonal);
	$insertAntcMedicPersonales->bindParam(':otros', $antecedenteMedicoPersonalCual);
	$insertAntcMedicPersonales->execute();

	$insertAntcMedicFamiliares = $db->prepare("INSERT INTO gddt_cosultas_antecentes_medicos_familiares(id_consulta,	des_antecedente_medico_familiar, otros) 
			VALUES (:id_consulta, :des_antecedente_medico_familiar, :otros);");
	$insertAntcMedicFamiliares->bindParam(':id_consulta', $fetchIdConsulta[0]);	
	$insertAntcMedicFamiliares->bindParam(':des_antecedente_medico_familiar', $antecedenteMedicoFamiliares);
	$insertAntcMedicFamiliares->bindParam(':otros', $antecedenteMedicoFamiliaresCual);
	$insertAntcMedicFamiliares->execute();

	$insertAntcMedicGinecoObstericos = $db->prepare("INSERT INTO gddt_consultas_antecedentes_gineco_obstetricos (id_consulta, des_antecedente_gineco_obstetrico) 
				VALUES (:id_consulta, :des_antecedente_gineco_obstetrico);");
	$insertAntcMedicGinecoObstericos->bindParam(':id_consulta', $fetchIdConsulta[0]);	
	$insertAntcMedicGinecoObstericos->bindParam(':des_antecedente_gineco_obstetrico', $antecedenteGinecoObstetricos);
	$insertAntcMedicGinecoObstericos->execute();

	$insertAntcMedicGinecoObstericosDos = $db->prepare("INSERT INTO gddt_consultas_antecedentes_gineco_obstetricos_2 (id_consulta,des_antecedente_gineco_obstetrico_2, otros) VALUES(:id_consulta, :des_antecedente_gineco_obstetrico_2, :otros);");
	$insertAntcMedicGinecoObstericosDos->bindParam(':id_consulta', $fetchIdConsulta[0]);	
	$insertAntcMedicGinecoObstericosDos->bindParam(':des_antecedente_gineco_obstetrico_2', $antecedenteGinecoObstetricos2);
	$insertAntcMedicGinecoObstericosDos->bindParam(':otros', $AntecedentesGinecoObstétricosCuales);
	$insertAntcMedicGinecoObstericosDos->execute();


	header('location:'.$base_url.'pages/gerontologia/pacientes.php?param='.$fetchCedula[0]);
	break;
	case 'update':
		$update = $db->prepare("UPDATE gddt_consultas SET id_cuenta='".$_SESSION['ID_CUENTA']."', ano_mod='".$ano."', mes_mod='".$mes."', dia_mod='".$dia."', hora_mod='".$hora."', acompanante='".$acompanante."', tel_acompanante='".$telAcompanante."', fecha_ultima_citologia='".$fechaUltimaCitologia."', periodo_ciclo='".$periodoCiclo."', metodo_planificacion='".$metodoPlanificacion."', eruptiva='".$eruptiva."', cual='".$eruptivaCual."', esquema_evacuacion_dia='".$esquemaEvacuacionDia."', trastorno_ortopedico='".$trastornoOrtopedico."', cual2='".$cualTrastornoOrtopedico."', otro_antecedente='".$otroAntecedente."', alergia='".$alergia."', quirurgico='".$quirurgico."', droga_sicoactiva='".$drogasicoactiva."', cigarrillo='".$cigarrillo."', cuanto='".$cuantoCigarrillo."', licor='".$licor."', frecuencia='".$frecuenciaLicor."', medicamento_consume='".$medicamentoConsume."', motivo_consulta='".$motivoConsulta."', enfermedad_actual='".$enfermedadActual."', descripcion_profesional='".$descripcionProfesional."'  WHERE id_consulta ='".$idRegistro."';");
		$update->execute();

		$updateAntcMedicPersonales = $db->prepare(" UPDATE gddt_consultas_antecedentes_medicos_personales SET des_antecedente_medico_personal='".$antecedenteMedicoPersonal."',otros='".$antecedenteMedicoPersonalCual."'  WHERE id_consulta ='".$idRegistro."';");
		$updateAntcMedicPersonales->execute();

		$updateAntcMedicFamiliares = $db->prepare(" UPDATE gddt_cosultas_antecentes_medicos_familiares SET des_antecedente_medico_familiar='".$antecedenteMedicoFamiliares."', otros='".$antecedenteMedicoFamiliaresCual."'  WHERE id_consulta ='".$idRegistro."';");
		$updateAntcMedicFamiliares->execute();

		$updateAntcMedicGinecoObstericos = $db->prepare(" UPDATE gddt_consultas_antecedentes_gineco_obstetricos SET des_antecedente_gineco_obstetrico ='".$antecedenteGinecoObstetricos."'  WHERE id_consulta ='".$idRegistro."';");
		$updateAntcMedicGinecoObstericos->execute();

		$updateAntcMedicGinecoObstericosDos = $db->prepare(" UPDATE gddt_consultas_antecedentes_gineco_obstetricos_2 SET des_antecedente_gineco_obstetrico_2='".$antecedenteGinecoObstetricos2."', otros='".$AntecedentesGinecoObstétricosCuales."'  WHERE id_consulta ='".$idRegistro."';");
		$updateAntcMedicGinecoObstericosDos->execute();

		//header('location:'.$base_url.'pages/gerontologia/pacientes.php?param='.$fetchCedula[0]);
	break;
	case 'delete':
	$sqlBuscarCedula = "SELECT md.num_documento FROM gddt_consultas nd INNER JOIN gddt_matricula_datos md ON md.id_matricula_dato = nd.id_matricula_dato WHERE nd.id_consulta=".$idRegistro;
	$queryBuscarCedula = mysqli_query($conn, $sqlBuscarCedula);
	$fetchBuscarCedula = mysqli_fetch_row($queryBuscarCedula);

	$delete = $db->prepare("DELETE FROM gddt_consultas WHERE id_consulta='".$idRegistro."';");
	$delete->execute();

	$deleteAntcMedicPersonales = $db->prepare("DELETE FROM gddt_consultas_antecedentes_medicos_personales WHERE id_consulta='".$idRegistro."';");
	$deleteAntcMedicPersonales->execute();

	$deleteAntcMedicFamiliares = $db->prepare("DELETE FROM gddt_cosultas_antecentes_medicos_familiares WHERE id_consulta='".$idRegistro."';");
	$deleteAntcMedicFamiliares->execute();

	$deleteAntcMedicGinecoObstericos = $db->prepare("DELETE FROM gddt_consultas_antecedentes_gineco_obstetricos WHERE id_consulta='".$idRegistro."';");
	$deleteAntcMedicGinecoObstericos->execute();

	$deleteAntcMedicGinecoObstericosDos = $db->prepare("DELETE FROM gddt_consultas_antecedentes_gineco_obstetricos_2 WHERE id_consulta='".$idRegistro."';");
	$deleteAntcMedicGinecoObstericosDos->execute();

	header('location:'.$base_url.'pages/gerontologia/pacientes.php?param='.$fetchBuscarCedula[0]);
	break;
	case 'delete':
	break;

	default:
		# code...
	break;
}