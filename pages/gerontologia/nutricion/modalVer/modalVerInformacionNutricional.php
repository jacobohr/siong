<?php

//include connection file 
require "../../../../session.php";

$sqlUsuario = "
SELECT 
(CONVERT(CAST(CONVERT(CONCAT(UPPER(c.primer_nombre),' ',UPPER(c.segundo_nombre),' ',UPPER(c.primer_apellido),' ',UPPER(c.segundo_apellido)) USING latin1) AS BINARY) USING utf8)) AS nombreUsuario,
CONCAT(nd.dia_mod,'/',nd.mes_mod,'/',nd.ano_mod) fecha,
nd.hora_mod hora,
nd.peso,
nd.estatura,
nd.circunferencia_brazo,
nd.circunferencia_pantorrilla,
nd.riesgo_cardiovascular,
nd.complexion,
ed.descripcion estadoDental,
cn.descripcion clasificacionNutricional,
md.id_genero
FROM gddt_nutricion_datos nd
LEFT JOIN gddt_cuentas c ON nd.id_cuenta = c.id_cuenta
LEFT JOIN gddt_estados_dentales ed ON nd.id_estado_dental = ed.id_estado_dental 
LEFT JOIN gddt_clasif_nutricionales cn ON nd.id_clasif_nutricional = cn.id_clasif_nutricional
LEFT JOIN gddt_matricula_datos md ON nd.id_matricula_dato = md.id_matricula_dato 
WHERE nd.id_nutricion_datos = " . $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);
$rows = mysqli_num_rows($queryUsuario);

if($rows == 0){
    echo "<p style='color:red;font-size:0.9em;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No se encontraron datos :(</p>";
}else{



    ?>

    <div class="content">
        <h6 class="text-center mb-4">Actualizado por <?= $fetchUsuario[0];?> el <?= $fetchUsuario[1]; ?> a las <?= $fetchUsuario[2]; ?></h6>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Peso
            </label>
            <p><?php if(empty($fetchUsuario[3])){echo 'Sin dato';}else{echo $fetchUsuario[3];} ?></p>
        </div>

        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Estatura
            </label>
            <p><?php if(empty($fetchUsuario[4])){echo 'Sin dato';}else{echo $fetchUsuario[4];} ?></p>
        </div>

        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Indice de masa corporal
            </label>
            <!--Fórmula: peso (kg) / [estatura (m)]2 -->
            <?php 
            if(!(empty($fetchUsuario[3]) or empty($fetchUsuario[4]))){
                $peso = $fetchUsuario[3];
                $estatura = pow(($fetchUsuario[4]/100),2);
                $resultado = round(($peso/$estatura),1);
                echo '<p>'.$resultado.'</p>';
            }else{
                echo '<p>Debe ingresar todos los datos requeridos para calcular el indice de masa corporal</p>';
            }
            ?>

        </div>

        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Circunferencia de brazo
            </label>
            <p><?php if(empty($fetchUsuario[5])){echo 'Sin dato';}else{echo strtoupper($fetchUsuario[5]);} ?></p>
        </div>

        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Circunferencia de pantorrilla:
            </label>
            <p><?php if(empty($fetchUsuario[6])){echo 'Sin dato';}else{echo strtoupper($fetchUsuario[6]);} ?></p>
        </div>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
             Riesgo cardiovascular:
         </label>
         <?php 
         $idGenero = $fetchUsuario[11];
         $riesgoCardiovascular = $fetchUsuario[7];
         

         if(empty($riesgoCardiovascular)){
            echo "<p>Sin datos</p>";
        }else{
            $riesgoCardiovascular = number_format($fetchUsuario[7],1,".",".");
            switch ($idGenero) {
                case 1:
                    if($riesgoCardiovascular > 0.95){echo '<p>Muy alto</p>';break;}
                    if($riesgoCardiovascular >= 0.88 && $riesgoCardiovascular <= 0.95){echo '<p>Alto</p>';break;}
                    if($riesgoCardiovascular >= 0.82 && $riesgoCardiovascular <=0.87){echo '<p>Mediano</p>';break;}
                    if($riesgoCardiovascular >= 0.60 && $riesgoCardiovascular <= 0.81){echo '<p>Bajo</p>';break;}
                    if($riesgoCardiovascular < 0.59){echo '<p>Muy bajo</p>';break;}
                case 2:
                    if($riesgoCardiovascular > 0.82){echo '<p>Muy bajo</p>';break;}
                    if($riesgoCardiovascular >= 0.77 && $riesgoCardiovascular <= 0.81){echo '<p>Alto</p>';break;}
                    if($riesgoCardiovascular >= 0.70 && $riesgoCardiovascular <= 0.76){echo '<p>Mediano</p>';break;}
                    if($riesgoCardiovascular >= 0.60 && $riesgoCardiovascular <= 0.69){echo '<p>Bajo</p>';break;}
                    if($riesgoCardiovascular < 0.59){echo '<p>Muy bajo</p>';break;}
            }    
        }
        ?>
    </div>
    <div class="user-block col-sm-4 col-xs-6">
        <label>
            Complexión:
        </label>
        <?php 
         $idGenero = $fetchUsuario[11];
         $complexion = $fetchUsuario[8];
         

         if(empty($complexion)){
            echo "<p>Sin datos</p>";
        }else{
            $complexion = number_format($fetchUsuario[8],1,".",".");
            switch ($idGenero) {
                case 1:
                    if($complexion > 10.4){echo '<p>Pequeña</p>';break;}
                    if($complexion >= 9.6 && $complexion <= 10.3){echo '<p>Mediana</p>';break;}
                    if($complexion > 9.5){echo '<p>Recia</p>';break;}
                case 2:
                    if($complexion > 11){echo '<p>Pequeña</p>';break;}
                    if($complexion >= 10.1 && $complexion <= 10.9){echo '<p>Mediana</p>';break;}
                    if($complexion > 10){echo '<p>Recia</p>';break;}
                default:
                    echo "<p>---</p>";
                    break;
            }    
        }
        ?>
    </div>
    <div class="user-block col-sm-4 col-xs-6">
        <label>
            Estado dental:
        </label>
        <p><?php echo strtoupper($fetchUsuario[9]); ?></p>
    </div>
    <div class="user-block col-sm-4 col-xs-6">
        <label>
            Clasificación nutricional:
        </label>
        <p><?php echo strtoupper($fetchUsuario[10]); ?></p>
    </div>

    <div class="user-block">&nbsp;</div>

</div><!-- Fin del content -->
<?php } ?>