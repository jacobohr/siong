<?php 
require "../../../../session.php";

$tipo = $_REQUEST["tipo"];
$idMatriculaDato = $_REQUEST["idMatriculaDato"];
$idRegistro = $_REQUEST["idRegistro"];

$peso = $_POST['peso'];
$estatura = $_POST['estatura']/100;
$circunferenciaCintura = $_POST['circunferenciaCintura'];
$circunferenciaCadera = $_POST['circunferenciaCadera'];
$riesgoCardiovascular = $_POST['riesgoCardiovascular'];
$circunferenciaBrazo = $_POST['circunferenciaBrazo'];
$circunferenciaMuneca = $_POST['circunferenciaMuneca'];
$circunferenciaPantorrilla = $_POST['circunferenciaPantorrilla'];
$circunferenciaTobilloRodilla = $_POST['circunferenciaTobilloRodilla'];
$complexion = $_POST['complexion'];
$idEstadoDental = $_POST['estadoDental'];
$idclasificacionNutricional = $_POST['clasificacionNutricional'];
$pesoReferencia = $_POST['pesoReferencia'];
$estadoDental = $_POST['estadoDental'];
$imc = $_POST['imc'];

$fecha = strtotime($_POST['fecha']);
$ano = date("Y",$fecha);
$dia = date("d",$fecha);
$mes = date("m",$fecha);
$hora = date("H:i");

$sqlCedula = "SELECT num_documento FROM gddt_matricula_datos WHERE id_matricula_dato =".$idMatriculaDato;
$queryCedula = mysqli_query($conn, $sqlCedula);
$fetchCedula = mysqli_fetch_row($queryCedula);

switch ($tipo) {

	case 'insert':
		$insert = $db->prepare("INSERT INTO gddt_nutricion_datos (id_cuenta,ano_mod,mes_mod,dia_mod,hora_mod,id_matricula_dato,peso,estatura,imc,circunferencia_cintura,circunferencia_cadera,riesgo_cardiovascular,circunferencia_brazo,circunferencia_muneca,circunferencia_pantorrilla,altura_tobillo_rodilla,complexion,id_estado_dental,id_clasif_nutricional,peso_referencia)
		VALUES (:id_cuenta, :ano_mod , :mes_mod, :dia_mod, :hora_mod, :id_matricula_dato, :peso, :estatura, :imc, :circunferencia_cintura, :circunferencia_cadera, :riesgo_cardiovascular, :circunferencia_brazo, :circunferencia_muneca, :circunferencia_pantorrilla, :altura_tobillo_rodilla, :complexion, :id_estado_dental, :id_clasif_nutricional, :peso_referencia);");
		$insert->bindParam(':id_cuenta', $_SESSION['ID_CUENTA']);
		$insert->bindParam(':ano_mod', $ano);
		$insert->bindParam(':mes_mod', $mes);
		$insert->bindParam(':dia_mod', $dia);
		$insert->bindParam(':hora_mod', $hora);
		$insert->bindParam(':id_matricula_dato', $idMatriculaDato);
		$insert->bindParam(':peso', $peso);
		$insert->bindParam(':estatura', $estatura);
		$insert->bindParam(':imc', $imc);
		$insert->bindParam(':circunferencia_cintura', $circunferenciaCintura);
		$insert->bindParam(':circunferencia_cadera', $circunferenciaCadera);
		$insert->bindParam(':riesgo_cardiovascular', $riesgoCardiovascular);
		$insert->bindParam(':circunferencia_brazo', $circunferenciaBrazo);
		$insert->bindParam(':circunferencia_muneca', $circunferenciaMuneca);
		$insert->bindParam(':circunferencia_pantorrilla', $circunferenciaPantorrilla);
		$insert->bindParam(':altura_tobillo_rodilla', $circunferenciaTobilloRodilla);
		$insert->bindParam(':complexion', $complexion);
		$insert->bindParam(':id_estado_dental', $idEstadoDental);
		$insert->bindParam(':id_clasif_nutricional', $idclasificacionNutricional);
		$insert->bindParam(':peso_referencia', $pesoReferencia);
		$insert->execute();

		header('location:'.$base_url.'pages/gerontologia/pacientes.php?param='.$fetchCedula[0]);
		break;

	case 'update':
		$update = $db->prepare("UPDATE gddt_nutricion_datos SET ano_mod ='".$ano."',mes_mod='".$mes."',dia_mod='".$dia."',hora_mod='".$hora."',peso='".$peso."',estatura='".$estatura."',imc='".$imc."',circunferencia_cintura='".$circunferenciaCintura."',circunferencia_cadera='".$circunferenciaCadera."',riesgo_cardiovascular='".$riesgoCardiovascular."',circunferencia_brazo='".$circunferenciaBrazo."',circunferencia_muneca='".$circunferenciaMuneca."',circunferencia_pantorrilla='".$circunferenciaPantorrilla."',altura_tobillo_rodilla='".$circunferenciaTobilloRodilla."',complexion='".$complexion."',id_estado_dental='".$idEstadoDental."',id_clasif_nutricional='".$idclasificacionNutricional."',peso_referencia='".$pesoReferencia."' WHERE id_nutricion_datos='".$idRegistro."';");
		$update->execute();
		
		header('location:'.$base_url.'pages/gerontologia/pacientes.php?param='.$fetchCedula[0]);
		break;
	case 'delete':
		$sqlBuscarCedula = "SELECT md.num_documento FROM gddt_nutricion_datos nd INNER JOIN gddt_matricula_datos md ON md.id_matricula_dato = nd.id_matricula_dato WHERE nd.id_nutricion_datos=".$idRegistro;
		$queryBuscarCedula = mysqli_query($conn, $sqlBuscarCedula);
		$fetchBuscarCedula = mysqli_fetch_row($queryBuscarCedula);

		$delete = $db->prepare("DELETE FROM gddt_nutricion_datos WHERE id_nutricion_datos='".$idRegistro."';");
		$delete->execute();
		
		header('location:'.$base_url.'pages/gerontologia/pacientes.php?param='.$fetchBuscarCedula[0]);
		break;
	break;

	default:
		# code...
	break;
}