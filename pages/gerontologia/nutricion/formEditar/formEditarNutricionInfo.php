<?php require "../../../../session.php";



$sqlUsuario = "SELECT * FROM gddt_nutricion_datos WHERE id_nutricion_datos = ". $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);

?>
<form id="registrarSeguimientoNutricionalEditar"  onsubmit="return registroInfoNutricionalEditar(event);" method="POST" action="<?php echo $base_url ?>pages/gerontologia/nutricion/procesarDatos/procesarInformacionNutricional.php?tipo=update&idRegistro=<?= $_REQUEST["idu"] ?>&idMatriculaDato=<?= $fetchUsuario[6] ?>" autocomplete="off">
	<h4 class="text-center mb-4"> Informacion Nutricional</h4>
	<div class="row">
		<?php 
		if(strlen($fetchUsuario[3]) < 2){$mes = '0'.$fetchUsuario[3];}else{$mes = $fetchUsuario[3];}
		if(strlen($fetchUsuario[4]) < 2){$dia = '0'.$fetchUsuario[4];}else{$dia = $fetchUsuario[4];}
		$year = $fetchUsuario[2];
		$fecha = $year.'-'.$mes.'-'.$dia;
		?>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Fecha <i style="color: darkorange">*</i></label>
			<input type="date" class="form-control" name="fecha" value="<?= $fecha ?>" required="true"/>
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Peso (En kilogramos)<i style="color: darkorange">*</i></label>
			<input type="number" class="form-control" name="peso" id="pesoEditar" value="<?= $fetchUsuario[7] ?>" required="true"/>
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Estatura (En centimetros)<i style="color: darkorange">*</i></label>
			<input type="number" class="form-control" name="estatura" id="estaturaEditar" value="<?= $fetchUsuario[8]*100 ?>" required="true"/>
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Circunferencia de la cintura <i style="color: darkorange">*</i></label>
			<input type="number" class="form-control" name="circunferenciaCintura" value="<?= $fetchUsuario[10] ?>" required="true"/>
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Circunferencia de la cadera <i style="color: darkorange">*</i></label>
			<input type="number" class="form-control" name="circunferenciaCadera" value="<?= $fetchUsuario[11] ?>" required="true"/>
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Riesgo cardiovascular <i style="color: darkorange">*</i></label>
			<input type="number" class="form-control" name="riesgoCardiovascular" value="<?= $fetchUsuario[12] ?>" required="true"/>
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Circunferencia de el brazo <i style="color: darkorange">*</i></label>
			<input type="number" class="form-control" name="circunferenciaBrazo" value="<?= $fetchUsuario[13] ?>" required="true"/>
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Circunferencia de la muñeca <i style="color: darkorange">*</i></label>
			<input type="number" class="form-control" name="circunferenciaMuneca" value="<?= $fetchUsuario[14] ?>" required="true"/>
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Circunferencia de la pantorrilla <i style="color: darkorange">*</i></label>
			<input type="number" class="form-control" name="circunferenciaPantorrilla" value="<?= $fetchUsuario[15] ?>" required="true"/>
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Circunferencia tobillo rodilla <i style="color: darkorange">*</i></label>
			<input type="number" class="form-control" name="circunferenciaTobilloRodilla" value="<?= $fetchUsuario[16] ?>" required="true"/>
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Complexion <i style="color: darkorange">*</i></label>
			<input type="number" class="form-control" name="complexion" value="<?= $fetchUsuario[17] ?>" required="true"/>
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Peso Referencia <i style="color: darkorange">*</i></label>
			<input type="number" class="form-control" name="pesoReferencia" id="pesoReferenciaEditar" value="<?= $fetchUsuario[20] ?>" required="true"/>
		</div>


		<div class="form-group col-md-4">
			<label for="recipient-name" class="form-control-label">Estado dental <i style="color: darkorange">*</i></label>
			<select class="form-control" name="estadoDental" id="estadoDentalEditar" required="true">
				<option value="">Selecciona una opción</option>
				<?php
				$queryAntecedente = mysqli_query($conn, "SELECT * FROM gddt_estados_dentales ;");
				while ($fetchAntecedente = mysqli_fetch_row($queryAntecedente)) {
					if ($fetchAntecedente[0] == $fetchUsuario[18]) {
						echo "<option selected value=" . $fetchAntecedente[0] . ">" . $fetchAntecedente[1] . "</option>";
					} else {
						echo "<option value=" . $fetchAntecedente[0] . ">" . $fetchAntecedente[1] . "</option>";
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="recipient-name" class="form-control-label">Clasificacion nutricional <i style="color: darkorange">*</i></label>
			<select class="form-control" name="clasificacionNutricional" id="clasificacionNutricionalEditar" required="true">
				<option value="">Selecciona una opción</option>
				<?php
				$queryEstadoDental = mysqli_query($conn, "SELECT id_clasif_nutricional, descripcion FROM gddt_clasif_nutricionales ;");
				while ($fetchEstadoDental = mysqli_fetch_row($queryEstadoDental)) {
					if ($fetchEstadoDental[0] == $fetchUsuario[18]) {
						echo "<option selected value=" . $fetchEstadoDental[0] . ">" . $fetchEstadoDental[1] . "</option>";
					} else {
						echo "<option value=" . $fetchEstadoDental[0] . ">" . $fetchEstadoDental[1] . "</option>";
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Imc <i style="color: darkorange">*</i></label>
			<input type="numeric" class="form-control" name="imc" id="imcEditar" value="<?= $fetchUsuario[9] ?>" required="true" />
		</div>
	</div>
	<!-- End Row -->
	<!-- Modal Footer -->
	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">
			Cancelar
		</button>
		<button type="button" class="btn btn-info" onclick="calcularIMCEDITAR()">
			Calcular IMC
		</button>
		<button type="submit" class="btn btn-primary" name="Actualizar">
			Actualizar
		</button>
	</div>
</div>    
</form>
<script type="text/javascript">
	function calcularIMCEDITAR(){
		let peso = document.getElementById('pesoEditar');
		let estatura = document.getElementById('estaturaEditar');
		if(peso.value == "" || estatura.value == ""	){
			if(peso == ''){
				swal({
					title: "Porfavor ingrese el peso para poder continuar",
					icon: "warning",
					buttons: true,
					dangerMode: true,
				})
			}else{
				swal({
					title: "Porfavor ingrese la estatura para poder continuar",
					icon: "warning",
					buttons: true,
					dangerMode: true,
				})
			}
		}else{
			estatura = estatura.value;
			estatura = estatura /100;
			estatura = estatura * estatura;
			let resultado = peso.value / estatura;
			resultado = resultado.toFixed(10);
			document.getElementById('imcEditar').value = resultado;
		}
	}


</script>