<?php require "../../../../session.php";



$sqlUsuario = "SELECT *,(CONVERT(CAST(CONVERT(seguimiento_descripcion USING latin1) AS BINARY) USING utf8))AS descripcion FROM gddt_seguimiento_nutricional WHERE id_seguimiento_nutricional =  ". $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);

?>
<form name="registrarSeguimientoNutricionalEditar" onsubmit="return registroSeguimientoNutricionalEditar(event);"  method="POST"action="<?php echo $base_url ?>pages/gerontologia/nutricion/procesarDatos/procesarSeguimientoNutricional.php?tipo=update&idRegistro=<?= $_REQUEST["idu"] ?>&idMatriculaDato=<?= $fetchUsuario[6] ?>" autocomplete="off">
	<h4 class="text-center mb-4"> Seguimiento Nutricional</h4>
	<div class="row">
		<?php 
		if(strlen($fetchUsuario[3]) < 2){$mes = '0'.$fetchUsuario[3];}else{$mes = $fetchUsuario[3];}
		if(strlen($fetchUsuario[4]) < 2){$dia = '0'.$fetchUsuario[4];}else{$dia = $fetchUsuario[4];}
		$year = $fetchUsuario[2];
		$fecha = $year.'-'.$mes.'-'.$dia;
		?>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Fecha de modificacion <i style="color: darkorange">*</i></label>
			<input type="date" class="form-control" name="fecha" value="<?= $fecha ?>" required="true"/>
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Tipo de seguimiento <i style="color: darkorange">*</i></label>
			<select class="form-control" name="tipoSeguimiento" id="tipoSeguimientoEditar" required="true">
				<option value="0">Selecciona una opción</option>
				<?php
				$sqlTipoSeguimiento = "SELECT (CONVERT(CAST(CONVERT(seguimiento_tipo USING latin1) AS BINARY) USING utf8))AS seguimiento_tipo FROM gddt_seguimiento_nutricional GROUP BY seguimiento_tipo";
				$queryTipoSeguimiento = $db->query($sqlTipoSeguimiento);
				$fetchTipoSeguimiento = $queryTipoSeguimiento->fetchAll(PDO::FETCH_OBJ);
				foreach ($fetchTipoSeguimiento as $fetch) {
					if($fetch->seguimiento_tipo == $fetchUsuario[7]){
						echo '<option selected value="'.$fetch->seguimiento_tipo.'">'.$fetch->seguimiento_tipo.'</option>';
					}else{
						echo '<option value="'.$fetch->seguimiento_tipo.'">'.$fetch->seguimiento_tipo.'</option>';
					}
					
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label">Descripcion <i style="color: darkorange">*</i></label>
			<textarea class="form-control" name="descripcion" cols="40" rows="5" required="true" ><?= $fetchUsuario[9] ?></textarea>
		</div>
	</div>
	<!-- End Row -->


	<!-- Modal Footer -->
	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">
			Cancelar
		</button>
		<button type="submit" class="btn btn-primary" name="Actualizar">
			Actualizar
		</button>
	</div>
</div>   
</form> 