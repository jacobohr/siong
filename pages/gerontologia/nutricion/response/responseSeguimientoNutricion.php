<?php

//include connection file 
require "../../../../session.php";

$id_matricula_dato = $_POST['id'];
//$params = $columns = $totalRecords = $data = array();

$params = filter_input_array(INPUT_POST);

//var_dump($params);

//define index of column
$columns = array(
	0 => 'numero',
    1 => 'id_seguimiento_nutricional',
    2 => 'nombreUsuario',
    3 => 'fecha',
    4 => 'hora',
    5 => 'seguimiento_tipo'
);

$sqlTot = $sqlRec = "";

// check search value exist
$where = " WHERE sn.id_matricula_dato = $id_matricula_dato";

if (isset($params['search']['value']) && $params['search']['value'] != '') {
    $where .= "
            AND 
                (sn.id_seguimiento_nutricional LIKE '%" . $params['search']['value'] . "%' 
                OR (CONVERT(CAST(CONVERT(CONCAT(UPPER(c.primer_nombre),' ',UPPER(c.segundo_nombre),' ',UPPER(c.primer_apellido),' ',UPPER(c.segundo_apellido)) USING latin1) AS BINARY) USING utf8)) LIKE '%" . $params['search']['value'] . "%'
                OR CONCAT(sn.dia_mod,'/',sn.mes_mod,'/',sn.ano_mod) LIKE '%" . $params['search']['value'] . "%' 
                OR hora_mod LIKE '%" . $params['search']['value'] . "%'
                OR sn.seguimiento_tipo LIKE '%" . $params['search']['value'] . "%')";
}


// getting total number records without any search
$sql = "SELECT 
        ROW_NUMBER() OVER() AS numero,
        sn.id_seguimiento_nutricional, 
        (CONVERT(CAST(CONVERT(CONCAT(UPPER(c.primer_nombre),' ',UPPER(c.segundo_nombre),' ',UPPER(c.primer_apellido),' ',UPPER(c.segundo_apellido)) USING latin1) AS BINARY) USING utf8)) AS nombreUsuario,
        CONCAT(sn.dia_mod,'/',sn.mes_mod,'/',sn.ano_mod) fecha,
        sn.hora_mod AS hora, 
        sn.seguimiento_tipo
        FROM gddt_seguimiento_nutricional sn
        INNER JOIN gddt_cuentas c ON c.id_cuenta = sn.id_cuenta";


$sqlTot .= $sql;
$sqlRec .= $sql;


//concatenate search sql if value exist
if (isset($where) && $where != '') {
    $sqlTot .= $where;
    $sqlRec .= $where;
}

$sqlRec .= " ORDER BY " . $columns[$params['order'][0]['column']] . "  " . $params['order'][0]['dir'] . "  LIMIT " . $params['start'] . " ," . $params['length'] . " ";

$queryTot = mysqli_query($conn, $sqlTot) or die("database error:" . mysqli_error($conn));

$totalRecords = mysqli_num_rows($queryTot);

$queryRecords = mysqli_query($conn, $sqlRec) or die("error to fetch employees data");

$data = array();
//iterate on results row and create new index array of data
while ($row = mysqli_fetch_row($queryRecords)) {
    $data[] = $row;
}

foreach ($data as $key => $val) {

    /*if (!in_array("82", $_SESSION["RESTRICCIONES"])) {*/
        $data[$key][6] = '
        &nbsp;
        <a href="#" style="color:#000;" onClick="fnVerSeguimientoNutricion(\'' . $val[1] . '\');">
        <span data-toggle="tooltip" title="Ver usuario" class="fa fa-eye"></span>
        </a>';    

        $data[$key][6] .= '
        &nbsp;
        <a href="#" style="color:#000;" data-toggle="modal" data-target="#modalModificarUsuario" onClick="fnEditarNutricionSeguimiento(\'' . $val[1] . '\');">
        <span data-toggle="tooltip" title="Editar usuario" class="fas fa-pencil-alt"></span>
        </a>
        ';
        $data[$key][6] .= '
        &nbsp;
        <a href="#" style="color:#000;" data-toggle="modal" data-target="#modalEliminarUsuario" onClick="fnEliminarSeguimientoNutricional(\'' . $val[1] . '\');">
        <span data-toggle="tooltip" title="Eliminar usuario" class="fa fa-times"></span>
        </a>';

}

$json_data = array(
    "draw" => intval($params['draw']),
    "recordsTotal" => intval($totalRecords),
    "recordsFiltered" => intval($totalRecords),
    "data" => $data   // total data array
);

echo json_encode($json_data);  // send data as json format