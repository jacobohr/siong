<?php

//incluye el archivo de conexion a la base de datos
include_once(dirname(__FILE__) . '/../../../session.php');

if(isset($_GET["term"])) {
	// Extraemos el query
	$query = $_GET["term"];
	// Buscamos coincidencias en la base de datos
	$sql = mysqli_query($conn, "SELECT id_enfermedad,codigo, des_enfermedad FROM gddt_enfermedades_cie WHERE (codigo LIKE '%{$query}%' OR des_enfermedad LIKE '%{$query}%')ORDER BY des_enfermedad ASC LIMIT 30");
	$array = array();

	$i = 0;
	while($row = mysqli_fetch_assoc($sql)) {

		$paciente = $row["codigo"]."-".$row["des_enfermedad"];

		$array[$i]['id'] = $row['id_enfermedad'];
		$array[$i]['text'] = $paciente;

		$i++;
	}

	// Retornamos el arreglo de json
	$result = ['results' => $array];
	echo json_encode($result);
}