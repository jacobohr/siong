<?php

//incluye el archivo de conexion a la base de datos
include_once(dirname(__FILE__) . '/../../../session.php');

if(isset($_GET["term"])) {
	// Extraemos el query
	$query = $_GET["term"];
	// Buscamos coincidencias en la base de datos
	$sql = mysqli_query($conn, "SELECT id_matricula_dato,num_documento, UPPER((CONVERT(CAST(CONVERT(CONCAT(primer_nombre,' ',segundo_nombre,' ',primer_apellido,' ',segundo_apellido) USING latin1) AS BINARY) USING utf8))) AS nom_paciente FROM gddt_matricula_datos  WHERE (primer_nombre LIKE '%{$query}%' OR segundo_nombre LIKE '%{$query}%' OR primer_apellido LIKE '%{$query}%' OR segundo_apellido LIKE '%{$query}%' OR num_documento LIKE '%{$query}%') GROUP BY id_matricula_dato ORDER BY primer_nombre ASC, segundo_nombre ASC, primer_apellido ASC, segundo_apellido ASC LIMIT 30");
	$array = array();

	$i = 0;
	while($row = mysqli_fetch_assoc($sql)) {

		$paciente = $row["num_documento"]."-".$row["nom_paciente"];

		$array[$i]['id'] = $row['num_documento'];
		$array[$i]['text'] = $paciente;

		$i++;
	}

	// Retornamos el arreglo de json
	$result = ['results' => $array];
	echo json_encode($result);
}