<?php

//include connection file 
require "../../../../session.php";

$sqlUsuario = "
SELECT 
UPPER((CONVERT(CAST(CONVERT(CONCAT(c.primer_nombre,' ',c.segundo_nombre,' ',c.primer_apellido,' ',c.segundo_apellido) USING latin1) AS BINARY) USING utf8))) AS nombreUsuario,
CONCAT(vd.ano_mod,'/',vd.mes_mod,'/',vd.dia_mod) AS fecha,  
vd.hora_mod,
(CONVERT(CAST(CONVERT(vd.barrio USING latin1) AS BINARY) USING utf8))AS barrio,
(CONVERT(CAST(CONVERT(vd.direccion USING latin1) AS BINARY) USING utf8))AS direccion,
vd.telefono,
(CONVERT(CAST(CONVERT(vd.objetivo_entrevista USING latin1) AS BINARY) USING utf8))AS objetivo_entrevista,
(CONVERT(CAST(CONVERT(vd.problematica_familiar USING latin1) AS BINARY) USING utf8))AS problematica_familiar,
(CONVERT(CAST(CONVERT(vd.tipologia USING latin1) AS BINARY) USING utf8))AS tipologia,
(CONVERT(CAST(CONVERT(vd.historia_familiar USING latin1) AS BINARY) USING utf8))AS historia_familiar,
(CONVERT(CAST(CONVERT(vd.relacion_subsistema USING latin1) AS BINARY) USING utf8))AS relacion_subsistema
FROM gddt_visita_domiciliaria vd
INNER JOIN gddt_cuentas c ON c.id_cuenta = vd.id_cuenta 
WHERE vd.id_visita_domiciliaria = " . $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);
$rows = mysqli_num_rows($queryUsuario);

if($rows == 0){
    echo "<p style='color:red;font-size:0.9em;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No se encontraron datos :(</p>";
}else{

    ?>

    <div class="content">
        <h6 class="text-center mb-4">Actualizado por <?= $fetchUsuario[0];?> el <?= $fetchUsuario[1]; ?> a las <?= $fetchUsuario[2]; ?></h6>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Barrio
            </label>
            <p><?php if(empty($fetchUsuario[3])){echo "Sin dato";}else{echo $fetchUsuario[3];} ?></p>
        </div>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Dirreción
            </label>
            <p><?php if(empty($fetchUsuario[4])){echo "Sin dato";}else{echo $fetchUsuario[4];} ?></p>
        </div>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Teléfono
            </label>
            <p><?php if(empty($fetchUsuario[5])){echo "Sin dato";}else{echo $fetchUsuario[5];} ?></p>
        </div>
        <h6><b>NOMBRE DE LAS PERSONAS ENTREVISTADAS Y NEXO</b></h6>
        <?php 
        $sqlPersonasEntrevistadasPDO = "
        SELECT 
        pe.nombre, 
        n.nexo 
        FROM gddt_personas_entrevistadas pe, gddt_nexos n 
        WHERE pe.id_nexo = n.id_nexo 
        AND pe.id_visita_domiciliaria = ". $_REQUEST["idu"];
        $queryPersonasEntrevistadasPDO = $db->query($sqlPersonasEntrevistadasPDO);
        $fetchPersonasEntrevistadasPDO = $queryPersonasEntrevistadasPDO->fetchAll(PDO::FETCH_OBJ);
        if(empty($fetchPersonasEntrevistadasPDO)){
            ?>
            <div class="user-block col-sm-12 col-xs-12">
                <p style='color:red;font-size:0.9em;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No se encontraron datos :(</p>
            </div>
        <?php }else{
            foreach ($fetchPersonasEntrevistadasPDO as $fetch) {
                echo '<div class="user-block col-sm-4 col-xs-6">';
                echo '<label>Nombre</label>';
                echo "<p>{$fetch->nombre}</p>";
                echo '</div>';
                echo '<div class="user-block col-sm-8 col-xs-6">';
                echo '<label>Nexo</label>';
                echo "<p>{$fetch->nexo}</p>";
                echo '</div>';
            }
        }
        ?>
        <div class="user-block col-sm-12 col-xs-12">
            <label>
             Objetivo de la entrevista
         </label>
         <p><?php if(empty($fetchUsuario[6])){echo "Sin dato";}else{echo $fetchUsuario[6];} ?></p>
     </div>
     <h6 class="text-center"><b>DATOS FAMILIARES</b></h6>
     <h6><b>PERSONAS QUE HABITAN LA VIVIENDA</b></h6>
     <?php 
     $sqlPersonasVivienda = "
     SELECT 
     df.nombre, 
     n.nexo, 
     df.edad, 
     ne.descripcion, 
     df.ocupacion 
     FROM gddt_datos_familiares df
     INNER JOIN gddt_nexos n ON df.parentesco = n.id_nexo
     INNER JOIN gddt_nivel_estudios ne ON df.id_nivel_estudio = ne.id_nivel_estudio
     WHERE df.id_visita_domiciliaria = ". $_REQUEST["idu"];
     $queryPersonasVivienda = $db->query($sqlPersonasVivienda);
     $fetchPersonasVivienda = $queryPersonasVivienda->fetchAll(PDO::FETCH_OBJ);
     if(empty($fetchPersonasVivienda)){
       ?>
       <div class="user-block col-sm-12 col-xs-12">
        <p style='color:red;font-size:0.9em;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No se encontraron datos :(</p>
    </div>
<?php }else{
    echo '<table class="table table-bordered">';
    echo '<thead>';
    echo '<tr>';
    echo '<th scope="col">Nombre</th>';
    echo '<th scope="col">Parentesco</th>';
    echo '<th scope="col">Edad</th>';
    echo '<th scope="col">Escolaridad</th>';
    echo '<th scope="col">Ocupación</th>';
    echo '</tr>';
    echo '</thead>';
    echo '<tbody>';
    foreach ($fetchPersonasVivienda as $fetch) {
        echo '<tr>';
        echo '<th scope="row">'.$fetch->nombre.'</th>';
        echo '<td>'.$fetch->nexo.'</td>';
        echo '<td>'.$fetch->edad.'</td>';
        echo '<td>'.$fetch->descripcion.'</td>';
        echo '<td>'.$fetch->ocupacion.'</td>';
        echo '</tr>';
    }
    echo '</tbody>';
    echo '</table>';
} ?>
<h6><b>SITUACION FAMILIAR</b></h6>
<div class="user-block col-sm-12 col-xs-12">
    <label>
      Tipologia
  </label>
  <p><?php if(empty($fetchUsuario[8])){echo "Sin dato";}else{echo $fetchUsuario[8];} ?></p>
</div>
<div class="user-block col-sm-12 col-xs-12">
    <label>
      Elementos de La historia familiar
  </label>
  <p><?php if(empty($fetchUsuario[9])){echo "Sin dato";}else{echo $fetchUsuario[9];} ?></p>
</div>
<div class="user-block col-sm-12 col-xs-12">
    <label>
      Relaciones entre subsistemas
  </label>
  <p><?php if(empty($fetchUsuario[10])){echo "Sin dato";}else{echo $fetchUsuario[10];} ?></p>
</div>



<div class="user-block">&nbsp;</div>

</div><!-- Fin del content -->
<?php } ?>