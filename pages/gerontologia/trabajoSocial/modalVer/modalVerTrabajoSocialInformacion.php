<?php

//include connection file 
require "../../../../session.php";

$sqlUsuario = "
SELECT 
UPPER((CONVERT(CAST(CONVERT(CONCAT(c.primer_nombre,' ',c.segundo_nombre,' ',c.primer_apellido,' ',c.segundo_apellido) USING latin1) AS BINARY) USING utf8))) AS nombreUsuario,
CONCAT(its.ano_mod,'/',its.mes_mod,'/',its.dia_mod) AS fecha, 
its.hora_mod,
its.adicciones,
its.entidad_recuperacion,
ti.tiempo_indigencia,
(CONVERT(CAST(CONVERT(its.lugar_frecuenta USING latin1) AS BINARY) USING utf8))AS lugar_frecuenta,
(CONVERT(CAST(CONVERT(its.motivo_calle USING latin1) AS BINARY) USING utf8))AS motivo_calle,
(CONVERT(CAST(CONVERT(its.situacion_familiar USING latin1) AS BINARY) USING utf8))AS situacion_familiar,
(CONVERT(CAST(CONVERT(its.tipologia USING latin1) AS BINARY) USING utf8))AS tipologia,
(CONVERT(CAST(CONVERT(its.posibilidad_reintegro_familiar USING latin1) AS BINARY) USING utf8))AS posibilidad_reintegro_familiar
FROM gddt_informacion_trabajo_social its
INNER JOIN gddt_cuentas c ON c.id_cuenta = its.id_cuenta 
INNER JOIN gddt_tiempo_indigencia ti ON ti.id_tiempo_indigencia = its.id_tiempo_indigencia
WHERE its.id_informacion_trabajo_social = " . $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);
$rows = mysqli_num_rows($queryUsuario);

if($rows == 0){
    echo "<p style='color:red;font-size:0.9em;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No se encontraron datos :(</p>";
}else{



?>

<div class="content">
<h6 class="text-center mb-4">Actualizado por <?= $fetchUsuario[0];?> el <?= $fetchUsuario[1]; ?> a las <?= $fetchUsuario[2]; ?></h6>
 <div class="user-block col-sm-4 col-xs-6">
    <label>
        Adicciones
    </label>
    <p><?php if(empty($fetchUsuario[3])){echo "Sin dato";}else{echo $fetchUsuario[3];} ?></p>
</div>
 <div class="user-block col-sm-8 col-xs-6">
    <label>
        ¿Ha estado en alguna entidad de recuperación? ¿Cual?
    </label>
    <p><?php if(empty($fetchUsuario[4])){echo "Sin dato";}else{echo $fetchUsuario[4];} ?></p>
</div>
 <div class="user-block col-sm-4 col-xs-6">
    <label>
        Tiempo de indigencia
    </label>
    <p><?php if(empty($fetchUsuario[5])){echo "Sin dato";}else{echo $fetchUsuario[5];} ?></p>
</div>
 <div class="user-block col-sm-4 col-xs-6">
    <label>
        Lugar que frecuenta
    </label>
    <p><?php if(empty($fetchUsuario[6])){echo "Sin dato";}else{echo $fetchUsuario[6];} ?></p>
</div>
 <div class="user-block col-sm-4 col-xs-6">
    <label>
        Motivo que lo impulso a la calle
    </label>
    <p><?php if(empty($fetchUsuario[7])){echo "Sin dato";}else{echo $fetchUsuario[7];} ?></p>
</div>
 <div class="user-block col-sm-12 col-xs-12">
    <label>
        Situcación familiar
    </label>
    <p><?php if(empty($fetchUsuario[8])){echo "Sin dato";}else{echo $fetchUsuario[8];} ?></p>
</div>
 <div class="user-block col-sm-12 col-xs-12">
    <label>
        Tipologia
    </label>
    <p><?php if(empty($fetchUsuario[9])){echo "Sin dato";}else{echo $fetchUsuario[9];} ?></p>
</div>
 <div class="user-block col-sm-12 col-xs-12">
    <label>
        Posibiliades de reintegro familiar
    </label>
    <p><?php if(empty($fetchUsuario[10])){echo "Sin dato";}else{echo $fetchUsuario[10];} ?></p>
</div>

<div class="user-block">&nbsp;</div>

</div><!-- Fin del content -->
<?php } ?>