<?php

//include connection file 
require "../../../../session.php";

$sqlUsuario = "
SELECT 
UPPER((CONVERT(CAST(CONVERT(CONCAT(c.primer_nombre,' ',c.segundo_nombre,' ',c.primer_apellido,' ',c.segundo_apellido) USING latin1) AS BINARY) USING utf8))) AS nombreUsuario,
CONCAT(asp.ano_mod,'/',asp.mes_mod,'/',asp.dia_mod) AS fecha,  
asp.hora_mod,
asp.caracteristica_vivienda,
asp.piso,
asp.techo,
asp.pared,
asp.tendencia,
(CONVERT(CAST(CONVERT(asp.condicion_higienica USING latin1) AS BINARY) USING utf8))AS condicion_higienica,
(CONVERT(CAST(CONVERT(asp.distribucion_vivienda USING latin1) AS BINARY) USING utf8))AS distribucion_vivienda,
(CONVERT(CAST(CONVERT(asp.acceso_vivienda USING latin1) AS BINARY) USING utf8))AS acceso_vivienda,
asp.caracteristica_economica,
(CONVERT(CAST(CONVERT(asp.ingreso_mensual USING latin1) AS BINARY) USING utf8))AS ingreso_mensual,
asp.fuente_ingreso,
(CONVERT(CAST(CONVERT(asp.proveedor_economico USING latin1) AS BINARY) USING utf8))AS proveedor_economico,
(CONVERT(CAST(CONVERT(asp.observacion USING latin1) AS BINARY) USING utf8))AS observacion,
(CONVERT(CAST(CONVERT(asp.recomendacion USING latin1) AS BINARY) USING utf8))AS recomendacion
FROM gddt_aspecto_socioeconomico asp
INNER JOIN gddt_cuentas c ON c.id_cuenta = asp.id_cuenta 
WHERE asp.id_aspecto_socioeconomico = " . $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);
$rows = mysqli_num_rows($queryUsuario);

if($rows == 0){
    echo "<p style='color:red;font-size:0.9em;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No se encontraron datos :(</p>";
}else{

    ?>

    <div class="content">
        <h6 class="text-center mb-4">Actualizado por <?= $fetchUsuario[0];?> el <?= $fetchUsuario[1]; ?> a las <?= $fetchUsuario[2]; ?></h6>
        <div class="user-block col-sm-12 col-xs-12">
            <h6 class="text-center mb-4"><b>ASPECTO SOCIO ECONOMICO</b></h6>
        </div>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Características de la vivienda
            </label>
            <p>
                <?php 
                if(empty($fetchUsuario[3])){
                    echo "Sin dato";
                }else{
                    switch ($fetchUsuario[3]) {
                        case 'p':echo 'Propia';break;
                        case 'a':echo 'Alquilada';break;
                        case 'h':echo 'Hipotecada';break;
                        case 'c':echo 'Comodato';break;
                        case 'p':echo 'Pieza';break;
                        case 't':echo 'Tugurio';break;
                        case 'i':echo 'Invación';break;
                        case 'ca':echo 'Calle';break;
                    }
                } ?>
            </p>
        </div>
        
        <div class="user-block col-sm-12 col-xs-12">
            <h6 class="text-center mb-4"><b>MATERIAL DE CONSTRUCCION</b></h6>
        </div>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Pisos
            </label>
            <p><?php if(empty($fetchUsuario[4])){echo "Sin dato";}else{echo $fetchUsuario[4];} ?></p>
        </div>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Techos
            </label>
            <p><?php if(empty($fetchUsuario[5])){echo "Sin dato";}else{echo $fetchUsuario[5];} ?></p>
        </div>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Paredes
            </label>
            <p><?php if(empty($fetchUsuario[6])){echo "Sin dato";}else{echo $fetchUsuario[6];} ?></p>
        </div>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Tendencia
            </label>
            <p>
                <?php 
                if(empty($fetchUsuario[7])){
                    echo "Sin dato";
                }else{
                    switch ($fetchUsuario[7]) {
                        case 'p':echo 'Propietario';break;
                        case 'a':echo 'Arrendatario';break;
                        case 'c':echo 'Comodato';break;
                    }
                } ?>
            </p>
        </div>
        <div class="user-block col-sm-12 col-xs-12">
            <label>
                Servicios
            </label>
            <div class="row">
                <?php 
                $sqlServicios = "
                SELECT 
                id_aspecto_socioeconomicoxservicios,(CONVERT(CAST(CONVERT(servicio USING latin1) AS BINARY) USING utf8))AS servicio 
                FROM gddt_aspecto_socioeconomicoxservicios WHERE id_aspecto_socioeconomico =  ". $_REQUEST["idu"];
                $querServicios = $db->query($sqlServicios);
                $fetchServicios = $querServicios->fetchAll(PDO::FETCH_OBJ);
                foreach ($fetchServicios as $fetch) {
                    echo '<div class="user-block col-sm-4 col-xs-6">';
                    echo '<p>'. $fetch->servicio.'</p>';
                    echo '</div>';
                }
                ?>
            </div>
        </div>
        <div class="user-block col-sm-12 col-xs-12">
            <label>
                Condiciones Higenicas
            </label>
            <p><?php if(empty($fetchUsuario[8])){echo "Sin dato";}else{echo $fetchUsuario[8];} ?></p>
        </div>
        <div class="user-block col-sm-12 col-xs-12">
            <label>
                Distribución de la Vivienda(# alcobas, # personas por pieza y cama)
            </label>
            <p><?php if(empty($fetchUsuario[9])){echo "Sin dato";}else{echo $fetchUsuario[9];} ?></p>
        </div>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Acceso de la vivienda
            </label>
            <p><?php if(empty($fetchUsuario[10])){echo "Sin dato";}else{echo $fetchUsuario[10];} ?></p>
        </div>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Características económicas
            </label>
            <p><?php if(empty($fetchUsuario[11])){echo "Sin dato";}else{echo $fetchUsuario[11];} ?></p>
        </div>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Ingreso Mensual de la Familia
            </label>
            <p><?php if(empty($fetchUsuario[12])){echo "Sin dato";}else{echo $fetchUsuario[12];} ?></p>
        </div>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Fuentes de ingreso
            </label>
            <p><?php if(empty($fetchUsuario[13])){echo "Sin dato";}else{echo $fetchUsuario[13];} ?></p>
        </div>
        <div class="user-block col-sm-8 col-xs-6">
            <label>
                Proveedores económico de la familia
            </label>
            <p><?php if(empty($fetchUsuario[14])){echo "Sin dato";}else{echo $fetchUsuario[14];} ?></p>
        </div>
        <div class="user-block col-sm-12 col-xs-12">
            <label>
                Observaciones
            </label>
            <p><?php if(empty($fetchUsuario[15])){echo "Sin dato";}else{echo $fetchUsuario[15];} ?></p>
        </div>
        <div class="user-block col-sm-12 col-xs-12">
            <label>
                Recomendaciones
            </label>
            <p><?php if(empty($fetchUsuario[16])){echo "Sin dato";}else{echo $fetchUsuario[16];} ?></p>
        </div>

        <div class="user-block">&nbsp;</div>

    </div><!-- Fin del content -->
    <?php } ?>