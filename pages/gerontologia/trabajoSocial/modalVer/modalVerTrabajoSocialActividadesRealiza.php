<?php

//include connection file 
require "../../../../session.php";

$sqlUsuario = "
SELECT 
UPPER((CONVERT(CAST(CONVERT(CONCAT(c.primer_nombre,' ',c.segundo_nombre,' ',c.primer_apellido,' ',c.segundo_apellido) USING latin1) AS BINARY) USING utf8))) AS nombreUsuario,
sg.fecha, 
sg.hora_mod,
sg.participo,
(CONVERT(CAST(CONVERT(sg.descripcion USING latin1) AS BINARY) USING utf8))AS descripcion 
FROM gddt_actividades_realiza_tsocial sg
INNER JOIN gddt_cuentas c ON c.id_cuenta = sg.id_cuenta 
WHERE sg.id_actividad_realiza_tsocial =   " . $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);
$rows = mysqli_num_rows($queryUsuario);

if($rows == 0){
    echo "<p style='color:red;font-size:0.9em;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No se encontraron datos :(</p>";
}else{

    ?>

    <div class="content">
        <h6 class="text-center mb-4">Actualizado por <?= $fetchUsuario[0];?> el <?= $fetchUsuario[1]; ?> a las <?= $fetchUsuario[2]; ?></h6>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Participo?
            </label>
            <p><?php if(empty($fetchUsuario[3])){echo "Sin dato";}else{echo $fetchUsuario[3];} ?></p>
        </div>
        <div class="user-block col-sm-8 col-xs-6">
            <label>
                Descripcion
            </label>
            <p><?php if(empty($fetchUsuario[4])){echo "Sin dato";}else{echo $fetchUsuario[4];} ?></p>
        </div>

        <div class="user-block">&nbsp;</div>

    </div><!-- Fin del content -->
    <?php } ?>