<?php

//include connection file 
require "../../../../session.php";

$sqlUsuario = "
SELECT 
UPPER((CONVERT(CAST(CONVERT(CONCAT(c.primer_nombre,' ',c.segundo_nombre,' ',c.primer_apellido,' ',c.segundo_apellido) USING latin1) AS BINARY) USING utf8))) AS nombreUsuario,
CONCAT(raf.ano_mod,'/',raf.mes_mod,'/',raf.dia_mod) AS fecha,  
raf.hora_mod
FROM gddt_red_apoyo_familiar raf
INNER JOIN gddt_cuentas c ON c.id_cuenta = raf.id_cuenta 
WHERE raf.id_red_apoyo_familiar = " . $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);
$rows = mysqli_num_rows($queryUsuario);

$sqlUsuarioDatos = "SELECT rfd.nombre,n.nexo,rfd.id_nexo_amigo,rfd.barrio,rfd.telefono,rfd.tipo_apoyo,rfd.red_apoyo FROM gddt_red_apoyo_familiar_datos rfd
INNER JOIN gddt_nexos n ON n.id_nexo = rfd.id_nexo
LEFT JOIN gddt_nexos_amigos na ON na.id_nexo_amigo = rfd.id_nexo_amigo
WHERE id_red_apoyo_familiar = " . $_REQUEST["idu"];
$queryUsuarioDatos = $db->query($sqlUsuarioDatos);
$fetchUsuarioDatos = $queryUsuarioDatos->fetchAll(PDO::FETCH_OBJ);

if($rows == 0){
    echo "<p style='color:red;font-size:0.9em;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No se encontraron datos :(</p>";
}else{

    ?>

    <div class="content">
        <h6 class="text-center mb-4">Actualizado por <?= $fetchUsuario[0];?> el <?= $fetchUsuario[1]; ?> a las <?= $fetchUsuario[2]; ?></h6>
        <div class="user-block col-sm-12 col-xs-12">
            <table class="table table-sm table-bordered">
                <thead>
                    <tr>
                      <th scope="col">Nombre y apellido</th>
                      <th scope="col">Parentesco primario</th>
                      <th scope="col">Parentesco secundario</th>
                      <th scope="col">Barrio</th>
                      <th scope="col">Teléfono</th>
                      <th scope="col">Tipo apoyo</th>
                      <th scope="col">Red de apoyo</th>
                  </tr>
              </thead>
              <tbody>
                <?php 
                foreach ($fetchUsuarioDatos as $fetch) {
                    echo '<tr>';
                    echo '<td>'.$fetch->nombre.'</td>';
                    echo '<td>'.$fetch->nexo.'</td>';
                    echo '<td>';if($fetch->id_nexo_amigo == 0){echo '';}else{echo $fetch->id_nexo_amigo;} echo'</td>';
                    echo '<td>'.$fetch->barrio.'</td>';
                    echo '<td>'.$fetch->telefono.'</td>';
                    echo '<td>'.$fetch->tipo_apoyo.'</td>';
                    echo '<td>'.$fetch->red_apoyo.'</td>';
                    echo '</tr>';
                }
                ?>
            </tbody>
        </table>
    </div>

    <div class="user-block">&nbsp;</div>

</div><!-- Fin del content -->
<?php } ?>