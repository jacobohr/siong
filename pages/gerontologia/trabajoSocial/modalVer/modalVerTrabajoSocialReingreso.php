<?php

//include connection file 
require "../../../../session.php";

$sqlUsuario = "
SELECT
UPPER((CONVERT(CAST(CONVERT(CONCAT(c.primer_nombre,' ',c.segundo_nombre,' ',c.primer_apellido,' ',c.segundo_apellido) USING latin1) AS BINARY) USING utf8))) AS nombreUsuario,
CONCAT(r.ano_mod,'/',r.mes_mod,'/',r.dia_mod) AS fecha,  
r.hora_mod,
CONCAT(r.ano_egreso,'/',r.mes_egreso,'/',r.dia_egreso) AS fecha_egreso,
r.hora_egreso,
(CONVERT(CAST(CONVERT(r.motivo_reingreso USING latin1) AS BINARY) USING utf8))AS motivo_reingreso,
(CONVERT(CAST(CONVERT(r.remitido_de USING latin1) AS BINARY) USING utf8))AS remitido_de,
(CONVERT(CAST(CONVERT(r.recomendacion USING latin1) AS BINARY) USING utf8))AS recomendacion
FROM gddt_reingresos r
INNER JOIN gddt_cuentas c ON c.id_cuenta = r.id_cuenta 
WHERE r.id_reingreso = " . $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);
$rows = mysqli_num_rows($queryUsuario);

if($rows == 0){
    echo "<p style='color:red;font-size:0.9em;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No se encontraron datos :(</p>";
}else{

    ?>

    <div class="content">
        <h6 class="text-center mb-4">Actualizado por <?= $fetchUsuario[0];?> el <?= $fetchUsuario[1]; ?> a las <?= $fetchUsuario[2]; ?></h6>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Fecha de reingreso
            </label>
            <p><?php if(empty($fetchUsuario[1])){echo "Sin dato";}else{echo $fetchUsuario[1];} ?></p>
        </div>
        <div class="user-block col-sm-4 col-xs-6">
            <label>
                Fecha egreso
            </label>
            <p><?php if(empty($fetchUsuario[3])){echo "Sin dato";}else{echo $fetchUsuario[3];} ?></p>
        </div>
        <div class="user-block col-sm-12 col-xs-12">
            <label>
                Motivo de reingreso
            </label>
            <p><?php if(empty($fetchUsuario[5])){echo "Sin dato";}else{echo $fetchUsuario[5];} ?></p>
        </div>
        <div class="user-block col-sm-12 col-xs-12">
            <label>
                Remitido por
            </label>
            <p><?php if(empty($fetchUsuario[6])){echo "Sin dato";}else{echo $fetchUsuario[6];} ?></p>
        </div>
        <div class="user-block col-sm-12 col-xs-12">
            <label>
                Recomendaciones Especiales
            </label>
            <p><?php if(empty($fetchUsuario[7])){echo "Sin dato";}else{echo $fetchUsuario[7];} ?></p>
        </div>


        <div class="user-block">&nbsp;</div>

    </div><!-- Fin del content -->
    <?php } ?>