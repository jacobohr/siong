<?php

//include connection file 
require "../../../../session.php";

$sqlUsuario = "
	SELECT 
    UPPER((CONVERT(CAST(CONVERT(CONCAT(c.primer_nombre,' ',c.segundo_nombre,' ',c.primer_apellido,' ',c.segundo_apellido) USING latin1) AS BINARY) USING utf8))) AS nombreUsuario,
    CONCAT(tp.ano_mod,'/',tp.mes_mod,'/',tp.dia_mod) AS fecha,  
    tp.hora_mod,
    tp.seguimiento_tipo,
    (CONVERT(CAST(CONVERT(tp.seguimiento_descripcion USING latin1) AS BINARY) USING utf8))AS descripcion
    FROM gddt_seguimiento_t_social tp
    INNER JOIN gddt_cuentas c ON c.id_cuenta = tp.id_cuenta 
    WHERE tp.id_seguimiento_tsocial =" . $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);
$rows = mysqli_num_rows($queryUsuario);


if($rows == 0){
    echo "<p style='color:red;font-size:0.9em;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No se encontraron datos :(</p>";
}else{


?>

<div class="content">
<h6 class="text-center mb-4">Actualizado por <?= $fetchUsuario[0];?> el <?= $fetchUsuario[1]; ?> a las <?= $fetchUsuario[2]; ?></h6>
 <div class="user-block col-sm-12 col-xs-12">
    <label>
        Tipo
    </label>
    <p><?php  if(empty($fetchUsuario[3])){echo 'Sin datos';}else{ echo $fetchUsuario[3];} ?></p>
</div>

 <div class="user-block col-sm-12 col-xs-12">
    <label>
        Descripcion
    </label>
    <p><?php  if(empty($fetchUsuario[4])){echo 'Sin datos';}else{ echo $fetchUsuario[4];} ?></p>
</div>


<div class="user-block">&nbsp;</div>

</div><!-- Fin del content -->
<?php } ?>