<?php require "../../../../session.php";



$sqlUsuario = "
SELECT 
*,
(CONVERT(CAST(CONVERT(lugar_frecuenta USING latin1) AS BINARY) USING utf8))AS lugar_frecuenta,
(CONVERT(CAST(CONVERT(motivo_calle USING latin1) AS BINARY) USING utf8))AS motivo_calle,
(CONVERT(CAST(CONVERT(situacion_familiar USING latin1) AS BINARY) USING utf8))AS situacion_familiar,
(CONVERT(CAST(CONVERT(tipologia USING latin1) AS BINARY) USING utf8))AS tipologia,
(CONVERT(CAST(CONVERT(posibilidad_reintegro_familiar USING latin1) AS BINARY) USING utf8))AS posibilidad_reintegro_familiar
FROM gddt_informacion_trabajo_social 
WHERE id_informacion_trabajo_social =". $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);

?>
<form name="editarUsuario" method="POST" action="procesarPsicologiaAntceMental.php?tipo=update" enctype="multipart/form-data">
	<h4 class="text-center mb-4"> Informacion General </h4>
	<div class="row">
		<div class="form-group col-md-12">
			<label for="recipient-name" class="form-control-label">Usuario que registra <i style="color: darkorange">*</i></label>
			<select class="form-control" name="usuario" >
				<option>Selecciona una opción</option>
				<?php
				$queryCuenta = mysqli_query($conn, "SELECT id_cuenta,
					UPPER((CONVERT(CAST(CONVERT(CONCAT(primer_nombre,' ',segundo_nombre,' ',primer_apellido,' ',segundo_apellido) USING latin1) AS BINARY) USING utf8))) AS nombreUsuario FROM gddt_cuentas ;");
				while ($fetchCuenta = mysqli_fetch_row($queryCuenta)) {
					if ($fetchCuenta[0] == $fetchUsuario[1]) {
						echo "<option selected value=" . $fetchCuenta[0] . ">" . $fetchCuenta[1] . "</option>";
					} else {
						echo "<option value=" . $fetchCuenta[0] . ">" . $fetchCuenta[1] . "</option>";
					}
				}
				?>
			</select>
		</div>
		<?php 
		if(strlen($fetchUsuario[3]) < 2){$mes = '0'.$fetchUsuario[3];}else{$mes = $fetchUsuario[3];}
		if(strlen($fetchUsuario[4]) < 2){$dia = '0'.$fetchUsuario[4];}else{$dia = $fetchUsuario[4];}
		$year = $fetchUsuario[2];
		$fecha = $year.'-'.$mes.'-'.$dia;
		?>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label">Fecha de modificacion <i style="color: darkorange">*</i></label>
			<input type="date" class="form-control" name="fechaMod" value="<?= $fecha ?>"/>
		</div>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label">Adicciones <i style="color: darkorange">*</i></label>
			<input type="text" class="form-control" name="adicciones" value="<?= $fetchUsuario[7] ?>"/>
		</div>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label">¿Ha estado en alguna entidad de recuperación? ¿Cual? <i style="color: darkorange">*</i></label>
			<textarea class="form-control" name="descripcionSeguimiento" cols="40" rows="5" ><?= $fetchUsuario[8] ?></textarea>
		</div>
		<div class="form-group col-md-12">
			<label for="recipient-name" class="form-control-label">Tiempo de indigencia <i style="color: darkorange">*</i></label>
			<select class="form-control" name="tiempoIndigencia" >
				<option>Selecciona una opción</option>
				<?php
				$queryTiempoindigencia = mysqli_query($conn, "SELECT id_tiempo_indigencia, tiempo_indigencia FROM gddt_tiempo_indigencia;");
				while ($fetchTiempoindigencia = mysqli_fetch_row($queryTiempoindigencia)) {
					if ($fetchTiempoindigencia[0] == $fetchUsuario[9]) {
						echo "<option selected value=" . $fetchTiempoindigencia[0] . ">" . $fetchTiempoindigencia[1] . "</option>";
					} else {
						echo "<option value=" . $fetchTiempoindigencia[0] . ">" . $fetchTiempoindigencia[1] . "</option>";
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label">Lugar que frecuenta <i style="color: darkorange">*</i></label>
			<input type="text" class="form-control" name="lugarFrecuenta" value="<?= $fetchUsuario[15] ?>"/>
		</div><div class="form-group col-md-12">
			<label for="message-text" class="form-control-label">Motivo que lo impulso a la calle <i style="color: darkorange">*</i></label>
			<input type="text" class="form-control" name="motivoCalle" value="<?= $fetchUsuario[16] ?>"/>
		</div>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label">Situación familiar <i style="color: darkorange">*</i></label>
			<textarea class="form-control" name="situacionFamiliar" cols="40" rows="5" ><?= $fetchUsuario[17] ?></textarea>
		</div>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label">Tipologia <i style="color: darkorange">*</i></label>
			<textarea class="form-control" name="tipologia" cols="40" rows="5" ><?= $fetchUsuario[18] ?></textarea>
		</div>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label">Posibiliades de reintegro familiar <i style="color: darkorange">*</i></label>
			<textarea class="form-control" name="posibiliadesReintegroFamiliar" cols="40" rows="5" ><?= $fetchUsuario[19] ?></textarea>
		</div>
	</div>
	<!-- End Row -->


	<!-- Modal Footer -->
	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">
			Cancelar
		</button>
		<button type="submit" class="btn btn-primary" name="Actualizar">
			Actualizar
		</button>
	</div>
</div>    