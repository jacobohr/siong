<?php require "../../../../session.php";



$sqlUsuario = "SELECT * FROM gddt_red_apoyo_familiar WHERE id_red_apoyo_familiar = ". $_REQUEST["idu"];
$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);
$rows = mysqli_num_rows($queryUsuario);

if($rows == 0){
	echo "<p style='color:red;font-size:0.9em;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No se encontraron datos :(</p>";
}else{
	?>
	<form name="editarUsuario" method="POST" action="procesarPsicologiaAntceMental.php?tipo=update" enctype="multipart/form-data">
		<h4 class="text-center mb-4"> Red de apoyo familiar </h4>
		<div class="row">
			<div class="form-group col-md-12">
				<label for="recipient-name" class="form-control-label">Usuario que registra <i style="color: darkorange">*</i></label>
				<select class="form-control" name="usuario" >
					<option>Selecciona una opción</option>
					<?php
					$queryCuenta = mysqli_query($conn, "SELECT id_cuenta,
						UPPER((CONVERT(CAST(CONVERT(CONCAT(primer_nombre,' ',segundo_nombre,' ',primer_apellido,' ',segundo_apellido) USING latin1) AS BINARY) USING utf8))) AS nombreUsuario FROM gddt_cuentas ;");
					while ($fetchCuenta = mysqli_fetch_row($queryCuenta)) {
						if ($fetchCuenta[0] == $fetchUsuario[1]) {
							echo "<option selected value=" . $fetchCuenta[0] . ">" . $fetchCuenta[1] . "</option>";
						} else {
							echo "<option value=" . $fetchCuenta[0] . ">" . $fetchCuenta[1] . "</option>";
						}
					}
					?>
				</select>
			</div>
			<?php 
			if(strlen($fetchUsuario[3]) < 2){$mes = '0'.$fetchUsuario[3];}else{$mes = $fetchUsuario[3];}
			if(strlen($fetchUsuario[4]) < 2){$dia = '0'.$fetchUsuario[4];}else{$dia = $fetchUsuario[4];}
			$year = $fetchUsuario[2];
			$fecha = $year.'-'.$mes.'-'.$dia;
			?>
			<div class="form-group col-md-12">
				<label for="message-text" class="form-control-label">Fecha de modificacion <i style="color: darkorange">*</i></label>
				<input type="date" class="form-control" name="fechaMod" value="<?= $fecha ?>"/>
			</div>
			<div class="form-group col-md-12">
				<table class="table table-sm table-bordered">
					<thead>
						<tr>
							<th scope="col">Nombre y apellido</th>
							<th scope="col">Parentesco primario</th>
							<th scope="col">Parentesco secundario</th>
							<th scope="col">Barrio</th>
							<th scope="col">Teléfono</th>
							<th scope="col">Tipo apoyo</th>
							<th scope="col">Red de apoyo</th>
						</tr>
					</thead>
					<tbody>
						<?php 
						$sqlUsuarioDatos = "SELECT nombre,id_nexo,id_nexo_amigo,barrio,telefono,tipo_apoyo,red_apoyo FROM gddt_red_apoyo_familiar_datos  WHERE id_red_apoyo_familiar = " . $_REQUEST["idu"];
						$queryUsuarioDatos = $db->query($sqlUsuarioDatos);
						$fetchUsuarioDatos = $queryUsuarioDatos->fetchAll(PDO::FETCH_OBJ);

						$sqlUsuarioNexo = "SELECT id_nexo, nexo FROM gddt_nexos";
						$queryUsuarioNexo = $db->query($sqlUsuarioNexo);
						$fetchUsuarioNexo = $queryUsuarioNexo->fetchAll(PDO::FETCH_OBJ);

						$sqlUsuarioNexoAmigos = "SELECT id_nexo_amigo, nexo FROM gddt_nexos_amigos";
						$queryUsuarioNexoAmigos = $db->query($sqlUsuarioNexoAmigos);
						$fetchUsuarioNexoAmigos = $queryUsuarioNexoAmigos->fetchAll(PDO::FETCH_OBJ);

						$contador = 0;

						foreach ($fetchUsuarioDatos as $fetch) {
							echo '<tr>';
							echo '<td><input type="text" class="form-control" name="nombre'.$contador.'" value="'.$fetch->nombre.'"/></td>';
							echo '<td><select class="form-control" name="id_nexo'.$contador.'">';
							echo '<option>Selecciona una opción</option>';
							foreach ($fetchUsuarioNexo as $fetchNexo) {
								if($fetchNexo->id_nexo == $fetch->id_nexo){
									echo '<option selected value="'.$fetchNexo->id_nexo.'">'.$fetchNexo->nexo.'</option>';
								}else{
									echo '<option value="'.$fetchNexo->id_nexo.'">'.$fetchNexo->nexo.'</option>';
								}
							}
							echo '</select></td>';
							echo '<td><select class="form-control" name="id_nexo_amigo'.$contador.'">';
							echo '<option>Selecciona una opción</option>';
							foreach ($fetchUsuarioNexoAmigos as $fetchNexoAmigo) {
								if($fetchNexoAmigo->id_nexo == $fetch->id_nexo_amigo){
									echo '<option selected value="'.$fetchNexoAmigo->id_nexo_amigo.'">'.$fetchNexoAmigo->nexo.'</option>';
								}else{
									echo '<option value="'.$fetchNexoAmigo->id_nexo_amigo.'">'.$fetchNexoAmigo->nexo.'</option>';
								}
							}
							echo '</select></td>';
							echo '<td><input type="text" class="form-control" name="barrio'.$contador.'" value="'.$fetch->barrio.'"/></td>';
							echo '<td><input type="number" class="form-control" name="telefono'.$contador.'" value="'.$fetch->telefono.'"/></td>';
							echo '<td><input type="text" class="form-control" name="tipo_apoyo'.$contador.'" value="'.$fetch->tipo_apoyo.'"/></td>';
							echo '<td><input type="text" class="form-control" name="red_apoyo'.$contador.'" value="'.$fetch->red_apoyo.'"/></td>';
							echo '</tr>';
							$contador++;
						}
						if($contador < 4){
							while($contador < 4){
								echo '<tr>';
								echo '<td><input type="text" class="form-control" name="nombre'.$contador.'"/></td>';
								echo '<td><input type="text" class="form-control" name="nexo'.$contador.'"/></td>';
								echo '<td><input type="text" class="form-control" name="id_nexo_amigo'.$contador.'"/></td>';
								echo '<td><input type="text" class="form-control" name="barrio'.$contador.'"/></td>';
								echo '<td><input type="text" class="form-control" name="telefono'.$contador.'"/></td>';
								echo '<td><input type="text" class="form-control" name="tipo_apoyo'.$contador.'"/></td>';
								echo '<td><input type="text" class="form-control" name="red_apoyo'.$contador.'"/></td>';
								echo '</tr>';
								$contador++;
							}
						}
						?>
					</tbody>
				</table>
			</div>
		</div>
		<!-- End Row -->


		<!-- Modal Footer -->
		<div class="modal-footer">
			<button type="button" class="btn btn-secondary" data-dismiss="modal">
				Cancelar
			</button>
			<button type="submit" class="btn btn-primary" name="Actualizar">
				Actualizar
			</button>
		</div>
	</div>    
<?php } ?>

<script type="text/javascript">
	
	function validarFecha(){
		var fechaIngreso = document.getElementById('fechaIngreso').value;
		var fechaEgreso = document.getElementById('fechaEgreso').value;
		if(fechaIngreso > fechaEgreso){
			alert('Lo siento, la fecha de ingreso no puede ser mayor a la fecha de egreso');
		}else{
			if(fechaIngreso == fechaEgreso){
				alert('Lo siento, la fecha de ingreso no puede ser igual a la fecha de egreso');
			}
		}
	}


</script>