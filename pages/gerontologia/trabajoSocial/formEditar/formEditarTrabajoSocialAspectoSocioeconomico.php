<?php require "../../../../session.php";



$sqlUsuario = "
SELECT 
*,
(CONVERT(CAST(CONVERT(condicion_higienica USING latin1) AS BINARY) USING utf8))AS condicion_higienica,
(CONVERT(CAST(CONVERT(distribucion_vivienda USING latin1) AS BINARY) USING utf8))AS distribucion_vivienda,
(CONVERT(CAST(CONVERT(acceso_vivienda USING latin1) AS BINARY) USING utf8))AS acceso_vivienda,
(CONVERT(CAST(CONVERT(ingreso_mensual USING latin1) AS BINARY) USING utf8))AS ingreso_mensual,
(CONVERT(CAST(CONVERT(proveedor_economico USING latin1) AS BINARY) USING utf8))AS proveedor_economico,
(CONVERT(CAST(CONVERT(observacion USING latin1) AS BINARY) USING utf8))AS observacion,
(CONVERT(CAST(CONVERT(recomendacion USING latin1) AS BINARY) USING utf8))AS recomendacion
FROM gddt_aspecto_socioeconomico 
WHERE id_aspecto_socioeconomico =". $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);

?>
<form name="editarUsuario" method="POST" action="procesarPsicologiaAntceMental.php?tipo=update" enctype="multipart/form-data">

	<div class="row">
		<div class="form-group col-md-6">
			<label for="recipient-name" class="form-control-label">Usuario que registra <i style="color: darkorange">*</i></label>
			<select class="form-control" name="usuario" >
				<option>Selecciona una opción</option>
				<?php
				$queryCuenta = mysqli_query($conn, "SELECT id_cuenta,
					UPPER((CONVERT(CAST(CONVERT(CONCAT(primer_nombre,' ',segundo_nombre,' ',primer_apellido,' ',segundo_apellido) USING latin1) AS BINARY) USING utf8))) AS nombreUsuario FROM gddt_cuentas ;");
				while ($fetchCuenta = mysqli_fetch_row($queryCuenta)) {
					if ($fetchCuenta[0] == $fetchUsuario[1]) {
						echo "<option selected value=" . $fetchCuenta[0] . ">" . $fetchCuenta[1] . "</option>";
					} else {
						echo "<option value=" . $fetchCuenta[0] . ">" . $fetchCuenta[1] . "</option>";
					}
				}
				?>
			</select>
		</div>
		<?php 
		if(strlen($fetchUsuario[3]) < 2){$mes = '0'.$fetchUsuario[3];}else{$mes = $fetchUsuario[3];}
		if(strlen($fetchUsuario[4]) < 2){$dia = '0'.$fetchUsuario[4];}else{$dia = $fetchUsuario[4];}
		$year = $fetchUsuario[2];
		$fecha = $year.'-'.$mes.'-'.$dia;
		?>
		<div class="form-group col-md-6">
			<label for="message-text" class="form-control-label">Fecha de modificacion <i style="color: darkorange">*</i></label>
			<input type="date" class="form-control" name="fechaMod" value="<?= $fecha ?>" />
		</div>
		<div class="form-group col-md-12">
			<h4 class="text-center"> ASPECTO SOCIO ECONOMICO </h4>
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Características de la vivienda <i style="color: darkorange">*</i></label>
			<?php 
			$selectCaracteristicasVivienda = array(
				0 => 'Propia',
				1 => 'Alquilada',
				2 => 'Hipotecada',
				3 => 'Comodato',
				4 => 'Pieza',
				5 => 'Tugurio',
				6 => 'Invación',
				7 => 'Calle'
			);
			function returnCaracteristica($caracteristica){
				switch ($caracteristica) {
					case 'p':return 'Propia';break;
					case 'a':return 'Alquilada';break;
					case 'h':return 'Hipotecada';break;
					case 'c':return 'Comodato';break;
					case 'p':return 'Pieza';break;
					case 't':return 'Tugurio';break;
					case 'i':return 'Invación';break;
					case 'ca':return 'Calle';break;
				}
			}
			function returnInicial($caracteristica){
				switch ($caracteristica) {
					case 'Propia':return 'p';break;
					case 'Alquilada':return 'a';break;
					case 'Hipotecada':return 'h';break;
					case 'Comodato':return 'c';break;
					case 'Pieza':return 'p';break;
					case 'Tugurio':return 't';break;
					case 'Invación':return 'i';break;
					case 'Calle':return 'ca';break;
				}
			}
			?>
			<select class="form-control" name="caracteristicasVivienda" >
				<option>Selecciona una opción</option>
				<?php
				foreach ($selectCaracteristicasVivienda as $key) {
					$dato = returnCaracteristica($fetchUsuario[7]);
					$sigla = returnInicial($key);
					if($key == $dato){
						echo '<option selected value='.$sigla.'>'.$key.'</option>';
					}else{
						echo '<option value='.$sigla.'>'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-12">
			<h4 class="text-center">MATERIAL DE CONSTRUCCION</h4>
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Pisos <i style="color: darkorange">*</i></label>
			<input type="text" class="form-control" name="piso" value="<?= $fetchUsuario[8] ?>" />
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Techos <i style="color: darkorange">*</i></label>
			<input type="text" class="form-control" name="techo" value="<?= $fetchUsuario[9] ?>" />
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Pared <i style="color: darkorange">*</i></label>
			<input type="text" class="form-control" name="pared" value="<?= $fetchUsuario[10] ?>" />
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Tendecia <i style="color: darkorange">*</i></label>
			<?php 
			$selectTendencia = array(
				0 => 'Propietario',
				1 => 'Arrendatario',
				2 => 'Comodato'
			);
			function returnTendencia($caracteristica){
				switch ($caracteristica) {
					case 'p':return 'Propietario';break;
					case 'a':return 'Arrendatario';break;
					case 'c':return 'Comodato';break;
				}
			}
			function returnInicialTendencia($caracteristica){
				switch ($caracteristica) {
					case 'Propietario':return 'p';break;
					case 'Arrendatario':return 'a';break;
					case 'Comodato':return 'c';break;
				}
			}
			?>
			<select class="form-control" name="tendecia" >
				<option>Selecciona una opción</option>
				<?php
				foreach ($selectTendencia as $key) {
					$dato = returnTendencia($fetchUsuario[11]);
					$sigla = returnInicialTendencia($key);
					if($key == $dato){
						echo '<option selected value='.$sigla.'>'.$key.'</option>';
					}else{
						echo '<option value='.$sigla.'>'.$key.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-12 text-center">
			
			<div class="container-fluid"></div>
			<?php 
			$arrayServicios = array(
				0 => 'Acueducto',
				1 => 'Alcantarillado',
				2 => 'Energia',
				3 => 'Baño',
				4 => 'Letrina',
				5 => 'Sanitario',
				6 => 'Telefono',
				7 => 'Gas'
			);
			$sqlServicios = "SELECT id_aspecto_socioeconomicoxservicios,(CONVERT(CAST(CONVERT(servicio USING latin1) AS BINARY) USING utf8))AS servicio FROM gddt_aspecto_socioeconomicoxservicios WHERE id_aspecto_socioeconomico =  ". $_REQUEST["idu"];
			$querServicios = $db->query($sqlServicios);
			$fetchServicios = $querServicios->fetchAll(PDO::FETCH_OBJ);

			foreach ($arrayServicios as $keyArrayServicio) {
				echo '<div class="custom-control custom-checkbox custom-control-inline" style="margin-right: 10px;">';
				foreach ($fetchServicios as $keyFetchSercicio) {
					if($keyArrayServicio == $keyFetchSercicio->servicio){
						echo '<input type="checkbox" class="custom-control-input" id="'.$keyArrayServicio.'" value="'.$keyArrayServicio.'" checked>';
						break;
					}else{
						echo '<input type="checkbox" class="custom-control-input" id="'.$keyArrayServicio.'" value="'.$keyArrayServicio.'">';
						break;
					}
				}
				echo '<label class="custom-control-label" for="'.$keyArrayServicio.'">'.$keyArrayServicio.'</label>';
				echo '</div>';

			}
			?>
		</div>
		
	</div>

	<div class="form-group col-md-12">
		<label for="message-text" class="form-control-label">Condiciones Higenicas <i style="color: darkorange">*</i></label>
		<textarea class="form-control" name="condicionesHigenicas" cols="40" rows="5" ><?= $fetchUsuario[21] ?></textarea>
	</div>
	<div class="form-group col-md-12">
		<label for="message-text" class="form-control-label">Distribución de la Vivienda(# de alcobas,# de personas por pieza y cama) <i style="color: darkorange">*</i></label>
		<textarea class="form-control" name="distribuicionVivienda" cols="40" rows="5" ><?= $fetchUsuario[22] ?></textarea>
	</div>
	<div class="form-group col-md-12">
		<label for="message-text" class="form-control-label">Acceso a la vivienda <i style="color: darkorange">*</i></label>
		<input type="text" class="form-control" name="accesoVivienda" value="<?= $fetchUsuario[23] ?>" />
	</div>
	<div class="form-group col-md-12">
		<label for="message-text" class="form-control-label">Ingreso Mensual de la Familia <i style="color: darkorange">*</i></label>
		<input type="text" class="form-control" name="ingresoMensualVivienda" value="<?= $fetchUsuario[24] ?>" />
	</div>
	<div class="form-group col-md-12">
		<label for="message-text" class="form-control-label">Proveedores económico de la familia <i style="color: darkorange">*</i></label>
		<input type="text" class="form-control" name="proveedorEconomico" value="<?= $fetchUsuario[25] ?>" />
	</div>
	<div class="form-group col-md-12">
		<label for="message-text" class="form-control-label">Observaciones <i style="color: darkorange">*</i></label>
		<textarea class="form-control" name="observaciones" cols="40" rows="5" ><?= $fetchUsuario[26] ?></textarea>
	</div>
		<div class="form-group col-md-12">
		<label for="message-text" class="form-control-label">Recomendaciones <i style="color: darkorange">*</i></label>
		<textarea class="form-control" name="recomendaciones" cols="40" rows="5" ><?= $fetchUsuario[27] ?></textarea>
	</div>
</div>
<!-- End Row -->


<!-- Modal Footer -->
<div class="modal-footer">
	<button type="button" class="btn btn-secondary" data-dismiss="modal">
		Cancelar
	</button>
	<button type="submit" class="btn btn-primary" name="Actualizar">
		Actualizar
	</button>
</div>
</div>    
