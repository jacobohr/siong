<?php require "../../../../session.php";



$sqlUsuario = "
SELECT 
*,
(CONVERT(CAST(CONVERT(descripcion USING latin1) AS BINARY) USING utf8))AS descripcion 
FROM gddt_actividades_realiza_tsocial 
WHERE id_actividad_realiza_tsocial = ". $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);

?>
<form name="editarUsuario" method="POST" action="procesarPsicologiaAntceMental.php?tipo=update" enctype="multipart/form-data">
	<h4 class="text-center mb-4"> Actividades que realiza </h4>
	<div class="row">
		<div class="form-group col-md-12">
			<label for="recipient-name" class="form-control-label">Usuario que registra <i style="color: darkorange">*</i></label>
			<select class="form-control" name="usuario" >
				<option>Selecciona una opción</option>
				<?php
				$queryCuenta = mysqli_query($conn, "SELECT id_cuenta,
					UPPER((CONVERT(CAST(CONVERT(CONCAT(primer_nombre,' ',segundo_nombre,' ',primer_apellido,' ',segundo_apellido) USING latin1) AS BINARY) USING utf8))) AS nombreUsuario FROM gddt_cuentas ;");
				while ($fetchCuenta = mysqli_fetch_row($queryCuenta)) {
					if ($fetchCuenta[0] == $fetchUsuario[4]) {
						echo "<option selected value=" . $fetchCuenta[0] . ">" . $fetchCuenta[1] . "</option>";
					} else {
						echo "<option value=" . $fetchCuenta[0] . ">" . $fetchCuenta[1] . "</option>";
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label">Fecha <i style="color: darkorange">*</i></label>
			<input id="fechaIngreso" type="date" class="form-control" name="fechaReingreso" value="<?= $fetchUsuario[1] ?>" />
		</div>
		
		<div class="form-group col-md-12">
			<label for="recipient-name" class="form-control-label">Participo? <i style="color: darkorange">*</i></label>
			<select class="form-control" name="participo" >
				<option>Selecciona una opción</option>
				<?php 
				$selectSiNo = array (
					0 => 'Si',
					1 => 'No'
				);
				foreach ($selectSiNo as $key) {
					if ($key == $fetchUsuario[2]) {
						echo "<option selected value=" . $key . ">" . $key . "</option>";
					} else {
						echo "<option value=" . $key . ">" . $key . "</option>";
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label">Descripcion <i style="color: darkorange">*</i></label>
			<textarea class="form-control" name="descripcion" cols="40" rows="5" ><?= $fetchUsuario[7] ?></textarea>
		</div>
	</div>
	<!-- End Row -->


	<!-- Modal Footer -->
	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">
			Cancelar
		</button>
		<button type="submit" class="btn btn-primary" name="Actualizar">
			Actualizar
		</button>
	</div>
</div>    