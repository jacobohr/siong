<?php require "../../../../session.php";



$sqlUsuario = "
SELECT
*,
(CONVERT(CAST(CONVERT(motivo_reingreso USING latin1) AS BINARY) USING utf8))AS motivo_reingreso,
(CONVERT(CAST(CONVERT(remitido_de USING latin1) AS BINARY) USING utf8))AS remitido_de,
(CONVERT(CAST(CONVERT(recomendacion USING latin1) AS BINARY) USING utf8))AS recomendacion
FROM gddt_reingresos 
WHERE id_reingreso =". $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);

?>
<form name="editarUsuario" method="POST" action="procesarPsicologiaAntceMental.php?tipo=update" enctype="multipart/form-data">
	<h4 class="text-center mb-4"> Informacion General </h4>
	<div class="row">
		<div class="form-group col-md-12">
			<label for="recipient-name" class="form-control-label">Usuario que registra <i style="color: darkorange">*</i></label>
			<select class="form-control" name="usuario" >
				<option>Selecciona una opción</option>
				<?php
				$queryCuenta = mysqli_query($conn, "SELECT id_cuenta,
					UPPER((CONVERT(CAST(CONVERT(CONCAT(primer_nombre,' ',segundo_nombre,' ',primer_apellido,' ',segundo_apellido) USING latin1) AS BINARY) USING utf8))) AS nombreUsuario FROM gddt_cuentas ;");
				while ($fetchCuenta = mysqli_fetch_row($queryCuenta)) {
					if ($fetchCuenta[0] == $fetchUsuario[1]) {
						echo "<option selected value=" . $fetchCuenta[0] . ">" . $fetchCuenta[1] . "</option>";
					} else {
						echo "<option value=" . $fetchCuenta[0] . ">" . $fetchCuenta[1] . "</option>";
					}
				}
				?>
			</select>
		</div>
		<?php 
		if(strlen($fetchUsuario[3]) < 2){$mes = '0'.$fetchUsuario[3];}else{$mes = $fetchUsuario[3];}
		if(strlen($fetchUsuario[4]) < 2){$dia = '0'.$fetchUsuario[4];}else{$dia = $fetchUsuario[4];}
		$year = $fetchUsuario[2];
		$fecha = $year.'-'.$mes.'-'.$dia;
		?>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label">Fecha de reingreso <i style="color: darkorange">*</i></label>
			<input id="fechaIngreso" type="date" class="form-control" name="fechaReingreso" value="<?= $fecha ?>" onchange="validarFecha();"/>
		</div>
		<?php 
		if(strlen($fetchUsuario[8]) < 2){$mes = '0'.$fetchUsuario[8];}else{$mes = $fetchUsuario[8];}
		if(strlen($fetchUsuario[9]) < 2){$dia = '0'.$fetchUsuario[9];}else{$dia = $fetchUsuario[9];}
		$year = $fetchUsuario[7];
		$fechaEgreso = $year.'-'.$mes.'-'.$dia;
		?>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label">Fecha de egreso <i style="color: darkorange">*</i></label>
			<input id="fechaEgreso" type="date" class="form-control" name="fecharEgreso" value="<?= $fechaEgreso ?>" onchange="validarFecha();"/>
		</div>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label">Motivo de Reingreso <i style="color: darkorange">*</i></label>
			<textarea class="form-control" name="motivoReingreso" cols="40" rows="5" ><?= $fetchUsuario[14] ?></textarea>
		</div>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label">Remitido por <i style="color: darkorange">*</i></label>
			<textarea class="form-control" name="situacionFamiliar" cols="40" rows="5" ><?= $fetchUsuario[15] ?></textarea>
		</div>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label">Recomendaciones Especiales <i style="color: darkorange">*</i></label>
			<textarea class="form-control" name="tipologia" cols="40" rows="5" ><?= $fetchUsuario[16] ?></textarea>
		</div>
	</div>
	<!-- End Row -->


	<!-- Modal Footer -->
	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">
			Cancelar
		</button>
		<button type="submit" class="btn btn-primary" name="Actualizar">
			Actualizar
		</button>
	</div>
</div>    

<script type="text/javascript">
	
	function validarFecha(){
		var fechaIngreso = document.getElementById('fechaIngreso').value;
		var fechaEgreso = document.getElementById('fechaEgreso').value;
		if(fechaIngreso > fechaEgreso){
			alert('Lo siento, la fecha de ingreso no puede ser mayor a la fecha de egreso');
		}else{
			if(fechaIngreso == fechaEgreso){
				alert('Lo siento, la fecha de ingreso no puede ser igual a la fecha de egreso');
			}
		}
	}


</script>