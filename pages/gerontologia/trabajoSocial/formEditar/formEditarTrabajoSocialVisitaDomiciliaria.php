<?php require "../../../../session.php";



$sqlUsuario = "
SELECT 
*,
(CONVERT(CAST(CONVERT(barrio USING latin1) AS BINARY) USING utf8))AS barrio,
(CONVERT(CAST(CONVERT(direccion USING latin1) AS BINARY) USING utf8))AS direccion,
(CONVERT(CAST(CONVERT(objetivo_entrevista USING latin1) AS BINARY) USING utf8))AS objetivo_entrevista,
(CONVERT(CAST(CONVERT(problematica_familiar USING latin1) AS BINARY) USING utf8))AS problematica_familiar,
(CONVERT(CAST(CONVERT(tipologia USING latin1) AS BINARY) USING utf8))AS tipologia,
(CONVERT(CAST(CONVERT(historia_familiar USING latin1) AS BINARY) USING utf8))AS historia_familiar,
(CONVERT(CAST(CONVERT(relacion_subsistema USING latin1) AS BINARY) USING utf8))AS relacion_subsistema
FROM gddt_visita_domiciliaria 
WHERE id_visita_domiciliaria =". $_REQUEST["idu"];

$queryUsuario = mysqli_query($conn, $sqlUsuario);
$fetchUsuario = mysqli_fetch_row($queryUsuario);

?>
<form name="editarUsuario" method="POST" action="procesarPsicologiaAntceMental.php?tipo=update" enctype="multipart/form-data">
	<h4 class="text-center mb-4"> Visita Domiciliaria </h4>
	<div class="row">
		<div class="form-group col-md-4">
			<label for="recipient-name" class="form-control-label">Usuario que registra <i style="color: darkorange">*</i></label>
			<select class="form-control" name="usuario" >
				<option>Selecciona una opción</option>
				<?php
				$queryCuenta = mysqli_query($conn, "SELECT id_cuenta,
					UPPER((CONVERT(CAST(CONVERT(CONCAT(primer_nombre,' ',segundo_nombre,' ',primer_apellido,' ',segundo_apellido) USING latin1) AS BINARY) USING utf8))) AS nombreUsuario FROM gddt_cuentas ;");
				while ($fetchCuenta = mysqli_fetch_row($queryCuenta)) {
					if ($fetchCuenta[0] == $fetchUsuario[1]) {
						echo "<option selected value=" . $fetchCuenta[0] . ">" . $fetchCuenta[1] . "</option>";
					} else {
						echo "<option value=" . $fetchCuenta[0] . ">" . $fetchCuenta[1] . "</option>";
					}
				}
				?>
			</select>
		</div>
		<?php 
		if(strlen($fetchUsuario[3]) < 2){$mes = '0'.$fetchUsuario[3];}else{$mes = $fetchUsuario[3];}
		if(strlen($fetchUsuario[4]) < 2){$dia = '0'.$fetchUsuario[4];}else{$dia = $fetchUsuario[4];}
		$year = $fetchUsuario[2];
		$fecha = $year.'-'.$mes.'-'.$dia;
		?>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Fecha <i style="color: darkorange">*</i></label>
			<input id="fechaMod" type="date" class="form-control" name="fechaMod" value="<?= $fecha ?>"/>
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Barrio <i style="color: darkorange">*</i></label>
			<input type="text" class="form-control" name="barrio" value="<?= $fetchUsuario[15] ?>"/>
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Direccion <i style="color: darkorange">*</i></label>
			<input type="text" class="form-control" name="direccion" value="<?= $fetchUsuario[16] ?>"/>
		</div>
		<div class="form-group col-md-4">
			<label for="message-text" class="form-control-label">Telefono <i style="color: darkorange">*</i></label>
			<input type="number" class="form-control" name="telefono" value="<?= $fetchUsuario[9] ?>"/>
		</div>
		<div class="form-group col-md-12 text-center">
			<h5 >NOMBRE DE LAS PERSONAS ENTREVISTADAS Y NEXO</h5>
		</div>
		<?php 
		$sqlPersonasEntrevistadasPDO = "
		SELECT 
		pe.nombre, 
		n.nexo,
		pe.id_nexo
		FROM gddt_personas_entrevistadas pe, gddt_nexos n 
		WHERE pe.id_nexo = n.id_nexo 
		AND pe.id_visita_domiciliaria = ". $_REQUEST["idu"];
		$queryPersonasEntrevistadasPDO = $db->query($sqlPersonasEntrevistadasPDO);
		$fetchPersonasEntrevistadasPDO = $queryPersonasEntrevistadasPDO->fetchAll(PDO::FETCH_OBJ);


		if(empty($fetchPersonasEntrevistadasPDO)){
			?>
			<div class="user-block col-sm-12 col-xs-12">
				<p style='color:red;font-size:0.9em;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No se encontraron datos :(</p>
			</div>
		<?php }else{
			$contador = 0;
			foreach ($fetchPersonasEntrevistadasPDO as $fetch) {
				$contador++;
				echo '<div class="form-group col-md-6">';
				echo '<label for="message-text" class="form-control-label">Nombre <i style="color: darkorange">*</i></label>';
				echo '<input type="text" class="form-control" name="nombreEntrevistado'.$contador.'" value="'.$fetch->nombre.'"/>';
				echo '</div>';

				echo '<div class="form-group col-md-6">';
				echo '<label for="recipient-name" class="form-control-label">Nexo <i style="color: darkorange">*</i></label>';
				echo '<select class="form-control" name="nexoEntrevistado'.$contador.'" >';
				echo '<option>Selecciona una opción</option>';
				$queryNexos = mysqli_query($conn, "SELECT id_nexo, nexo FROM gddt_nexos;");
				while ($fetchNexos = mysqli_fetch_row($queryNexos)) {
					if ($fetchNexos[0] == $fetch->id_nexo) {
						echo "<option selected value=" . $fetchNexos[0] . ">" . $fetchNexos[1] . "</option>";
					} else {
						echo "<option value=" . $fetchNexos[0] . ">" . $fetchNexos[1] . "</option>";
					}
				}

				echo '</select>';
				echo '</div>';
			}
			if($contador < 4){
				while($contador < 4){
					$contador++;
					echo '<div class="form-group col-md-6">';
					echo '<label for="message-text" class="form-control-label">Nombre </label>';
					echo '<input type="text" class="form-control" name="nombreEntrevistado'.$contador.'"/>';
					echo '</div>';
					echo '<div class="form-group col-md-6">';
					echo '<label for="recipient-name" class="form-control-label">Nexo</label>';
					echo '<select class="form-control" name="nexoEntrevistado'.$contador.'" >';
					echo '<option>Selecciona una opción</option>';
					$queryNexos = mysqli_query($conn, "SELECT id_nexo, nexo FROM gddt_nexos;");
					while ($fetchNexos = mysqli_fetch_row($queryNexos)) {
						echo "<option value=" . $fetchNexos[0] . ">" . $fetchNexos[1] . "</option>";
					}
					echo '</select>';
					echo '</div>';
				}
				
			}
		}
		?>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label">Objetivo de la entrevista <i style="color: darkorange">*</i></label>
			<textarea class="form-control" name="situacionFamiliar" cols="40" rows="5" ><?= $fetchUsuario[17] ?></textarea>
		</div>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label">Problemática familiar que se presenta <i style="color: darkorange">*</i></label>
			<textarea class="form-control" name="tipologia" cols="40" rows="5" ><?= $fetchUsuario[18] ?></textarea>
		</div>
		<div class="form-group col-md-12 text-center">
			<h5 >DATOS FAMILIARES</h5>
		</div>
		<div class="form-group col-md-12 text-center">
			<h5 >NOMBRE DE LAS PERSONAS ENTREVISTADAS Y NEXO</h5>
		</div>
		<table class="table table-bordered">
			<thead>
				<tr class="text-center">
					<th scope="col">Nombre</th>
					<th scope="col">Parentesco</th>
					<th scope="col">Edad</th>
					<th scope="col">Escolaridad</th>
					<th scope="col">Ocupacion</th>
				</tr>
			</thead>
			<tbody>
				<?php 
				$sqlPersonasEntrevistadas = "SELECT * FROM gddt_datos_familiares WHERE id_visita_domiciliaria = ". $_REQUEST["idu"];
				$queryPersonasEntrevistadas = $db->query($sqlPersonasEntrevistadas);
				$fetchPersonasEntrevistadas = $queryPersonasEntrevistadas->fetchAll(PDO::FETCH_OBJ);
				$cont = 0;
				foreach ($fetchPersonasEntrevistadas as $fetch) {
					$cont++;
					echo '<tr>';
					echo '<th scope="row">';
					echo '<input type="text" class="form-control" name="nombreFamiliar'.$cont.'" value="'.$fetch->nombre.'"/>';
					echo '</th>';
					echo '<td>';
					echo '<select class="form-control" name="parentescoFamiliar'.$cont.'" >';
					echo '<option>Selecciona una opcion</option>';
					$queryNexos = mysqli_query($conn, "SELECT id_nexo, nexo FROM gddt_nexos;");
					while ($fetchNexos = mysqli_fetch_row($queryNexos)) {
						if ($fetchNexos[0] == $fetch->parentesco) {
							echo "<option selected value=" . $fetchNexos[0] . ">" . $fetchNexos[1] . "</option>";
						} else {
							echo "<option value=" . $fetchNexos[0] . ">" . $fetchNexos[1] . "</option>";
						}
					}
					echo '</select>';
					echo '</td>';
					echo '<td>';
					echo '<input type="number" class="form-control" name="edadFamiliar'.$cont.'" value="'.$fetch->edad.'" max="120" />';
					echo '</td>';
					echo '<td>';
					echo '<select class="form-control" name="nivelEstudioFamiliar'.$cont.'" >';
					echo '<option>Selecciona una opcion</option>';
					$queryNivelEstudios = mysqli_query($conn, "SELECT id_nivel_estudio, descripcion FROM gddt_nivel_estudios;");
					while ($fetchNivelEstudios = mysqli_fetch_row($queryNivelEstudios)) {
						if ($fetchNivelEstudios[0] == $fetch->id_nivel_estudio) {
							echo "<option selected value=" . $fetchNivelEstudios[0] . ">" . $fetchNivelEstudios[1] . "</option>";
						} else {
							echo "<option value=" . $fetchNivelEstudios[0] . ">" . $fetchNivelEstudios[1] . "</option>";
						}
					}
					echo '</select>';
					echo '</td>';
					echo '<td>';
					echo '<input type="text" class="form-control" name="ocupacionFamiliar'.$cont.'" value="'.$fetch->ocupacion.'"/>';
					echo '</td>';
					echo '</tr>';
				}
				if($cont < 7){
					while($cont < 7){
						$cont++;
						echo '<tr>';
						echo '<th scope="row">';
						echo '<input type="text" class="form-control" name="nombreFamiliar'.$cont.'"/>';
						echo '</th>';
						echo '<td>';
						echo '<select class="form-control" name="parentescoFamiliar'.$cont.'" >';
						echo '<option>Selecciona una opcion</option>';
						$queryNexos = mysqli_query($conn, "SELECT id_nexo, nexo FROM gddt_nexos;");
						while ($fetchNexos = mysqli_fetch_row($queryNexos)) {
							echo "<option value=" . $fetchNexos[0] . ">" . $fetchNexos[1] . "</option>";
						}
						echo '</select>';
						echo '</td>';
						echo '<td>';
						echo '<input type="number" class="form-control" name="edadFamiliar'.$cont.'"  max="120" />';
						echo '</td>';
						echo '<td>';
						echo '<select class="form-control" name="nivelEstudioFamiliar'.$cont.'" >';
						echo '<option>Selecciona una opcion</option>';
						$queryNivelEstudios = mysqli_query($conn, "SELECT id_nivel_estudio, descripcion FROM gddt_nivel_estudios;");
						while ($fetchNivelEstudios = mysqli_fetch_row($queryNivelEstudios)) {
							echo "<option value=" . $fetchNivelEstudios[0] . ">" . $fetchNivelEstudios[1] . "</option>";
						}
						echo '</select>';
						echo '</td>';
						echo '<td>';
						echo '<input type="text" class="form-control" name="ocupacionFamiliar'.$cont.'"/>';
						echo '</td>';
						echo '</tr>';
					}
				}
				?>
			</tbody>
		</table>
		<div class="form-group col-md-12">
			<h5>SITUACION FAMILIAR</h5>
		</div>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label">Tipologia <i style="color: darkorange">*</i></label>
			<textarea class="form-control" name="tipologia" cols="40" rows="5" ><?= $fetchUsuario[19] ?></textarea>
		</div>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label">Elementos de La historia familiar <i style="color: darkorange">*</i></label>
			<textarea class="form-control" name="elementosHistoriaFamiliar" cols="40" rows="5" ><?= $fetchUsuario[20] ?></textarea>
		</div>
		<div class="form-group col-md-12">
			<label for="message-text" class="form-control-label">Relaciones entre subsistemas <i style="color: darkorange">*</i></label>
			<textarea class="form-control" name="relacionesSubsistemas" cols="40" rows="5" ><?= $fetchUsuario[21] ?></textarea>
		</div>
	</div>
	<!-- End Row -->


	<!-- Modal Footer -->
	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">
			Cancelar
		</button>
		<button type="submit" class="btn btn-primary" name="Actualizar">
			Actualizar
		</button>
	</div>
</div>    
