<?php

//Eliminamos todas las variables de sesion y cookies
require_once('session.php');

setcookie("ID_CUENTA", "");
setcookie("IDEMPRESA", "");
setcookie("PROFESIONAL", "");

session_destroy();

header("location:" . $base_url);
